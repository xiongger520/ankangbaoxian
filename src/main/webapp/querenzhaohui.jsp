
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="lib/html5shiv.js"></script>
    <script type="text/javascript" src="lib/respond.min.js"></script>
    <![endif]-->
    <link href="static/h-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="static/h-ui.admin/css/H-ui.login.css" rel="stylesheet" type="text/css" />
    <link href="static/h-ui.admin/css/style.css" rel="stylesheet" type="text/css" />
    <link href="lib/Hui-iconfont/1.0.8/iconfont.css" rel="stylesheet" type="text/css" />
    <!--[if IE 6]>
    <script type="text/javascript" src="lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>后台登录 - H-ui.admin v3.1</title>
    <meta name="keywords" content="H-ui.admin v3.1,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
    <meta name="description" content="H-ui.admin v3.1，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>
<body>
<input type="hidden" id="TenantId" name="TenantId" value="" />
<div class="header" style="background-size: 600px 70px;"></div>
<div class="loginWraper">
    <div id="loginform" class="loginBox" style="height:700px;max-height:700px">
        <form class="form form-horizontal" action="/zhaohui/zhaohui.do" method="post" onsubmit="return checkLoginForm()">

            <div class="row cl">
                <label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe605;</i></label>
                <div class="formControls col-xs-8">
                    <input id="password" name="password" type="password" placeholder="新密码" class="input-text size-L">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe605;</i></label>
                <div class="formControls col-xs-8">
                    <input id="password1" name="password1" type="password1" placeholder="确认密码" class="input-text size-L">
                </div>
            </div>
            <div class="row cl">
                <div class="formControls col-xs-8 col-xs-offset-3">
                    <input class="input-text size-L" id="duanxin" name="duanxin" type="text" placeholder="验证码"  value="" style="width:150px;">
                    <input id="btnSendCode" name="btnSendCode" type="button" class="btn btn-primary radius"   value="点击获取验证码" onclick="sendMessage();"/>

                </div>
            </div>
                <div class="row cl" style="margin-top: 2px;margin-bottom: -14px;">

                    <div class="formControls col-xs-8 col-xs-offset-3" >
                        <a href="/login.jsp" onclick="">返回登录</a>
                    </div>
                </div>

                <div class="row cl">
                    <div class="formControls col-xs-8 col-xs-offset-3">
                        <input name="" type="submit" class="btn btn-success radius size-L" value="&nbsp;提&nbsp;&nbsp;&nbsp;&nbsp;交&nbsp;">
                        <input name="" type="reset" class="btn btn-default radius size-L" value="&nbsp;取&nbsp;&nbsp;&nbsp;&nbsp;消&nbsp;">
                    </div>
                </div>
        </form>
    </div>
</div>
<div class="footer">Copyright 你的公司名称 by H-ui.admin v3.1</div>
<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="static/h-ui/js/H-ui.min.js"></script>
<script>

    if('${zhh_flag}'!=""){
        alert('${zhh_flag}')
    }

    var InterValObj; //timer变量，控制时间
    var count = 60; //间隔函数，1秒执行
    var curCount;//当前剩余秒数
    function sendMessage() {
        curCount = count;
// 设置button效果，开始计时
        document.getElementById("btnSendCode").setAttribute("disabled","true" );//设置按钮为禁用状态
        document.getElementById("btnSendCode").value="请在" + curCount + "后再次获取";//更改按钮文字
        InterValObj = window.setInterval(SetRemainTime, 1000); // 启动计时器timer处理函数，1秒执行一次
// 向后台发送处理数据
        $.ajax({
            type: "POST", // 用POST方式传输
            dataType: "json", // 数据格式:JSON
            url: "/zhaohui/captcha.do", // 目标地址
            data: "flag=2",
            success: function (data){
                alert(data.flag)
                if(data.flag==1){
                    layer.msg('验证码已发送，请注意查收',{icon:5,time:1000});
                }else{
                    layer.msg('请先验证身份',{icon:5,time:1000});
                }

            }
        });
    }

    //timer处理函数

    function SetRemainTime() {
        if (curCount == 0) {
            window.clearInterval(InterValObj);// 停止计时器
            document.getElementById("btnSendCode").removeAttribute("disabled");//移除禁用状态改为可用
            document.getElementById("btnSendCode").value="重新发送验证码";
        }else {
            curCount--;
            document.getElementById("btnSendCode").value="请在" + curCount + "秒后再次获取";
        }
    }

    function checkLoginForm(){

        var password = $('#password').val();
        var password1 = $('#password1').val();
        var duanxin = $('#duanxin').val();

        if(isNull(password) && isNull(password1)){
            layer.msg('请输入密码和确认密码!!!',{icon:5,time:1000});
            return false;
        }
        if(isNull(password)){
            layer.msg('请输入密码!!!',{icon:5,time:1000});

            return false;
        }
        if(isNull(password1)){
            layer.msg('请输入确认密码!!!',{icon:5,time:1000});

            return false;
        }
        if(password!=password1){
            layer.msg('两次输入的密码不相同!!!',{icon:5,time:1000});

            return false;
        }

            if(isNull(duanxin)){
                layer.msg('请输入验证码!!!',{icon:5,time:1000});
                return false;
            }


    }

    /**
     * check the param if it's null or '' or undefined
     * @param input
     * @returns {boolean}
     */
    function isNull(input){
        if(input == null || input == '' || input == undefined){
            return true;
        }
        else{
            return false;
        }
    }

</script>

</body>
</html>
