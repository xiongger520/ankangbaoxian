<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/1/3
  Time: 12:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="lib/html5shiv.js"></script>
    <script type="text/javascript" src="lib/respond.min.js"></script>
    <![endif]-->
    <link href="static/h-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="static/h-ui.admin/css/H-ui.login.css" rel="stylesheet" type="text/css" />
    <link href="static/h-ui.admin/css/style.css" rel="stylesheet" type="text/css" />
    <link href="lib/Hui-iconfont/1.0.8/iconfont.css" rel="stylesheet" type="text/css" />
    <!--[if IE 6]>
    <script type="text/javascript" src="lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>中国安康保险</title>
    <meta name="keywords" content="H-ui.admin v3.1,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
    <meta name="description" content="H-ui.admin v3.1，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>
<body>
<input type="hidden" id="TenantId" name="TenantId" value="" />
<div class="header" style="background-size: 600px 70px;"></div>
<div class="loginWraper">
    <div id="loginform" class="loginBox">
        <form class="form form-horizontal" action="/user/login.do" method="post"  onsubmit="return checkLoginForm()">
            <div class="row cl">
                <label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60d;</i></label>
                <div class="formControls col-xs-8">
                    <input id="username" name="userid" type="text" placeholder="账户" class="input-text size-L">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60e;</i></label>
                <div class="formControls col-xs-8">
                    <input type="password" id="password" name="password" placeholder="密码" class="input-text size-L">
                </div>
            </div>
            <c:if test="${loginflag ge 3}">
            <div class="row cl">
                <div class="formControls col-xs-8 col-xs-offset-3">
                    <input class="input-text size-L" id="duanxin" name="duanxin" type="text" placeholder="验证码"  value="" style="width:150px;">
                    <input id="btnSendCode" name="btnSendCode" type="button" class="btn btn-primary radius"   value="点击获取验证码" onclick="sendMessage();"/>

                </div>
            </c:if>

                <div class="row cl">

                    <div class="formControls col-xs-8 col-xs-offset-3">
                       <a href="mimazhaohui.jsp" onclick="">忘记密码</a>
                    </div>
                </div>

            <div class="row cl">

                <div class="formControls col-xs-8 col-xs-offset-3">
                    <input name="" type="submit"   class="btn btn-success radius size-L" value="&nbsp;登&nbsp;&nbsp;&nbsp;&nbsp;录&nbsp;">
                    <input name="" type="reset" class="btn btn-default radius size-L" value="&nbsp;取&nbsp;&nbsp;&nbsp;&nbsp;消&nbsp;">
                </div>
            </div>
        </form>
    </div>
</div>

<div class="footer">Copyright 安康保险有限公司 by ankang v1.0</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/lib/laypage/1.2/laypage.js"></script>
<!--此乃百度统计代码，请自行删除-->
<script type="text/javascript">



    $(function(){
        <c:if test="${not empty param.timeout}">
        layer.msg('连接超时,请重新登陆!', {
            offset: 0,
            shift: 6
        });
        </c:if>

        if("${error}"!=""){
            layer.msg("${error}",{icon:5,time:1000});
        }


        if("${message}"!=""){
            layer.msg('${message}',{icon:6,time:1000});
        }

        $('.close').on('click', function(c){
            $('.login-form').fadeOut('slow', function(c){
                $('.login-form').remove();
            });
        });


    });

    var InterValObj; //timer变量，控制时间
    var count = 60; //间隔函数，1秒执行
    var curCount;//当前剩余秒数
    function sendMessage() {
        curCount = count;
// 设置button效果，开始计时
        document.getElementById("btnSendCode").setAttribute("disabled","true" );//设置按钮为禁用状态
        document.getElementById("btnSendCode").value="请在" + curCount + "后再次获取";//更改按钮文字
        InterValObj = window.setInterval(SetRemainTime, 1000); // 启动计时器timer处理函数，1秒执行一次
// 向后台发送处理数据
        $.ajax({
            type: "POST", // 用POST方式传输
            dataType: "text", // 数据格式:JSON
            url: "/user/captcha.do", // 目标地址
            data: "flag=2",
            success: function (data){
                if(data.flag==1)
                layer.msg('验证码已发送，请注意查收',{icon:5,time:1000});
            }
        });
    }

    //timer处理函数

    function SetRemainTime() {
        if (curCount == 0) {
            window.clearInterval(InterValObj);// 停止计时器
            document.getElementById("btnSendCode").removeAttribute("disabled");//移除禁用状态改为可用
            document.getElementById("btnSendCode").value="重新发送验证码";
        }else {
            curCount--;
            document.getElementById("btnSendCode").value="请在" + curCount + "秒后再次获取";
        }
    }

    /*var tijiao = function(){
        checkLoginForm();
        //layer.msg('开始登陆，请稍后...',{icon:6,time:3000});

            var username = $('#username').val();
            var password = $('#password').val();
            $.ajax({
                type: 'POST',
                url: '/user/login.do',
                dataType: 'json',
                data:{
                    "userid":username,
                    "password":password
                },
                success: function(data){
                   if(data.error!=""){
                       layer.msg(data.error,{icon:1,time:1000});
                   }
                   if(data.message=="登陆成功！"){
                      /!* window.location="/index/toindex.do";*!/
                       //layer.msg(data.message,{icon:6,time:1000});
                       alert(1)
                   }
                  alert(1)

                },
                error:function(data) {
                    console.log(data.msg);
                }
            });*/
    /**
     * check the login form before user login.
     * @returns {boolean}
     */
    function checkLoginForm(){
        var username = $('#username').val();
        var password = $('#password').val();
        var duanxin = $('#duanxin').val();

       if(isNull(username) && isNull(password)){
           layer.msg('请输入账号和密码!!!',{icon:5,time:1000});
           return false;
        }
        if(isNull(username)){
            layer.msg('请输入账号!!!',{icon:5,time:1000});

            return false;
        }

        if(isNull(password)){
            layer.msg('请输入密码!!!',{icon:5,time:1000});
            return false;
        }

        if(${loginflag ge 3})
        if(isNull(duanxin)){
            layer.msg('请输入验证码!!!',{icon:5,time:1000});
            return false;
        }


    }

    /**
     * check the param if it's null or '' or undefined
     * @param input
     * @returns {boolean}
     */
    function isNull(input){
        if(input == null || input == '' || input == undefined){
            return true;
        }
        else{
            return false;
        }
    }
</script>
<!--/此乃百度统计代码，请自行删除
</body>
</html>
