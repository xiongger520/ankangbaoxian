<!--
项目二组  鲍康
-->
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
        PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="Bookmark" href="/favicon.ico" >
    <link rel="Shortcut Icon" href="/favicon.ico" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/lib/html5shiv.js"></script>
    <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
    <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <!--/meta 作为公共模版分离出去-->

    <title>新建网站角色 - 管理员管理 - H-ui.admin v3.1</title>
    <meta name="keywords" content="H-ui.admin v3.1,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
    <meta name="description" content="H-ui.admin v3.1，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>
<body>
<article class="page-container">
    <form action="/guanliyuan/editRole.do" method="post" class="form form-horizontal" id="form-admin-role-add">
        <input type="hidden" value="${role1.r_id}" name="r_id" id="r_id">
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>角色名称：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="${role1.r_name}" placeholder="" id="r_name" name="r_name">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3">修改权限：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <!--  -->
                <dl class="permission-list">
                    <dt>
                        <label>
                            <input type="checkbox" value="快捷菜单新增" name="caritem" id="user-Character-1">
                            快捷菜单新增</label>
                    </dt>
                    <dl class="cl permission-list2">
                        </dl>

                </dl>
                <!-- 保险管理 -->
                <dl class="permission-list">
                    <dt>
                        <label>
                            <input type="checkbox" value="保险管理一级" name="caritem" id="user-Character-1">
                            保险管理</label>
                    </dt>
                    <dd>
                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="user-Character-1-0" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>
                                <label class="">
                                    <input type="checkbox" value="保险管理二级" name="caritem" id="user-Character-1-0">
                                    保险管理</label>
                                <label class="">
                                    <input type="checkbox" value="添加保险" name="caritem" id="user-Character-1-0-0">
                                    添加保险</label>
                                <label class="">
                                    <input type="checkbox" value="保险上架下架" name="caritem" id="user-Character-1-0-1">
                                    保险上架下架</label>


                            </dd>
                        </dl>
                    </dd>
                </dl>

                <!-- 营销管理 -->
                <dl class="permission-list">
                    <dt>
                        <label>
                            <input type="checkbox" value="营销管理 " name="caritem" id="user-Character-1">
                            营销管理</label>
                    </dt>
                    <dd>
                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="user-Character-1-0" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>
                                <label class="">
                                    <input type="checkbox" value="保险套餐管理" name="caritem" id="user-Character-1-0">
                                    保险套餐管理</label>
                                <label class="">
                                    <input type="checkbox" value="保险套餐推出" name="caritem" id="user-Character-1-0-1">
                                    保险套餐推出</label>
                                <label class="">
                                    <input type="checkbox" value="套餐上架下架" name="caritem" id="user-Character-1-0-1">
                                    套餐上架</label>

                            </dd>
                        </dl>
                    </dd>
                </dl>




                <!-- 用户管理 -->
                <dl class="permission-list">
                    <dt>
                        <label>
                            <input type="checkbox" value="用户管理一级" name="caritem" id="user-Character-0">
                            用户管理</label>
                    </dt>
                    <dd>
                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>
                                <label class="">
                                    <input type="checkbox" value="用户管理二级" name="caritem" id="user-Character-0-0">
                                    用户管理</label>
                                <label class="">
                                    <input type="checkbox" value="添加用户" name="caritem" id="user-Character-0-0-0">
                                    添加用户</label>
                                <label class="">
                                    <input type="checkbox" value="用户信息修改" name="caritem" id="user-Character-0-0-2">
                                    用户信息修改</label>
                                <label class="">
                                    <input type="checkbox" value="用户黑名单" name="caritem" id="user-Character-0-0-3">
                                    用户黑名单</label>
                                <label class="">
                                    <input type="checkbox" value="用户冻结" name="caritem" id="user-Character-0-0-4">
                                    用户冻结</label>
                                <label class="">
                                    <input type="checkbox" value="用户解冻" name="caritem" id="user-Character-0-0-4">
                                    用户解冻</label>
                            </dd>
                        </dl>
                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="user-Character-1-0" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>
                                <label class="">
                                    <input type="checkbox" value="车辆管理" name="caritem" id="user-Character-0-1">
                                    车辆管理</label>
                                <label class="">
                                    <input type="checkbox" value="添加车辆" name="caritem" id="user-Character-0-1-0">
                                    添加车辆</label>
                                <label class="">
                                    <input type="checkbox" value="修改车辆" name="caritem" id="user-Character-0-1-1">
                                    修改车辆</label>
                                <label class="">
                                    <input type="checkbox" value="删除车辆" name="caritem" id="user-Character-0-1-2">
                                    删除车辆</label>
                                <input type="checkbox" value="批量删除车辆" name="caritem" id="user-Character-0-1-2">
                                批量删除车辆</label>
                            </dd>
                        </dl>
                    </dd>
                </dl>

                <!-- 交易管理 -->
                <dl class="permission-list">
                    <dt>
                        <label>
                            <input type="checkbox" value="交易管理" name="caritem" id="user-Character-1">
                            交易管理</label>
                    </dt>
                    <dd>
                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="user-Character-1-0" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>
                                <label class="">
                                    <input type="checkbox" value="保险单导入导出" name="caritem" id="user-Character-1-0-0">
                                    保险单导入导出</label>
                                <label class="">
                                    <input type="checkbox" value="保险单信息管理" name="caritem" id="user-Character-1-0">
                                    保险单信息管理</label>
                                <label class="">
                                    <input type="checkbox" value="导出保险单" name="caritem" id="user-Character-1-0-2">
                                    导出保险单</label>

                                <label class="">
                                    <input type="checkbox" value="购买保险" name="caritem" id="user-Character-1-0-1">
                                    购买保险</label>

                                <label class="">
                                    <input type="checkbox" value="保险缴费管理" name="caritem" id="user-Character-1-0-3">
                                    保险缴费管理</label>
                                <label class="">
                                    <input type="checkbox" value="保险缴费" name="caritem" id="user-Character-1-0-2">
                                    保险缴费</label>
                                <br/>
                                <label class="">
                                    <input type="checkbox" value="公司账单图" name="caritem" id="user-Character-1-0-2">
                                    公司账单图</label>
                            </dd>
                        </dl>
                    </dd>
                </dl>

                <!-- 理赔管理 -->
                <dl class="permission-list">
                    <dt>
                        <label>
                            <input type="checkbox" value="理赔管理" name="caritem" id="user-Character-1">
                            理赔管理</label>
                    </dt>
                    <dd>
                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="user-Character-1-0" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>

                                <label class="">
                                    <input type="checkbox" value="报案单导入导出" name="caritem" id="user-Character-1-0-0">
                                    报案单导入导出</label>
                                <label class="">
                                    <input type="checkbox" value="报案单管理" name="caritem" id="user-Character-1-0">
                                    报案单管理</label>
                                <label class="">
                                    <input type="checkbox" value="报案单导出" name="caritem" id="user-Character-1-0">
                                    报案单导出</label>
                                <label class="">
                                    <input type="checkbox" value="增加报案单" name="caritem" id="user-Character-1-0-1">
                                    增加报案单</label>
                                <label class="">
                                    <input type="checkbox" value="赔偿结算" name="caritem" id="user-Character-1-0-2">
                                    赔偿结算</label>
                                <label class="">
                                    <input type="checkbox" value="赔偿记录" name="caritem" id="user-Character-1-0-3">
                                    赔偿记录</label>
                            </dd>
                        </dl>
                    </dd>
                </dl>

                <!-- 管理员管理 -->
                <dl class="permission-list">
                    <dt>
                        <label>
                            <input type="checkbox" value="管理员管理" name="caritem" id="user-Character-0">
                            管理员管理</label>
                    </dt>
                    <dd>

                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>
                                <label class="">
                                    <input type="checkbox" value="权限管理" name="caritem" id="user-Character-0-0">
                                    权限管理</label>
                                <label class="">
                                    <input type="checkbox" value="添加权限" name="caritem" id="user-Character-0-0-0">
                                    添加权限</label>
                                <label class="">
                                    <input type="checkbox" value="修改权限" name="caritem" id="user-Character-0-0-1">
                                    修改权限</label>
                                <label class="">
                                    <input type="checkbox" value="删除权限" name="caritem" id="user-Character-0-0-2">
                                    删除权限</label>

                                <label class="">
                                    <input type="checkbox" value="批量删除权限" name="caritem" id="user-Character-0-0-2">
                                    批量删除权限</label>
                            </dd>
                        </dl>

                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>
                                <label class="">
                                    <input type="checkbox" value="角色管理" name="caritem" id="user-Character-0-0">
                                    角色批量</label>
                                <label class="">
                                    <input type="checkbox" value="添加角色" name="caritem" id="user-Character-0-0-0">
                                    添加角色</label>
                                <label class="">
                                    <input type="checkbox" value="修改角色" name="caritem" id="user-Character-0-0-1">
                                    修改角色</label>
                                <label class="">
                                    <input type="checkbox" value="删除角色" name="caritem" id="user-Character-0-0-2">
                                    删除角色</label>
                                <label class="">
                                    <input type="checkbox" value="批量删除角色" name="caritem" id="user-Character-0-0-2">
                                    批量删除角色</label>
                            </dd>
                        </dl>
                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="user-Character-1-0" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>
                                <label class="">
                                    <input type="checkbox" value="管理员列表" name="caritem" id="user-Character-0-1">
                                    管理员列表</label>
                                <label class="">
                                    <input type="checkbox" value="添加管理员" name="caritem" id="user-Character-0-1-0">
                                    添加管理员</label>
                                <label class="">
                                    <input type="checkbox" value="修改管理员" name="caritem" id="user-Character-0-1-1">
                                    修改管理员</label>
                                <label class="">
                                    <input type="checkbox" value="删除管理员" name="caritem" id="user-Character-0-1-2">
                                    删除管理员</label>
                                <label class="">
                                    <input type="checkbox" value="批量删除管理员" name="caritem" id="user-Character-0-1-2">
                                    批量删除管理员</label>
                            </dd>
                        </dl>
                    </dd>
                </dl>
                <!-- 系统管理 -->
                <dl class="permission-list">
                    <dt>
                        <label>
                            <input type="checkbox" value="系统管理" name="caritem" id="user-Character-1">
                            系统管理</label>
                    </dt>
                    <dd>
                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="user-Character-1-0" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>
                                <label class="">
                                    <input type="checkbox" value="日志管理" name="caritem" id="user-Character-1-0-3">
                                    日志管理</label>

                            </dd>
                        </dl>
                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="user-Character-1-0" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>
                                <input type="checkbox" value="支点管理" name="caritem" id="user-Character-1-0">
                                支点管理</label>
                                <label class="">
                                    <input type="checkbox" value="添加支点" name="caritem" id="user-Character-1-0-0">
                                    添加支点</label>
                                <label class="">
                                    <input type="checkbox" value="修改支点" name="caritem" id="user-Character-1-0-1">
                                    修改支点</label>
                                <label class="">
                                    <input type="checkbox" value="删除支点" name="caritem" id="user-Character-1-0-2">
                                    删除支点</label>
                                <label class="">
                                    <input type="checkbox" value="批量删除支点" name="caritem" id="user-Character-1-0-2">
                                    批量删除支点</label>

                            </dd>
                        </dl>
                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="user-Character-1-0" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>
                                <label class="">
                                    <input type="checkbox" value="管理员在线管理" name="caritem" id="user-Character-0-1">
                                    管理员在线管理</label>
                                <label class="">
                                    <input type="checkbox" value="管理员冻结解冻" name="caritem" id="user-Character-0-1-0">
                                    管理员冻结解冻</label>
                                <label class="">
                                    <input type="checkbox" value="管理员强制下线" name="caritem" id="user-Character-0-1-1">
                                    管理员强制下线</label>

                            </dd>
                        </dl>

                    </dd>
                </dl>

                <!-- 用户服务 -->
                <dl class="permission-list">
                    <dt>
                        <label>
                            <input type="checkbox" value="用户服务" name="caritem" id="user-Character-1">
                            用户服务</label>
                    </dt>
                    <dd>
                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="user-Character-1-0" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>
                                <label class="">
                                    <input type="checkbox" value="个人信息查看" name="caritem" id="user-Character-1-0">
                                    个人信息查看</label>
                                <label class="">
                                    <input type="checkbox" value="报案信息查询" name="caritem" id="user-Character-1-0-0">
                                    报案信息查询</label>
                                <label class="">
                                    <input type="checkbox" value="退保申请" name="caritem" id="user-Character-1-0-1">
                                    退保申请</label>
                                <label class="">
                                    <input type="checkbox" value="续保申请" name="caritem" id="user-Character-1-0-2">
                                    续保申请</label>
                                <label class="">
                                    <input type="checkbox" value="在线保险计算器" name="caritem" id="user-Character-1-0-3">
                                    在线保险计算器</label>
                                <label class="">
                                    <input type="checkbox" value="支点查询" name="caritem" id="user-Character-1-0-4">
                                    支点查询</label>
                            </dd>
                        </dl>
                    </dd>
                </dl>
                <!-- 核验管理 -->
                <dl class="permission-list">
                    <dt>
                        <label>
                            <input type="checkbox" value="信息核验管理" name="caritem" id="user-Character-1">
                            信息核验管理</label>
                    </dt>
                    <dd>
                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>
                                <label class="">
                                    <input type="checkbox" value="车辆信息核验" name="caritem" id="user-Character-1-0">
                                    车辆信息核验</label>
                                <label class="">
                                    <input type="checkbox" value="车辆核验" name="caritem" id="user-Character-1-0">
                                    车辆核验</label>
                                <label class="">
                                    <input type="checkbox" value="用户信息核验" name="caritem" id="user-Character-1-0-0">
                                    用户信息核验</label>
                                <label class="">
                                    <input type="checkbox" value="用户核验" name="caritem" id="user-Character-1-0-0">
                                    用户核验</label>

                                <label class="">
                                    <input type="checkbox" value="理赔信息核验" name="caritem" id="user-Character-1-0-1">
                                    理赔信息核验</label>
                                <label class="">
                                    <input type="checkbox" value="理赔核验" name="caritem" id="user-Character-1-0-1">
                                    理赔核验</label>
                                <br/>
                                <label class="">
                                    <input type="checkbox" value="新增保险核验" name="caritem" id="user-Character-1-0-2">
                                    新增保险核验</label>
                                <label class="">
                                    <input type="checkbox" value="保险核验" name="caritem" id="user-Character-1-0-2">
                                    保险核验</label>
                                <label class="">
                                    <input type="checkbox" value="保险订单核验" name="caritem" id="user-Character-1-0-3">
                                    保险订单核验</label>
                                <label class="">
                                    <input type="checkbox" value="订单核验" name="caritem" id="user-Character-1-0-3">
                                    订单核验</label>

                                <label class="">
                                    <input type="checkbox" value="续保信息核验" name="caritem" id="user-Character-1-0-4">
                                    续保信息核验</label>
                                <label class="">
                                    <input type="checkbox" value="续保核验" name="caritem" id="user-Character-1-0-4">
                                    续保核验</label>
                                <br/>
                                <label class="">
                                    <input type="checkbox" value="退保信息核验" name="caritem" id="user-Character-1-0-4">
                                    退保信息核验</label>
                                <label class="">
                                    <input type="checkbox" value="退保核验" name="caritem" id="user-Character-1-0-4">
                                    退保核验</label>
                            </dd>
                        </dl>
                    </dd>
                </dl>
                <!-- 核验管理 -->
                <dl class="permission-list">
                    <dt>
                        <label>
                            <input type="checkbox" value="管理员在线" name="caritem" id="user-Character-1">
                            管理员在线</label>
                    </dt>
                    <dd>
                        <dl class="cl permission-list2">
                            <dt>
                                <label class="">
                                    <input type="checkbox" value="" name="" id="user-Character-1-0">
                                    全选</label>
                            </dt>
                            <dd>
                                <label class="">

                                    <label class="">
                                        <input type="checkbox" value="业务交流" name="caritem" id="user-Character-1-0-0">
                                        业务交流</label>

                            </dd>
                        </dl>
                    </dd>
                </dl>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3">权限备注：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <textarea class="input-text" placeholder="说点什么..." id="r_msg" name="r_msg">${role1.r_msg}</textarea>
            </div>
        </div>
        <input type="hidden" id="powers" name="powers" value="${role1.powers}"/>
        <div class="row cl">
            <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
            <#--<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">-->
                <input type="button" class="btn btn-success radius" id="queding" value="提交" name="admin-role-save"/>
                <input class="btn btn-primary radius" type="reset" value="&nbsp;&nbsp;重置&nbsp;&nbsp;">
            </div>
        </div>
    </form>
</article>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript">
    $(function(){
        $(".permission-list dt input:checkbox").click(function(){
            $(this).closest("dl").find("dd input:checkbox").prop("checked",$(this).prop("checked"));
        });
        $(".permission-list2 dd input:checkbox").click(function(){
            var l =$(this).parent().parent().find("input:checked").length;
            var l2=$(this).parents(".permission-list").find(".permission-list2 dd").find("input:checked").length;
            if($(this).prop("checked")){
                $(this).closest("dl").find("dt input:checkbox").prop("checked",true);
                $(this).parents(".permission-list").find("dt").first().find("input:checkbox").prop("checked",true);
            }
            else{
                if(l==0){
                    $(this).closest("dl").find("dt input:checkbox").prop("checked",false);
                }
                if(l2==0){
                    $(this).parents(".permission-list").find("dt").first().find("input:checkbox").prop("checked",false);
                }
            }
        });

        $("#form-admin-role-add").validate({
            rules:{
                username:{
                    required:true,
                },
                un_nickname:{
                    required:true,
                },
                un_pwdsalt:{
                    required:true,
                },
                r_id:{
                    required:true,
                },
                password:{
                    required:true,
                },
                un_state:{
                    required:true,
                },
            },
            onkeyup:false,
            focusCleanup:true,
            success:"valid",
            submitHandler:function(form){
                $(form).ajaxSubmit();
                var index = parent.layer.getFrameIndex(window.name);
//                parent.$('.btn-refresh').click();
                parent.layer.close(index);
            }
        });
    });
    $(function () {
        var powers = $("#powers").val();
        if(powers!=""){
            var ss = powers.split(",");
            for (var i = 0;i<ss.length;i++){
                var power = ss[i];
                $("input[value="+power+"]").attr("checked","checked");
            }
        }


        $("#queding").on("click",function () {
            //获取选择项的值
            var checkedVal = "";
            $("input[name='caritem']:checked").each(function(){
                if($(this).val()!=""){
                    checkedVal += $(this).val();
                    checkedVal += ",";
                }
            });
            checkedVal = checkedVal.substr(0,checkedVal.length-1);
            /*alert(checkedVal)*/
                       $.ajax({
                            type: "POST",
                            url: "/guanliyuan/editRole.do",
                            dataType:'json',
                            data: {
                                "powers":checkedVal,
                                "r_name":$("#r_name").val(),
                                "r_msg":$("#r_msg").val(),
                                "r_id":$("#r_id").val(),
                            },
                            success: function(msg){
                                layer_close();
                            },
                            error:function(){
                                alert(error);
                            }
                        });
        });
    });



</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>