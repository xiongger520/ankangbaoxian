<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>大气的jQuery树型时间轴特效 - 站长素材</title>

    <link href="/shiJianZhou/css/style.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div class="content">
    <div class="wrapper">
        <div class="light"><i></i></div>
        <hr class="line-left">
        <hr class="line-right">
        <div class="main">
            <h1 class="title">安康车险出险记录</h1>

                <div class="year">
                        <h2><a href="#">2018年<i></i></a></h2>
                        <div class="list">
                            <ul>
                            <#list list as z>
                                <#if z.cl_addtime?string("yyyy") == 2018>
                                <li class="cls highlight">
                                    <p class="date">${z.cl_addtime?string("MM月dd日")}</p>
                                    <p class="intro">${z.c_id}</p>
                                    <p class="version">&nbsp;</p>
                                    <div class="more">
                                        <p>
                                            审核状态：
                                            <#if z.cl_state == 0>
                                                正在审核
                                            <#elseif z.cl_state == 1>
                                            合格
                                            <#else >
                                            未合格
                                            </#if>
                                        </p>
                                        <p>
                                            车险估保金额：${z.zonghe}
                                        </p>
                                        <p>
                                            是否结算：
                                            <#if z.cl_jiesuan == 0>
                                            未结算
                                            <#else>
                                            已结算
                                            </#if>
                                        </p>
                                    </div>
                                </li>
                        </#if>
                    </#list>

                            </ul>
                        </div>
                    </div>

                <div class="year">
                <h2><a href="#">2017年<i></i></a></h2>
                <div class="list">
                    <ul>
                    <#list list as z>
                        <#if z.cl_addtime?string("yyyy") == 2017>
                            <li class="cls highlight">
                                <p class="date">${z.cl_addtime?string("MM月dd日")}</p>
                                <p class="intro">${z.c_id}</p>
                                <p class="version">&nbsp;</p>
                                <div class="more">
                                    <p>
                                        审核状态：
                                        <#if z.cl_state == 0>
                                            正在审核
                                        <#elseif z.cl_state == 1>
                                            合格
                                        <#else >
                                            未合格
                                        </#if>
                                    </p>
                                    <p>
                                        车险估保金额：${z.zonghe}
                                    </p>
                                    <p>
                                        是否结算：
                                        <#if z.cl_jiesuan == 0>
                                            未结算
                                        <#else>
                                            已结算
                                        </#if>
                                    </p>
                                </div>
                            </li>
                        </#if>
                    </#list>

                    </ul>
                </div>
            </div>

                <div class="year">
                <h2><a href="#">2016年<i></i></a></h2>
                <div class="list">
                    <ul>
                    <#list list as z>
                        <#if z.cl_addtime?string("yyyy") == 2016>
                            <li class="cls highlight">
                                <p class="date">${z.cl_addtime?string("MM月dd日")}</p>
                                <p class="intro">${z.c_id}</p>
                                <p class="version">&nbsp;</p>
                                <div class="more">
                                    <p>
                                        审核状态：
                                        <#if z.cl_state == 0>
                                            正在审核
                                        <#elseif z.cl_state == 1>
                                            合格
                                        <#else >
                                            未合格
                                        </#if>
                                    </p>
                                    <p>
                                        车险估保金额：${z.zonghe}
                                    </p>
                                    <p>
                                        是否结算：
                                        <#if z.cl_jiesuan == 0>
                                            未结算
                                        <#else>
                                            已结算
                                        </#if>
                                    </p>
                                </div>
                            </li>
                        </#if>
                    </#list>

                    </ul>
                </div>
            </div>

                <div class="year">
                <h2><a href="#">2015年<i></i></a></h2>
                <div class="list">
                    <ul>
                    <#list list as z>
                        <#if z.cl_addtime?string("yyyy") == 2015>
                            <li class="cls highlight">
                                <p class="date">${z.cl_addtime?string("MM月dd日")}</p>
                                <p class="intro">${z.c_id}</p>
                                <p class="version">&nbsp;</p>
                                <div class="more">
                                    <p>
                                        审核状态：
                                        <#if z.cl_state == 0>
                                            正在审核
                                        <#elseif z.cl_state == 1>
                                            合格
                                        <#else >
                                            未合格
                                        </#if>
                                    </p>
                                    <p>
                                        车险估保金额：${z.zonghe}
                                    </p>
                                    <p>
                                        是否结算：
                                        <#if z.cl_jiesuan == 0>
                                            未结算
                                        <#else>
                                            已结算
                                        </#if>
                                    </p>
                                </div>
                            </li>
                        </#if>
                    </#list>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript" src="/shiJianZhou/js/jquery.min.js"></script>
<script type="text/javascript">
    $(".main .year .list").each(function(e, target){
        var $target=  $(target),
                $ul = $target.find("ul");
        $target.height($ul.outerHeight()), $ul.css("position", "absolute");
    });
    $(".main .year>h2>a").click(function(e){
        e.preventDefault();
        $(this).parents(".year").toggleClass("close");
    });
</script>

</body>
</html>
