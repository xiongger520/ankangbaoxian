<!DOCTYPE html>
<html lang="en" xmlns="http://java.sun.com/jsf/html">
<head>
    <meta charset="UTF-8">
    <title>$Title$</title>
    <!--必要样式-->
    <link href="/fw-jilian-sucai/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/fw-jilian-sucai/css/city-picker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/layui/css/layui.css" type="text/css">
    <script type="text/javascript" src="/layui/layui.all.js"></script>
    <script type="text/javascript" src="/layui/layui.js"></script>
    <script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
</head>
<style>
    .wenzitou-anKang{
        font-family: 华文行楷;
        font-size: 33px;
        color: #FD7244;
        position: relative;
        top: 17px;
        left: 222px;
    }
    .wenzitou-yongxin{
        font-size: 20px;
        font-family: 华文楷体;
        font-weight: 900;
        position: relative;
        top: 22px;
        left: 231px;
        color: #A60000;
    }
    .touBuTuPian{
        margin: 0px auto;
        position: relative;
        top: -12px;
    }
    .youBian-tuPian{
        position: relative;
        top: 58px;
    }

    #houzhui p{
        width: 350px;
        margin: 0px auto;
        font-size: 5px;
    }
    #houzhui .p-one{
        position: relative;
        top: 15px;
        left: 85px;
    }
    #houzhui .p-two{
        position: relative;
        left: 45px;
        top: 15px;
    }
    #houzhui .p-three{
        position: relative;
        left: 95px;
        top: 15px;
    }
</style>
<body>
<!--主框架-->
<div style="background-color: ; width: 1200px;margin: 0px auto;">
    <!--头部分-->
    <div style="width: 1200px;height: 60px; display: block; background-color: ;">
        <span class="wenzitou-anKang">安康车险</span>
        <span class="wenzitou-yongxin">省&nbsp;&nbsp;心&nbsp;.&nbsp;省&nbsp;&nbsp;钱</span>
    </div>
    <!--图片-->
    <!--轮播-->
    <div class="layui-carousel" id="test1"  style="background-color: ; width: 1200px;height: 280px">
        <div carousel-item>
            <div style="background-color: ; "><img src="/temp/yonghu/2345截图20180110201906.png"></div>
            <div style="background-color: ; "><img src="/temp/yonghu/2345截图20180110154312.png"></div>
        </div>
    </div>


    <!--表格部分-->
    <div style="width: 1000px;height: 400px; background-color: #FD7244; margin: 0px auto; ">
        <!--左边-->
        <div style="display: inline-block;width: 485px; height: 380px; background-color: #FFFFFF;margin-left: 10px; margin-top: 10px;margin-right: 7px; ">
            <!--头部图片-->
            <div style="width: 485px; height: 58px; background-color: #FFFFFF;position: absolute;" id="biaoti">
                <div style="width: 320px;height: 58px;" class="touBuTuPian">
                    <img  src="/temp/yonghu/2345截图20180111150816.png">
                </div>
            </div>
            <form action="/fuwu/cheXianjs.do" method="post">
            <div style="width: 485px; height: 322px;background-color: ;" class="youBian-tuPian">
                <!--年月-->
                <div class="layui-inline">
                        <label class="layui-form-label" style="width: 101px;">购车年月：</label>
                        <div class="layui-input-inline">
                            <input type="text" style="width: 262px;" class="layui-input" id="test3" placeholder="yyyy年MM月">
                        </div>
                </div>

                <!--级联-->
                <div id="distpicker" style="margin-right: 20px;display: inline">
                    <div class="form-group" style="width: 260px;">
                        <label class="layui-form-label" style="width: 101px;">所在城市：</label>
                        <div style="position: relative;width: 260px;margin-left: 101px;">
                            <input id="city-picker3" style="width: 260px;" class="form-control" readonly type="text" value="河南省/郑州市/二七区" data-toggle="city-picker">

                        </div>
                </div>

                    <!--年月-->
                    <div class="layui-inline">
                        <label class="layui-form-label" style="width: 101px;">车辆价格：</label>
                        <div class="layui-input-inline">
                            <input style="width: 85px;" type="text" name="jiage" required  lay-verify="required" placeholder="单位：元" value="" autocomplete="off" class="layui-input">
                            <input style="width: 85px;" type="hidden" name="fangan" required    value="1"  >
                        </div>
                    </div>

                    <div class="layui-inline">
                        <label class="layui-form-label" style="width: 101px;">手机号码：</label>
                        <div class="layui-input-inline">
                            <input id="shouJiHao" style="width: 262px;" type="text" name="" required  lay-verify="required" placeholder="手机号码" value="" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            <button type="submit" class="layui-btn">确定</button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <!--右边-->
        <div style="display: inline-block; width: 485px; height: 380px; background-color: #FFFFFF;position: absolute;margin-top: 10px; ">
            <!--头部图片-->
            <div style="width: 485px; height: 58px; background-color: #FFFFFF;position: absolute;" >
                <div style="width: 320px;height: 58px;" class="touBuTuPian">
                    <img  src="/temp/yonghu/2345截图20180110204914.png">
                </div>
            </div>
            <div style="width: 485px; height: 322px;background-color: ;" class="youBian-tuPian">
                <img  src="/temp/yonghu/2345截图20180111091534.png">
            </div>
        </div>
    </div>

    <br>

    <!--尾部-->
    <div id="houzhui">
        <p class="p-one">关于安康 | 软件著作权 | 感谢捐赠</p>
        <p class="p-two">Copyright ©2013-2017 H-ui.net All Rights Reserved. </p>
        <p class="p-three">用心做车险，做不一样的车险</p>
    </div>


</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/messages_zh.js"></script>

<!--级联-->
<script src="/fw-jilian-sucai/js/jquery.js"></script>
<script src="/fw-jilian-sucai/js/bootstrap.js"></script>
<script src="/fw-jilian-sucai/js/city-picker.data.js"></script>
<script src="/fw-jilian-sucai/js/city-picker.js"></script>
<script src="/fw-jilian-sucai/js/main.js"></script>


<script>

</script>

</body>
</html>
<script>
    layui.use('laydate', function() {
        var laydate = layui.laydate;
        var myDate = new Date();

        //年月选择器
        laydate.render({
            elem: '#test3'
            , type: 'month',
            format:'yyyy年MM月',
            max:'myDate'
        });
    });

    layui.use('carousel', function(){
        var carousel = layui.carousel;
        //建造实例
        carousel.render({
            elem: '#test1'
            ,width: '100%' //设置容器宽度
            ,arrow: 'always' //始终显示箭头
            //,anim: 'updown' //切换动画方式
        });
    });



</script>