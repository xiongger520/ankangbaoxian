<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="/lib/html5shiv.js"></script>
  <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
<<script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>-->
    <![endif]-->
    <title>汽车核验</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 信息核验管理 <span class="c-gray en">&gt;</span> 汽车核验<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
    <div class="text-c"> <#--日期范围：
        <input type="text" onfocus="WdatePicker({ maxDate:'#F{$dp.$D(\'datemax\')||\'%y-%M-%d\'}' })" id="datemin" class="input-text Wdate" style="width:120px;">
        -
        <input type="text" onfocus="WdatePicker({ minDate:'#F{$dp.$D(\'datemin\')}',maxDate:'%y-%M-%d' })" id="datemax" class="input-text Wdate" style="width:120px;">
        <input type="text" class="input-text" style="width:250px" placeholder="输入会员名称、电话、邮箱" id="" name="">
        <button type="submit" class="btn btn-success radius" id="" name=""><i class="Hui-iconfont">&#xe665;</i> 搜用户</button>
-->
        <#--<h1>汽车信息核验</h1>-->
    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20">
        <button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button>
        <span class="r">共有数据：<strong>${count}</strong> 条</span></div>
    <div class="mt-20">
        <table class="table table-border table-bordered table-hover table-bg table-sort">
            <thead>
            <tr class="text-c">
                <th width="25"><input type="checkbox" name="" value=""></th>
                <th width="80">车牌编号</th>
                <th width="100">用户名</th>
                <th width="70">车辆颜色</th>
                <th width="90">车辆类型</th>
                <th width="150">车辆品牌</th>
                <th width="">车辆图片</th>
                <th width="130">排量大小</th>
                <th width="130">座位个数</th>
                <th width="130">车辆价格</th>
                <th width="130">出险次数</th>
                <th width="70">状态</th>
                <th width="100">操作</th>
            </tr>
            </thead>
            <tbody>
            <#list carslist as car>
            <tr class="text-c" >
                <td><input type="checkbox" name="caritem" value="${car.c_id}" name=""></td>
                <td>${car.c_id}</td>
                <td>${car.users.u_name}</td>
                <td>${car.c_color}</td>
                <td>${car.c_type}</td>
                <td>${car.c_brand}</td>
                <td><img style="width:200px;height: 80px;" src="/uploadsFile/${car.c_pic}"></td>
                <td>${car.c_enginesize}</td>
                <td>${car.seatnum}座</td>
                <td>${car.c_price?string.currency}元</td>
                <td>${car.chuxian_num}次</td>

                <#if (car.c_state==1)>
                    <td class="td-status"><span class="label label-success radius">已通过</span></td>
                <#elseif (car.c_state==2)>
                    <td class="td-status"><span class="label label-danger radius">未通过</span></td>
                <#else>
                    <td class="td-status"><span class="label label-defaunt radius">待审核</span></td>
                </#if>
                <td class="td-manage">
                    <#--<a style="text-decoration:none" class="ml-5" onClick="change_password('修改车牌','/yonghu/tocar-edit.do?c_id=${car.c_id}','10001','600','270')" href="javascript:;" title="修改车牌"><i class="Hui-iconfont">&#xe63f;</i></a> <a title="删除" href="javascript:;" onclick="member_del(this,'${car.c_id}')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a>-->
                        <#if UsersPower?index_of("车辆信息核验")!=-1>
                            <#if (car.c_state==1)>
                                <a style="text-decoration:none" onClick="article_no(this,'${car.c_id}')" href="javascript:;" title="不通过"><i class="Hui-iconfont">&#xe6dd;</i></a>
                            <#elseif (car.c_state==2)>
                                <a style="text-decoration:none" onClick="article_yeas(this,'${car.c_id}')" href="javascript:;" title="通过"><i class="Hui-iconfont">&#xe6e1;</i></a>
                            <#else >
                                <a style="text-decoration:none" onClick="article_shencha(this,'${car.c_id}')" href="javascript:;" title="待审核"><i class="Hui-iconfont">&#xe63f;</i></a>
                            </#if>
                        <#else>
                            <span class="label label-danger radius">没有权限</span>
                        </#if>
                </td>
            </tr>
            </#list>
            </tbody>
        </table>
    </div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    $(function(){
        $('.table-sort').dataTable({
            "aaSorting": [[ 1, "desc" ]],//默认第几个排序
            "bStateSave": true,//状态保存
            "aoColumnDefs": [
                //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
                {"orderable":false,"aTargets":[0,8,9]}// 制定列不参与排序
            ]
        });
    });

   /* /!*用户-还原*!/
    function member_huanyuan(obj,id){
        layer.confirm('确认要还原吗？',function(index){

            $(obj).remove();
            layer.msg('已还原!',{icon: 6,time:1000});
        });
    }

    /!*用户-删除*!/
    function member_del(obj,id){
        layer.confirm('确认要删除吗？',function(index){
            $.ajax({
                type: 'POST',
                url: '',
                dataType: 'json',
                success: function(data){
                    $(obj).parents("tr").remove();
                    layer.msg('已删除!',{icon:1,time:1000});
                },
                error:function(data) {
                    console.log(data.msg);
                },
            });
        });
    }*/
   function article_shencha(obj,id){
        layer.confirm('审核车辆？', {
                    btn: ['通过','不通过','取消'],
                    shade: false,
                    closeBtn: 0
                },
                function(){

                    $.ajax({
                        type: 'POST',
                        url: '/heyan/editCarstates.do',
                        data:{
                            "c_id":id,
                            "c_state":1,
                        },
                        dataType: 'json',
                        success: function(data) {
                            id = "'"+id+"'";
                            var xinxi = $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="article_no(this,'+id+')" href="javascript:;" title="通过"><i class="Hui-iconfont">&#xe6dd;</i></a>');;

                            if (data.c==1) {

                                $(obj).parents("tr").find(".td-manage").prepend(xinxi);
                                $(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已通过</span>');
                                $(obj).remove();
                                layer.msg('已通过!',{icon: 6,time:1000});
                            }
                        },
                        error:function(data) {
                            console.log(data.msg);
                        },
                    });
                },
                function(){
                    $.ajax({
                        type: 'POST',
                        url: '/heyan/editCarstates.do',
                        data:{
                            "c_id":id,
                            "c_state":2,
                        },
                        dataType: 'json',
                        success: function(data) {

                            //alert(id)
                            if (data.c==1) {
                                id = "'"+id+"'";
                                var xinxi = $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="article_yeas(this,'+id+')" href="javascript:;" title="通过"><i class="Hui-iconfont">&#xe6e1;</i></a>');;

                                $(obj).parents("tr").find(".td-manage").prepend(xinxi);
                                $(obj).parents("tr").find(".td-status").html('<span class="label label-danger radius">未通过</span>');
                                $(obj).remove();
                                layer.msg('未通过!', {icon: 5, time: 1000});
                            }
                        },
                        error:function(data) {
                            console.log(data.msg);
                        },
                    });
                }

                );
    }

    function article_no(obj,id){
        layer.confirm('确认不通过吗？',function(index){
            $.ajax({
                type: 'POST',
                url: '/heyan/editCarstates.do',
                data:{
                    "c_id":id,
                    "c_state":2,
                },
                dataType: 'json',
                success: function(data) {

                    //alert(id)
                    if (data.c==1) {
                        id = "'"+id+"'";
                        var xinxi = $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="article_yeas(this,'+id+')" href="javascript:;" title="通过"><i class="Hui-iconfont">&#xe603;</i></a>');;

                        $(obj).parents("tr").find(".td-manage").prepend(xinxi);
                        $(obj).parents("tr").find(".td-status").html('<span class="label label-danger radius">未通过</span>');
                        $(obj).remove();
                        layer.msg('未通过!', {icon: 5, time: 1000});
                    }
                },
                error:function(data) {
                    console.log(data.msg);
                },
            });

        });
    }


    function article_yeas(obj,id){
        layer.confirm('确认通过吗？',function(index){
            $.ajax({
                type: 'POST',
                url: '/heyan/editCarstates.do',
                data:{
                    "c_id":id,
                    "c_state":1,
                },
                dataType: 'json',
                success: function(data) {
                    id = "'"+id+"'";
                    var xinxi = $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="article_no(this,'+id+')" href="javascript:;" title="通过"><i class="Hui-iconfont">&#xe6de;</i></a>');;

                    if (data.c==1) {

                        $(obj).parents("tr").find(".td-manage").prepend(xinxi);
                        $(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已通过</span>');
                        $(obj).remove();
                        layer.msg('已通过!',{icon: 6,time:1000});
                    }
                },
                error:function(data) {
                    console.log(data.msg);
                },
            });
        });
    }

</script>
</body>
</html>