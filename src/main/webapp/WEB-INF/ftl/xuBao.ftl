<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
  <meta charset="UTF-8">
  <title>$Title$</title>
    <link rel="stylesheet" href="/layui/css/layui.css" type="text/css">
    <script type="text/javascript" src="/layui/layui.all.js"></script>
    <script type="text/javascript" src="/layui/layui.js"></script>
    <script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
</head>
<style>
    .biaotou{
        font-size: 20%;
        color: #A60000;
        font-weight: 800px;
        position: relative;
        top: 3px;
    }
    #aaaa{
        position: relative;
        top: 59px;
        left: 32px;
    }
    a {color: #0e90d2;}
    a:visited {text-decoration:none;}
    a:hover {color:#ba2636;text-decoration:underline;}
    a:active {color:#ba2636;}
</style>
<body>
<form class="layui-form" action="/fuwu/xubao_2.do" method="post" id="tijiaoxubao">
    <br>
    <div class="layui-form-item">
        <label class="layui-form-label">保险名称：</label>
        <div class="layui-input-block">
            <input style="width: 350px;" type="text" name="" required  lay-verify="required" readonly="readonly" value="${insurContract.carInsur.ci_name}" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">订单编号：</label>
        <div class="layui-input-block">
            <input style="width: 350px;" type="text" name="ic_id" required  lay-verify="required" readonly="readonly" value="${insurContract.ic_id}" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">有效日期：</label>
        <div class="layui-input-block">
            <input style="width: 350px;" type="text" name="" required  lay-verify="required" readonly="readonly" value="${insurContract.ic_totime?string("yyyy-MM-dd HH:mm:ss")}" autocomplete="off" class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">估算金额：</label>
        <div class="layui-input-block">
            <input style="width: 350px;" type="text" name="" required  lay-verify="required" readonly="readonly" value="${money}" autocomplete="off" class="layui-input">
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">保险状态：</label>
        <div class="layui-input-block">
            <#if (insurContract.ic_state ==0)>
                 <input style="width: 350px;" type="text" name="" required  lay-verify="required" readonly="readonly" value="待审核" autocomplete="off" class="layui-input" id="a">
            <#elseif (insurContract.ic_state ==1)>
                <input style="width: 350px;" type="text" name="" required  lay-verify="required" readonly="readonly" value="已生效" autocomplete="off" class="layui-input" id="b">
            <#elseif (insurContract.ic_state ==2)>
                <input style="width: 350px;" type="text" name="" required  lay-verify="required" readonly="readonly" value="订单无效" autocomplete="off" class="layui-input" id="c">
           <#else >
                <input style="width: 350px;" type="text" name="" required  lay-verify="required" readonly="readonly" value="续保处理中" autocomplete="off" class="layui-input" id="e">

            </#if>
            </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
        <#if (insurContract.ic_state ==0)>
            <button type="submit" class="layui-btn layui-btn-disabled">确定续保</button>
        <#elseif (insurContract.ic_state ==1)>
            <button type="submit" class="layui-btn">确定续保</button>
        <#elseif (insurContract.ic_state ==2)>
            <button type="submit" class="layui-btn layui-btn-disabled">确定续保</button>
        <#else >
            <button type="submit" class="layui-btn">确定续保</button>
        </#if>

            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>

    <div id="aaaa" class="layui-form-item">
        <span>
            <img src="/temp/yonghu/2345_image_file_copy_1.jpg">
        </span>

    <#if (insurContract.ic_state ==0)>
        <p class="biaotou" style="display: inline">注意，您的订单未审核，暂时不能续保！</p>
    <#elseif (insurContract.ic_state ==1)>
        <p class="biaotou" style="display: inline">注意，短时间内连续多次续保，我们将按一次续保处理，如有问题请联系<a href="/index.jsp">客服</a>!</br>上述金额为估算金额，一切以审核后通知为主！</p>
    <#elseif (insurContract.ic_state ==2)>
        <p class="biaotou" style="display: inline">抱歉! 您的订单未能受理成功，如有需要请联系<a href="/index.jsp" style="color: #0b76ac">客服!</a>！</p>
    <#else >
        <p class="biaotou" style="display: inline">注意，您的续保订单已经受理，确定再次续保？</p>
    </#if>

    </div>


</form>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script>
    //Demo
    layui.use('form', function(){
        var form = layui.form;
    });
    $(function () {
        $("#tijiaoxubao").validate({
            rules:{
            },
            onkeyup:false,
            focusCleanup:true,
            success:"valid",
            submitHandler:function(form){
                $(form).ajaxSubmit();
                var index = parent.layer.getFrameIndex(window.name);
                parent.$('.btn-refresh').click();
                parent.layer.close(index);
                alert("您已续保完成，等待管理员审核，祝您生活安康！")
            }
        });


    })
    



</script>
</body>
</html>