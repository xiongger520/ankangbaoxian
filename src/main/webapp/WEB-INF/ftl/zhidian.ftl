<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="/lib/html5shiv.js"></script>
  <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
  <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>资讯列表</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 资讯管理 <span class="c-gray en">&gt;</span> 资讯列表 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
    <div class="cl pd-5 bg-1 bk-gray mt-20">
        <span class="l">
            <a href="javascript:;" onclick="removeIframe()" class="btn btn-primary radius">
                <#--<i class="Hui-iconfont">&#xe6e2;</i>-->
                    关闭选项卡</a>
            <#if UsersPower?index_of("批量删除支点")!=-1>
                <a href="javascript:;" onclick="datadel()" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a>
            </#if>

            <#if UsersPower?index_of("添加支点")!=-1>
            <a class="btn btn-primary radius" data-title="添加支点" onclick="dept_add('添加支点','/xitong/tozhidianAdd.do','800','600')" href="javascript:;">
                <i class="Hui-iconfont">&#xe600;</i>添加支点
            </#if>
            </a>
        </span>
        <#--<button onclick="removeIframe()" class="btn btn-primary radius" style="line-height:1.6em;margin-left:1050px;margin-bottom:3px ">关闭选项卡</button>-->
        <span class="r">共有数据：<strong>${count}</strong> 条</span>
    </div>
    <div class="mt-20">
        <table class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
            <thead>
            <tr class="text-c">
                <th width="25"><input type="checkbox" name="" value=""></th>
                <th width="80">支点编号</th>
                <th>支点名称</th>
                <th width="120">支点电话</th>
                <th width="80">负责人</th>
                <th width="120">联系方式</th>
                <th width="75">所属城市</th>
                <th width="160">详细地址</th>
                <th width="80">位置经度</th>
                <th width="80">位置维度</th>
                <th width="120">备注描述</th>
                <th width="160">添加日期</th>
                <th width="120">操作</th>
            </tr>
            </thead>
            <tbody>
              <#list list as deptList>
                <tr class="text-c">
                    <td><input type="checkbox" value="${deptList.d_id}" name="caritem"></td>
                    <td>${deptList.d_id}</td>
                    <td class="text-l"><u style="cursor:pointer" class="text-primary" onClick="article_edit('支点信息编辑','/xitong/toEditDept.do?d_id=${deptList.d_id}',${deptList.d_id},'1200','810')" href="javascript:;" title="编辑">${deptList.d_name}</u></td>
                    <td>${deptList.d_phone}</td>
                    <td>${deptList.d_ownername}</td>
                    <td>${deptList.ownermobile}</td>
                    <td>${deptList.incity}</td>
                    <td>${deptList.address}</td>
                    <td>${deptList.gpslon}</td>
                    <td>${deptList.gpslat}</td>
                    <td class="td-status"><span class="label label-success radius">${deptList.remark}</span></td>
                    <td>${deptList.addtime?string('yyyy-MM-dd HH:mm:ss')}</td>
                    <td class="f-14 td-manage">
                        <#--<#if UsersPower?index_of("支点审核")!=-1>
                            <a style="text-decoration:none" onClick="article_shenhe(this,'10001')" href="javascript:;" title="审核">审核</a>
                        </#if>-->
                            <#if UsersPower?index_of("修改支点	")!=-1>
                                <a style="text-decoration:none" class="ml-5" onClick="article_edit('支点信息编辑','/xitong/toEditDept.do?d_id=${deptList.d_id}',${deptList.d_id},'1200','810')" href="javascript:;" title="编辑">
                                    <i class="Hui-iconfont">&#xe6df;</i></a>
                            </#if>
                            <#if UsersPower?index_of("删除支点")!=-1>
                            <a style="text-decoration:none" class="ml-5" onClick="article_del(this,${deptList.d_id})" href="javascript:;" title="删除">
                                <i class="Hui-iconfont">&#xe6e2;</i>
                            </#if>
                        </a>
                    </td>
                </tr>
              </#list>
            </tbody>
        </table>
    </div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    $('.table-sort').dataTable({
        "aaSorting": [[ 1, "desc" ]],//默认第几个排序
        "bStateSave": true,//状态保存
        "pading":false,
        "aoColumnDefs": [
            //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
            //{"orderable":false,"aTargets":[0,8]}// 不参与排序的列
        ]
    });
    /*$('.table-sort').dataTable({
        "aaSorting": [[ 1, "desc" ]],//默认第几个排序
        "bStateSave": true,//状态保存
        "pading":false,
        "aoColumnDefs": [
            {"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
            {"orderable":false,"aTargets":[0,8]}// 不参与排序的列
        ]
    });*/

    function dept_add(title,url,w,h){
        layer_show(title,url,w,h);
    }
    /*资讯-添加*/
    function article_add(title,url,w,h){
        var index = layer.open({
            type: 2,
            title: title,
            content: url
        });
        layer.full(index);
    }
    /*资讯-编辑*/
    function article_edit(title,url,id,w,h){
        /*var index = layer.open({
            type: 4,
            title: title,
            content: url,
        });*/
        layer_show(title,url,w,h);
    }
    /*资讯-删除*/
    function article_del(obj,id){
        layer.confirm('确认要删除吗？',function(index){
            $.ajax({
                type: 'POST',
                url: '/xitong/delDeptById.do',
                dataType: 'json',
                data:{
                    d_id:id
                },
                success: function(data){
                    if(data.flag==1){
                        $(obj).parents("tr").remove();
                        layer.msg('已删除!',{icon:1,time:1000});
                    }else {
                        alert("删除失败");
                    }
                },
                error:function(data) {
                    alert("删除失败");
                    console.log(data.msg);
                },
            });
        });
    }

    /*资讯-审核*/
    function article_shenhe(obj,id){
        layer.confirm('审核支点？', {
                    btn: ['通过','不通过','取消'],
                    shade: false,
                    closeBtn: 0
                },
                function(){
                    $(obj).parents("tr").find(".td-manage").prepend('<a class="c-primary" onClick="article_start(this,id)" href="javascript:;" title="申请上线">申请上线</a>');
                    $(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已发布</span>');
                    $(obj).remove();
                    layer.msg('已发布', {icon:6,time:1000});
                },
                function(){
                    $(obj).parents("tr").find(".td-manage").prepend('<a class="c-primary" onClick="article_shenqing(this,id)" href="javascript:;" title="申请上线">申请上线</a>');
                    $(obj).parents("tr").find(".td-status").html('<span class="label label-danger radius">未通过</span>');
                    $(obj).remove();
                    layer.msg('未通过', {icon:5,time:1000});
                });
    }
    /*资讯-下架*/
    function article_stop(obj,id){
        layer.confirm('确认要下架吗？',function(index){
            $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="article_start(this,id)" href="javascript:;" title="发布"><i class="Hui-iconfont">&#xe603;</i></a>');
            $(obj).parents("tr").find(".td-status").html('<span class="label label-defaunt radius">已下架</span>');
            $(obj).remove();
            layer.msg('已下架!',{icon: 5,time:1000});
        });
    }

    /*资讯-发布*/
    function article_start(obj,id){
        layer.confirm('确认要发布吗？',function(index){
            $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="article_stop(this,id)" href="javascript:;" title="下架"><i class="Hui-iconfont">&#xe6de;</i></a>');
            $(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已发布</span>');
            $(obj).remove();
            layer.msg('已发布!',{icon: 6,time:1000});
        });
    }
    /*资讯-申请上线*/
    function article_shenqing(obj,id){
        $(obj).parents("tr").find(".td-status").html('<span class="label label-default radius">待审核</span>');
        $(obj).parents("tr").find(".td-manage").html("");
        layer.msg('已提交申请，耐心等待审核!', {icon: 1,time:2000});
    }

    var datadel = function(){

        //判断选择项是否为空
        var checkedNum = $("input[name='caritem']:checked").length;
        if(checkedNum==0){
            layer.msg('请选中要删除的列表项!',{icon: 5,time:1000});
            return;
        }
        //获取选择项的值
        var checkedVal = [];
        var index=-1;
        $("input[name='caritem']:checked").each(function(){
            index++;
            checkedVal[index]=$(this).val();
            alert(checkedVal[index]);
        });

        var itemsTipStr = "这条记录";
        if(checkedNum>1){
            itemsTipStr = "这些选项";
        }

        // alert(JSON.stringify(checkedVal))
        layer.confirm('确认要删除'+itemsTipStr+'吗？',function(){
            layer.closeAll('dialog');

            //此处请求后台程序，下方是成功后的前台处理……
            $.ajax({
                type: "POST",
                url: "/xitong/delDepts.do",
                data: JSON.stringify(checkedVal),
                contentType: 'application/json',
                success: function(msg){
                    var index=0;
                    $("input[name='caritem']:checked").each(function(){
                        // alert($(this).val()==inde[index])
                        if($(this).val()==checkedVal[index]){
                            $(this).parents("tr").remove();
                            index++;
                        }
                    });
                    layer.msg('已删除!',{icon: 1,time:1000});

                },
                error:function(){
                    layer.msg('删除失败',{icon: 6,time:1000});
                }
            });
        });
    }
</script>
</body>
</html>