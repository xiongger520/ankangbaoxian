${孙明珠}

<!--_meta 作为公共模版分离出去-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="Bookmark" href="/favicon.ico" >
    <link rel="Shortcut Icon" href="/favicon.ico" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/lib/html5shiv.js"></script>
    <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
    <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <!--/meta 作为公共模版分离出去-->

    <title>交易管理</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页
    <span class="c-gray en">&gt;</span>
    保险单导入导出
    <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
</nav>

<div class="page-container">

   <#-- <form class="form form-horizontal" id="form-article-add"  method="post" enctype="multipart/form-data" action="/jiaoyi/file.do">-->
        <div id="tab-system" class="HuiTab">
            <div class="tabBar cl">
                <span>交强险</span>
                <span>商业险</span>
            </div>

            <div class="tabCon">
                <table data-sort="sortDisabled" border="1px" style="width:900px " align="center">
                    <caption>
                        <span style="font-size: 36px;">中 国 安 康 保 险 股 份 有 限 公 司</span></br>
                        <span  style="font-size: 30px;">机动车交通事故责任强制保险单</span></br>
                        <span><em style="color: red;">保险单号：</em></span>
                    </caption>

                    <tbody >
                    <tr class="firstRow">
                        <td valign="top" colspan="2" rowspan="1" style="width:180px;">
                            <p>
                                <span style="font-size: 20px;">&nbsp;被保险人</span>
                            </p>
                        </td>
                        <td valign="top" colspan="8" rowspan="1" style="width:720px; color:red; font-size: 20px;">张三</td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="5" rowspan="1" style="width:450px;">
                            <span style="font-size: 20px;">被保险人身份证号(组织机构代码)<br/></span>
                        </td>
                        <td valign="top" colspan="5" rowspan="1" style="width: 450px; color:red; font-size: 20px;">410XXXXXXXXXXXXXXXXX</td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px;">
                            <span style="font-size: 20px;">被保人地址<br/></span>
                        </td>
                        <td valign="top" colspan="4" rowspan="1" style="width:360px;color:red; font-size: 20px;">现居住地</td>
                        <td valign="top" colspan="2" rowspan="1" style=" width:180px;">
                            <span style="font-size: 20px;">联系电话<br/></span>
                        </td>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px; color:red; font-size: 20px;" >151XXXXXXXX</td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px ">
                            <span style="font-size: 20px;">车牌号码</span>
                        </td>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px; color:red; font-size: 20px;">豫A12001</td>
                        <td valign="top" colspan="2" rowspan="1" style=" width:180px">
                            <span style="font-size:20px;">机动车类型</span>
                        </td>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px;  color:red; font-size: 20px;">小型汽车</td>
                        <td valign="top" colspan="1" rowspan="1" style=" width:90px ">
                            <span style="font-size: 20px;">使用性质</span>
                        </td>
                        <td valign="top" colspan="1" rowspan="1" style="width:90px; color:red; font-size: 20px;" >非运营</td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px ">
                            <span style="font-size: 20px;"> 发动机号码</span>
                        </td>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px;  color:red; font-size: 20px;" >0000000</td>
                        <td valign="top" colspan="3" rowspan="1" style="width:270px ">
                            <span style="font-size: 20px;"> 识别代码(车驾号)<br/></span>
                        </td>
                        <td valign="top" colspan="3" rowspan="1" style="width:270px; color:red; font-size: 20px;" >ABC00000000</td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="1" rowspan="1" style=" width:90px ">
                            <span style="font-size: 20px;">厂牌型号</span>
                        </td>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px; color:red; font-size: 20px; ">某某AA00000</td>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px ">
                            <span style="font-size: 20px;">核定载客</span>
                        </td>
                        <td valign="top" colspan="1" rowspan="1"  style="width:90px;color:red; font-size: 20px; " >5人</td>
                        <td valign="top" colspan="3" rowspan="1" style="width:270px ">
                            <span style="font-size: 20px;">核定载质量<br/></span>
                        </td>
                        <td valign="top" colspan="1" rowspan="1"  style="width:90px;color:red; font-size: 20px; ">0.00千克</td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="1" rowspan="1" style=" width:90px ">
                            <span style="font-size: 20px;"> 排量</span>
                        </td>
                        <td valign="top" colspan="2" rowspan="1" style=" width:90px;color:red; font-size: 20px; ">0.000L</td>
                        <td valign="top" colspan="1" rowspan="1" style=" width:90px ">
                            <span style="font-size: 20px;">功率</span>
                        </td>
                        <td valign="top" colspan="1" rowspan="1" style=" width:90px " ></td>
                        <td valign="top" colspan="2" rowspan="1" style=" width:180px ">
                            <span style="font-size: 20px;">登记日期<br/></span>
                        </td>
                        <td valign="top" colspan="3" rowspan="1" style="width:270px; color:red; font-size: 20px;">0000-00-00</td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px ">
                            <span style="font-size: 20px;">死亡伤残赔偿限额<br/></span>
                        </td>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px ">
                            <span style="font-size: 20px;">110000元</span>
                        </td>
                        <td valign="top" colspan="5" rowspan="1" style=" width:450px ">
                            <span style="font-size: 20px;"> 无责任死亡伤残赔偿限额</span>
                        </td>
                        <td rowspan="1"  style=" width:180px" colspan="1">
                            <span style="font-size: 20px;">110000元</span>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px ">
                            <span style="font-size: 20px;"> 医疗费用赔偿限额</span>
                        </td>
                        <td valign="top" colspan="2" rowspan="1" style=" width:180px ">
                            <span style="font-size: 20px;">110000元</span>
                        </td>
                        <td valign="top" colspan="5" rowspan="1" style=" width:450px ">
                            <span style="font-size: 20px;"> 无责任医疗费用赔偿限额</span>
                        </td>
                        <td rowspan="1" style=" width: 90px " colspan="1">
                            <span style="font-size: 20px;">110000元</span>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px ">
                            <span style="font-size: 20px;"> 财产损失赔偿限额</span>
                        </td>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px">
                            <span style="font-size: 20px;">2000元</span>
                        </td>
                        <td valign="top" colspan="5" rowspan="1" style="width:450px">
                            <span style="font-size: 20px;">无责任财产损失赔偿限额</span>
                        </td>
                        <td rowspan="1"  style="width:90px" colspan="1">
                            <span style="font-size: 20px;"> 2000元</span>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="10" rowspan="1" style="width: 900px">
                            <span style="font-size: 20px;">与道路交通安全违法行为和道路交通事故相联系的浮动比率<br/></span>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="4" rowspan="1" style="width:360px">
                            <span style="font-size: 20px;">保险费合计（人民币大写）</span>
                        </td>
                        <td valign="top" colspan="6" rowspan="1" style="width:540px;color:red; font-size: 20px;">伍佰捌拾元壹角整</td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px">
                            <span style="font-size: 20px;">保险期限</span>
                        </td>
                        <td valign="top" colspan="8" rowspan="1" style="width:720px;color:red; font-size: 20px;">0000年00月00日至0000年00月00日</td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="6" rowspan="1" style="width:540px">
                            <span style="font-size: 20px;">保险合同争议解决方式</span>
                        </td>
                        <td valign="top" colspan="4" rowspan="1" style="width:360px">
                            <span style="font-size: 20px;">诉讼<br/></span>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="1" rowspan="4" style="width:90px">
                            <p>
                                <span style="font-size: 20px;">代收车船税</span>
                            </p>
                        </td>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px">
                            <span style="font-size: 20px;">整备质量</span>
                        </td>
                        <td valign="top" colspan="1" rowspan="1" style="width:90px;color:red; font-size: 20px;">0.0千克</td>
                        <td valign="top" colspan="3" rowspan="1" style="width:270px">
                            <span style="font-size: 20px;">纳税人识别号<br/></span>
                        </td>
                        <td valign="top" colspan="3" rowspan="1" style="width:270px"></td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px">
                            <span style="font-size: 20px;">当年应缴</span>
                        </td>
                        <td valign="top" colspan="1" rowspan="1" style="width:90px"></td>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px">
                            <span style="font-size: 20px;">往年补缴</span>
                        </td>
                        <td valign="top" colspan="1" rowspan="1" style="width:90px"></td>
                        <td valign="top" colspan="2" rowspan="1"style="width:180px">
                            <span style="font-size: 20px;">滞纳金</span>
                        </td>
                        <td valign="top" colspan="1" rowspan="1" style="width:90px"></td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px">
                            <span style="font-size: 20px;">合计(人民币)</span>
                        </td>
                        <td valign="top" colspan="7" rowspan="1" style="width:630px"></td>
                    </tr>
                    <tr>
                        <td valign="top" style="width:270px" rowspan="1" colspan="3">
                            <span style="font-size: 20px;">完税凭证号(减免税证明)</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="6" style="width:540px"></td>
                    </tr>
                    <tr>
                        <td width="120" valign="top" rowspan="2" colspan="1" style="width:90px">
                            <p>
                                <br/>
                            </p>
                            <p>
                                <span style="font-size: 20px;">特别约定</span>
                            </p>
                            <p>
                                <span style="font-size: 20px;">&nbsp; &nbsp;</span>
                            </p>
                        </td>
                        <td valign="top" rowspan="2" colspan="9" style="width:810px"></td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td width="120" valign="top" rowspan="3" colspan="1" style="width:90px">
                            <p>
                                <br/>
                            </p>
                            <p>
                                <br/>
                            </p>
                            <p>
                                <span style="font-size: 20px;">重要提示</span>
                            </p>
                            <p>
                                <br/>
                            </p>
                            <p>
                                <br/>
                            </p>
                        </td>
                        <td valign="top" rowspan="3" colspan="9" style="width:810px">
                            <span style="font-size: 20px;">1.请详细阅读保险条款，特别是责任免除和投保人，被保险人义务。<br/></span>
                            <p>
                                <span style="font-size: 20px;">2.收到本保险单后，请立即核对，如有不符合和疏漏。请及时通知保险人并办理变更或补充手续。</span>
                            </p>
                            <p>
                                <span style="font-size: 20px;">3.保险费一次性付清，及时核对保险单和发票（收据），如有不符请及时联系保险人。</span>
                            </p>
                            <p>
                                <span style="font-size: 20px;">4.被保险人应当在交通事故发生后及时通知保险人。</span>
                            </p>
                        </td>
                    </tr>
                    <tr></tr>
                    <tr></tr>
                    <tr>
                        <td width="120" valign="top" rowspan="3" colspan="1" style="width:90px">
                            <p>
                                <br/>
                            </p>
                            <p>
                                <span style="font-size: 20px;">&nbsp;</span>
                            </p>
                            <p>
                                <span style="font-size: 20px;">&nbsp;保险人</span>
                            </p>
                            <p>
                                <br/>
                            </p>
                        </td>
                        <td valign="top" style="width:810px" rowspan="3" colspan="9">
                            <p>
                                <span style="font-size: 20px;">公司名称：中国安康保险股份有限公司。</span>
                            </p>
                            <p>
                                <span style="font-size: 20px;">公司地址：河南省郑州市马寨</span>
                            </p>
                            <p>
                                <span style="font-size: 20px;">邮政编码：20152500&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;服务电话：666666&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 签单日期：</span>
                            </p>
                        </td>
                    </tr>
                    <tr></tr>
                    <tr></tr>
                    <tr>
                        <td  style="width:90px">
                            <span style="font-size: 20px;">核保：</span>
                        </td>
                        <td style="width:180px"  >
                            <span style="font-size: 20px;">自动核保</span>
                        </td>
                        <td style="width:90px"  >
                            <span style="font-size: 20px;">制单：</span>
                        </td>
                        <td  rowspan="1" colspan="2" style="width:180px"></td>
                        <td  style="width:90px">
                            <span style="font-size: 20px;">经办：</span>
                        </td>
                        <td  rowspan="1" colspan="4" style="width:270px;color:red; font-size: 20px;">某某某</td>
                    </tr>
                    </tbody>
                </table>
                 </br>
                <#--文件上传下载按钮-->
                <div class="row cl">
                    <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2" align="right">
                        <form class="form form-horizontal" id="form-article-add"  method="post" enctype="multipart/form-data" action="/jiaoyi/file.do" onsubmit="return checkForm('file1')">
                            <button onClick="article_save_submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i><a href="/jiaoyi/download.do">下载模板</a></button>
                                <span class="btn-upload form-group">
                                    <input class="input-text upload-url" type="text" name="uploadfile" id="uploadfile" readonly nullmsg="请添加附件！" style="width:200px">
                                    <a href="javascript:void();" class="btn btn-primary radius upload-btn"><i class="Hui-iconfont">&#xe642;</i> 上传文件</a>
                                    <input type="file" id="file1" multiple name="file" class="input-file">
                                </span>
                               <input type="submit" class="btn btn-default radius" value="提交">
                        </form>
                    </div>
                </div>
            </div>

            <div class="tabCon">
                <table data-sort="sortDisabled" border="1px" style="width:900px " align="center">
                    <caption>
                        <span style="font-size: 36px;">中 国 安 康 保 险 股 份 有 限 公 司</span></br>
                        <span style="font-size: 30px;">商业险保险单</span></br>
                        <span><em style="color: red;">保险单号：</em></span>
                    </caption>
                    <tbody>
                    <tr class="firstRow">
                        <td colspan="2"  rowspan="1" valign="top" style="width:180px">
                            <span style="font-size: 18px;">被保险人</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="1" style="width:90px;color:red; font-size:18px;">李四</td>
                        <td valign="top" rowspan="1" colspan="3" style="width: 180px">
                            <span style="font-size: 18px;">被保人身份证（组织机构代码）<br/></span>
                        </td>
                        <td valign="top" rowspan="1" colspan="4" style="width:360px;color:red; font-size:18px;" >410XXXXXXXXXXXXX</td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2" style="width:180px;">
                            <span style="font-size: 18px;">被保险人地址<br/></span>
                        </td>
                        <td valign="top" rowspan="1" colspan="5" style="width:360px;color:red; font-size:18px;" >现居住地</td>
                        <td  colspan="2" valign="top" style="width:180px">
                            <span style="font-size: 18px;">邮政编码</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="1" style="width:90px;color:red; font-size:18px;">452100</td>
                    </tr>
                    <tr>
                        <td  valign="top" style="width:90px;">
                            <span style="font-size: 18px;">联系人<br/></span>
                        </td>
                        <td valign="top" rowspan="1" colspan="2" style="width:180px;color:red; font-size:18px;">李四</td>
                        <td  valign="top" style=" width:180px;">
                            <span style="font-size: 18px;">联系号码</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="2" style="width: 180px;color:red; font-size:18px;">151XXXXXXX</td>
                        <td  valign="top" style="width:90px;">
                            <span style="font-size: 18px;">E-mai</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="3" style="width:270px;color:red; font-size:18px;">973990841@qq.com</td>
                    </tr>
                    <tr>
                        <td  valign="top" style="width:360px;">
                            <span style="font-size: 18px;">交强险承保公司</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="3" style="width:270px;color:red; font-size:18px;">某某公司</td>
                        <td valign="top" style="width:260px;">
                            <span style="font-size: 18px;">交强险保单号</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="5" style="width:450px;"></td>
                    </tr>
                    <tr>
                        <td rowspan="1" valign="top"  colspan="4" style="width:360px;">
                            <span style="font-size: 18px;">交强险保险期限</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="6" style="width:540px;color:red; font-size:18px;">0000年00月00日至0000年00月00日</td>
                    </tr>
                    <tr>
                        <td valign="top" style="width:180px;">
                            <span style="font-size: 18px;">车牌号码</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="2" style="width:180px;color:red; font-size:18px;">京A1234</td>
                        <td  valign="top" style="width:180px;">
                            <span style="font-size: 18px;">车牌型号</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="2" style="width:180px;color:red; font-size:18px;">GB9417-88</td>
                        <td  valign="top" style="width:90px;">
                            <span style="font-size: 18px;">功率</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="3" style="width:270px;color:red; font-size:18px;" ></td>
                    </tr>
                    <tr>
                        <td  valign="top" style="width:180px;">
                            <span style="font-size: 18px;">核定载客</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="2" style="width:180px;color:red; font-size:18px;">8人</td>
                        <td  valign="top" style="width:180px;">
                            <span style="font-size: 18px;">发动机号</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="2"  style="width:180px;color:red; font-size:18px;">00000000</td>
                        <td  valign="top" style="width:270px;">
                            <span style="font-size: 18px;">新车购置价</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="3"  style="width:270px;color:red; font-size:18px;">2000000元</td>
                    </tr>
                    <tr>
                        <td  valign="top" style="width:180px;">
                            <span style="font-size: 18px;">机动车类型</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="2"  style="width:180px;color:red; font-size:18px;">小型汽车</td>
                        <td valign="top" style="width:270px;">
                            <span style="font-size: 18px;">使用性质</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="1"  style="width:90px;color:red; font-size:18px;">运营</td>
                        <td  valign="top" style="width:270px;">
                            <span style="font-size: 18px;">行驶证车主</span>
                        </td>
                        <td valign="top" rowspan="1" colspan="4"  style="width:360px;color:red; font-size:18px;">李四</td>
                    </tr>
                    <tr>
                        <td valign="top" style="width:360px;" rowspan="1" colspan="4">
                            <span style="font-size: 18px;">&nbsp; &nbsp; &nbsp; &nbsp;购买保险类别</span>
                        </td>
                        <td valign="top" style="width:270px;" rowspan="1" colspan="3" >
                            <span style="font-size: 18px;">&nbsp; &nbsp; &nbsp; &nbsp;保险金额(元)</span>
                        </td>
                        <td valign="top" style="width:270px;" rowspan="1" colspan="3" >
                            <span style="font-size: 18px;">&nbsp; &nbsp; &nbsp; &nbsp; 签单保费(元)</span>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" rowspan="6" colspan="4" style="width:360px;">
                            <p>
                                <span style="font-size: 18px;">&nbsp; &nbsp; &nbsp;</span>
                            </p>
                            <p>
                                <br/>
                            </p>
                            <p>
                                <br/>
                            </p>
                            <p>
                                <br/>
                            </p>
                            <p>
                                <br/>
                            </p>
                            <p>
                                <br/>
                            </p>
                            <p>
                                <br/>
                            </p>
                        </td>
                        <td valign="top" rowspan="6" colspan="3" style="width:270px;"></td>
                        <td valign="top" rowspan="6" colspan="3" style="width:270px;"></td>
                    </tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr>
                        <td valign="top" colspan="2" rowspan="1" style="width:180px;">
                            <span style="font-size: 18px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;保险期限</span>
                        </td>
                        <td valign="top" colspan="8" rowspan="1" style="width:180px;color:red; font-size:18px;">0000年00月00日至0000年00月00日</td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="1" rowspan="2" style="width:90px;">
                            <p>
                                <br/>
                            </p>
                            <p>
                                <br/>
                            </p>
                            <p>
                                <br/>
                            </p>
                            <p>
                                <span style="color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; background-color: rgb(255, 255, 255); font-size: 18px;">重要提示</span>
                            </p>
                        </td>
                        <td valign="top" colspan="9" rowspan="2" style="width:720px;>
                            <span style="font-size: 18px;">&nbsp;&nbsp;<span style="word-wrap: break-word; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; background-color: rgb(255, 255, 255); font-size: 20px;">1.请详细阅读保险条款，特别是责任免除和投保人，被保险人义务。<br style="word-wrap: break-word;"/></span><span style="color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 12px; background-color: rgb(255, 255, 255);"></span></span>
                        <p style="word-wrap: break-word; margin-top: 0px; margin-bottom: 10px; padding: 0px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255);">
                            <span style="word-wrap: break-word; font-size: 18px;">2.收到本保险单后，请立即核对，如有不符合和疏漏。请及时通知保险人并办理变更或补充手续。</span>
                        </p>
                        <p style="word-wrap: break-word; margin-top: 0px; margin-bottom: 10px; padding: 0px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255);">
                            <span style="word-wrap: break-word; font-size: 18px;">3.保险费一次性付清，及时核对保险单和发票（收据），如有不符请及时联系保险人。</span>
                        </p>
                        <p style="word-wrap: break-word; margin-top: 0px; margin-bottom: 10px; padding: 0px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255);">
                            <span style="word-wrap: break-word; font-size: 18px;">4.被保险人应当在交通事故发生后及时通知保险人。</span>
                        </p>
                        </td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td valign="top" colspan="1" rowspan="3" style="width:90px;>
                           <p>
                                <br/>
                            </p>
                            <p>
                                <span style="font-size: 20px;">&nbsp;</span>
                        </p>
                        <p>
                            <span style="font-size: 20px;">&nbsp;保险人</span>
                        </p>
                        <p>
                            <br/>
                        </p>
                        </td>
                        <td valign="top" colspan="9" rowspan="2" style="width:720px;>
                            <p style="word-wrap: break-word; margin-top: 0px; margin-bottom: 10px; padding: 0px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255);">
                        <span style="word-wrap: break-word; font-size: 18px;">公司名称：中国安康保险股份有限公司。</span>
                        </p>
                        <p style="word-wrap: break-word; margin-top: 0px; margin-bottom: 10px; padding: 0px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255);">
                            <span style="word-wrap: break-word; font-size: 18px;">公司地址：河南省郑州市马寨</span>
                        </p>
                        <p style="word-wrap: break-word; margin-top: 0px; margin-bottom: 10px; padding: 0px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255);">
                            <span style="word-wrap: break-word; font-size: 18px;">邮政编码：20152500&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;服务电话：666666&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 签单日期：</span>
                        </p>
                        </td>
                    </tr>
                    <tr></tr>
                    <tr>
                    <tr>
                        <td  style="width:90px">
                            <span style="font-size: 20px;">核保：</span>
                        </td>
                        <td style="width:270px"  >
                            <span style="font-size: 20px;">自动核保</span>
                        </td>
                        <td style="width:180px"  >
                            <span style="font-size: 20px;">制单：</span>
                        </td>
                        <td  rowspan="1" colspan="1" style="width:90px"></td>
                        <td  style="width:90px">
                            <span style="font-size: 20px;">经办：</span>
                        </td>
                        <td  rowspan="1" colspan="5" style="width:450px;color:red; font-size: 20px;">某某某</td>

                    </tr>
                    </tbody>
                </table>
                <div class="row cl">
                    <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2" align="right">
                        <form class="form form-horizontal" id="form-article-add"  method="post" enctype="multipart/form-data" action="/jiaoyi/file.do"  onsubmit="return checkForm('file2')">
                            <button onClick="article_save_submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i><a href="/jiaoyi/download2.do">下载模板</a></button>
                                    <span class="btn-upload form-group">
                                        <input class="input-text upload-url" type="text" name="uploadfile" id="uploadfile" readonly nullmsg="请添加附件！" style="width:200px">
                                        <a href="javascript:void();" class="btn btn-primary radius upload-btn"><i class="Hui-iconfont">&#xe642;</i> 上传文件</a>
                                        <input type="file" id="file2" multiple name="file" class="input-file">
                                    </span>
                            <input type="submit" class="btn btn-default radius" value="提交">
                        </form>
                    </div>
                </div>
            </div>


        </div>
       <#-- <div class="row cl">
            <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2" align="right">
              <button onClick="article_save_submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i><a href="/jiaoyi/download.do">下载模板</a></button>
              <form class="form form-horizontal" id="form-article-add"  method="post" enctype="multipart/form-data" action="/jiaoyi/file.do">
                    <span class="btn-upload form-group">
                        <input class="input-text upload-url" type="text" name="uploadfile" id="uploadfile" readonly nullmsg="请添加附件！" style="width:200px">
                        <a href="javascript:void();" class="btn btn-primary radius upload-btn"><i class="Hui-iconfont">&#xe642;</i> 上传文件</a>
                        <input type="file" multiple name="file" class="input-file">
                    </span>
                <input type="submit" class="btn btn-default radius" value="提交">
              </form>
            </div>
        </div>-->
  <#--  </form>-->
</div>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript">
    $(function(){
        if('${msg}'!="")
            layer.msg('${msg}',{icon: 1,time:1000});

        $('.skin-minimal input').iCheck({
            checkboxClass: 'icheckbox-blue',
            radioClass: 'iradio-blue',
            increaseArea: '20%'
        });
        $("#tab-system").Huitab({
            index:0
        });
    });
    function checkForm(oop) {
        /*alert(1)*/
        //alert($("#"+oop)[0].files[0].size)
        if($("#"+oop)[0].files[0].size>5242880){
            layer.msg('文件过大，请重新选择!',{icon:6,time:1000});
            return false;
        }

        }

</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>
