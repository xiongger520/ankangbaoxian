<!--
项目二组  鲍康
-->
<!--_meta 作为公共模版分离出去-->
<!DOCTYPE HTML>
<html>
<style type="text/css">
    .btn-success{color:#fff;background-color:#5eb95e; border-color:#5eb95e}
    .btn-success:hover,
    .btn-success:focus,
    .btn-success:active,
    .btn-success.active{color:#fff;background-color:#429842;border-color:#429842}

</style>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="Bookmark" href="/favicon.ico" >
    <link rel="Shortcut Icon" href="/favicon.ico" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="/lib/html5shiv.js"></script>
  <script type="text/javascript" src="/lib/respond.min.js"></script>

    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />

    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
  <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <!--/meta 作为公共模版分离出去-->

    <link href="/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet" type="text/css" />
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 保险管理 <span class="c-gray en">&gt;</span> 添加保险<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
    <div class="text-c">

    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20"><button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button>  </div>

<div class="page-container">
    <form action="/baoxian/addBaoxian.do" method="post" class="form form-horizontal" id="form-article-add" >

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>保险名称：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" onblur="jianyan()" class="input-text" value=""  placeholder="请输入保险的名称" id="ci_name" name="ci_name">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>保险类型：</label>
            <div class="formControls col-xs-8 col-sm-9"> <span class="select-box">
				<select name="ci_type" class="select">
					<option value="交强险">交强险</option>
					<option value="商业险">商业险</option>

				</select>
				</span> </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>赔付费率：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text"  placeholder="例如0.25" id="" name="comp_rate">
            </div>
        </div>

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>最大保额：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="" placeholder="例如500000" id="" name="maxmoney">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>保费费率：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="" placeholder="例如0.25" id="" name="ins_rale">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>保险状态：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="待审核"  id="" name="" readonly>
            </div>
        </div>



        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>保险基础价：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" name="ci_money" id=""  value="" placeholder="例如100000" class="input-text">
            </div>
        </div>

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"></label>
            <div class="formControls col-xs-8 col-sm-9">
                <div class="uploader-thum-container">
                    <div id="fileList" class="uploader-list"></div>
                    <input class="btn btn-primary radius" id="chaxun" type="submit" value="&nbsp;确认提交&nbsp;" >
                    <input class="btn btn-primary radius" type="reset" value="&nbsp;&nbsp;重置&nbsp;&nbsp;">
                </div>
            </div>
        </div>

                </form>
                </div>

                <!--_footer 作为公共模版分离出去-->
                <script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
                <script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
                <script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
                <script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

                <!--请在下方写此页面业务相关的脚本-->
                <script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
                <script type="text/javascript" src="/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
                <script type="text/javascript" src="/lib/jquery.validation/1.14.0/validate-methods.js"></script>
                <script type="text/javascript" src="/lib/jquery.validation/1.14.0/messages_zh.js"></script>
                <script type="text/javascript" src="/lib/webuploader/0.1.5/webuploader.min.js"></script>
                <script type="text/javascript" src="/lib/ueditor/1.4.3/ueditor.config.js"></script>
                <script type="text/javascript" src="/lib/ueditor/1.4.3/ueditor.all.min.js"> </script>
                <script type="text/javascript" src="/lib/ueditor/1.4.3/lang/zh-cn/zh-cn.js"></script>
                <script type="text/javascript">
                   /* function queren(){
                        alert(1)
                        return false;
                        layer.confirm('确认要添加吗？',function(index){

                            $.ajax({
                                type: 'POST',
                                url: '',
                                dataType: 'json',
                                data:{

                                },
                                success: function(data){
                                  /!*  $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="member_start(this,id)" href="javascript:;" title="启用"><i class="Hui-iconfont">&#xe6e1;</i></a>');
                                    $(obj).parents("tr").find(".td-status").html('<span class="label label-defaunt radius">已停用</span>');
                                    $(obj).remove();*!/
                                    layer.msg('已取消!',{icon: 5,time:1000});
                                },
                                error:function(data) {
                                    console.log(data.msg);
                                },
                            });
                        });
                    }*/


                   /*   Window.onload=function(${flag}){
                           if(flag!=""){
                               if(flag){
                                   layer.msg('添加保险成功',{icon:6,time:1000});
                               }else{
                                   layer.msg('添加保险失败，请重新添加！',{icon: 5,time:1000});
                               }
                           }
                       }*/


                   if('${fag}'!="")
                       layer.msg('保险添加成功!',{icon:6,time:1000});


                   var jianyan= function() {
                       if($("#ci_name").val()!="")
                       $.ajax({
                           type: 'POST',
                           url: '/baoxian/chazhao.do',
                           dataType: 'json',
                           data:{
                               "ci_name":$("#ci_name").val(),
                           },
                           success: function(date){
                               if(date.flag!=0){
                                   layer.msg('保险名已存在!',{icon: 5,time:1000});
                                   $("#chaxun").hide();
                                   /*return false;*/
                               }else{
                                   layer.msg('保险名有效!',{icon:6,time:1000});
                                   $("#chaxun").show();
                                   /*return true;*/
                               }
                             },
                           error:function(){
                               alert(2)
                           }

                           }
                    )
                   }
                   $(function(){



                       $("#form-article-add").validate({
                           rules:{
                               ci_name:{
                                   required:true,
                                   minlength:2,
                                   maxlength:10,
                                   isChinese:true
                               },
                               comp_rate:{
                                   required:true,
                                   isFloat:true
                               },
                               maxmoney:{
                                   required:true,
                                   isInteger:true

                               },
                               ins_rale:{
                                   required:true,
                                   isNumber:true,

                               },
                               ci_money:{
                                   required:true,
                                   isInteger:true
                               },

                           }
                          /* submitHandler:function(form){
                               jianyan()
                           }*/
                       });
                   });
                </script>
</body>
</html>