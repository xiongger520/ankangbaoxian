<!--_meta 作为公共模版分离出去-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="Bookmark" href="/favicon.ico" >
    <link rel="Shortcut Icon" href="/favicon.ico" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/lib/html5shiv.js"></script>
    <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
    <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>
    <!--[if IE 6]>
    <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <!--/meta 作为公共模版分离出去-->

    <link href="/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet" type="text/css" />

    <style>
        .ss{
            display: none;
            border: 1px solid;
            padding-bottom: 20px;
            padding-left: 10px;
            padding-right: 10px;
            padding-top: 10px;
        }
    </style>
</head>

<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 理赔管理 <span class="c-gray en">&gt;</span> 增加报案单<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
    <div class="text-c">

    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20"><button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button>  </div>
    <article class="page-container">
        <form action="/lipei/addClaimant.do"  method="post" class="form form-horizontal" id="form-article-add" enctype="multipart/form-data">
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>报案车辆：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="" placeholder="" onblur="blu()" id="c_id" name="c_id">
                </div>
            </div>

            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2">报案用户：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="" placeholder="" id="u_name" name="u_name" readonly="readonly">
                    <input type="hidden" class="input-text" value="" placeholder="" id="u_id" name="u_id" readonly="readonly">
                </div>
            </div>

            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2">事故类型：</label>
                <div class="formControls col-xs-8 col-sm-9">
                            <span class="select-box">
                                <select class="select" onchange="blu()" size="1" id="shiGuLeiXing">
                                    <option value="0">交通事故</option>
                                    <option value="1">车辆盗抢</option>
                                    <option value="2">车辆涉水</option>
                                    <option value="3">车辆玻璃</option>
                                    <option value="4">车辆刮蹭</option>
                                </select>
                            </span>
                </div>
            </div>

        <#--<div class="row cl">
            <label class="form-label col-xs-4 col-sm-2">责任比率：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="0" placeholder="" id="cl_type" name="cl_type">
            </div>
        </div>-->

            <!--
            被保险机动车一方负主要事故责任的，事故责任比例为70%；
            被保险机动车一方负同等事故责任的，事故责任比例为50%；
            被保险机动车一方负次要事故责任的，事故责任比例为30%。
            -->
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2">责任比例：</label>
                <div class="formControls col-xs-8 col-sm-9">
                        <span class="select-box">
                            <select class="select" size="1" id="cl_type" name="cl_type">
                                <option value="0.70">主要事故责任</option>
                                <option value="0.50">同等事故责任</option>
                                <option value="0.30">次要事故责任</option>
                                <option value="0.00">无责任</option>
                            </select>
				        </span>
                </div>
            </div>

            <!--
            负次要事故责任的，实行5%的事故责任免赔率；
            负同等事故责任的，实行10%的事故责任免赔率；
            负主要事故责任的，实行15%的事故责任免赔率；
            负全部事故责任的，实行20%的事故责任免赔率；
            -->
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2">保险机动车责任等级：</label>
                <div class="formControls col-xs-8 col-sm-9">
                        <span class="select-box">
                            <select class="select" size="1" name="sgzr">
                                <option value="0.20">全部事故责任</option>
                                <option value="0.15">主要事故责任</option>
                                <option value="0.10">同等事故责任</option>
                                <option value="0.05">次要事故责任</option>
                                <option value="0.00">无责任</option>
                            </select>
				        </span>
                </div>
            </div>

        <#-- <div class="row cl">
             <label class="form-label col-xs-4 col-sm-2">报案详细文件：</label>
             <div class="formControls col-xs-8 col-sm-9">
                 <input type="file" name="msg_table" id="msg_table" >
             </div>
         </div>-->
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2">报案单：</label>
                <div class="formControls col-xs-8 col-sm-9"> <span class="btn-upload form-group">
				<input class="input-text upload-url" type="text" name="uploadfile" id="uploadfile" readonly  nullmsg="请添加附件！" style="width:200px">
				<a href="javascript:void();" class="btn btn-primary radius upload-btn"><i class="Hui-iconfont">&#xe642;</i> 浏览文件</a>
				<input type="file" multiple  name="msg_table" class="input-file">
				</span> </div>
            </div>

        <#--<div class="row cl">
        <label class="form-label col-xs-4 col-sm-2">申请赔付金额：</label>
        <div class="formControls col-xs-8 col-sm-9">
            <input type="text" class="input-text" value="0" placeholder="" name="pfje">
        </div>
        </div>

        <div class="row cl">
        <label class="form-label col-xs-4 col-sm-2">申请赔付金额：</label>
        <div class="formControls col-xs-8 col-sm-9">
            <input type="text" class="input-text" value="0" placeholder="" name="pfje">
        </div>
        </div>-->


            <div class="row cl" id="baoxianlipei" style="display: none">
                <label class="form-label col-xs-4 col-sm-2">保险理赔：</label>
                <div class="formControls col-xs-8 col-sm-9">
                <#-- <form class="form form-horizontal" id="form-article-add"  method="post" enctype="multipart/form-data" action="/jiaoyi/file.do">-->
                    <div id="tab-system" class="HuiTab">
                        <div class="tabBar cl" id="xianzhong">

                        </div>
                    </div>
                    <div>

                        <div class="ss" style="display: none;" id="biaodan1">

                            <div class="row cl">
                                <label class="form-label col-xs-4 col-sm-2">死亡伤残费：</label>
                                <div class="formControls col-xs-8 col-sm-9">
                                    <input type="text" class="input-text" value="0" placeholder=""  name="cl_money">
                                </div>
                            </div>

                            <div class="row cl">
                                <label class="form-label col-xs-4 col-sm-2">治疗就医费：</label>
                                <div class="formControls col-xs-8 col-sm-9">
                                    <input type="text" class="input-text" value="0" placeholder=""  name="cl_money1">
                                </div>
                            </div>

                            <div class="row cl">
                                <label class="form-label col-xs-4 col-sm-2">财产损失费：</label>
                                <div class="formControls col-xs-8 col-sm-9">
                                    <input type="text" class="input-text" value="0" placeholder="" name="cl_money2">
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="ss" id="biaodan2">
                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2">第三方损失费：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <input type="text" class="input-text" value="0" placeholder="" name="peifu_dsf">
                            </div>
                        </div>
                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2">绝对免赔率：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <input type="text" class="input-text" value="0" placeholder="" id="jdmp_ds" name="jdmp_ds">
                            </div>
                        </div>

                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2"><#--是否违反安全装载规定：--></label>

                            <div class="formControls col-xs-8 col-sm-9">

                            <#--<input type="checkbox"   id="checkbox-1" value="0.10" onclick="mianPeiJiSuan(this.value)">-->
                            <#--<label for="checkbox-1">&nbsp;</label>-->
                                是否违反安全装载规定： <input type="checkbox"   id="checkbox-11" value="0.10" onclick="mianPeiJiSuan(this,'#checkbox-11')">
                            <#--<label for="checkbox-1">&nbsp;</label>-->
                                是否找到责任人：<input type="checkbox"   id="checkbox-12" value="0.10" onclick="mianPeiJiSuan(this,'#checkbox-12')">
                                是否违反投保约定： <input type="checkbox"   id="checkbox-13" value="0.10" onclick="mianPeiJiSuan(this,'#checkbox-13')">

                            </div>
                        </div>





                    </div>
                    <div class="ss" id="biaodan3" >

                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2">绝对免赔额：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <input type="text" class="input-text" value="0" placeholder=""  name="jdmpe">
                            </div>
                        </div>

                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2">绝对免赔率：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <input type="text" class="input-text" value="0" placeholder=""  name="jdmp_cs">
                            </div>
                        </div>



                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2">第三方获得金额：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <input type="text" class="input-text" value="0" placeholder=""  name="dsf_money">
                            </div>
                        </div>
                        <div class="row cl" id="quanju">
                            <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>类别：</label>
                            <div class="formControls col-xs-8 col-sm-9 ">
                            <#--<div class="radio-box">-->
                                <input name="category"  value="0" type="radio" id="sex-1"  checked>
                                <label for="sex-1" >全车丢失</label>
                            <#--</div>-->
                            <#--<div class="radio-box">-->
                                <input type="radio" id="sex-2" value="1" name="category">
                                <label for="sex-2" >维修损失</label>
                            <#--</div>-->

                            </div>
                        </div>

                    </div>
                    <div class="ss" id="biaodan4">

                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2">座位平均损失：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <input type="text" class="input-text" value="0" placeholder=""  name="zwss">
                            </div>
                        </div>

                    </div>
                    <div class="ss" id="biaodan5">
                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2">申请赔付金额：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <input type="text" class="input-text" value="0" placeholder="" name="pfje">
                            </div>
                        </div>

                    </div>

                    <div class="ss" id="biaodan6">

                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2">绝对免赔率：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <input type="text" class="input-text" value="0" placeholder="" id="jdmp_dq" name="jdmp_dq">
                            </div>
                        </div>

                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2"><#--是否违反安全装载规定：--></label>

                            <div class="formControls col-xs-8 col-sm-9">
                                是否找到责任人：<input type="checkbox"   id="checkbox-22" value="0.10" onclick="mianPeiJiSuan1(this,'#checkbox-22')">
                                是否违反投保约定： <input type="checkbox"   id="checkbox-23" value="0.10" onclick="mianPeiJiSuan1(this,'#checkbox-23')">
                            </div>
                        </div>


                    </div>
                </div>

            </div>


            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2">详情描述：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <textarea name="s_msg" id="s_msg" class="textarea"  placeholder="说点什么...最多输入2000个字符，一个汉字两个字符哦" datatype="*10-100" dragonfly="true" nullmsg="备注不能为空！"<#-- onKeyUp="$.Huitextarealength(this,200)"-->></textarea>
                    <p class="textarea-numberbar"><em class="textarea-length" id="count">0</em>/2000</p>
                </div>
            </div>

            <div id="baoxianlist">

            </div>

            <div class="row cl" >
                <label class="form-label col-xs-4 col-sm-2"></label>
                <div class="formControls col-xs-8 col-sm-9" >
                <#--<button type="button" class="btn btn-primary radius" id="saveBtn" >提交</button>-->
                    <input class="btn btn-primary radius" id="saveBtn" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
                    <input class="btn btn-primary radius" type="reset" value="&nbsp;&nbsp;重置&nbsp;&nbsp;">
                </div>
            </div>

        </form>
    </article>

    <!--_footer 作为公共模版分离出去-->
    <script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
    <script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
    <script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

    <!--请在下方写此页面业务相关的脚本-->
    <script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
    <script type="text/javascript" src="/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
    <script type="text/javascript" src="/lib/jquery.validation/1.14.0/validate-methods.js"></script>
    <script type="text/javascript" src="/lib/jquery.validation/1.14.0/messages_zh.js"></script>
    <script>

        if('${msg1}'!=""){
            /*layer.msg('',{icon: 1,time:1000});*/
            alert('${msg1}');
            location.href="/lipei/tolipeidan-add.do";

        }

        function mianPeiJiSuan(oop,id){
            var rate = $("#jdmp_ds").val();
            if($(oop).prop("checked")){
                rate = parseFloat(rate)+parseFloat($(id).val());
                $("#jdmp_ds").val(rate);
            }else{
                rate = parseFloat(rate)-parseFloat($(id).val());
                $("#jdmp_ds").val(rate);
            }
        }
        function mianPeiJiSuan1(obj,id){
            var rate = $("#jdmp_dq").val();
            if($(id).prop('checked')) {
                rate = parseFloat(rate)+parseFloat(obj.value);
                $("#jdmp_dq").val(rate);
            } else{
                rate = parseFloat(rate)-parseFloat(obj.value);
                $("#jdmp_dq").val(rate);
            }
        }

        function xuanze(oop){

        }


        /*$("span[name='1']").on("click",function () {
            $(".1").display("block");
            $(".2").display("none");
        })*/

        $(function(){
            $('.skin-minimal input').iCheck({
                checkboxClass: 'icheckbox-blue',
                radioClass: 'iradio-blue',
                increaseArea: '20%'
            });

            $("#form-article-add").validate({
                rules:{
                    c_id:{
                        required:true,
                        rangelength:[7,7]
                    },
                    u_name:{
                        required:true
                    },
                    cl_type:{
                        required:true,
                        range:[0,1]
                    },
                    msg_table:{
                        required:true
                    },
                    s_msg:{
                        required:true
                    },
                    jdmp_ds:{
                        required:true,
                        range:[0,1]
                    },
                    jdmp_cs:{
                        required:true,
                        range:[0,1]
                    },
                    jdmp_dq:{
                        required:true,
                        range:[0,1]
                    },
                    cl_money:{
                        required:true,
                        min:0
                    },
                    cl_money1:{
                        required:true,
                        min:0
                    },
                    cl_money2:{
                        required:true,
                        min:0
                    },
                    jdmpe:{
                        required:true,
                        min:0
                    },
                    dsf_money:{
                        required:true,
                        min:0
                    },
                    zwss:{
                        required:true,
                        min:0
                    },
                    pfje:{
                        required:true,
                        min:0
                    },
                },

            });
            $("#tab-system span").on("click",function () {
                //alert($(this).attr("name"))
                $(".ss").hide();
                $("#biaodan"+$(this).attr("name")).show()

                $("#tab-system span").removeClass();
                $(this).addClass("current")
            })


        });

        var qiehuan = function(oop){
            $(".ss").hide();
            $("#biaodan"+$(oop).attr("name")).show()

            $("#tab-system span").removeClass();
            $(oop).addClass("current")
            var text= $("#quanju").clone(true)
            $("#quanju").remove();
            text.appendTo($("#biaodan"+$(oop).attr("name")));
        }

        //        var b = $('#c_id').blur(function () {
        var blu = function () {
            if($("#c_id").val()!=""){
                $.ajax({
                    type:"GET",
                    dataType:"json",
                    url:"/lipei/queryCarById.do",
                    data:{
                        c_id: $("#c_id").val()
                    },
                    success: function(msg){

                        if(msg.flag){
                            $("#u_name").val(msg.cars.users.u_name);
                            $("#u_id").val(msg.cars.users.u_id);
                            $("#saveBtn").show();
                        }else {
                            layer.msg('该车不存在',{icon: 5,time:1000});
                            $("#saveBtn").hide();
                        }
                        //alert(msg.icon)
                        var baoxain='';
                        var baoxain1='';
                        if(msg.icon==''){
                            $("#biaodan1").hide();
                            $("#baoxianlipei").hide()
                            layer.msg('该车辆还没购买保险或还未生效',{icon: 5,time:1000});
                        }else{
                            $.each(msg.icon,function(i,val){
                                //alert(val.carInsur.ci_name)
                                baoxain1+="<input type='hidden' name='ci_id' class='baoxian1' id='"+val.carInsur.ci_name+"' value='"+val.carInsur.ci_id+"'> "
                                if(val.carInsur.ci_name=='机动车交通事故责任强制保险')
                                    baoxain+="<span class='current baoxian' onclick='qiehuan(this)' id='"+val.carInsur.ci_name+"1'  name='1'>机动车交通事故责任强制保险</span>";

                                else if(val.carInsur.ci_name=='机动车第三者责任保险')
                                    baoxain+="<span  onclick='qiehuan(this)' class='baoxian' id='"+val.carInsur.ci_name+"1'  name='2'>机动车第三者责任保险</span>";

                                else if(val.carInsur.ci_name=='机动车损失保险')
                                    baoxain+="<span onclick='qiehuan(this)' class='baoxian' id='"+val.carInsur.ci_name+"1'  name='3'>机动车损失保险</span>";

                                else if(val.carInsur.ci_name=='机动车车上人员责任保险')
                                    baoxain+="<span onclick='qiehuan(this)' class='baoxian' id='"+val.carInsur.ci_name+"1'  name='4'>机动车车上人员责任保险</span>";
                                else if(val.carInsur.ci_name=='机动车全车盗抢保险')
                                    baoxain+="<span onclick='qiehuan(this)' class='baoxian' id='"+val.carInsur.ci_name+"1'   name='6'>机动车全车盗抢保险</span>";
                                else
                                    baoxain+="<span onclick='qiehuan(this)' class='baoxian' id='"+val.carInsur.ci_name+"1'  name='5'>"+val.carInsur.ci_name+"</span>";
                            });

                            $("#baoxianlipei").show();
                            $("#biaodan1").show();

                        }


                        $("#xianzhong").html(baoxain)
                        $("#baoxianlist").html(baoxain1)
                        xuanze();
                        /*if(msg.icon.size()==0){
                            layer.msg('该车辆还没购买保险或还未生效',{icon: 5,time:1000});
                        }else{
                            alert(msg.icon.size())
                        }*/
                    },

                });
            }else{
                layer.msg('请输入车牌',{icon: 5,time:1000});
            }
        }
        //        });


        function xuanze(){
            $(function () {
                if($("#shiGuLeiXing").val()==0){
                    $(".baoxian").each(function(){
                        if($(this).attr("id")  !=  "机动车交通事故责任强制保险1" && $(this).attr("id")+'' !="机动车第三者责任保险1" && $(this).attr("id")+'' !="机动车损失保险1" && $(this).attr("id")+'' !="机动车车上人员责任保险1"){
                            $(this).remove();
                        }
                    });
                    $(".baoxian1").each(function(){
                        if(($(this).attr("id")!="机动车交通事故责任强制保险") && ($(this).attr("id")!="机动车第三者责任保险" && $(this).attr("id")+'' !="机动车损失保险" && $(this).attr("id")+'' !="机动车车上人员责任保险")){
                            $(this).remove();
                        }
                    });
                }else if($("#shiGuLeiXing").val()==1){
                    $(".baoxian").each(function(){
                        if($(this).attr("id")  !=  "机动车交通事故责任强制保险1" && $(this).attr("id")!="机动车损失保险1" && $(this).attr("id")!="机动车全车盗抢保险1"){
                            $(this).remove();
                        }
                    });
                    $(".baoxian1").each(function(){
                        if(($(this).attr("id")!="机动车交通事故责任强制保险") && $(this).attr("id")!="机动车损失保险" && $(this).attr("id")!="机动车全车盗抢保险"){
                            $(this).remove();
                        }
                    });
                }else if($("#shiGuLeiXing").val()==2){
                    $(".baoxian").each(function(){
                        if($(this).attr("id")  !=  "机动车交通事故责任强制保险1" && $(this).attr("id")!="机动车损失保险1" && $(this).attr("id")!="机动车涉水险1"){
                            $(this).remove();
                        }
                    });
                    $(".baoxian1").each(function(){
                        if(($(this).attr("id")!="机动车交通事故责任强制保险") && $(this).attr("id")!="机动车损失保险" && $(this).attr("id")!="机动车涉水险"){
                            $(this).remove();
                        }
                    });

                }else if($("#shiGuLeiXing").val()==3){
                    $(".baoxian").each(function(){
                        if($(this).attr("id")  !=  "机动车交通事故责任强制保险1" && $(this).attr("id")!="机动车损失保险1" && $(this).attr("id")!="车窗玻璃险1"){
                            $(this).remove();
                        }
                    });
                    $(".baoxian1").each(function(){
                        if(($(this).attr("id")!="机动车交通事故责任强制保险") && $(this).attr("id")!="机动车损失保险" && $(this).attr("id")!="车窗玻璃险"){
                            $(this).remove();
                        }
                    });
                }else if($("#shiGuLeiXing").val()==4){
                    $(".baoxian").each(function(){
                        if($(this).attr("id")  !=  "机动车交通事故责任强制保险1" && $(this).attr("id")!="机动车损失保险1" && $(this).attr("id")!="机动车剐蹭险1"){
                            $(this).remove();
                        }
                    });
                    $(".baoxian1").each(function(){
                        if(($(this).attr("id")!="机动车交通事故责任强制保险") && $(this).attr("id")!="机动车损失保险" && $(this).attr("id")!="机动车剐蹭险"){
                            $(this).remove();
                        }
                    });
                }
            })

        }


        /**
         * jq文本框输入计数
         */

        var maxCount = 2000; // 最高字数，这个值可以自己配置
        $("#s_msg").on('keyup',function() {
            var len = getStrLength(this.value);
            $("#count").html(maxCount-len);
        })// 中文字符判断

        function getStrLength(str) {
            var len = str.length;
            var reLen = 0;
            for (var i = 0; i < len; i++) {
                if (str.charCodeAt(i) < 27 || str.charCodeAt(i) > 126) {
                    // 全角
                    reLen += 2;
                } else {
                    reLen++;
                }
            }
            return reLen;
        }
    </script>
</body>
</html>
