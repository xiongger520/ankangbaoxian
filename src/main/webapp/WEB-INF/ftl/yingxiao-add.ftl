<!--
项目二组  鲍康
-->
<!--_meta 作为公共模版分离出去-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="Bookmark" href="/favicon.ico" >
    <link rel="Shortcut Icon" href="/favicon.ico" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="/lib/html5shiv.js"></script>
  <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
  <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <!--/meta 作为公共模版分离出去-->

    <title>新建网站角色 - 管理员管理 - H-ui.admin v3.1</title>
    <meta name="keywords" content="H-ui.admin v3.1,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
    <meta name="description" content="H-ui.admin v3.1，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 营销管理 <span class="c-gray en">&gt;</span> 保险套餐推出<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
    <div class="text-c">

    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20"><button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button>  </div>

<article class="page-container">
    <form  method="post" class="form form-horizontal" id="form-admin-role-add">
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>套餐名称：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" onblur="hecha()" class="input-text" value="" placeholder="请填写套餐名称，例如：套餐n（0，1，2,...）" id="ip_name" name="roleName">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>套餐状态：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="待审核" placeholder="" id="roleName"  readonly>
            </div>
        </div>
        <#--<div class="row cl">
            <label class="form-label col-xs-4 col-sm-3">备注：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="" placeholder="" id="" name="">
            </div>
        </div>-->
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2">套餐选择：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <dl class="permission-list">
                    <dt>
                        <label>
                            <#--<input type="checkbox" value="" name="user-Character-0" id="user-Character-0">-->
                            批量管理</label>
                    </dt>
                    <dd>
                            <#list alist as a>
                        <dl class="cl permission-list2">

                            <dt>

                                <label class="">
                                    <input type="checkbox" class="baoxian" onclick="jianyan()" value="${a.ci_id}" name="user-Character-0-0" >
                                ${a.ci_id}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ${a.ci_name}</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;赔付费率：${a.comp_rate}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;最大保额：${a.maxmoney}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;保费费率：${a.ins_rale}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;保险基础价：${a.ci_money}

                            </dt>


                        </dl>
                            </#list>

                    </dd>
                </dl>

            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>套餐优惠率：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="" placeholder="请填写套餐优惠率，例如：0.9" id="ip_rate" name="youhui">
            </div>
        </div>
        <div class="row cl">

            <label class="form-label col-xs-4 col-sm-2"></label>
            <div class="formControls col-xs-8 col-sm-9">
                <div class="uploader-thum-container">
                    <div id="fileList" class="uploader-list"></div>
                    <button type="submit" onclick="tijiao()" class="btn btn-primary radius" id="admin-role-save" name="admin-role-save"><i class="icon-ok"></i> 确认提交</button>
                    <input class="btn btn-primary radius" type="reset" value="&nbsp;&nbsp;重置&nbsp;&nbsp;">
                </div>
            </div>

            <#--<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
               &lt;#&ndash; <input class="btn btn-success radius" onclick="tijiao()" id="chaxun" type="submit" value="&nbsp确认提交&nbsp" >&ndash;&gt;
                <button type="submit" onclick="tijiao()" class="btn btn-primary radius" id="admin-role-save" name="admin-role-save"><i class="icon-ok"></i> 确认提交</button>
                   <input class="btn btn-primary radius" type="reset" value="&nbsp;&nbsp;重置&nbsp;&nbsp;">
            </div>-->
        </div>
    </form>
</article>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript">
    $(function(){

        $(".permission-list dt input:checkbox").click(function(){
            $(this).closest("dl").find("dd input:checkbox").prop("checked",$(this).prop("checked"));
        });
        $(".permission-list2 dd input:checkbox").click(function(){
            var l =$(this).parent().parent().find("input:checked").length;
            var l2=$(this).parents(".permission-list").find(".permission-list2 dd").find("input:checked").length;
            if($(this).prop("checked")){
                $(this).closest("dl").find("dt input:checkbox").prop("checked",true);
                $(this).parents(".permission-list").find("dt").first().find("input:checkbox").prop("checked",true);
            }
            else{
                if(l==0){
                    $(this).closest("dl").find("dt input:checkbox").prop("checked",false);
                }
                if(l2==0){
                    $(this).parents(".permission-list").find("dt").first().find("input:checkbox").prop("checked",false);
                }
            }
        });



    });

    jQuery.validator.addMethod("isFloatGtZero1", function(value, element) {
        value=parseFloat(value);
        return this.optional(element) || value>0&&value<1;
    }, "优惠率必须大于0小于1");

    $("#form-admin-role-add").validate({
        rules: {
            roleName: {
                required: true,
            },
            youhui: {
                required: true,
                isFloatGtZero1:true,
            },

        }





        /* onkeyup:false,
         focusCleanup:true,
         success:"valid",
         submitHandler:function(form){
             $(form).ajaxSubmit();
             var index = parent.layer.getFrameIndex(window.name);
             parent.layer.close(index);*/

    });

    if('${flag}'!="")
        layer.msg('保险套餐添加成功!',{icon:6,time:1000});
    var $baoxian = [];
    var jianyan= function (){
        $baoxian = []
        var obj = $(".baoxian")
        check_val = [];
        for(k in obj){
            if(obj[k].checked)
                check_val.push(obj[k].value);
        }

        for(var i = 0;i<=check_val.length;i++){
                if(i<4)
               $baoxian[i]=check_val[i]
        }
        if(check_val.length>4){
            layer.msg('套餐最多包含四个保险，多选以前四个为主',{icon: 5,time:1000});
        }


    }

    function tijiao(){
       if ($("#ip_name").val()!=""&&$("#ip_rate").val()!="") {
           if (($baoxian.length) > 2) {
               var ci1 = $baoxian[0];
               var ci2 = $baoxian[1];
               var ci3;
               var ci4
               if ($baoxian[2] == undefined)
                   ci3 = 0;
               else
                   ci3 = $baoxian[2]
               if ($baoxian[3] == undefined)
                   ci4 = 0;
               else
                   ci4 = $baoxian[3];
               /*  alert(ci2)
                 alert(ci4)*/
               $.ajax({
                   type: 'POST',
                   url: '/yingxiao/addtaocan.do',
                   data: {
                       "ci1": ci1,
                       "ci2": ci2,
                       "ci3": ci3,
                       "ci4": ci4,
                       "ip_name": $("#ip_name").val(),
                       "ip_rate": $("#ip_rate").val()
                   },
                   dataType: 'json',
                   success: function (data) {
                     /*  alert(1)*/
                       layer.msg('套餐添加成功!', {icon: 6, time: 1000});
                   },
                   error: function (data) {
                       alert("套餐添加成功!")
                       layer.msg('套餐添加成功!',{icon:6,time:1000});
                   }
               });
           } else {

               layer.msg('套餐最少包含两个保险', {icon: 5, time: 1000});
               return false;
           }
       }else {
          return false;
       }

    }

    var hecha= function() {
        if($("#ip_name").val()!="")
            $.ajax({
                        type: 'POST',
                        url: '/yingxiao/querytaocan.do',
                        dataType: 'json',
                        data:{
                            "ip_name":$("#ip_name").val(),
                        },
                        success: function(date){
                            if(date.flag!=0){
                                layer.msg('套餐名已存在!',{icon: 5,time:1000});
                                $("#admin-role-save").hide();
                                /*return false;*/
                            }else{
                                layer.msg('套餐名有效!',{icon:6,time:1000});
                                $("#admin-role-save").show();
                                /*return true;*/
                            }
                        },
                        error:function(){
                            alert(2)
                        }

                    }
            )
    }




</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>