<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="/lib/html5shiv.js"></script>
  <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
  <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>退保核验</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 信息核验管理 <span class="c-gray en">&gt;</span> 退保核验<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
    <div class="text-c">
        <#--<h1>续保核验管理</h1>-->
    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20">
        <button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button>
        <span class="r">共有数据：<strong>${count}</strong> 条</span></div>
    <div class="mt-20">
        <table class="table table-border table-bordered table-hover table-bg table-sort">
            <thead>
            <tr class="text-c">
                <th width="25"><input type="checkbox" name="" value=""></th>
                <th width="80">续保编号</th>
                <th width="60">订单编号</th>
                <th width="80">车牌号</th>
                <th width="100">保险名称</th>
                <th width="40">姓名</th>
                <th width="150">联系电话</th>
                <th width="">地址</th>
                <th width="150">邮箱</th>
                <th width="100">理论金额</th>
                <th width="130">申请时间</th>
                <th width="70">状态</th>
                <th width="100">操作</th>
            </tr>
            </thead>
            <tbody>
            <#list slist as s>
            <tr class="text-c">
                <td><input type="checkbox" value="1" name=""></td>
                <td>${s.r_id}</td>
                <td>${s.insurContract.ic_id}</td>
                <td>${s.insurContract.cars.c_id}</td>
                <td>${s.insurContract.carInsur.ci_name}</td>
                <td>${s.users.u_name}</td>
                <td>${s.users.u_phone}</td>
                <td  class="text-l">${s.users.u_addr}</td>
                <td >${s.users.u_emal}</td>
                <td >${s.insurContract.ic_money}元</td>

                <td>${s.r_addtime?string("yyyy-MM-dd HH:mm:ss")}</td>

              <#if (s.r_state==1)>
                  <td class="td-status"><span class="label label-success radius">通过</span></td>
              <#elseif (s.r_state==2)>
                  <td class="td-status"><span class="label label-danger radius">不通过</span></td>
              <#else>
                  <td class="td-status"><span class="label label-defaunt radius">待审核</span></td>
              </#if>

                <td class="td-manage">
                    <#if UsersPower?index_of("续保核验")!=-1>
                        <#if (s.r_state==1)>
                            <a style="text-decoration:none" <#--onclick="article_ting(this,'${users.u_id}')"--> href="javascript:;" title="合格"><i class="Hui-iconfont">&#xe6e1;</i></a>

                        <#elseif (s.r_state==2)>
                            <a style="text-decoration:none" <#--onClick="article_qiyong(this,'${users.u_id}')"--> href="javascript:;" title="不合格"><i class="Hui-iconfont">&#xe6dd;</i></a>
                        <#else >
                            <a style="text-decoration:none" onClick="article_shenkan(this,'${s.insurContract.ic_id}','${s.r_id}')" href="javascript:;" title="待审核"><i class="Hui-iconfont">&#xe63f;</i></a>
                        </#if>
                        <#else>
                            <span class="label label-danger radius">没有权限</span>
                    </#if>


                </td>      </tr>
            </#list>
            </tbody>
        </table>
    </div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    $(function(){
        $('.table-sort').dataTable({
            "aaSorting": [[ 1, "desc" ]],//默认第几个排序
            "bStateSave": true,//状态保存
            "aoColumnDefs": [
                //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
                {"orderable":false,"aTargets":[0,8,9]}// 制定列不参与排序
            ]
        });
    });

    function article_shenkan(obj,id,ic){
        layer.confirm('退保审核？', {
                    btn: ['通过','不通过','取消'],
                    shade: false,
                    closeBtn: 0
                },
                function(){

                    $.ajax({
                        type: 'POST',
                        url: '/heyan/editRenewal.do',
                        data:{
                            "ic_id":id,
                            "r_state":1,
                            "r_id":ic,

                        },
                        dataType: 'json',
                        success: function(data) {
                            /* id = "'"+id+"'";
                              var xinxi = $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="article_no(this,'+id+')" href="javascript:;" title="通过"><i class="Hui-iconfont">&#xe6de;</i></a>');;
*/

                            if (data.s1==1) {
                                $(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已审核</span>');
                                $(obj).parents("tr").find(".td-manage").html('<i class="Hui-iconfont">&#xe6e1;</i>');


                            }else{
                                $(obj).parents("tr").find(".td-status").html('<span class="label label-danger radius">不通过</span>');
                                $(obj).parents("tr").find(".td-manage").html('<i class="Hui-iconfont">&#xe6dd;</i>');
                            }
                            layer.msg(data.s,{icon: 6,time:2000});
                        },
                        error:function(data) {
                            console.log(data.msg);
                        },
                    });
                },
                function(){
                    $.ajax({
                        type: 'POST',
                        url: '/heyan/editRenewal.do',
                        data:{
                            "ic_id":id,
                            "r_state":2,
                            "r_id":ic,
                        },
                        dataType: 'json',
                        success: function(data) {

                            //alert(id)
                            if (data.s1==1) {
                                /*     id = "'"+id+"'";
                                     var xinxi = $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="article_yeas(this,'+id+')" href="javascript:;" title="通过"><i class="Hui-iconfont">&#xe603;</i></a>');;
     */
                                $(obj).parents("tr").find(".td-status").html('<span class="label label-danger radius">不通过</span>');
                                $(obj).parents("tr").find(".td-manage").html('<i class="Hui-iconfont">&#xe6dd;</i>');
                                layer.msg('审核不通过!', {icon: 5, time: 1000});
                            }
                        },
                        error:function(data) {
                            console.log(data.msg);
                        },
                    });
                }

        );
    }

</script>
</body>
</html>