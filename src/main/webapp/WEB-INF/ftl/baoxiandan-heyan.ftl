<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="/lib/html5shiv.js"></script>
  <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
  <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>保险单核验</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 信息核验管理 <span class="c-gray en">&gt;</span> 保险单核验<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
    <div class="text-c"> <#--日期范围：
        <input type="text" onfocus="WdatePicker({ maxDate:'#F{$dp.$D(\'datemax\')||\'%y-%M-%d\'}' })" id="datemin" class="input-text Wdate" style="width:120px;">
        -
        <input type="text" onfocus="WdatePicker({ minDate:'#F{$dp.$D(\'datemin\')}',maxDate:'%y-%M-%d' })" id="datemax" class="input-text Wdate" style="width:120px;">
        <input type="text" class="input-text" style="width:250px" placeholder="输入会员名称、电话、邮箱" id="" name="">
        <button type="submit" class="btn btn-success radius" id="" name=""><i class="Hui-iconfont">&#xe665;</i> 搜用户</button>-->
        <#--<h1>保险单核验管理</h1>-->
    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20">
        <button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button>
        <span class="r">共有数据：<strong>${count}</strong> 条</span></div>
    <div class="mt-20">
        <table class="table table-border table-bordered table-hover table-bg table-sort">
            <thead>
            <tr class="text-c">
                <th width="25"><input type="checkbox" name="" value=""></th>
                <th width="120">订单编号</th>
                <th width="80">车辆编号</th>
                <th width="100">保险编号</th>
                <th width="100">保险名称</th>
                <th width="100">保险类型</th>
                <th width="70">是否缴费</th>
               <#-- <th width="130">审核人编号</th>-->
                <th width="90">订单状态</th>
                <th width="80">操作</th>
            </tr>
            </thead>
            <tbody>
            <#list insclist as i>
            <tr class="text-c">
                <td><input type="checkbox" value="" name=""></td>
                <td>${i.ic_id}</td>
                <td class="text-l">${i.cars.c_id}</td>
                <td>${i.carInsur.ci_id}</td>
                <td>${i.carInsur.ci_name}</td>
                <td>${i.carInsur.ci_type}</td>
                <#if (i.ic_ispayment==1)>
                    <td class=""><span class="label label-success radius">已缴费</span></td>
                <#else>

                    <td class=""><span class="label label-warning radius">未交费</span></td>
                </#if>
              <#--  <td>${i.un_shenid}</td>-->
                <#if (i.ic_state==0)>
                    <td class="td-status"><span class="label label-defaunt radius">待审核</span></td>
                <#elseif (i.ic_state==1)>
                    <td class="td-status"><span class="label label-success radius">生效</span></td>
                <#elseif (i.ic_state==2)>
                    <td class="td-status"><span class="label label-danger radius">不合格</span></td>
                <#else >
                    <td class="td-status"><span class="label label-warning radius">退保</span></td>
                </#if>

                <td class="f-14 td-manage">
                    <#if UsersPower?index_of("新增保险核验")!=-1>
                        <#if (i.ic_state==0)>
                            <a style="text-decoration:none" onClick="article_shenkan(this,'${i.ic_id}')" href="javascript:;" title="审核"><i class="Hui-iconfont">&#xe61d;</i></a>
                        <#-- <a style="text-decoration:none" onClick="article_stop(this,'${c.ci_id}')" href="javascript:;" title="下架"><i class="Hui-iconfont">&#xe6e1;</i></a>-->
                        <#elseif (i.ic_state==1)>
                        <#--<a style="text-decoration:none" onClick="article_start(this,'${c.ci_id}')" href="javascript:;" title="发布"><i class="Hui-iconfont">&#xe6dd;</i></a>-->
                            <i class="Hui-iconfont">&#xe6e1;</i>
                        <#elseif (i.ic_state==2)>
                            <i class="Hui-iconfont">&#xe6dd;</i>
                        <#-- <a style="text-decoration:none" onClick="article_shenkan(this,'${c.ci_id}')" href="javascript:;" title="审核"><i class="Hui-iconfont">&#xe63f;</i></a>-->
                        <#else >
                            <i class="Hui-iconfont">&#xe631;</i>
                        </#if>
                    <#else>
                        <span class="label label-danger radius">没有权限</span>
                    </#if>


                </td>

            </tr>
            </#list>
            </tbody>
        </table>
    </div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    $(function(){
        $('.table-sort').dataTable({
            "aaSorting": [[ 1, "desc" ]],//默认第几个排序
            "bStateSave": true,//状态保存
            "aoColumnDefs": [
                //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
               /* {"orderable":false,"aTargets":[0,8,9]}// 制定列不参与排序*/
            ]
        });
    });

    function article_shenkan(obj,id){
        layer.confirm('保险单审核？', {
                    btn: ['生效','不合格','取消'],
                    shade: false,
                    closeBtn: 0
                },
                function(){

                    $.ajax({
                        type: 'POST',
                        url: '/heyan/heyanBaoxiandan.do',
                        data:{
                            "ic_id":id,
                            "ic_state":1,


                        },
                        dataType: 'json',
                        success: function(data) {
                            /*    id = "'"+id+"'";
                                var xinxi = $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="article_no(this,'+id+')" href="javascript:;" title="通过"><i class="Hui-iconfont">&#xe6de;</i></a>');;*/

                            if (data.d==1) {

                                // $(obj).parents("tr").find(".td-manage").prepend("<a style='text-decoration:none' /!*onClick='article_ting(this,"+id+")'*!/ href='javascript:;' title=''><i class='Hui-iconfont'>&#xe6e1;</i></a>");
                                $(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">生效</span>');
                                $(obj).parents("tr").find(".td-manage").html('<i class="Hui-iconfont">&#xe6e1;</i>');
                                // $(obj).remove();
                                layer.msg('生效!',{icon: 6,time:1000});
                            }
                        },
                        error:function(data) {
                            console.log(data.msg);
                        },
                    });
                },
                function(){
                    $.ajax({
                        type: 'POST',
                        url: '/heyan/heyanBaoxiandan.do',
                        data:{
                            "ic_id":id,
                            "ic_state":2,
                        },
                        dataType: 'json',
                        success: function(data) {

                            //alert(id)
                            if (data.d==1) {
                                /* id = "'"+id+"'";
                                 var xinxi = $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="article_yeas(this,'+id+')" href="javascript:;" title="通过"><i class="Hui-iconfont">&#xe603;</i></a>');;
 */
                                //$(obj).parents("tr").find(".td-manage").prepend("<a style='text-decoration:none' /!*onClick='article_qiyong(this,"+id+")'*!/ href='javascript:;' title=''><i class='Hui-iconfont'>&#xe6dd;</i></a>");
                                $(obj).parents("tr").find(".td-status").html('<span class="label label-danger radius">不合格</span>');
                                $(obj).parents("tr").find(".td-manage").html('<i class="Hui-iconfont">&#xe6dd;</i>');
                                // $(obj).remove();
                                layer.msg('不合格!', {icon: 5, time: 1000});
                            }
                        },
                        error:function(data) {
                            console.log(data.msg);
                        },
                    });
                }

        );
    }



</script>
</body>
</html>