<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/lib/html5shiv.js"></script>
    <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
    <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>系统日志</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页
    <span class="c-gray en">&gt;</span>
    系统管理
    <span class="c-gray en">&gt;</span>
    系统日志
    <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
</nav>
<div class="page-container">
    <div class="text-c"> 日期范围：
        <input type="text" onfocus="WdatePicker({ maxDate:'#F{$dp.$D(\'logmax\')||\'%y-%M-%d\'}' })" id="logmin" class="input-text Wdate" style="width:120px;">
        -
        <input type="text" onfocus="WdatePicker({ minDate:'#F{$dp.$D(\'logmin\')}',maxDate:'%y-%M-%d' })" id="logmax" class="input-text Wdate" style="width:120px;">
        <input type="text" name="log_busitype" id="log_busitype" placeholder="操作类型" style="width:250px" class="input-text">
        <button name="" id="sousuo" class="btn btn-success" type="submit"><i class="Hui-iconfont">&#xe665;</i> 搜日志</button>
    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20">
        <button onclick="removeIframe()" class="btn btn-primary radius" style="line-height:1.6em;margin-left:20px;margin-bottom:3px ">关闭选项卡</button>
        <span class="r">共有数据：<strong id="count"></strong> 页</span>
    </div>
    <div class="mt-20" id="app">
        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">
            <div class="dataTables_length" id="DataTables_Table_0_length">
                <label>显示
                    <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="select" id="pageSize">
                        <option value="15" selected="selected">15</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select> 条</label>
            </div>

            <div id="DataTables_Table_0_filter" class="dataTables_filter">
                <label>从当前数据中检索:
                    <input type="search" class="input-text " id="jiansuo">
                </label>
            </div>

            <table class="table table-border table-bordered table-bg table-hover table-sort">
                <thead>
                <tr class="text-c">
                <#--<th width="25"><input type="checkbox" name="" value=""></th>-->
                    <th width="80">编号</th>
                    <th width="150">操作类型</th>
                    <th>操作业务</th>
                    <th width="120">操作人</th>
                    <th width="200">操作时间</th>
                <#--<th width="100">操作</th>-->
                </tr>
                </thead>
                <tbody id="tbody">

                <tr class="text-c" v-for="(item,index) in result">
                <#--<td><input type="checkbox" value="" name=""></td>-->
                <#--<td>{{index+1}}</td>-->
                    <td>{{item.log_id}}</td>
                    <td>{{item.log_busitype}}</td>
                    <td>{{item.log_busimasg}}</td>
                    <td><a href="javascript:;" onclick="admin_username_xiangqing()"><span id="un_id" hidden="hidden">{{item.un_id}}</span>{{item.username}}</a></td>
                    <td>{{new Date(item.log_addtime).toLocaleString()}}</td>
                <#--<td>
                    <a title="详情" href="javascript:;" onclick="system_log_show(this,item.log_id)" class="ml-5" style="text-decoration:none">
                        <i class="Hui-iconfont">&#xe665;</i>
                    </a>
                    <a title="删除" href="javascript:;" onclick="system_log_del(this,item.log_id)" class="ml-5" style="text-decoration:none">
                        <i class="Hui-iconfont">&#xe6e2;</i>
                    </a>
                </td>-->
                </tr>
                <#-- <#list list as busilogList>
                 <tr class="text-c">
                     <td><input type="checkbox" value="" name=""></td>
                     <td>${busilogList.log_id}</td>
                     <td>${busilogList.log_busitype}</td>
                     <td>${busilogList.log_busimasg}</td>
                     <td>${busilogList.un_id}</td>
                     <td>${busilogList.log_addtime?date}</td>
                     <td>
                         <a title="详情" href="javascript:;" onclick="system_log_show(this,'10001')" class="ml-5" style="text-decoration:none">
                             <i class="Hui-iconfont">&#xe665;</i>
                         </a>
                         <a title="删除" href="javascript:;" onclick="system_log_del(this,'10001')" class="ml-5" style="text-decoration:none">
                             <i class="Hui-iconfont">&#xe6e2;</i>
                         </a>

                     </td>
                 </tr>
                 </#list>-->
                </tbody>
            </table>

        <#--<div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">显示 1 到 2 ，共 2 条</div>
        <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
            <a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">上一页</a>
            <span>
                <a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a>
            </span>
            <a class="paginate_button next disabled" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">下一页</a>
        </div>-->


        </div>
    </div>
    <div id="pageNav" class="pageNav"></div>
    <input type="hidden" id="curr">
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/lib/vue/vue.min.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    var app = new Vue({
        el:'#app',
        data:{
            result:[]
        }
    })

    var getBusilogList = function (curr,pageSize) {
        $.ajax({
            type:"POST",
            url:"/xitong/queryLikeBusilog.do",
            dataType:'json',
            data:{
                begintime: $("#logmin").val() ||"2015-1-1",
                endtime: $("#logmax").val() ||"2050-1-1",
                pageNum: curr || 1,
                pageSize:pageSize,
                log_busitype:$("#log_busitype").val() || "",
                jiansuo:$("#jiansuo").val() || ""
            },
            success:function (data) {
                console.log(data.result);
                app.result = data.result;
                $("#count").html(data.totalPage);

                laypage({
                    cont: 'pageNav',
                    pages: data.totalPage,
                    skip: true,
                    skin: '#7B68EE',
                    first : '首页',
                    last : '尾页',
                    curr: curr || 1,
                    jump: function (obj,first) {
                        if (!first) {
                            getBusilogList(obj.curr,pageSize);
                        }
                    }
                });



                /* var $msg="";
                 var $count = 0;
                 $.each(data.busilogList,function (i,val) {
                     $msg+="<tr class=\"text-c\">";
                     $msg+="<td><input type=\"checkbox\" value=\"\" name=\"\"></td>";
                     $msg+="<td>"+val.log_id+"</td>";
                     $msg+="<td>"+val.log_busitype+"</td>";
                     $msg+="<td>"+val.log_busimasg+"</td>";
                     $msg+="<td>"+val.un_id+"</td>";
                     $msg+="<td>"+val.log_addtime+"</td>";
                     $msg+="<td><a title=\"详情\" href=\"javascript:;\" onclick=\"system_log_show(this,'10001')\" class=\"ml-5\" style=\"text-decoration:none\"><i class=\"Hui-iconfont\">&#xe665;</i></a>";
                     $msg+="<a title=\"删除\" href=\"javascript:;\" onclick=\"system_log_del(this,'10001')\" class=\"ml-5\" style=\"text-decoration:none\"><i class=\"Hui-iconfont\">&#xe6e2;</i></a></td>";
                     $msg+="</tr>";
                     $count ++;
                 })
                 $("#tbody").html($msg);
                 $("#count").html($count);*/
            },
            error:function () {
                alert("error");
            }
        })
    }
    getBusilogList(1,15);

    var getBusilogByTiaojian = function (tiaojian) {
        $.ajax({
            type:"POST",
            url:"/xitong/queryBusilogByTiaojian.do",
            dataType:'json',
            data:{
                tiaojian:tiaojian
            },
            success:function (data) {
                console.log(data.result);
                app.result = data.result;
            },
            error:function () {
                alert("error");
            }
        })
    }

    $("body").keydown(function() {
        var tiaojian = $("#jiansuo").val();
        if (event.keyCode == "13"||event.keyCode<="105"||event.keyCode>="96") {
            if (tiaojian != "") {
                $("#pageNav").html("");
                getBusilogByTiaojian(tiaojian);
            } else {
                getBusilogList(1, 15);
            }
        }
    });


    $("#pageSize").on("change",function () {
        var pageSize = $("#pageSize option:selected").val();
        getBusilogList(1,pageSize);
    })


    $("#sousuo").on("click",function () {
        var pageSize = $("#pageSize option:selected").val();
        getBusilogList(1,pageSize);
    })

    /*查看日志*/
    function system_log_show(){

    }
    /*日志-删除*/
    function system_log_del(obj,id){
        layer.confirm('确认要删除吗？',function(index){
            $.ajax({
                type: 'POST',
                url: '',
                dataType: 'json',
                success: function(data){
                    $(obj).parents("tr").remove();
                    layer.msg('已删除!',{icon:1,time:1000});
                },
                error:function(data) {
                    console.log(data.msg);
                },
            });
        });
    }

    function admin_username_xiangqing(){
        layer_show('操作人详细信息','/guanliyuan/queryUserNumber.do?un_id='+$("#un_id").html(),'800','600');
    }
</script>
</body>
</html>