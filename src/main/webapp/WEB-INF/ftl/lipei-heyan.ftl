<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="/lib/html5shiv.js"></script>
  <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
  <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>理赔信息核验</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 信息核验管理 <span class="c-gray en">&gt;</span> 理赔信息核验<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
    <div class="text-c"> <#--日期范围：
        <input type="text" onfocus="WdatePicker({ maxDate:'#F{$dp.$D(\'datemax\')||\'%y-%M-%d\'}' })" id="datemin" class="input-text Wdate" style="width:120px;">
        -
        <input type="text" onfocus="WdatePicker({ minDate:'#F{$dp.$D(\'datemin\')}',maxDate:'%y-%M-%d' })" id="datemax" class="input-text Wdate" style="width:120px;">
        <input type="text" class="input-text" style="width:250px" placeholder="输入会员名称、电话、邮箱" id="" name="">
        <button type="submit" class="btn btn-success radius" id="" name=""><i class="Hui-iconfont">&#xe665;</i> 搜用户</button>-->
        <#--<h1>理赔信息核验管理</h1>-->
    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20">
        <button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button>
        <span class="r">共有数据：<strong>${count}</strong> 条</span></div>
    <div class="mt-20">
        <table class="table table-border table-bordered table-hover table-bg table-sort">
            <thead>
            <tr class="text-c">
                <th width="25"><input type="checkbox" name="" value=""></th>
                <th width="80">保险单ID</th>
                <th width="100">报案用户</th>
                <th width="100">车牌号</th>
                <th width="100">手机</th>
                <th width="150">邮箱</th>
                <th width="">地址</th>
                <th width="100">赔付金额</th>
                <th width="130">报案时间</th>
                <th width="70">状态</th>
                <th width="100">操作</th>
            </tr>
            </thead>
            <tbody>
            <#list clist as c>
            <tr class="text-c">
                <td><input type="checkbox" value="" name=""></td>
                <td>${c.cl_id}</td>
                <td>${c.users.u_name}</td>
                <td>${c.c_id}</td>
                <td>${c.users.u_phone}</td>
                <td>${c.users.u_emal}</td>
                <td class="text-l">${c.users.u_addr}</td>
                <td>${c.zonghe?string.currency}元</td>
                <td>${c.cl_addtime?string("yyyy-MM-dd HH:mm:ss")}</td>

                <#if (c.cl_state==1)>
                    <td class="td-status"><span class="label label-success radius">合格</span></td>
                <#elseif (c.cl_state==2)>
                    <td class="td-status"><span class="label label-danger radius">不合格</span></td>
                <#else>
                    <td class="td-status"><span class="label label-defaunt radius">待审核</span></td>
                </#if>

                <td class="td-manage">
                    <#if UsersPower?index_of("理赔信息核验")!=-1>
                        <#if (c.cl_state==1)>
                            <a style="text-decoration:none" <#--onclick="article_ting(this,'${users.u_id}')"--> href="javascript:;" title="合格"><i class="Hui-iconfont">&#xe6e1;</i></a>

                        <#elseif (c.cl_state==2)>
                            <a style="text-decoration:none" <#--onClick="article_qiyong(this,'${users.u_id}')"--> href="javascript:;" title="不合格"><i class="Hui-iconfont">&#xe6dd;</i></a>
                        <#else >
                            <a style="text-decoration:none" onClick="article_shenkan(this,'${c.cl_id}','${c.cl_money}')" href="javascript:;" title="待审核"><i class="Hui-iconfont">&#xe63f;</i></a>
                        </#if>
                    <#else>
                        <span class="label label-danger radius">没有权限</span>
                    </#if>

                </td>
            </tr>
            </#list>
            </tbody>
        </table>
    </div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    $(function(){
        $('.table-sort').dataTable({
            "aaSorting": [[ 1, "desc" ]],//默认第几个排序
            "bStateSave": true,//状态保存
            "aoColumnDefs": [
                //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
                {"orderable":false,"aTargets":[0,8,9]}// 制定列不参与排序
            ]
        });
    });

    function article_shenkan(obj,id,money){
        layer.confirm('案情审核？', {
                    btn: ['合格','不合格','取消'],
                    shade: false,
                    closeBtn: 0
                },
                function(){

                    $.ajax({
                        type: 'POST',
                        url: '/heyan/editClaimantstate.do',
                        data:{
                            "cl_id":id,
                            "cl_state":1,
                            "cla_money":money,

                        },
                        dataType: 'json',
                        success: function(data) {
                            /*    id = "'"+id+"'";
                                var xinxi = $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="article_no(this,'+id+')" href="javascript:;" title="通过"><i class="Hui-iconfont">&#xe6de;</i></a>');;*/

                            if (data.u==1) {

                                $(obj).parents("tr").find(".td-manage").prepend("<a style='text-decoration:none' href='javascript:;' title=''><i class='Hui-iconfont'>&#xe6e1;</i></a>");
                                $(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">合格</span>');
                                $(obj).remove();
                                layer.msg('合格!',{icon: 6,time:1000});
                            }
                        },
                        error:function(data) {
                            console.log(data.msg);
                        },
                    });
                },
                function(){
                    $.ajax({
                        type: 'POST',
                        url: '/heyan/editClaimantstate.do',
                        data:{
                            "cl_id":id,
                            "cl_state":2,
                            "cla_money":0,
                        },
                        dataType: 'json',
                        success: function(data) {

                            //alert(id)
                            if (data.u==1) {
                                /* id = "'"+id+"'";
                                 var xinxi = $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="article_yeas(this,'+id+')" href="javascript:;" title="通过"><i class="Hui-iconfont">&#xe603;</i></a>');;
 */
                                $(obj).parents("tr").find(".td-manage").prepend("<a style='text-decoration:none' href='javascript:;' title=''><i class='Hui-iconfont'>&#xe6dd;</i></a>");
                                $(obj).parents("tr").find(".td-status").html('<span class="label label-danger radius">不合格</span>');
                                $(obj).remove();
                                layer.msg('不合格!', {icon: 5, time: 1000});
                            }
                        },
                        error:function(data) {
                            console.log(data.msg);
                        },
                    });
                }

        );
    }






</script>
</body>
</html>