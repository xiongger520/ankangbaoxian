<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/lib/html5shiv.js"></script>
    <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
    <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>角色管理</title>
</head>
<body>
<nav class="breadcrumb">
    <i class="Hui-iconfont">&#xe67f;</i> 首页
    <span class="c-gray en">&gt;</span> 管理员管理
    <span class="c-gray en">&gt;</span> 管理员列表
    <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" >
        <i class="Hui-iconfont">&#xe68f;</i>
    </a>
</nav>
<div class="page-container">
    <div class="cl pd-5 bg-1 bk-gray  mt-20"  style="margin-bottom: 20px">

        <span class="l">
            <#--<#if UsersPower?index_of("批量删除管理员")!=-1>
                <a href="javascript:;" onclick="datadel()" class="btn btn-danger radius">
                    <i class="Hui-iconfont">&#xe6e2;</i> 批量删除
                </a>
            </#if>-->
                <a href="javascript:;" onclick="removeIframe()" class="btn btn-primary radius">
                <#--<i class="Hui-iconfont">&#xe6e2;</i>-->
                    关闭选项卡</a>
            <#if UsersPower?index_of("添加管理员")!=-1>
                <a class="btn btn-primary radius" href="javascript:;" onclick="admin_role_add('添加管理员','/guanliyuan/toadmin-add.do','800','600')">
                    <i class="Hui-iconfont">&#xe600;</i> 添加管理员
                </a>
            </#if>
        </span>
        <#--<button onclick="removeIframe()" class="btn btn-primary radius" style="line-height:1.6em;margin-left:10px;margin-bottom:3px ">关闭选项卡</button>-->
        <span class="r">共有数据：<strong>${count}</strong> 条</span>
    </div>

    <table class="table table-border table-bordered table-hover table-bg table-sort">
        <thead>
        <tr>
            <th scope="col" colspan="8">管理员管理</th>
        </tr>
        <tr class="text-c">
            <th width="50"><input type="checkbox" value="" name=""></th>
            <th width="80">ID</th>
            <th width="200">管理员名称</th>
            <th>昵称</th>
            <th width="250">角色名称</th>
            <th width="150">状态</th>
            <th width="100">对应用户</th>
            <th width="150">操作</th>
        </tr>
        </thead>
        <tbody>
        <#list list as list>
        <tr class="text-c">
            <#if list.un_id <= Session.UsernumberRoleUsers.un_id>
                <td><span class="label label-danger radius">  </span></td>
            <#else>
                <td><input type="checkbox" value="${list.un_id}" name="caritem"></td>
            </#if>

            <td>${list.un_id}</td>
            <td>${list.username}</td>
            <td><a href="#">${list.un_nickname}</a></td>
            <td>${list.role.r_name}</td>
            <#if list.un_state==1>
                <td class="td-status"><span class="label label-success radius">已启用</span></td>
            <#else >
                <td class="td-status"><span class="label label-defaunt radius">已停用</span></td>
            </#if>
            <td>${list.users.u_name}</td>

            <#if list.role.r_id <= Session.UsernumberRoleUsers.role.r_id>
                <td><span class="label label-danger radius">没有权限</span></td>
            <#else>
                <td class="f-14">
                    <#if UsersPower?index_of("修改管理员")!=-1>
                        <a title="编辑" href="javascript:;" onclick="admin_role_edit('管理员编辑','/guanliyuan/queryUserNumberById.do?un_id=${list.un_id}','1','800','600')" style="text-decoration:none">
                            <i class="Hui-iconfont">&#xe6df;</i>
                        </a>
                    </#if>
                    <#if UsersPower?index_of("删除管理员")!=-1>
                        <a title="删除" href="javascript:;" onclick="admin_del(this,${list.un_id})" class="ml-5" style="text-decoration:none">
                            <i class="Hui-iconfont">&#xe6e2;</i>
                        </a>
                    </#if>

                </td>
            </#if>
        </tr>
        </#list>


        </tbody>
    </table>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $('.table-sort').dataTable({
        "aaSorting": [[ 1, "desc" ]],//默认第几个排序
        "bStateSave": true,//状态保存
        "pading":false,
        "aoColumnDefs": [
            //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
            //{"orderable":false,"aTargets":[0,8]}// 不参与排序的列
        ]
    });
    /*管理员-角色-添加*/
    function admin_role_add(title,url,w,h){
        layer_show(title,url,w,h);
    }
    /*管理员-角色-编辑*/
    function admin_role_edit(title,url,id,w,h){
        layer_show(title,url,w,h);
    }
    /*管理员-角色-删除*/
    function admin_del(obj,id){
        layer.confirm('管理员删除须谨慎，确认要删除吗？',function(index){
            $.ajax({
                type: 'POST',
                url: '/guanliyuan/delUserNumber.do',
                dataType: 'json',
                data:{
                  un_id:id
                },
                success: function(data){
                    if(data.flag==1){
                        $(obj).parents("tr").remove();
                        layer.msg('已删除!',{icon:1,time:1000});
                    }else{
                        layer.msg('存在主外键关系，删除失败!',{icon:5,time:1000});
                    }
                },
                error:function(data) {
                    layer.msg('删除失败!',{icon:5,time:1000});
                    console.log(data.msg);
                },
            });
        });
    }
    var datadel = function(){

        //判断选择项是否为空
        var checkedNum = $("input[name='caritem']:checked").length;
        if(checkedNum==0){
            layer.msg('请选中要删除的列表项!',{icon: 5,time:1000});
            return;
        }
        //获取选择项的值
        var checkedVal = [];
        var index=-1;
        $("input[name='caritem']:checked").each(function(){
            index++;
            checkedVal[index]=$(this).val();
        });

        var itemsTipStr = "这条记录";
        if(checkedNum>1){
            itemsTipStr = "这些选项";
        }

        // alert(JSON.stringify(checkedVal))
        layer.confirm('确认要删除'+itemsTipStr+'吗？',function(){
            layer.closeAll('dialog');

            //此处请求后台程序，下方是成功后的前台处理……
            $.ajax({
                type: "POST",
                url: "/guanliyuan/delUserNumbers.do",
                data: JSON.stringify(checkedVal),
                contentType: 'application/json',
                success: function(msg){
                    var index=0;
                    $("input[name='caritem']:checked").each(function(){
                        // alert($(this).val()==inde[index])
                        if($(this).val()==checkedVal[index]){
                            $(this).parents("tr").remove();
                            index++;
                        }
                    });
                    layer.msg('已删除!',{icon: 1,time:1000});

                },
                error:function(){
                    layer.msg('其中存在投保车辆，不能删除',{icon: 6,time:1000});
                }
            });
        });
    }
</script>
</body>
</html>