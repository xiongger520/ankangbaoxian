<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="/lib/html5shiv.js"></script>
  <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
  <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>车辆信息管理</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 理赔管理 <span class="c-gray en">&gt;</span> 报案单管理 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
    <div class="cl pd-5 bg-1 bk-gray mt-20"><button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button> <#--<span class="r">共有数据：<strong>88</strong> 条</span> --></div>
    <div class="mt-20">
        <table class="table table-border table-bordered table-hover table-bg table-sort">
            <thead>
            <tr class="text-c">
                <th width="80">报案单编号</th>
                <th width="100">报案车辆</th>
                <th width="40">报案用户</th>
                <th width="80">保险名称</th>
                <th width="40">责任比率</th>
                <th width="80">审核状态</th>

                <th width="130">添加日期</th>

                <th width="40">是否结算</th>
                <th width="80"> 应理赔金额</th>
                <th width="80"> 详细单据</th>
            </tr>
            </thead>
            <tbody>
            <#list claimantlist as cl>
            <tr class="text-c">
                <td>${cl.cl_id}</td>
                <td>${cl.c_id}</td>
                <td>${cl.users.u_name}</td>
                <td>${cl.carInsur.ci_name}</td>
                <td>${cl.cl_type}</td>
                <#if (cl.cl_state==0)>
                    <td class="td-status"><span class="label label-defaunt radius">待审核</span></td>
                <#elseif (cl.cl_state==1)>
                    <td class="td-status"><span class="label label-success radius">合格</span></td>
                <#else>
                    <td class="td-status"><span class="label label-danger radius">不合格</span></td>
                </#if>

                <td>${cl.cl_addtime?string("yyyy-MM-dd HH:mm:ss")}</td>

                <#if (cl.cl_jiesuan==0)>
                    <td class="td-status"><span class="label label-defaunt radius">未结算</span></td>
                <#elseif (cl.cl_jiesuan==1)>
                    <td class="td-status"><span class="label label-success radius">已结算</span></td>
                </#if>
                <#--<td class="td-status">${cl.cl_money!"尚未核算"}</td>-->
                <#if (cl.zonghe!=null)>
                    <td class="td-status">￥${cl.zonghe}元</td>
                 <#elseif (cl.zonghe==null)>
                    <td class="td-status"><span class="label label-success radius">尚未核算</span></td>
                </#if>
                <#--<button type="button" name="msgtable"><a href="/lipei/downloadMsgTable.do">${cl.msg_table}</a></button>-->
                <#if UsersPower?index_of("报案单导出")!=-1>
                    <td class="f-14 td-manage">
                        <a style="text-decoration:none" class="ml-5" href="/lipei/downloadMsgTable.do?msgtable=${cl.msg_table}" title="导出"><i class="Hui-iconfont">&#xe644;</i></a>
                    </td>
                <#else >
                    <td><span class="label label-danger radius">没有权限</span></td>
                </#if>
            </tr>
            </#list>
        </tbody>
        </table>

    </div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    $(function(){
        $('.table-sort').dataTable({
            "aaSorting": [[ 1, "desc" ]],//默认第几个排序
            "bStateSave": true,//状态保存
            "aoColumnDefs": [
                //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
               /* {"orderable":false,"aTargets":[0,8,9]}// 制定列不参与排序*/
            ]
        });

    });
</script>
</body>
</html>