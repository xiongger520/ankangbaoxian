<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="Bookmark" href="/favicon.ico" >
    <link rel="Shortcut Icon" href="/favicon.ico" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/lib/html5shiv.js"></script>
    <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
    <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>中国安康保险</title>
    <meta name="keywords" content="H-ui.admin v3.1,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
    <meta name="description" content="H-ui.admin v3.1，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>
<body>

<header class="navbar-wrapper">

    <div class="navbar navbar-fixed-top">
        <div class="container-fluid cl"> <a class="logo navbar-logo f-l mr-10 hidden-xs" href="/aboutHui.shtml">安康保险管理系统</a> <a class="logo navbar-logo-m f-l mr-10 visible-xs" href="/aboutHui.shtml">H-ui</a>
            <span class="logo navbar-slogan f-l mr-10 hidden-xs">v1.0</span>
            <a aria-hidden="false" class="nav-toggle Hui-iconfont visible-xs" href="javascript:;">&#xe667;</a>
            <nav class="nav navbar-nav">
                <ul class="cl">
                    <li class="dropDown dropDown_hover"><a href="javascript:;" class="dropDown_A"><i class="Hui-iconfont">&#xe600;</i> 新增 <i class="Hui-iconfont">&#xe6d5;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                        <#if UsersPower?index_of("添加保险")!=-1>
                            <li><a href="javascript:;" onclick="article_add(' 添加保险种类','/baoxian/tobaoxian-add.do')"><i class="Hui-iconfont">&#xe72d;</i> 添加保险</a></li>
                        </#if>
                        <#if UsersPower?index_of("保险套餐推出")!=-1>
                            <li><a href="javascript:;" onclick="picture_add(' 推出保险套餐','/yingxiao/toyingxiao-add.do')"><i class="Hui-iconfont">&#xe616;</i> 推出保险套餐推出</a></li>
                        </#if>
                        <#if UsersPower?index_of("添加用户")!=-1>
                            <li><a href="javascript:;" onclick="member_add(' 添加新用户','/yonghu/tomember-add.do','','510')"><i class="Hui-iconfont">&#xe60d;</i> 添加用户</a></li>
                        </#if>
                        <#if UsersPower?index_of("添加管理员")!=-1>
                            <li><a href="javascript:;" onclick="member_add(' 添加管理员','/guanliyuan/toadmin-add.do','','510')"><i class="Hui-iconfont">&#xe62c;</i> 添加管理员</a></li>
                        </#if>
                        </ul>
                    </li>
                </ul>
            </nav>
            <nav id="Hui-userbar" class="nav navbar-nav navbar-userbar hidden-xs">
                <ul class="cl">

                    <li >
                        <p>当前时间：<label id="aa" />
                            <script type="text/javascript">
                                setInterval("aa.innerHTML=new Date().toLocaleString()+' 星期'+'日一二三四五六'.charAt(new Date().getDay());",1000);
                            </script>
                        </p>
                    </li>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <li>会员等级：${UsersMember.m_name}</li>
                    <li>&nbsp;&nbsp;角色名称：${role.r_name}</li>                    <li class="dropDown dropDown_hover">
                        <a href="#" class="dropDown_A">${UsernumberRoleUsers.un_nickname} <i class="Hui-iconfont">&#xe6d5;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li><a href="javascript:;" onClick="myselfinfo()">个人信息</a></li>
                            <li><a href="/user/logout.do">安全退出</a></li>

                        </ul>
                    </li>
                    <!--<li id="Hui-msg"> <a href="#" title="消息"><span class="badge badge-danger">1</span><i class="Hui-iconfont" style="font-size:18px">&#xe68a;</i></a> </li>-->
                    <li id="Hui-skin" class="dropDown right dropDown_hover"> <a href="javascript:;" class="dropDown_A" title="换肤"><i class="Hui-iconfont" style="font-size:18px">&#xe62a;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li><a href="javascript:;" data-val="default" title="默认（黑色）">默认（黑色）</a></li>
                            <li><a href="javascript:;" data-val="blue" title="蓝色">蓝色</a></li>
                            <li><a href="javascript:;" data-val="green" title="绿色">绿色</a></li>
                            <li><a href="javascript:;" data-val="red" title="红色">红色</a></li>
                            <li><a href="javascript:;" data-val="yellow" title="黄色">黄色</a></li>
                            <li><a href="javascript:;" data-val="orange" title="橙色">橙色</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<aside class="Hui-aside">
    <div class="menu_dropdown bk_2">
    <#if UsersPower?index_of("保险管理一级")!=-1>
        <dl id="menu-article">
            <dt><i class="Hui-iconfont">&#xe72d;</i> 保险管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>

                    <#if UsersPower?index_of("添加保险")!=-1>
                        <li><a data-href="/baoxian/tobaoxian-add.do" data-title="添加保险" href="javascript:void(0)">添加保险</a></li>
                    </#if>
                    <#if UsersPower?index_of("保险套餐管理")!=-1>
                        <li><a data-href="/baoxian/carinsurAll.do" data-title="保险管理" href="javascript:void(0)">保险管理</a></li>
                    </#if>
                </ul>            </dd>

        </dl>
    </#if>
    <#if UsersPower?index_of("营销管理")!=-1>
        <dl id="menu-picture">
            <dt><i class="Hui-iconfont">&#xe616;</i> 营销管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>

                    <#if UsersPower?index_of("保险套餐推出")!=-1>
                        <li><a data-href="/yingxiao/toyingxiao-add.do" data-title="保险套餐推出" href="javascript:void(0)">保险套餐推出</a></li>
                    </#if>
                    <#if UsersPower?index_of("保险套餐管理")!=-1>
                        <li><a data-href="/yingxiao/topicture-list.do" data-title="保险套餐管理" href="javascript:void(0)">保险套餐管理</a></li>
                    </#if>                </ul>

            </dd>
        </dl>
    </#if>

    <#if UsersPower?index_of("用户管理一级")!=-1>
        <dl id="menu-member">
            <dt><i class="Hui-iconfont">&#xe60d;</i> 用户管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>

                    <#if UsersPower?index_of("添加用户")!=-1>
                        <li><a data-href="/yonghu/tomember-add.do" data-title="添加用户" href="javascript:;">添加用户</a></li>
                    </#if>
                    <#if UsersPower?index_of("添加车辆")!=-1>
                        <li><a data-href="/yonghu/tocar-add.do" data-title="添加车辆" href="javascript:;">添加车辆</a></li>
                    </#if>
                    <#if UsersPower?index_of("用户管理二级")!=-1>
                        <li><a data-href="/yonghu1/tomember-list.do" data-title="用户管理" href="javascript:;">用户管理</a></li>
                    </#if>
                    <#if UsersPower?index_of("用户黑名单")!=-1>
                        <li><a data-href="/yonghu1/tomember-del.do" data-title="用户黑名单" href="javascript:;">用户黑名单</a></li>
                    </#if>
                    <#if UsersPower?index_of("车辆管理")!=-1>
                        <li><a data-href="/yonghu1/tocar-list.do" data-title="车辆管理" href="javascript:;">车辆管理</a></li>
                    </#if>                </ul>

            </dd>
        </dl>
    </#if>

    <#if UsersPower?index_of("交易管理")!=-1>
        <dl id="menu-product">
            <dt><i class="Hui-iconfont">&#xe620;</i> 交易管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>

                    <#if UsersPower?index_of("保险单导入导出")!=-1>
                        <li><a data-href="/jiaoyi/tobaoxiandan-info.do" data-title="保险单导入导出" href="javascript:void(0)">保险单导入导出</a></li>
                    </#if>
                    <#if UsersPower?index_of("购买保险")!=-1>
                        <li><a data-href="/jiaoyi/tobaoxiandan-add.do" data-title="购买保险" href="javascript:void(0)">购买保险</a></li>
                    </#if>
                    <#if UsersPower?index_of("保险单信息管理")!=-1>
                        <li><a data-href="/jiaoyi/tobaoxiandan-list.do" data-title="保险单信息管理" href="javascript:void(0)">保险单信息管理</a></li>
                    </#if>
                    <#if UsersPower?index_of("保险缴费管理")!=-1>
                        <li><a data-href="/jiaoyi/tobaoxian-jiaofei.do" data-title="保险缴费" href="javascript:void(0)">保险缴费</a></li>
                    </#if>
                    <#if UsersPower?index_of("公司账单图")!=-1>
                        <li><a data-href="/jiaoyi/tobaoxiandan-bill.do" data-title="公司账单图" href="javascript:void(0)"><span class="c-red">*</span>公司账单图</a></li>
                    </#if>                </ul>

            </dd>
        </dl>
    </#if>

    <#if UsersPower?index_of("理赔管理")!=-1>
        <dl id="menu-comments">
            <dt><i class="Hui-iconfont">&#xe628;</i> 理赔管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>

                    <#if UsersPower?index_of("报案单导入导出")!=-1>
                        <li><a data-href="/lipei/tolipeidan-info.do" data-title="报案单的导入导出" href="javascript:void(0)">报案单的导入导出</a></li>
                    </#if>
                    <#if UsersPower?index_of("增加报案单")!=-1>
                        <li><a data-href="/lipei/tolipeidan-add.do" data-title="增加报案单" href="javascript:void(0)">增加报案单</a></li>
                    </#if>
                    <#if UsersPower?index_of("报案单管理")!=-1>
                        <li><a data-href="/lipei/tolipeidan-list.do" data-title="报案单管理" href="javascript:void(0)">报案单管理</a></li>
                    </#if>
                    <#if UsersPower?index_of("赔偿结算")!=-1>
                        <li><a data-href="/lipei/tolipei-jiesuan.do" data-title="赔偿结算" href="javascript:void(0)">赔偿结算</a></li>
                    </#if>
                    <#if UsersPower?index_of("赔偿记录")!=-1>
                        <li><a data-href="/lipei/tolipei-jilu.do" data-title="赔偿记录" href="javascript:void(0)">赔偿记录</a></li>
                    </#if>                </ul>

            </dd>
        </dl>
    </#if>


    <#if UsersPower?index_of("管理员管理")!=-1>
        <dl id="menu-admin">
            <dt><i class="Hui-iconfont">&#xe62d;</i> 管理员管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <#if UsersPower?index_of("权限管理")!=-1>
                        <li><a data-href="/guanliyuan/toPowerList.do" data-title="权限管理" href="javascript:void(0)">权限管理</a></li>
                    </#if>

                    <#if UsersPower?index_of("角色管理")!=-1>
                        <li><a data-href="/guanliyuan/toadmin-role.do" data-title="角色管理" href="javascript:void(0)">角色管理</a></li>
                    </#if>

                    <#if UsersPower?index_of("管理员列表")!=-1>
                        <li><a data-href="/guanliyuan/toadmin-list.do" data-title="管理员列表" href="javascript:void(0)">管理员列表</a></li>
                    </#if>
                </ul>
            </dd>
        </dl>
    </#if>

    <#if UsersPower?index_of("系统管理")!=-1>
        <dl id="menu-tongji">
            <dt><i class="Hui-iconfont">&#xe61d;</i> 系统管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <#if UsersPower?index_of("日志管理")!=-1>
                        <li><a data-href="/xitong/tosystem-log.do" data-title="日志管理" href="javascript:void(0)">日志管理</a></li>
                    </#if>
                    <#if UsersPower?index_of("管理员在线管理")!=-1>
                        <li><a data-href="/xitong/toadmin-zaixian.do" data-title="管理员在线管理" href="javascript:void(0)">管理员在线管理</a></li>
                    </#if>
                    <#if UsersPower?index_of("支点管理")!=-1>
                        <li><a data-href="/xitong/tozhidian.do" data-title="支点管理" href="javascript:void(0)">支点管理</a></li>
                    </#if>
                </ul>
            </dd>
        </dl>
    </#if>

    <#if UsersPower?index_of("用户服务")!=-1>
        <dl id="menu-system">
            <dt><i class="Hui-iconfont">&#xe72b;</i> 用户服务<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>

                    <#if UsersPower?index_of("个人信息查看")!=-1>
                        <li><a data-href="/fuwu/touser-msg.do" data-title="个人信息" href="javascript:void(0)">个人信息</a></li>
                    </#if>
                    <#if UsersPower?index_of("报案信息查询")!=-1>
                        <li><a data-href="/fuwu/tobaoanxinxi-list.do" data-title="报案信息查询" href="javascript:void(0)">报案信息查询</a></li>
                    </#if>
                    <#if UsersPower?index_of("退保申请")!=-1>
                        <li><a data-href="/fuwu/totuibao-list.do" data-title="退保申请" href="javascript:void(0)">退保申请</a></li>
                    </#if>
                    <#if UsersPower?index_of("在线保险计算器")!=-1>
                        <li><a data-href="/fuwu/cheXian.do" data-title="在线保险计算器" href="javascript:void(0)">在线保险计算器</a></li>
                    </#if>
                    <#if UsersPower?index_of("支点查询")!=-1>
                        <li><a data-href="/fuwu/tomap.do" data-title="支点查询" href="javascript:void(0)">支点查询</a></li>
                    </#if>
                <#--<li><a data-href="/fuwu/toxubao-list.do" data-title="续保申请" href="javascript:void(0)">续保申请</a></li>-->                </ul>

            </dd>
        </dl>
    </#if>


    <#if UsersPower?index_of("信息核验管理")!=-1>        <dl id="menu-system">
            <dt><i class="Hui-iconfont">&#xe62e;</i> 信息核验管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <#if UsersPower?index_of("车辆信息核验")!=-1>
                        <li><a data-href="/heyan/tocar-heyan.do" data-title="车辆信息核验" href="javascript:void(0)">车辆信息核验</a></li>
                    </#if>
                    <#if UsersPower?index_of("用户信息核验")!=-1>
                        <li><a data-href="/heyan/touser-heyan.do" data-title="用户信息核验" href="javascript:void(0)">用户信息核验</a></li>
                    </#if>
                    <#if UsersPower?index_of("理赔信息核验")!=-1>
                        <li><a data-href="/heyan/tolipei-heyan.do" data-title="理赔信息核验" href="javascript:void(0)">理赔信息核验</a></li>
                    </#if>
                    <#if UsersPower?index_of("新增保险核验")!=-1>
                        <li><a data-href="/heyan/tobaoxian-heyan.do" data-title="新增保险核验" href="javascript:void(0)">新增保险核验</a></li>
                    </#if>
                    <#if UsersPower?index_of("保险订单核验")!=-1>
                        <li><a data-href="/heyan/tobaoxiandan-heyan.do" data-title="保险订单核验" href="javascript:void(0)">保险订单核验</a></li>
                    </#if>
                    <#if UsersPower?index_of("续保信息核验")!=-1>
                        <li><a data-href="/heyan/toxubao-heyan.do" data-title="续保信息核验" href="javascript:void(0)">续保信息核验</a></li>
                    </#if>
                    <#if UsersPower?index_of("退保信息核验")!=-1>
                        <li><a data-href="/heyan/totuibao-heyan.do" data-title="退保信息核验" href="javascript:void(0)">退保信息核验</a></li>
                    </#if>                </ul>
            </dd>
        </dl>
    </#if>
    <#if UsersPower?index_of("管理员在线")!=-1>
        <dl id="menu-admin">
            <dt><i class="Hui-iconfont">&#xe62d;</i>管理员在线<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>            <dd>
                <ul>
                    <#if UsersPower?index_of("管理员在线")!=-1>
                        <li><a data-href="/index.jsp" data-title="业务交流" href="javascript:void(0)">业务交流</a></li>
                    </#if>                </ul>

            </dd>
        </dl>
    </#if>
    </div>
</aside>
<div class="dislpayArrow hidden-xs"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a></div>
<section class="Hui-article-box">
    <div id="Hui-tabNav" class="Hui-tabNav hidden-xs">
        <div class="Hui-tabNav-wp">
            <ul id="min_title_list" class="acrossTab cl">
                <li class="active">
                    <span title="我的桌面" data-href="welcome.html">我的桌面</span>
                    <em></em></li>
            </ul>
        </div>
        <div class="Hui-tabNav-more btn-group"><a id="js-tabNav-prev" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d4;</i></a><a id="js-tabNav-next" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d7;</i></a></div>
    </div>
    <div id="iframe_box" class="Hui-article">
        <div class="show_iframe">
            <div style="display:none" class="loading"></div>
            <iframe scrolling="yes" frameborder="0" src="/index/towelcome.do"></iframe>
        </div>
    </div>
</section>

<div class="contextMenu" id="Huiadminmenu">
    <ul>
        <li id="closethis">关闭当前 </li>
        <li id="closeall">关闭全部 </li>
    </ul>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/jquery.contextmenu/jquery.contextmenu.r2.js"></script>
<script type="text/javascript">
    $(function(){
        if('${UsernumberRoleUsers.username}'==''){
            top.location = "/login.jsp";
        }else{
            if('${UsernumberRoleUsers.password}'=='${bijiaomima}'){
                //alert(1)
                myselfinfo1()
            }
        }



        /*$("#min_title_list li").contextMenu('Huiadminmenu', {
            bindings: {
                'closethis': function(t) {
                    console.log(t);
                    if(t.find("i")){
                        t.find("i").trigger("click");
                    }
                },
                'closeall': function(t) {
                    alert('Trigger was '+t.id+'\nAction was EmailUtil');
                },
            }
        });*/
    });

    var wsServer = null;
    var ws = null;
    wsServer = "ws://" + location.host+"${pageContext.request.contextPath}" + "/chatServer";
    ws = new WebSocket(wsServer); //创建WebSocket对象
    ws.onopen = function (evt) {
        layer.msg("已经建立连接", { offset: 0});
    };
    ws.onmessage = function (evt) {
        analysisMessage(evt.data);  //解析后台传回的消息,并予以展示
    };
    ws.onerror = function (evt) {
        layer.msg("产生异常", { offset: 0});
    };
    ws.onclose = function (evt) {
        layer.msg("已经关闭连接", { offset: 0});
    };

    /**
     * 连接
     */
    function getConnection(){
        if(ws == null){
            ws = new WebSocket(wsServer); //创建WebSocket对象
            ws.onopen = function (evt) {
                layer.msg("成功建立连接!", { offset: 0});
            };
            ws.onmessage = function (evt) {

                analysisMessage(evt.data);  //解析后台传回的消息,并予以展示
            };
            ws.onerror = function (evt) {
                layer.msg("产生异常", { offset: 0});
            };
            ws.onclose = function (evt) {
                layer.msg("已经关闭连接", { offset: 0});
            };
        }else{
            layer.msg("连接已存在!", { offset: 0, shift: 6 });
        }
    }

    function analysisMessage(message){
        message = JSON.parse(message);

        if(message.type == "xiaxian"){      //下线提示

            xiaxian(message.message);
        }

    }

    function xiaxian(message){
        alert(message.content);
        $.ajax({
            type: "POST", // 用POST方式传输
            dataType: "text", // 数据格式:JSON
            url: "/user/logout.do", // 目标地址
            data: "flag=2",
            success: function (data){
                //alert("已经关闭")
                top.location = "/login.jsp";
            }
        });

    }
    /*个人信息*/
    function myselfinfo1(){
        layer.open({
            type: 2,
            title: '编辑信息',
            fix: false, //不固定
            maxmin: true,
            shadeClose : false,
            closeBtn: 0,
            shade:0.4,
            area: ['500px','400px'],
            content: '/jiaoyi/toEditpwd.do',

        });
    }

    /*个人信息   修改密码*/
    function myselfinfo(){
        layer.open({
            type: 2,
            title: '编辑信息',
            fix: false, //不固定
            maxmin: true,
            shadeClose : true,
            shade:0.4,
            area: ['500px','400px'],
            content: '/jiaoyi/toEditpwd.do',

        });
    }

    /*资讯-添加*/
    function article_add(title,url){
        var index = layer.open({
            type: 2,
            title: title,
            content: url
        });
        layer.full(index);
    }
    /*图片-添加*/
    function picture_add(title,url){
        var index = layer.open({
            type: 2,
            title: title,
            content: url
        });
        layer.full(index);
    }
    /*产品-添加*/
    function product_add(title,url){
        var index = layer.open({
            type: 2,
            title: title,
            content: url
        });
        layer.full(index);
    }
    /*用户-添加*/
    function member_add(title,url,w,h){
        layer_show(title,url,w,h);
    }


</script>

<!--此乃百度统计代码，请自行删除-->

</body>
</html>