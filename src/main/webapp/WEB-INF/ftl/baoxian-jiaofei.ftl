<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="/lib/html5shiv.js"></script>
  <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
  <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>删除的用户</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 保险缴费 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>

<div class="page-container">
    <div class="text-c">

    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20"><button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button>  </div>
   <#-- <div class="text-c"> 日期范围：
        <input type="text" onfocus="WdatePicker({ maxDate:'#F{$dp.$D(\'datemax\')||\'%y-%M-%d\'}' })" id="datemin" class="input-text Wdate" style="width:120px;">
        -
        <input type="text" onfocus="WdatePicker({ minDate:'#F{$dp.$D(\'datemin\')}',maxDate:'%y-%M-%d' })" id="datemax" class="input-text Wdate" style="width:120px;">
        <input type="text" class="input-text" style="width:250px" placeholder="输入会员名称、电话、邮箱" id="" name="">
        <button type="submit" class="btn btn-success radius" id="" name=""><i class="Hui-iconfont">&#xe665;</i> 搜用户</button>
    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l"><a href="javascript:;" onclick="datadel()" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a> </span> <span class="r">共有数据：<strong>88</strong> 条</span> </div>-->
    <div class="mt-20">
        <table class="table table-border table-bordered table-hover table-bg table-sort">
            <thead>
                <tr class="text-c">
                    <th width="25"><input type="checkbox" name="" value="" disabled="disabled"></th>
                    <th width="100">车辆编号</th>
                    <th width="100">车主</th>
                    <th width="">购买保险</th>
                    <th width="100">应交金额</th>
                    <th width="150">添加时间</th>
                    <th width="150">有效时间</th>
                    <th width="130">工作人员</th>
                    <th width="70">状态</th>
                    <th width="100">操作</th>
                </tr>
            </thead>
            <tbody>
            <#list list as insurContract>
                <tr class="text-c">
                    <td><input type="checkbox" value="1" name="" disabled="disabled"></td>
                    <td>${insurContract.cars.c_id}</td>
                    <td>${insurContract.cars.users.u_name}</td>
                    <td>${insurContract.carInsur.ci_name}</td>
                    <td>${insurContract.ic_money}</td>
                    <td>${insurContract.ic_addtime?string('yyyy-MM-dd HH:mm:ss')}</td>
                    <td>${insurContract.ic_totime?string('yyyy-MM-dd HH:mm:ss')}</td>
                    <td>${insurContract.username}</td>
                    <#if (insurContract.ic_ispayment=1)>
                        <#if (insurContract.ic_state==4)>
                                <td class="td-status"><span class="label label-danger radius">未交续保费</span></td>
                            <#else>
                                <td class="td-status"><span class="label label-success radius">已缴费</span></td>
                        </#if>

                    <#else>
                        <td class="td-status"><span class="label label-defaunt radius">未交费</span></td>
                    </#if>
                    <#--<td class="td-manage"><a style="text-decoration:none" href="javascript:;" onClick="member_huanyuan(this,'1')" title="还原"><i class="Hui-iconfont">&#xe672;</i></a> <a title="删除" href="javascript:;" onclick="member_del(this,'1')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>-->
                        <td class="td-manage">
                            <#if UsersPower?index_of("保险缴费")!=-1>
                                <#if (insurContract.ic_ispayment=1)>
                                        <#if (insurContract.ic_state==4)>

                                            <a style="text-decoration:none" onclick="member_start(this,'${insurContract.ic_id}','${insurContract.ic_money}','${insurContract.cars.users.u_id}','${insurContract.cars.users.u_phone}','${insurContract.cars.users.u_emal}')" href="javascript:;" title="缴纳续保费"><i class="Hui-iconfont">&#xe672;</i></a>

                                        <#else>
                                            <a style="text-decoration:none"  href="javascript:;" title="已缴费"><i class="Hui-iconfont">&#xe673</i></a>
                                        </#if>
                                    <#else >
                                        <a style="text-decoration:none" onclick="member_start(this,'${insurContract.ic_id}','${insurContract.ic_money}','${insurContract.cars.users.u_id}','${insurContract.cars.users.u_phone}','${insurContract.cars.users.u_emal}')" href="javascript:;" title="未缴费"><i class="Hui-iconfont">&#xe672;</i></a>
                                    </#if>
                            <#else>
                                <span class="label label-danger radius">没有权限</span>
                            </#if>
                        </td>
                </tr>
             </#list>
            </tbody>
        </table>
    </div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    $(function(){
        $('.table-sort').dataTable({
            "aaSorting": [[ 1, "desc" ]],//默认第几个排序
            "bStateSave": true,//状态保存
            "aoColumnDefs": [
                //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
                {"orderable":false,"aTargets":[0,8,9]}// 制定列不参与排序
            ]
        });
    });

   /*用户-缴费*/
    function member_start(obj,id,money,u_id,u_phone,u_emal){
        layer.confirm('请认真检查个人信息是否正确！',function(index){
            $.ajax({
                type:"post",
                url:"/jiaoyi/editJiaoFeiNumber.do",
                dataType:'json',
                data:{
                    "ic_id":id,
                    "ic_ispayment":1,
                    "ic_money":money,
                    "u_id":u_id,
                    "u_phone":u_phone,
                    "u_emal":u_emal,
                },success:function (date) {
                    $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none"  href="javascript:;" title="已缴费"><i class="Hui-iconfont">&#xe673</i></a>');
                    $(obj).parents("tr").find(".td-status").html('<span class="label label-defaunt radius" style="background-color: #00B83F">已缴费</span>');;
                    $(obj).remove();
                    layer.msg('已缴费!',{icon: 6,time:1000});

                },error:function () {
                    alert("缴费失败");
                }
            })
        });
    }



    /*页面自带的*/
    /*用户-删除*/
    function member_del(obj,id){
        layer.confirm('确认要删除吗？',function(index){
            $.ajax({
                type: 'POST',
                url: '',
                dataType: 'json',
                success: function(data){
                    $(obj).parents("tr").remove();
                    layer.msg('已删除!',{icon:1,time:1000});
                },
                error:function(data) {
                    console.log(data.msg);
                },
            });
        });
    }
    /*用户-还原*/
    function member_huanyuan(obj,id){
        layer.confirm('请认真检查信息后再下单',function(index){

            $(obj).remove();
            layer.msg('已还原!',{icon: 6,time:1000});
        });
    }
</script>
</body>
</html>