<#--
这是自己建的freemarker
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="/lib/html5shiv.js"></script>
  <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
  <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <link rel="stylesheet" href="/layui/css/layui.css" type="text/css">
    <script type="text/javascript" src="/layui/layui.all.js"></script>
    <script type="text/javascript" src="/layui/layui.js"></script>
    <!-- <script type="text/javascript" src="/shiJianZhoulib/jquery/1.9.1/jquery.min.js"></script> -->
    <title>删除的用户</title>
</head>
<style>

    .adtext {
        position: absolute;
        height: 90px;
        left: 214px;
        bottom: -40px;
    }

    .adtext p {
        display: block;
        float: left;
        width: 90px;
        height: 90px;
        background: url(/temp/yonghu/icons.png) no-repeat 0 0;
        margin-right: 20px;
        font-size: 36px;
        color: #f63;
        text-align: center;

    }
    .adtext p span {
        display: block;
        font-size: 14px;
        line-height: 38px;
    }
    }

    .topbg {
        height: 99px;
        width: 100%;
        border-top: solid 2px #7d7d79;
        /* background: url(/shiJianZhouimages/topbg.jpg) no-repeat 50% 0; */
        background-color:red;
        position: relative;
        margin-bottom: -76px;
    }



    .mainTit {
        height: 100px;
        font: 28px/90px "Microsoft Yahei";
        color: #333;
        position: relative;
        padding: 0 0 12px 80px;
        background: url(/temp/yonghu/mainct_tit.png) #fff no-repeat 0 100%;
        width: 917px;
        margin-left: auto;
        margin-right: auto;
    }

    .mainTit i {
        display: block;
        width: 44px;
        height: 44px;
        background: url(/temp/yonghu/icons.png) no-repeat -90px 0;
        position: absolute;
        left: 20px;
        top: 30px;
    }

    .mainTit span {
        font-size: 25px;
        position: relative;
        top: 5px;
    }

    span.prc {
        margin-left: 5px;
        color: #f63;
        font-size: 34px;
    }

    * {
        font-family: "Microsoft Yahei";
    }

    .mainTit a {
        display: block;
        position: absolute;
        right: 14px;
        top: 14px;
        width: 208px;
        height: 67px;
        font: 33px/70px "Microsoft Yahei";
        color: #fff;
        text-align: center;
        background: #f63;
    }

    .mainCt {
        min-height: 275px;
        zoom: 1;
        padding: 32px 325px 80px 75px;
        background: url(/temp/yonghu/mainct_bg.png) #e0e0e0 no-repeat 100% 100%;
        position: relative;
        overflow: visible;
        width: 597px;
        margin-left: auto;
        margin-right: auto;
    }

    .mainCt .bxlist {
        font-size: 32px;
        color: #333;
        padding-left: 10px;
    }





    element.style {
        width: 154px;
    }
    html .pa_ui_dropselect, html .pa_ui_dropselect_input_Container, html .pa_ui_dropselect_input, html .pa_ui_dropselect_button {
        height: 38px;
        line-height: 38px;
    }
    html .pa_ui_dropselect_input_Container, html .pa_ui_dropselect_input {
        height: 24px;
        line-height: 24px;
        background-color: #FFF;
        font-size: 12px;
    }

    .pa_ui_dropselect_input_Container {
        overflow: hidden;
    }
    .pa_ui_dropselect_input_Container, .pa_ui_dropselect_input {
        float: left;
        height: 22px;
        line-height: 22px;
        border: 0;
        margin: 0;
        text-indent: .5em;
    }





    element.style {
        width: 154px;
    }

    .pa_ui_dropselect_input,  a.pa_ui_dropselect_item_link {
        font: 18px/36px "Microsoft Yahei";
        color: #666;
    }


    html .pa_ui_dropselect, html .pa_ui_dropselect_input_Container, html .pa_ui_dropselect_input, html .pa_ui_dropselect_button {
        height: 38px;
        line-height: 38px;
    }

    html .pa_ui_dropselect_input {
        background-color: #FFF;
    }

    html .pa_ui_dropselect_input_Container, html .pa_ui_dropselect_input {
        height: 24px;
        line-height: 24px;
        background-color: #FFF;
        font-size: 12px;
    }

    .pa_ui_dropselect_input_Container, .pa_ui_dropselect_input {
        float: left;
        height: 22px;
        line-height: 22px;
        border: 0;
        margin: 0;
        text-indent: .5em;
    }

    .pa_ui_dropselect_button {
        background-position: right -38px;
        width: 38px;
        background-image: url(/temp/yonghu/arrow.png);
    }


    .pa_ui_dropselect,.pa_ui_dropselect_input_Container,.pa_ui_dropselect_input,.pa_ui_dropselect_button {
        height: 38px;
        line-height: 38px;
    }

    .pa_ui_dropselect_button {
        background-position: right -30px;
        width: 20px;
        height: 24px;
        line-height: 24px;
    }

    .date_input,.pa_ui_dropselect_button {
        background-image: url(/temp/yonghu/paui.gif);
        background-color: #FFF;
    }

    .pa_ui_dropselect_button {
        float: left;
        width: 16px;
        height: 22px;
        overflow: hidden;
        line-height: 22px;
        display: inline-block;
        cursor: pointer;
    }

    .c {
        zoom: 1;
    }

    .pkgDetails_ul {
        margin: 10px 0 15px;
        padding-bottom: 15px;
    }

    ol, ul, li {
        list-style: none;
    }

    .pkgDetails_ul li {
        float: left;
        width: 250px;
        height: 35px;
        padding-left: 10px;
        font: 20px/35px "Microsoft Yahei";
        color: #666;

    }

    .last{
        width: 500px;
    }

    .mainCt .poptip {
        width: 168px;
        font-size: 14px;
        color: #666;
        text-align: center;
        position: absolute;
        bottom: 309px;
        line-height: 26px;
        left: 789px;
    }
</style>
<body>


<div class ="topbg">
    <img src="/temp/yonghu/topbg.jpg">
</div>
<form id="biaodan" action="/fuwu/cheXianjs.do" method="post">
    <input type="hidden" value="${jiage}" name="jiage" />
<div id="quoteResult" class="wrap" style="position: relative;top: -48px;">
    <div class="mainTit"><i></i>安康为您测算出的简易报价为：
        <span id="totalPremium" class="prc"><b>￥</b>
					<img style="vertical-align: middle; display: none;" src="" alt="数据加载中">
        <#list list as li>
          <#if li.ci_name="机动车交通事故责任强制保险">
              <b>${li.ci_money+shangye}</b>
          </#if>
        </#list>

            <b>.</b>00起</span>
        <a href="javascript:;" style="color:#fff !important;" id="btnNext" onclick="jisuan()" otitle="快速报价" otype="button">快速报价</a>

    </div>
    <div class="mainCt">
        <div class="bxlist">
            <div class="layui-form-item">
                <label class="layui-form-label" style="    width: 100px;font-size: 33px;">商业险</label>
                <div class="layui-input-inline">
                    <select name="fangan" lay-verify="required" id="fangan" style="width: 179px;height: 42px;font-size: 20px;">
                        <option value="1">基本方案</option>
                        <option value="2">性价比高方案</option>
                        <option value="3">周全保障方案</option>

                    </select>
                </div>
                <span id="bizPremium" class="prc">
							<b>¥</b>
							<img style="vertical-align: middle; display: none;" src="//img1.4008000000.com/app_series/shop/car/common/images/loading.gif" alt="数据加载中">
							<b>${shangye}</b>
							<b>.</b>00</span>
            </div>

        </div>
        <ul id="pkgDetails" class="c pkgDetails_ul">
            <#list list as li>
              <#if li.ci_name!="机动车交通事故责任强制保险">
                  <li>${li.ci_name}</li>
              </#if>
            </#list>
           <#-- <li>车损</li>
            <li>三者(100万)</li>
            <li>盗抢</li>
            <li>司机(2万/座)</li>
            <li>乘客(2万/座)</li>
            <li>玻璃(国产玻璃)</li>
            <li>划痕(2000元)</li>
            <li>车损无法找到第三方</li>
            <li >不计免赔</li>-->
        </ul>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>

        <div class="bxlist">交强险：<span id="forcePremium" class="prc"><b>¥</b><img style="vertical-align: middle; display: none;" src="" alt="数据加载中">
            <#list list as li>
              <#if li.ci_name="机动车交通事故责任强制保险">
                <b>${li.ci_money}</b>
              </#if>
            </#list>
            <b>.</b>00</span>
            <a href="javascript:var bd_wcxgpy;" class="wcxgpy" otitle="未出险更便宜" otype="button"><i></i>未出险更便宜</a>
        </div>
        <div class="wcxgpy_pop" style="top: 210px; "><i><em></em></i>
            1. 上一个年度未发生有责任道路交通事故-10%<br>
            2 .上两个年度未发生有责任道路交通事故-20%&nbsp;<br>
            3 .上三个及以上年度未发生有责任道路交通事故-30%<br>
            4 .上一个年度发生一次有责任不涉及死亡的道路交通事故0%<br>
            5 .上一个年度发生两次及两次以上有责任道路交通事故10%&nbsp;<br>
            6 .上一个年度发生有责任道路交通死亡事故30%
        </div>
        <div class="tips">小贴士：该套餐是根据您提供简单资料计算出来的，但受您车型、往年出险次数等因素影响，与最终价格存在一定误差，仅供参考。</div>
        <div class="poptip">您和最高<b>1000元礼包</b><br>只差一个精准报价的距离！</div>
        <div class="adtext">
            <p>快<span>49秒出单</span></p>
            <p>易<span>投保仅3步</span></p>
            <p>准<span>报价更清晰</span></p>
            <p>省<span>官网去中介</span></p>
            <p>赚<span>月月领好礼</span></p>
        </div>
    </div>
</div>
</form>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    var jisuan=function(){
        /*alert($("#fangan").val())*/
        $("#biaodan").submit();
    }
</script>
</body>
</html>