<!DOCTYPE HTML>
<html>
<head>
    <style type="text/css">
        .li {width:1300px;height: 1500px;border:1px solid transparent; margin-left:0px;}

    </style>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="Bookmark" href="/favicon.ico" >
    <link rel="Shortcut Icon" href="/favicon.ico" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/lib/html5shiv.js"></script>
    <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
    <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <!--/meta 作为公共模版分离出去-->
    <title>新增图片</title>
    <link href="/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet" type="text/css" />
    <title>添加用户 - H-ui.admin v3.1</title>
    <meta name="keywords" content="H-ui.admin v3.1,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
    <meta name="description" content="H-ui.admin v3.1，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 用户管理 <span class="c-gray en">&gt;</span> 添加车辆<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
    <div class="text-c">

    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20"><button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button>  </div>
    <article class="page-container">
        <form action="/yonghu1/fileUpload.do" method="post" class="form form-horizontal" id="form-member-add" enctype="multipart/form-data">


            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>用户姓名：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="" placeholder="" id="uname" name="u_name">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>联系方式：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" onblur="jianyanuser()" value="" placeholder="" id="no" name="u_phone">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>车辆号码：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" onblur="jianyan(this)" class="input-text" placeholder="" name="c_id" id="ma">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>车辆颜色：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="" placeholder="" id="yan" name="c_color">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>购买日期：</label>
                <div class="formControls col-xs-8 col-sm-9">

                    <input type="text" value="" name="time" onfocus="WdatePicker({ maxDate:'#F{$dp.$D(\'datemax\')||\'%y-%M-%d\'}' })" id="datemin" name="datemin" class="input-text Wdate" style="width:120px;">
                    <input type="text" name="11" onfocus="WdatePicker({ minDate:'#F{$dp.$D(\'datemin\')}',maxDate:'%y-%M-%d' })" id="datemax" class="input-text Wdate" style="width:120px;display:none;">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>发动机型号：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="" placeholder="" id="xing" name="c_enginenum">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>车辆类型：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="" placeholder="例如：SUV,轿车..." id="lei" name="c_type">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>车辆品牌：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="" placeholder="" id="pin" name="c_brand">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>品牌型号：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="" placeholder="" id="pai" name="c_brandtype">
                </div>
            </div>

            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2">车辆照片：</label>
                <div class="formControls col-xs-8 col-sm-9"> <span class="btn-upload form-group">
				<input class="input-text upload-url" type="text" name="uploadfile" id="uploadfile" readonly  nullmsg="请添加附件！" style="width:200px">
				<a href="javascript:void();" class="btn btn-primary radius upload-btn"><i class="Hui-iconfont">&#xe642;</i> 浏览文件</a>
				<input type="file" multiple  name="file" class="input-file">
				</span> </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>档位类型：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <span class="select-box">
                            <select name="c_isauto" class="select">
                                <option value="0">自动</option>
                                <option value="1">手动</option>

                            </select>
				     </span>
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>排量大小：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="" placeholder="" id="liang" name="c_enginesize">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>座位数量：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="" placeholder="" id="zuo" name="seatnum">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>车辆估价：</label>


                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="" placeholder="" id="jia" name="c_price">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>评估人员：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="" placeholder="" id="gu" name="c_evaluater">
                </div>
            </div>


            <div class="row cl">
                <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
                    <input class="btn btn-primary radius" id="tijiao" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
                    <input class="btn btn-primary radius" type="reset" value="&nbsp;&nbsp;重置&nbsp;&nbsp;">
                </div>
            </div>
        </form>
    </article>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript">
   if('${flag}'!="")
       layer.msg('${flag}',{icon: 1,time:1000});
var biaozhi1=0;//标志用户存在
var biaozhi2=0;//标志车号有效
   function jianyanuser(){
       //alert($(op).val())
       if($("#uname").val()!=""&&$("#no").val()!="")
       $.ajax({
           type: "POST",
           url: "/yonghu1/jianyanuser.do",
           dateType:'json',
           data: {
               "u_name":$("#uname").val(),
               "u_phone":$("#no").val(),
           },
           success: function(msg){

               if(msg.flag!=null){
                   layer.msg('该用户有效',{icon: 1,time:1000});
                   biaozhi1=1;
                   $("#tijiao").show();
                   if(biaozhi2==0)
                   $("#tijiao").hide();
               }else{
                   biaozhi1=0;
                   layer.msg('该用户不存在',{icon: 5,time:1000});
                   $("#tijiao").hide();
               }


           },
           error:function(){
               alert(2)
           }
       });
   }

   function jianyan(op){
       //alert($(op).val())
       if($(op).val()!="")
       $.ajax({
           type: "POST",
           url: "/yonghu1/jianyancarid.do",
           dateType:'json',
           data: {
               "c_id":$(op).val(),
           },
           success: function(msg){

               if(msg.flag!=0){

                   $("#tijiao").hide();
                   biaozhi2=0;
                   layer.msg('该车已存在',{icon: 5,time:1000});
               }else{
                   layer.msg('车牌有效',{icon: 1,time:1000});
                   biaozhi2=1;
                   //jianyanuser()
                   $("#tijiao").show();
                   if(biaozhi1==0)
                       $("#tijiao").hide();
               }


           },
           error:function(){
               alert(2)
           }
       });
   }


    $(function(){
        $('.skin-minimal input').iCheck({
            checkboxClass: 'icheckbox-blue',
            radioClass: 'iradio-blue',
            increaseArea: '20%'
        });

        $("#form-member-add").validate({
            rules:{
                uname:{
                    required:true,
                    isChinese:true,
                    minlength:2,
                    maxlength:16,
                },
                u_phone:{
                    required:true,
                    isMobile:true,
                },
                c_id:{
                    required:true,
                    isPlateNo:true,
                },
                time:{
                    required:true,
                },
                c_color:{
                    required:true,
                    isChinese:true,
                },
                c_type:{
                    required:true,


                },
                c_brandtype:{
                    required:true,
                },
                c_enginenum:{
                    required:true,
                    stringCheck:true,
                },
                c_brand:{
                    required:true,
                    stringCheck:true,
                },
                c_isauto:{
                    required:true,

                },
                file:{
                    required:true,
                },
                c_enginesize:{
                    required:true,

                },
                uploadfile:{
                    required:true,
                },

                seatnum:{
                    required: true,
                    isIntGtZero:true,
                    isDigits:true,
                },

                c_price:{
                    required: true,
                    isIntGtZero:true,
                    maxlength:10,
                },
                c_evaluater:{
                    required:true,
                    isChineseChar:true,
                },
            },
           /* onkeyup:false,
            focusCleanup:true,
            success:"valid",
            submitHandler:function(form){
                //$(form).ajaxSubmit();
                var index = parent.layer.getFrameIndex(window.name);
                //parent.$('.btn-refresh').click();
                parent.layer.close(index);
            }*/
        });
    });



</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>