<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="Bookmark" href="/favicon.ico" >
    <link rel="Shortcut Icon" href="/favicon.ico" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/lib/html5shiv.js"></script>
    <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
    <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>

    <link rel="stylesheet" href="/layui/css/layui.css"  media="all">
    <script type="text/javascript" src="/layui/layui.all.js" ></script>
    <![endif]-->
    <!--/meta 作为公共模版分离出去-->

    <title>交易管理</title>
</head>
<style>
    .biaoti{
        font-size: 20px;
        font-weight: 900;
    }
    .biaoti_1{
        margin: 0px auto;
        position: relative;
        top: 5px;
    }
    .biaoti_2{
        margin: 0px auto;
        position: relative;
        top: 35px;
    }
    .neirong{
        font-size: 18px;
        font-family: 宋体;
        position: relative;
        top: 7px;
        left: 15px;
    }
    .neirong-1{
        font-size: 18px;
        font-family: 宋体;
        position: relative;
        top: 15px;
        left: 35px;
    }
    .biaoti_3{
        margin: 0px auto;
        position: relative;
        top: 90px;
    }
    #houzhui p{
        width: 350px;
        margin: 0px auto;
        font-size: 5px;
    }
    #houzhui .p-one{
        position: relative;
        top: 15px;
        left: 85px;
    }
    #houzhui .p-two{
        position: relative;
        left: 45px;
        top: 15px;
    }
    #houzhui .p-three{
        position: relative;
        left: 95px;
        top: 15px;
    }
    #anniu{
        width: 200px;
        margin: 0px auto;
    }
</style>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页
    <span class="c-gray en">&gt;</span>
    保险单导入导出
    <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
</nav>

<div class="page-container">
    <form class="form form-horizontal" id="form-article-add"  method="post" enctype="multipart/form-data" action="/fuwu/tuibaodingdan.do" onsubmit="return shiFanBtn();" >
        <div id="tab-system" class="HuiTab">
            <div class="tabBar cl">
                <span>退保申请</span>
            </div>
            <div class="tabCon">

                <table data-sort="sortDisabled" border="1px" style="width:900px " align="center">
                    <caption>
                        <span style="font-size: 36px;">中 国 安 康 保 险 股 份 有 限 公 司</span></br>
                        <span  style="font-size: 30px;">退 保 申 请 书</span></br>
                    </caption>

                    <tbody>
                    <tr class="firstRow">
                        <td width="314" height="40" valign="top" style="word-break: break-all;" class="biaoti">
                            <div class="biaoti_1" style="width: 80px;">投保人：</div>
                        </td>
                        <td valign="top" rowspan="1" colspan="1" width="313" class="neirong" name="">
                        ${fw_users.u_name}
                        </td>
                        <td rowspan="1" valign="top" align="null" width="313" style="word-break: break-all;" class="biaoti">
                            <div class="biaoti_1" style="width: 110px;">联系方式：</div>
                        </td>
                        <td rowspan="1" valign="top" align="null" width="313" class="neirong">
                        ${fw_users.u_phone}
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" height="40" style="word-break: break-all;" rowspan="1" colspan="1" class="biaoti">
                            <div class="biaoti_1" style="width: 180px;">被保险人身份证号：</div>
                        </td>
                        <td valign="top" rowspan="1" colspan="3" class="neirong">
                        ${fw_users.u_idcard}
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" height="40" style="word-break: break-all;" rowspan="1" colspan="1" class="biaoti">
                            <div class="biaoti_1" style="width: 110px;">联系地址：</div>
                        </td>
                        <td valign="top" rowspan="1" colspan="3" class="neirong">
                        ${fw_users.u_addr}
                        </td>
                    </tr>
                    <tr>
                        <td width="314" height="40" valign="top" style="word-break: break-all;" class="biaoti">
                            <div class="biaoti_1" style="width: 80px;">车牌号：</div>
                        </td>
                        <td width="314" valign="top" class="neirong">
                            <select name="chepaihao" value="chepai" onchange="jilian(this)" style="width: 120px;height: 28px;font-size: 15px">
                                <option value="0">---请选择---</option>
                            <#list list as cl>
                                <option value="${cl.c_id}">${cl.c_id}</option>
                            </#list>
                            </select>
                        </td>
                        <td width="314" valign="top" style="word-break: break-all;" class="biaoti">
                            <div class="biaoti_1" style="width: 110px;">退订保险：</div>
                        </td>
                        <td width="314" valign="top" class="neirong">
                            <select name="ic_id" value="" id="baoxain" style="width: 120px;height: 28px;font-size: 15px">
                                <option value="0">---请选择---</option>

                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="314" height="100" valign="top" style="word-break: break-all;" class="biaoti">
                            <div class="biaoti_2" style="width: 110px;">退订理由：</div>
                        </td>
                        <td valign="top" rowspan="1" colspan="3">
                            <textarea name="s_msg" placeholder="请输入内容" style="width: 400px;height: 75px"  class="neirong-1"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td width="314" height="100" valign="top" style="word-break: break-all;" class="biaoti">
                            <div class="biaoti_3" style="width: 110px;">重要提示：</div>
                        </td>
                        <td valign="top" style="word-break: break-all;" rowspan="1" colspan="3">
                            <p>
                                <span style="font-size: 20px;">1.车辆的保险单必须在有效期内!<br/></span>
                            </p>
                            <p>
                                <span style="font-size: 20px;">2.请明确写明汽车保险退订的理由！<br/></span>
                            </p>
                            <p>
                                <span style="font-size: 20px;">3.在退保的愿意上，公司将对您的退订进行处理，只有符合退保的要求的车辆，才会对投保人进行退保！</span>
                            </p>
                            <p>
                                <span style="font-size: 20px;">4.在保险单有效期内，退保车辆没有向保险公司报案或索赔过可退保，从保险公司得到过赔偿的车辆不能退保！
                                    仅向保险公司报案而未得到赔偿的车辆也不能退保！</span>
                            </p>
                            <p>
                                <span style="font-size: 20px;">5.退保保费为自投保人提出退保申请之日起的未了责任期保险费，即：
                                    退保保费＝保费*（保单剩余有效天数/保单总有效天数）</span>
                            </p>
                            <p>
                                <span style="font-size: 20px;">6.商业车险是可以退保。交强险不能退保（除车辆报废）！</span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="314" height="100" valign="top" style="word-break: break-all;" class="biaoti">
                            <div class="biaoti_2" style="width: 80px;">备注：</div>
                        </td>
                        <td valign="top" rowspan="1" colspan="3" style="height: 300px">
                            <textarea placeholder="请输入内容" style="width: 400px;height: 205px"  class="neirong-1"></textarea>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="row cl" align="center">

                <div class="formControls col-xs-8 col-sm-9" style="width: 1680px;height: 40px">
                    <div>
                        <button type="submit" class="btn btn-primary radius" id="saveBtn" >提交</button>
                        <input class="btn btn-primary radius" type="reset" value="&nbsp;&nbsp;重置&nbsp;&nbsp;">
                    </div>
                </div>
            </div>
        <#--后缀-->
            <div id="houzhui">
                <p class="p-one">关于安康 | 软件著作权 | 感谢捐赠</p>
                <p class="p-two">Copyright ©2013-2017 H-ui.net All Rights Reserved. </p>
                <p class="p-three">用心做车险，做不一样的车险</p>
            </div>

    </form>


</div>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript">
    $(function(){
        $('.skin-minimal input').iCheck({
            checkboxClass: 'icheckbox-blue',
            radioClass: 'iradio-blue',
            increaseArea: '20%'
        });
        $("#tab-system").Huitab({
            index:0
        });
    });

    /*级联查询*/
   var jilian =  function(oop){

       /*alert($(oop).val())*/
       $.ajax({
           type: "POST",
           url: "/fuwu/baoxian.do",
           dateType:'json',
           data: {
               "ci_id":$(oop).val(),
           },
           success: function(msg){

               var baoxian="";
               $.each(msg.list,function (i,val) {
                   baoxian+="<option  value="+val.ic_id+">"+val.carInsur.ci_name+"</option>";
               })
               $("#baoxain").html(baoxian);
           },
           error:function(){
              /* alert(2)*/
           }
       });
   };

/*提交按钮*/
   var shiFanBtn=function () {
       if(confirm("确定要提交吗？")){
           return true;
       }else {
           return false;
       }
    }
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>
