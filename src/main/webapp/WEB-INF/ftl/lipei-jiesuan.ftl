<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="/lib/html5shiv.js"></script>
  <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
  <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>

    <![endif]-->
    <title>理赔结算</title>
    <style>

        .text th{
            text-align: center;
            background-color: #74B6F0;
        }
        .text td{
            text-align: center;
            background-color: #AFE2F7;
        }
    </style>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 理赔管理 <span class="c-gray en">&gt;</span> 赔偿结算<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>

   <div class="page-container">
        <div class="text-c">

        </div>
        <div class="cl pd-5 bg-1 bk-gray mt-20"><button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button>  </div>
    <div class="mt-20">
        <table class="table table-border table-bordered table-hover table-bg table-sort">
            <thead>
            <tr class="text-c">
                <th width="100">报案车辆</th>
                <th width="100">报案用户</th>
                <th width="100">添加时间</th>
                <th width="100">用户邮箱</th>
                <th width="100"> 应理赔金额</th>
                <th width="100">审核状态</th>
                <th width="100">是否结算</th>
                <th width="100">操作</th>
            </tr>
            </thead>
            <tbody>
                 <#list statelist as sl>
                   <tr class="text-c" onclick="qiehuan(this,'${sl.c_id}','${sl.cl_jiesuan}')">
                    <#--<td id="clid">${sl.cl_id}</td>-->
                    <td >${sl.c_id}</td>
                    <td>${sl.u_name}</td>
                        <td>${sl.cl_addtime?string("yyyy-MM-dd HH:mm:ss")}</td>
                        <td>${sl.u_emal}</td>
                    <td id="clmoney">￥${sl.zh}元</td>

                       <#if (sl.cl_state==0)>
                           <td class="td-status" ><span class="label label-defaunt radius">待审核</span></td>
                       <#elseif (sl.cl_state==1)>
                           <td class="td-status"><span class="label label-success radius">合格</span></td>
                       <#else>
                           <td class="td-status"><span class="label label-danger radius">不合格</span></td>
                       </#if>

                       <#if (sl.cl_jiesuan==0)>
                           <td class="td-status1"><span class="label label-success radius">未结算</span></td>
                       <#elseif (sl.cl_jiesuan==1)>
                           <td class="td-status"><span class="label label-defaunt radius">已结算</span></td>
                       </#if>
                   <td class="f-14 td-manage">
                       <#if (sl.cl_jiesuan==0)>
                           <#if UsersPower?index_of("赔偿结算")!=-1>
                               <a style="text-decoration:none" onclick="lipei_jiesuan(this,'${sl.c_id}')" href="javascript:;" title="结算"><i class="Hui-iconfont">&#xe631;</i></a>
                           <#else >
                               <span class="label label-danger radius">没有权限</span>
                           </#if>

                       <#else >
                           <a style='text-decoration:none'  href='javascript:;' title='已结算'><i class='Hui-iconfont'>&#xe615;</i></a>
                       </#if>
                   </td>
                  </tr>

              </#list>
            </tbody>
        </table>
    </div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    $(function(){
       $('.table-sort').dataTable({
           "aaSorting": [[ 1, "desc" ]],//默认第几个排序
           "bStateSave": true,//状态保存
           "aoColumnDefs": [
               //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
//               {"orderable":false,"aTargets":[0,8,9]}// 制定列不参与排序
           ]
       });

   });

    var flag=1;
    var qiehuan=function(oop,id,cl_jiesuan){
        /*alert(cl_jiesuan)*/
        if(flag==1){
           $.ajax({
               type: 'POST',
               url: '/lipei/queryClaimantById.do',
               data:{
                   "c_id":id,
                   "cl_jiesuan":cl_jiesuan
               },
               dataType: 'json',
               success: function(data){
                   $.each(data.claimants,function(i,val){
                       $(oop).after("<tr class='text'><td>"+val.cl_id+" </td><td>"+val.carInsur.ci_name+"</td>" +
                               "<td>"+val.carInsur.maxmoney+"</td><td>"+val.cl_money+"</td><td>"+val.cl_money1+"</td><td>"+val.cl_money2+"</td><td>"+val.dsf_money+"</td><td>"+val.zonghe+"</td></tr>");

                   });
                   $(oop).after("<tr class='text'><th>报案单编号</th> <th>保险名</th> <th>最大保额</th>  <th>死亡应赔金额</th> <th>医疗应赔金额</th> <th>财产应赔金额</th> <th>从第三方获取的金额</th><th>应赔金额</th></tr>");
                   flag=0;


               }
           });

        }else{
            flag=1;
            $(".text").hide();
        }
       }





    function lipei_jiesuan(obj,id){
        layer.confirm('确认要结算吗？',function(index){
            $.ajax({
                type: 'POST',
                url: '/lipei/edit_jiesuan.do',
                data:{
                    "c_id":id,
                    "cl_jiesuan":0,

                },
                dataType: 'json',
                success: function(data){
                    var cl_id=id;
                    if(data.flag==1){
                        $(obj).parents("tr").find(".td-manage").prepend(" <a style='text-decoration:none'  href='javascript:;' title='已结算'><i class='Hui-iconfont'>&#xe615;</i></a>");
                        $(obj).parents("tr").find(".td-status1").html('<span class="label label-defaunt radius">已结算</span>');
                        $(obj).remove();
                        layer.msg('已结算!',{icon: 5,time:1000});
                       /*$.ajax({
                          type:'POST',
                          url:'/lipei/addClairecord.do',
                           data:{
                               "c_id":id,
                           },
                           dataType: 'json',
                       });*/
                    }else{
                        layer.msg('失败!',{icon: 5,time:1000});
                    }

                },
                error:function(data) {
                    console.log(data.msg);
                },
            });
        });
    }
</script>
</body>
</html>