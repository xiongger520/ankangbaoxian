<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <title>新建网站角色 - 管理员管理 - H-ui.admin v3.1</title>
    <link rel="stylesheet" href="/layui/css/layui.css" type="text/css">
    <script type="text/javascript" src="/layui/layui.all.js"></script>
    <script type="text/javascript" src="/layui/layui.js"></script>
    <script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>

</head>
<style>
    #body{

        width: 1450px;
        margin: 0px auto;
    }
    .biaotou{
        font-size: 20%;
        color: #A60000;
        font-weight: 900;
        position: relative;
        top: 3px;
    }
    .xinxibiaoge{
        height: 30px;
        width: 300px;
        text-align: right;
    }
    .xinxitou{
        height: 28px;
        padding-top: 6px;
        padding-left: 15px;
        background-color: #aee1fb;
        font-weight: 900;
        color: #0b76ac;
        /*color: #a0a7b1;*/
    }
    .xinxitou_1{
        height: 28px;
        padding-top: 6px;
        padding-left: 15px;
        background-color: #F5FAFE;
        font-weight: 900;
    }
    table,th,td{
        border-color: #d2dcf8;
    }
    .neirong-one{
        width: 400px;
        height: 25px;
        padding-left: 25px;

    }
    #houzhui p{
        width: 350px;
        margin: 0px auto;
        font-size: 5px;
    }
    #houzhui .p-one{
        position: relative;
        top: 15px;
        left: 85px;
    }
    #houzhui .p-two{
        position: relative;
        left: 45px;
        top: 15px;
    }
    #houzhui .p-three{
        position: relative;
        left: 95px;
        top: 15px;
    }
    .aichexinxi{
        font-size: 15px;
        color: #0e90d2;
        position: relative;
        top: -11px;
        left: 6px;
    }
    .abq{
        text-decoration: none;
    }
    a {color:#252525; text-decoration:none;}
    a:visited {text-decoration:none;}
    a:hover {color:#ba2636;text-decoration:underline;}
    a:active {color:#ba2636;}

</style>
<body>
<#--版本号：1.0-->
<#--用户服务，个人信息模块-->
<div id="body">
        <div style="width: 300px;margin: 0px auto;">
            <span style="font-size: 35px;font-family: 华文楷体;font-weight: 900; color: #3faa53;" >安康保险欢迎您！</span>
        </div>
        <div>
            <hr>
        </div>
    <div>

        <div>
            <span>
                <img src="/temp/yonghu/2345_image_file_copy_1.jpg">
            </span>
            <p class="biaotou" style="display: inline">注意，请认真核实个人信息，信息有误将影响个人信誉!</p>
        </div>
    </div>
    <#--个人信息-->
    <table data-sort="sortDisabled" cellspacing="0px" style="border-top:1px solid #000;">
        <tbody>
        <tr class="firstRow">
            <td valign="top" rowspan="1" colspan="4" style="word-break: break-all;" class="xinxitou">
                <strong>被保险人信息--></strong><br/>
            </td>
        </tr>
        <tr>
            <td valign="top" style="word-break: break-all;" class="xinxibiaoge">
                姓名：<br/>
            </td>
            <td width="" valign="top" class="neirong-one">
               <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${users.u_name}</span>
            </td>
            <td valign="top" style="word-break: break-all;" class="xinxibiaoge">
                身份证号：
            </td>
            <td width="" valign="top" class="neirong-one">
                <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${users.u_idcard}</span>
            </td>
        </tr>
        <tr>
            <td valign="top" style="word-break: break-all;" class="xinxibiaoge">
                性别：<br/>
            </td>
            <td width="" valign="top" class="neirong-one">
                <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${users.u_sex}</span>
            </td>
            <td valign="top" style="word-break: break-all;" class="xinxibiaoge">
                出生年月：<br/>
            </td>
            <td width="" valign="top" class="neirong-one">
                <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${users.u_bir?string("yyyy-MM-dd")}</span>
            </td>
        </tr>
        <tr>
            <td valign="top" style="word-break: break-all;" class="xinxibiaoge">
                QQ：
            </td>
            <td width="" valign="top" class="neirong-one">
                <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${users.u_qq}</span>
            </td>
            <td valign="top" style="word-break: break-all;" class="xinxibiaoge">
                手机号：
            </td>
            <td width="" valign="top" class="neirong-one">
                <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${users.u_phone}</span>
            </td>
        </tr>
        <tr>
            <td width="" valign="top" style="word-break: break-all;" class="xinxibiaoge">
                Email：
            </td>
            <td width="" valign="top" class="neirong-one">
                <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${users.u_emal}</span>
            </td>
            <td valign="top" style="word-break: break-all;" class="xinxibiaoge">
                家庭电话：
            </td>
            <td width="" valign="top" class="neirong-one">
                <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${users.u_other_num}</span>
            </td>
        </tr>
        <tr>
            <td valign="top" style="word-break: break-all;" class="xinxibiaoge">
                通讯地址：
            </td>
            <td valign="top" rowspan="1" colspan="3" class="neirong-one">
                <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${users.u_addr}</span>
            </td>
        </tr>
        </tbody>
    </table>


<#--车辆信息-->
    <#list list as u>
        <table data-sort="sortDisabled" cellspacing="0px" style="/*border-right:1px solid #000;*/
    /*border-left: 1px solid #000;*/">
            <tbody>

            <tr class="firstRow">
                <td valign="top" rowspan="1" colspan="4" style="word-break: break-all;" class="xinxitou_1">
                    <img src="/temp/yonghu/ti1mg.jpg">
                    <div style="display: inline" class="aichexinxi"> 爱车信息<${u.c_id}>
                </td>
            </tr>
            <tr>
                <td width="" valign="top" style="word-break: break-all;" class="xinxibiaoge">
                    车牌号：<br/>
                </td>
                <td width="" valign="top"  class="neirong-one">
                    <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${u.c_id}</span>
                </td>
                <td width="" valign="top" style="word-break: break-all;" class="xinxibiaoge">
                    品牌：
                </td>
                <td width="" valign="top" style="word-break: break-all;" class="neirong-one">
                    <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${u.c_brand}</span>
                </td>
            </tr>
            <tr>
                <td width="" valign="top" style="word-break: break-all;" class="xinxibiaoge">
                    颜色：
                </td>
                <td width="" valign="top" class="neirong-one">
                    <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${u.c_color}</span>
                </td>
                <td width="" valign="top" style="word-break: break-all;" class="xinxibiaoge">
                    购车日期：
                </td>
                <td width="" valign="top" class="neirong-one">
                    <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${u.c_buydate?string("yyyy-MM-dd ")}</span>
                </td>
            </tr>
            <tr>
                <td width="" valign="top" style="word-break: break-all;" class="xinxibiaoge">
                    发动机型号
                </td>
                <td width="" valign="top" class="neirong-one">
                    <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${u.c_enginenum}</span>
                </td>
                <td width="" valign="top" style="word-break: break-all;" class="xinxibiaoge">
                    档位：
                </td>
                <td width="" valign="top" class="neirong-one">
                    <#if u.c_isauto == 0>
                        <span style="font-weight: 900;font-family: 楷体;font-size: 22px">自动档</span>
                    <#else>
                        <span style="font-weight: 900;font-family: 楷体;font-size: 22px">手动档</span>
                    </#if>
                </td>
            </tr>
            <tr>
                <td width="" valign="top" style="word-break: break-all;" class="xinxibiaoge">
                    排量大小：
                </td>
                <td width="" valign="top" class="neirong-one">
                    <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${u.c_enginesize}</span>
                </td>
                <td width="" valign="top" style="word-break: break-all;" class="xinxibiaoge">
                    座位数量：
                </td>
                <td width="" valign="top" class="neirong-one">
                    <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${u.seatnum}</span>
                </td>
            </tr>
            <tr>
                <td width="" valign="top" style="word-break: break-all;" class="xinxibiaoge">
                    车辆估价：
                </td>
                <td width="" valign="top" style="word-break: break-all;" class="neirong-one">
                    <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${u.c_price}</span>
                </td>
                <td width="" valign="top" style="word-break: break-all;" class="xinxibiaoge">
                    (价格)评估人：
                </td>
                <td width="" valign="top" class="neirong-one">
                    <span style="font-weight: 900;font-family: 楷体;font-size: 22px">${u.c_evaluater}</span>
                </td>
            </tr>
            <tr>
                <td width="" valign="top" style="word-break: break-al" class="xinxibiaoge">
                    购买保险：<br/>
                </td>
                <td width="" valign="top"  class="neirong-one" colspan="3">
                        <#list list1 as dd>
                        <#--${dd.cars.c_id}-->
                            <#if dd.cars.c_id == u.c_id>
                                <a href="#" class="abq" onclick="baoXian('${dd.ic_id}')"><span style="font-weight: 900;font-family: 楷体;font-size: 22px">${dd.carInsur.ci_name}；</span></a>
                            </#if>
                        </#list>
                </td>
            </tr>
            </tbody>
        </table>
    </#list>

    <br>
    <#--后缀-->
    <div id="houzhui">
        <p class="p-one">关于安康 | 软件著作权 | 感谢捐赠</p>
        <p class="p-two">Copyright ©2013-2017 H-ui.net All Rights Reserved. </p>
        <p class="p-three">用心做车险，做不一样的车险</p>
    </div>

</div>
</body>
</html>
<script>
    function baoXian(name){
        layer.open({
            type: 2,
            title: '续保申请',
            fix: false,
            maxmin: true,
            area: ['500px', '800px'],
            content: '/fuwu/xuBao.do?id='+name,
            end: function () {

            }
        });
    }

    /*function baoXian(name){
        //alert(name);
        $.ajax({
           type:'GET',
           dataType:'json',
           data:{
               ic_id:name
           },
            url:'/fuwu/xuBao.do',
            success:function(msg){
               alert(msg.in.ic_money);

            }
        });
    }*/

</script>