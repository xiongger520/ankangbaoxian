<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="Bookmark" href="/favicon.ico" >
    <link rel="Shortcut Icon" href="/favicon.ico" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/lib/html5shiv.js"></script>
    <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
    <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <!--/meta 作为公共模版分离出去-->

    <title>新建网站角色 - 管理员管理 - H-ui.admin v3.1</title>

<style>

   #div1{
       margin-left: 300px;
       font-size: 20px;
   }
   #div1 td{
       font-size: 20px;
   }
   #div2{
        right:250px;
    }
    #download{
        float: left;
        margin-left: 750px;
    }
</style>
</head>


<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 理赔管理 <span class="c-gray en">&gt;</span> 报案单的导入导出<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">

      <DIV style="width:1020px" id="div1">


              <p>
                <span style="color: rgb(255, 0, 0); font-size: 12px;"><span style="font-size: 12px; color: rgb(0, 0, 0);"><em>ZHONGGUOANKAN</em><em>G</em></span><em> </em></span><span style="color: rgb(255, 0, 0);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br/>

               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="font-size: 36px;font-style: normal;font-weight: 400;"><strong>中 国 安 康 保 险 股 份 有 限 公 司</strong></span>
                <br/>
                &nbsp; &nbsp;
            </p>

                <p>
                    <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong><em><strong>&nbsp;&nbsp; </strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="font-size: 20px;"> </span></em><span style="font-size: 20px;"><strong>机 动 车 辆 保 险 索 赔 申 请 书</strong></span><em><span style="font-size: 20px;"></span><br/></em>
                    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                </p>
                <p>
                    <em> 案件号:</em>
                    &nbsp;&nbsp; <span style="color: rgb(255, 0, 0);">9**8600301******</span>
                </p>




                <table data-sort="sortDisabled" align="center" cellspacing="0" cellpadding="0" border="1" width="800px">
                    <tbody>
                    <tr class="firstRow">
                        <td style="word-break: break-all;" width="150" valign="middle">
                            交强险保单号
                        </td>
                        <td rowspan="1" colspan="4" style="word-break: break-all;" width="200" valign="middle">
                            <span style="color: rgb(255, 0, 0);">2014220415160031<br/></span>
                        </td>
                        <td style="word-break: break-all;" width="150" valign="middle">
                            承保公司
                        </td>
                        <td rowspan="1" colspan="4" style="word-break: break-all;" width="200" valign="middle">
                            <span style="color: rgb(255, 0, 0);">安康保险<br/></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            商业险保单号
                        </td>
                        <td rowspan="1" colspan="4" style="word-break: break-all;" width="400" valign="middle">
                            <span style="color: rgb(255, 0, 0);">2014330415160031<br/></span>
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            承保公司
                        </td>
                        <td rowspan="1" colspan="4" style="word-break: break-all;" width="400" valign="middle">
                            <span style="color: rgb(255, 0, 0);">安康保险<br/></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            被保险人
                        </td>
                        <td rowspan="1" colspan="4" style="word-break: break-all;" width="400" valign="middle">
                            <span style="color: rgb(255, 0, 0);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 张三<br/></span>
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            车牌号码
                        </td>
                        <td rowspan="1" colspan="2" style="word-break: break-all;" width="200" valign="middle">
                            <span style="color: rgb(255, 0, 0);">豫A888888<br/></span>
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            <span style="color: rgb(0, 0, 0);">使用性质</span>
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            <span style="color: rgb(255, 0, 0);">非营运<br/></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            发动机号
                        </td>
                        <td rowspan="1" colspan="4" style="word-break: break-all;" width="400" valign="middle">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br/>
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            车架号
                        </td>
                        <td rowspan="1" colspan="4" width="400" valign="middle"></td>
                    </tr>
                    <tr>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            报案人
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            <span style="color: rgb(255, 0, 0);">张三</span><br/>
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            联系电话
                        </td>
                        <td rowspan="1" colspan="2" style="word-break: break-all;" width="200" valign="middle">
                            <span style="color: rgb(255, 0, 0);">156*********</span><br/>
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            驾驶员
                        </td>
                        <td rowspan="1" colspan="2" style="word-break: break-all;" width="100" valign="middle">
                            <span style="color: rgb(255, 0, 0);">张三</span><br/>
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            联系电话
                        </td>
                        <td style="word-break: break-all;" width="200" valign="middle">
                            <span style="color: rgb(255, 0, 0);">156*********</span><br/>
                        </td>
                    </tr>
                    <tr>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            出险时间
                        </td>
                        <td rowspan="1" colspan="3" style="word-break: break-all;" width="300" valign="middle">
                            &nbsp;<span style="color: rgb(255, 0, 0);">201*</span>年 <span style="color: rgb(255, 0, 0);">*</span> 月 <span style="color: rgb(255, 0, 0);">*&nbsp;</span> 日&nbsp;<span style="color: rgb(255, 0, 0);"> *&nbsp;</span> 时 <span style="color: rgb(255, 0, 0);">* </span>分
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            出险地点
                        </td>
                        <td rowspan="1" colspan="2" style="word-break: break-all;" width="200" valign="middle">
                            <span style="color: rgb(255, 0, 0);">**街道**</span><br/>
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            报案时间
                        </td>
                        <td rowspan="1" colspan="2" style="word-break: break-all;" width="200" valign="middle">
                            &nbsp; <span style="color: rgb(255, 0, 0);">201* <span style="color: rgb(0, 0, 0);">年</span> *<span style="color: rgb(0, 0, 0);">月</span> * <span style="color: rgb(0, 0, 0);">日</span>* <span style="color: rgb(0, 0, 0);">时</span>*分</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            出险原因
                        </td>
                        <td rowspan="1" colspan="9" style="word-break: break-all;" width="900" valign="middle">
                            &nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 18px;"> &nbsp;□<span style="font-size: 18px; color: rgb(255, 0, 0);">√</span></span> 碰撞&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="font-size: 18px;">&nbsp;□</span> 倾覆&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 18px;">&nbsp;□</span> 盗抢&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="font-size: 18px;">&nbsp;□</span>火灾 &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; <span style="font-size: 18px;">&nbsp;□</span> 爆炸&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="font-size: 18px;">&nbsp;□</span>台风&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 18px;"> □</span>自燃&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 18px;">&nbsp;□</span>暴雨&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="font-size: 18px;">&nbsp;□</span> 其他 <br/>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="1" colspan="10" style="word-break: break-all;" width="1000" valign="middle">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;<strong><span style="font-size: 18px;">&nbsp; 其他事故方交强险投保及损失信息</span></strong><br/>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            车牌号码
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            厂牌车型
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            被保险人
                        </td>
                        <td style="word-break: break-all;" rowspan="1" colspan="4" width="400" valign="middle">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 交强险单号
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            承保公司
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            损失金额
                        </td>
                        <td style="word-break: break-all;" width="100" valign="middle">
                            定损公司
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="1" style="word-break: break-all;" width="100" valign="middle" height="25">
                            <span style="color: rgb(255, 0, 0);">豫A666688</span>
                        </td>
                        <td colspan="1" rowspan="1" style="word-break: break-all;" width="100" valign="middle" height="25">
                            <span style="color: rgb(255, 0, 0);">&nbsp;**<br/></span>
                        </td>
                        <td colspan="1" rowspan="1" style="word-break: break-all;" width="100" valign="middle" height="25">
                            <span style="color: rgb(255, 0, 0);">李四<br/></span>
                        </td>
                        <td colspan="4" rowspan="1" style="word-break: break-all;" width="100" valign="middle" height="25">
                            <span style="color: rgb(255, 0, 0);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ********************<br/></span>
                        </td>
                        <td colspan="1" rowspan="1" style="word-break: break-all;" width="100" valign="middle" height="25">
                            <span style="color: rgb(255, 0, 0);">**<br/></span>
                        </td>
                        <td colspan="1" rowspan="1" style="word-break: break-all;" width="100" valign="middle" height="25">
                            <span style="color: rgb(255, 0, 0);">**<br/></span>
                        </td>
                        <td colspan="1" rowspan="1" style="word-break: break-all;" width="100" valign="middle" height="25">
                            <span style="color: rgb(255, 0, 0);">安康保险<br/></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="1" width="100" valign="middle" height="25"></td>
                        <td colspan="1" rowspan="1" width="100" valign="middle" height="25"></td>
                        <td colspan="1" rowspan="1" width="100" valign="middle" height="25"></td>
                        <td colspan="4" rowspan="1" width="100" valign="middle" height="25"></td>
                        <td colspan="1" rowspan="1" width="100" valign="middle" height="25"></td>
                        <td colspan="1" rowspan="1" width="100" valign="middle" height="25"></td>
                        <td colspan="1" rowspan="1" width="100" valign="middle" height="25"></td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="1" width="100" valign="middle" height="25"></td>
                        <td colspan="1" rowspan="1" width="100" valign="middle" height="25"></td>
                        <td colspan="1" rowspan="1" width="100" valign="middle" height="25"></td>
                        <td colspan="4" rowspan="1" width="100" valign="middle" height="25"></td>
                        <td colspan="1" rowspan="1" width="100" valign="middle" height="25"></td>
                        <td colspan="1" rowspan="1" width="100" valign="middle" height="25"></td>
                        <td colspan="1" rowspan="1" width="100" valign="middle" height="25"></td>
                    </tr>
                    <tr>
                        <td colspan="1" rowspan="1" style="word-break: break-all;" width="100" valign="middle">
                            开户名
                        </td>
                        <td colspan="2" rowspan="1" width="200" valign="middle"></td>
                        <td colspan="1" rowspan="1" style="word-break: break-all;" width="100" valign="middle">
                            开户银行
                        </td>
                        <td colspan="2" rowspan="1" width="200" valign="middle"></td>
                        <td colspan="1" rowspan="1" style="word-break: break-all;" width="100" valign="middle">
                            账号
                        </td>
                        <td colspan="3" rowspan="1" width="300" valign="middle"></td>
                    </tr>
                    <tr>
                        <td colspan="10" rowspan="1" style="word-break: break-all;" width="1000" valign="middle">
                            <p>
                                出险原因及经过：<br/>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="color: rgb(255, 0, 0);">&nbsp; **（时间）**（地点），张三驾驶的豫A888888与李四驾驶的豫A666688发生碰撞，导致驾驶保险事故发生，**受损，**受伤</span>
                            </p>
                            <p>
                                <br/>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            </p>
                            <p>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; <br/>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            </p>
                            <p>
                                以上信息为报案人电话报案时所述，如需补充，请在备注栏填写。&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <br/>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="10" rowspan="1" style="word-break: break-all;" width="1000" valign="middle">
                            <p>
                                备注：<br/>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            </p>
                            <p>
                                <br/>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            </p>
                            <p>
                                <br/>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            </p>
                            <p>
                                <br/>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            </p>
                        </td>
                    </tr>

                    </tbody>
                </table>
          </br>
          </br>
          </br>
          <hr>
          </br>
          </br>
          </br>
          <table data-sort="sortDisabled" align="center" cellspacing="0" cellpadding="0" border="1" width="800px">
              <tbody>
                    <tr>
                        <td colspan="10" rowspan="1" style="word-break: break-all;" width="1000" valign="middle">
                            &nbsp; &nbsp;&nbsp; <br/>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <p>
                                &nbsp;&nbsp;&nbsp;<em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 兹声明本人报案时所述及补充填写的资料均为真实情形，没有任何虚假及隐瞒，否则，愿放弃本保险单之一切权利并承担相应的法律责任。</em>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            </p>
                            <p>
                                <em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    本人同意提供给安康集团（指中国安康保险（集团）股份有限公司及其直接或间接控股的公司）的信息，及本人享受安康集团保险服务产生的信息（包括本单证签署之前提供和产生的），可用于安康集团及因服务必要而委托的第三方为本人提供服务及推荐产品，法律禁止的除外。安康集团及其委托的第三方对上述信息负有保密义务。本条款自本单证件签署时生效，具有法律效力，不受合同成立与否及效力状态变化的影响。</em>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            </p>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;被保险人签章： &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; 联系电话：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 年&nbsp;&nbsp;&nbsp;&nbsp; 月&nbsp;&nbsp; &nbsp;&nbsp; 日<br/>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            </p>
                            <#--<p>
                                <br/>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            </p>-->
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;报案人签章：&nbsp;&nbsp;&nbsp;<span style="color: rgb(255, 0, 0);"> 张三 &nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 联系电话：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="color: rgb(255, 0, 0);">156********* </span>&nbsp;&nbsp;&nbsp;&nbsp;年&nbsp;&nbsp;&nbsp;&nbsp; 月&nbsp;&nbsp;&nbsp;&nbsp; 日
                            </p><br/>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="1" colspan="10" style="word-break: break-all;" width="1000">
                            <p>
                                <br/>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            </p>
                            <p>
                                &nbsp;&nbsp; 特别告知：
                            </p>
                            <p>
                                &nbsp;&nbsp; 1，本索赔申请书是被保险人就所投保险种向保险人提出索赔的书面凭证
                            </p>
                            <p>
                                &nbsp;&nbsp; 2，保险人受理报案，现场查勘，参与诉讼，进行抗辩，向被保险人提供专业建议等行为，均不构成保险人对赔偿责任的承诺。
                            </p>
                            <p>
                                &nbsp;&nbsp; 3，为充分保障您的权益，根据《机动车交通事故责任强制保障条例》的相关规定。我司已书面告知您需要向保险公司提供的与赔偿有关的证明和材料
                            </p>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; 被保险人签章：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 联系电话:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 &nbsp;年&nbsp;&nbsp;&nbsp; &nbsp;月&nbsp;&nbsp;&nbsp;&nbsp; 日<br/>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            </p>
                            <p>
                                <br/>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <p>
                    <br/>
                </p>
</DIV>

      <div class="row cl" >
          <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2" align="right">
              <button onClick="article_save_submit();" class="btn btn-primary radius" type="submit" id="download"><i class="Hui-iconfont">&#xe632;</i> <a href="/lipei/download.do">点击下载</a></button>
              <form class="form form-horizontal" id="form-article-add"  method="post" onsubmit="return tijiao()" enctype="multipart/form-data" action="/lipei/upfile.do">

              <span class="btn-upload form-group">
				<input class="input-text upload-url" type="text" name="uploadfile" id="uploadfile" readonly nullmsg="请添加附件！" style="width:200px">
				<a href="javascript:void();" class="btn btn-primary radius upload-btn"><i class="Hui-iconfont">&#xe642;</i> 上传文件</a>
				<input type="file" multiple name="file" class="input-file">
				</span>
              <input type="submit" class="btn btn-default radius" id="saveSub" value="提交">
              </form>
          </div>
      </div>



      <!--_footer 作为公共模版分离出去-->
      <script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
      <script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
      <script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
      <script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

      <!--请在下方写此页面业务相关的脚本-->
      <script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
      <script type="text/javascript" src="/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
      <script type="text/javascript" src="/lib/jquery.validation/1.14.0/validate-methods.js"></script>
      <script type="text/javascript" src="/lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script>
    if('${msg}'!="")
        layer.msg('${msg}',{icon: 1,time:1000});
    var tijiao=function () {
        if($("#uploadfile").val()==""){
            alert("请选择上传文件");
            return false;
        }
       layer.msg('${flag}',{icon: 1,time:1000})
    }
</script>
</body>
</html>
