<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/lib/html5shiv.js"></script>
    <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />


    <link rel="stylesheet" href="/static/plugins/amaze/css/amazeui.min.css">
    <link rel="stylesheet" href="/static/plugins/amaze/css/admin.css">
    <link rel="stylesheet" href="/static/plugins/contextjs/css/context.standalone.css">
    <!--[if IE 6]>
    <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>资讯列表</title>
</head>
<body onUnload="top.location='/index/toindex.do';">

<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 资讯管理 <span class="c-gray en">&gt;</span> 资讯列表 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>

<div class="page-container">
<#--<div class="text-c">
    <button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button>
    <span class="select-box inline">
    <select name="" class="select">
        <option value="0">全部分类</option>
        <option value="1">分类一</option>
        <option value="2">分类二</option>
    </select>
    </span> 日期范围：
    <input type="text" onfocus="WdatePicker({ maxDate:'#F{$dp.$D(\'logmax\')||\'%y-%M-%d\'}' })" id="logmin" class="input-text Wdate" style="width:120px;">
    -
    <input type="text" onfocus="WdatePicker({ minDate:'#F{$dp.$D(\'logmin\')}',maxDate:'%y-%M-%d' })" id="logmax" class="input-text Wdate" style="width:120px;">
    <input type="text" name="" id="" placeholder=" 资讯名称" style="width:250px" class="input-text">
    <button name="" id="" class="btn btn-success" type="submit"><i class="Hui-iconfont">&#xe665;</i> 搜资讯</button>
</div>-->
    <!-- 在线列表区 -->
    <div class="am-panel am-panel-default" style="float:left;width: 100%;margin-left: 20px;">
        <div class="am-panel-hd" >
            <h3 class="am-panel-title" style="height: 40px">在线列表 [<span id="onlinenum"></span>]</h3>
        </div>
        <ul class="am-list am-list-static am-list-striped" id="list">
        </ul>
    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20">
        <span class="l">
            <button onclick="removeIframe()" class="btn btn-primary radius" style="line-height:1.6em;margin-left:20px;margin-bottom:3px ">关闭选项卡</button>
        </span>
        <span class="r">共有数据：<strong>${count}</strong> 条</span>
    </div>

    <div class="mt-20">
        <table class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
            <thead>
            <tr class="text-c">
                <th width="150">编号</th>
                <th width="150">账号</th>
                <th width="150">昵称</th>
                <th width="100">角色名称</th>
                <th width="150">对应用户</th>
                <th width="150">状态</th>
                <th width="100">操作</th>
            </tr>
            </thead>
            <tbody>

            <#list list as userNumber>
            <tr class="text-c">
                <td id="un_id">${userNumber.un_id}</td>
                <td>${userNumber.username}</td>
                <td>${userNumber.un_nickname}</td>
                <td>${userNumber.role.r_name}</td>
                <td><a href="javascript:;" onclick="admin_username_xiangqing()">${userNumber.users.u_name}</a></td>
                <#if (userNumber.un_state==1)>
                    <td class="td-status"><span class="label label-success radius">已启用</span></td>
                <#else >
                    <td class="td-status"><span class="label label-defaunt radius">已停用</span></td>
                </#if>

                <#if UsersPower?index_of("管理员冻结解冻")!=-1>
                    <#if userNumber.role.r_id <= Session.UsernumberRoleUsers.role.r_id>
                        <td><span class="label label-danger radius">没有权限</span></td>
                    <#else>
                        <td class="td-manage">
                            <#if (userNumber.un_state==1)>
                                <a style="text-decoration:none" onclick="member_stop(this,'${userNumber.un_id}')" href="javascript:;" title="停用"><i class="Hui-iconfont">&#xe631;</i></a>
                            <#else >
                                <a style="text-decoration:none" onclick="member_start(this,'${userNumber.un_id}')" href="javascript:;" title="启用"><i class="Hui-iconfont">&#xe6e1;</i></a>
                            </#if>
                        </td>
                    </#if>
                <#else>
                    <td><span class="label label-danger radius">没有权限</span></td>
                </#if>
            </tr>
            </#list>
            </tbody>
        </table>
    </div>
</div>
<BR>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    $('.table-sort').dataTable({
        "aaSorting": [[ 1, "desc" ]],//默认第几个排序
        "bStateSave": true,//状态保存
        "pading":false,
        "aoColumnDefs": [
            //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
            //{"orderable":false,"aTargets":[0,8]}// 不参与排序的列
        ]
    });
    /*用户-停用*/
    function member_stop(obj,id){
        layer.confirm('确认要停用吗？',function(index){
            $.ajax({
                type:"post",
                url:"/xitong/editUserNumber.do",
                dataType:'json',
                data:{
                    "un_id":id,
                    "un_state":0
                },success:function () {
                    $(obj).parents("tr").find(".td-manage").prepend("<a style='text-decoration:none' onclick='member_start(this,"+id+")' href='javascript:;' title='启用'><i class='Hui-iconfont'>&#xe6e1;</i></a>");
                    $(obj).parents("tr").find(".td-status").html('<span class="label label-defaunt radius">已停用</span>');
                    $(obj).remove();
                    layer.msg('已停用!',{icon: 5,time:1000});

                },error:function () {
                    alert("停用失败");
                }
            })
        });
    }

    /*用户-启用*/
    function member_start(obj,id){
        layer.confirm('确认要启用吗？',function(index){
            $.ajax({
                type: 'POST',
                url:"/xitong/editUserNumber.do",
                data:{
                    "un_id":id,
                    "un_state":1,
                },
                dataType: 'json',
                success: function(data){
                    $(obj).parents("tr").find(".td-manage").prepend("<a style='text-decoration:none' onclick='member_stop(this,"+id+")' href='javascript:;' title='停用'><i class='Hui-iconfont'>&#xe631;</i></a>");
                    $(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已启用</span>');
                    $(obj).remove();
                    layer.msg('已启用!',{icon: 6,time:1000});

                },
                error:function(data) {
                    alert("启用失败！")
                },
            });
        });
    }
    /*用户-编辑*/
    function member_edit(title,url,id,w,h){
        layer_show(title,url,w,h);
    }
    /*密码-修改*/
    function change_password(title,url,id,w,h){
        layer_show(title,url,w,h);
    }

    function admin_username_xiangqing(){
        layer_show('用户详细信息','/guanliyuan/queryUser.do?un_id='+$("#un_id").html(),'800','550');
    }


    var wsServer = null;
    var ws = null;
    wsServer = "ws://" + location.host+"${pageContext.request.contextPath}" + "/chatServer";
    ws = new WebSocket(wsServer); //创建WebSocket对象
    ws.onopen = function (evt) {
        layer.msg("已经建立连接", { offset: 0});
    };
    ws.onmessage = function (evt) {
        analysisMessage(evt.data);  //解析后台传回的消息,并予以展示
    };
    ws.onerror = function (evt) {
        layer.msg("产生异常", { offset: 0});
    };
    ws.onclose = function (evt) {
        layer.msg("已经关闭连接", { offset: 0});
    };

    /**
     * 连接
     */
    function getConnection(){
        if(ws == null){
            ws = new WebSocket(wsServer); //创建WebSocket对象
            ws.onopen = function (evt) {
                layer.msg("成功建立连接!", { offset: 0});
            };
            ws.onmessage = function (evt) {

                analysisMessage(evt.data);  //解析后台传回的消息,并予以展示
            };
            ws.onerror = function (evt) {
                layer.msg("产生异常", { offset: 0});
            };
            ws.onclose = function (evt) {
                layer.msg("已经关闭连接", { offset: 0});
            };
        }else{
            layer.msg("连接已存在!", { offset: 0, shift: 6 });
        }
    }

    function analysisMessage(message){
        message = JSON.parse(message);

        if(message.type == "xiaxian"){      //下线提示

            xiaxian(message.message);
        }

        if(message.list != null && message.list != undefined){      //在线列表
            showOnline(message.list);
        }

    }

    function xiaxian(message){
        alert(message.content);
        $.ajax({
            type: "POST", // 用POST方式传输
            dataType: "text", // 数据格式:JSON
            url: "/user/logout.do", // 目标地址
            data: "flag=2",
            success: function (data){
                //alert("已经关闭")
                top.location = "/login.jsp";
            }
        });

    }

    /**
     * 展示在线列表
     */
    function showOnline(list){
        $("#list").html("");    //清空在线列表
        $.each(list, function(index, item){     //添加私聊按钮
            var li = "<li>"+item+"<span class=\"label label-success radius\" style='margin-left: 100px'>在线</span></li>";
            if('${userid}' != item){    //排除自己
                li = "<li>"+item+"<span class=\"label label-success radius\" style='margin-left: 100px'>在线</span> <button type=\"button\" style='margin-left: 100px' class=\"am-btn am-btn-xs am-btn-primary am-round\" onclick=\"sendMessage('"+item+"');\"><span class=\"am-icon-phone\"><span>强制下线 </button></li>";
            }
            $("#list").append(li);
        });
        $("#onlinenum").text($("#list li").length);     //获取在线人数
    }

    function sendMessage(oop){
        if(ws == null){
            layer.msg("连接未开启!", { offset: 0, shift: 6 });
            return;
        }
        /*var message = $("#message").val();
        var to = $("#sendto").text() == "全体成员"? "": $("#sendto").text();
        if(message == null || message == ""){
            layer.msg("请不要惜字如金!", { offset: 0, shift: 6 });
            return;
        }
        $("#tuling").text() == "已上线"? tuling(message):console.log("图灵机器人未开启");  //检测是否加入图灵机器人*/
        var to = oop;
        ws.send(JSON.stringify({
            message : {
                content : "您已被管理员强制下线",
                from : to,
                to : to,      //接收人,如果没有则置空,如果有多个接收人则用,分隔

            },
            type : "xiaxian"
        }));
    }


</script>
</body>
</html>