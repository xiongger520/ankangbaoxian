<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="/lib/html5shiv.js"></script>
  <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
  <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>套餐列表</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 套餐管理 <span class="c-gray en">&gt;</span> 套餐列表 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
    <div class="text-c">

    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20">
        <button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button>
        <span class="r">共有数据：<strong>${count}</strong> 条</span></div>
    <div class="mt-20">
        <table class="table table-border table-bordered table-bg table-hover table-sort">
            <thead>
            <tr class="text-c">
                <th width="40"><input name="" type="checkbox" value=""></th>
                <th width="80">ID</th>
                <th width="100">套餐名称</th>

                <th>套餐包含项</th>
                <th width="150">套餐优惠率</th>
                <th width="150">更新时间</th>
                <th width="60">发布状态</th>
                <th width="100">操作</th>
            </tr>
            </thead>
            <tbody>
            <#list list as i>
            <tr class="text-c">
                <td><input name="" type="checkbox" value=""></td>
                <td>${i.ip_id}</td>
                <td>${i.ip_name}</td>

                <td class="text-l">&nbsp;&nbsp;保险编号&nbsp;${i.inserInclude.ci1}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;保险编号&nbsp;${i.inserInclude.ci2}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <#if (i.inserInclude.ci3!=0)>
                保险编号&nbsp;${i.inserInclude.ci3}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </#if>
                <#if (i.inserInclude.ci4!=0)>
                保险编号&nbsp;${i.inserInclude.ci4}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </#if>
                </td>

                <td class="text-c">${i.ip_rate}</td>
                <td>${i.ip_addtime?string('yyyy-MM-dd HH:mm:ss')}</td>
                <#if (i.ip_state==1)>
                    <td class="td-status"><span class="label label-success radius">已发布</span></td>
                <#elseif (i.ip_state==2)>
                    <td class="td-status"><span class="label label-danger radius">待发布</span></td>
                <#else>
                    <td class="td-status"><span class="label label-defaunt radius">待审核</span></td>
                </#if>
                <td class="td-manage">
                    <#if UsersPower?index_of("套餐上架下架")!=-1>
                        <#if (i.ip_state==1)>
                            <a style="text-decoration:none" onClick="article_xiajia(this,'${i.ip_id}')" href="javascript:;" title="下架"><i class="Hui-iconfont">&#xe6de;</i></a>
                        <#elseif (i.ip_state==2)>
                            <a style="text-decoration:none" onClick="article_fabu(this,'${i.ip_id}')" href="javascript:;" title="发布"><i class="Hui-iconfont">&#xe603;</i></a>
                        <#else >
                            <a style="text-decoration:none"  href="javascript:;" title="待审核"><i class="Hui-iconfont">&#xe63c;</i></a>
                        </#if>
                    <#else>
                        <span class="label label-danger radius">没有权限</span>
                    </#if>

                </td>
            </tr>
            </#list>
            </tbody>
        </table>
    </div>
</div>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    $('.table-sort').dataTable({
        "aaSorting": [[ 1, "desc" ]],//默认第几个排序
        "bStateSave": true,//状态保存
        "aoColumnDefs": [
            //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
            /*{"orderable":false,"aTargets":[0,8]}// 制定列不参与排序*/
        ]
    });


    function article_xiajia(obj,id){
        layer.confirm('确认要下架吗？',function(index){
            $.ajax({
                type: 'POST',
                url: '/yingxiao/edittaocan.do',
                data:{
                    "ip_id":id,
                    "ip_state":2,
                },
                dataType: 'json',
                success: function(data) {

                    if (data.m==1) {
                        $(obj).parents("tr").find(".td-manage").prepend("<a style='text-decoration:none' onClick='article_fabu(this,"+id+")' href='javascript:;' title='发布'><i class='Hui-iconfont'>&#xe603;</i></a>");
                        $(obj).parents("tr").find(".td-status").html('<span class="label label-danger radius">待发布</span>');
                        $(obj).remove();
                        layer.msg('已下架!', {icon: 5, time: 1000});
                    }
                },
                error:function(data) {
                    console.log(data.msg);
                },
            });

        });
    }

    /*资讯-发布*/
    function article_fabu(obj,id){
        layer.confirm('确认要发布吗？',function(index){
            $.ajax({
                type: 'POST',
                url: '/yingxiao/edittaocan.do',
                data:{
                    "ip_id":id,
                    "ip_state":1,
                },
                dataType: 'json',
                success: function(data) {

                    if (data.m==1) {

                        $(obj).parents("tr").find(".td-manage").prepend("<a style='text-decoration:none' onClick='article_xiajia(this,"+id+")' href='javascript:;' title='下架'><i class='Hui-iconfont'>&#xe6de;</i></a>");
                        $(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已发布</span>');
                        $(obj).remove();
                        layer.msg('已发布!',{icon: 6,time:1000});
                    }
                },
                error:function(data) {
                    console.log(data.msg);
                },
            });
        });
    }
</script>
</body>
</html>