<!--
项目二组  鲍康
-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="/lib/html5shiv.js"></script>
  <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
  <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>权限管理</title>
</head>
<body>
<nav class="breadcrumb">
    <i class="Hui-iconfont">&#xe67f;</i> 首页
    <span class="c-gray en">&gt;</span> 管理员管理
    <span class="c-gray en">&gt;</span> 权限列表
    <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" >
        <i class="Hui-iconfont">&#xe68f;</i>
    </a>
</nav>
<div class="page-container">
    <div class="cl pd-5 bg-1 bk-gray"  style="margin-bottom: 20px">
        <#--<button onclick="removeIframe()" class="btn btn-primary radius" style="line-height:1.6em;margin-left:10px;margin-bottom:3px ">关闭选项卡</button>-->
        <span class="l">
            <a href="javascript:;" onclick="removeIframe()" class="btn btn-primary radius">
                <#--<i class="Hui-iconfont">&#xe6e2;</i>-->
                    关闭选项卡</a>
          <a href="javascript:;" onclick="datadel()" class="btn btn-danger radius">
            <i class="Hui-iconfont">&#xe6e2;</i> 批量删除
          </a>
          <a class="btn btn-primary radius" href="javascript:;" onclick="admin_power_add('添加权限','/guanliyuan/power-add.do','800','600')">
            <i class="Hui-iconfont">&#xe600;</i> 添加权限
          </a>
        </span>
        <span class="r">共有数据：<strong>${count}</strong> 条</span>
    </div>
    <table class="table table-border table-bordered table-hover table-bg table-sort">
        <thead>
        <tr>
            <th scope="col" colspan="8">权限管理</th>
        </tr>
        <tr class="text-c">
            <th width="25"><input type="checkbox" value="" name=""></th>
            <th width="40">ID</th>
            <th width="200">权限名称</th>
            <th width="200">添加人</th>
            <th>请求路径</th>
            <th width="250">权限描述</th>
            <th width="200">添加日期</th>
            <th width="100">操作</th>
        </tr>
        </thead>
        <tbody>
        <#list list as power>
        <tr class="text-c">
            <td><input type="checkbox" value="${power.p_id}" name="caritem"></td>
            <td>${power.p_id}</td>
            <td>${power.p_name}</td>
            <td>${power.userNumber.username}</td>
            <td><a href="#">${power.url}</a></td>
            <td>${power.p_msg}</td>
            <td>${power.addtime?string('yyyy-MM-dd HH:mm:ss')}</td>
            <td class="f-14">
                <a title="编辑" href="javascript:;" onclick="admin_power_edit('权限编辑','/guanliyuan/toPowerEdit.do?p_id=${power.p_id}','1','800','600')" style="text-decoration:none">
                    <i class="Hui-iconfont">&#xe6df;</i>
                </a>
                <a title="删除" href="javascript:;" onclick="power_del(this,${power.p_id})" class="ml-5" style="text-decoration:none">
                    <i class="Hui-iconfont">&#xe6e2;</i>
                </a>
            </td>
        </tr>
        </#list>


        </tbody>
    </table>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $('.table-sort').dataTable({
        "aaSorting": [[ 1, "desc" ]],//默认第几个排序
        "bStateSave": true,//状态保存
        "pading":false,
        "aoColumnDefs": [
            //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
            //{"orderable":false,"aTargets":[0,8]}// 不参与排序的列
        ]
    });
    /*管理员-角色-添加*/
    function admin_power_add(title,url,w,h){
        layer_show(title,url,w,h);
    }
    /*管理员-角色-编辑*/
    function admin_power_edit(title,url,id,w,h){
        layer_show(title,url,w,h);
    }
    /*管理员-角色-删除*/
    function power_del(obj,id){
        layer.confirm('权限删除须谨慎，确认要删除吗？',function(index){
            $.ajax({
                type: 'POST',
                url: '/guanliyuan/delPower.do',
                dataType: 'json',
                data:{
                    p_id:id
                },
                success: function(data){
                    $(obj).parents("tr").remove();
                    layer.msg('已删除!',{icon:1,time:1000});
                },
                error:function(data) {
                    alert("error");
                    console.log(data.msg);
                },
            });
        });
    }
    var datadel = function(){

        //判断选择项是否为空
        var checkedNum = $("input[name='caritem']:checked").length;
        if(checkedNum==0){
            layer.msg('请选中要删除的列表项!',{icon: 5,time:1000});
            return;
        }
        //获取选择项的值
        var checkedVal = [];
        var index=-1;
        $("input[name='caritem']:checked").each(function(){
            index++;
            checkedVal[index]=$(this).val();
        });

        var itemsTipStr = "这条记录";
        if(checkedNum>1){
            itemsTipStr = "这些选项";
        }

        // alert(JSON.stringify(checkedVal))
        layer.confirm('确认要删除'+itemsTipStr+'吗？',function(){
            layer.closeAll('dialog');

            //此处请求后台程序，下方是成功后的前台处理……
            $.ajax({
                type: "POST",
                url: "/guanliyuan/delPowers.do",
                data: JSON.stringify(checkedVal),
                contentType: 'application/json',
                success: function(msg){
                    var index=0;
                    $("input[name='caritem']:checked").each(function(){
                        // alert($(this).val()==inde[index])
                        if($(this).val()==checkedVal[index]){
                            $(this).parents("tr").remove();
                            index++;
                        }
                    });
                    layer.msg('已删除!',{icon: 1,time:1000});
                },
                error:function(){
                    alert("error");
                }
            });
        });
    }
</script>
</body>
</html>