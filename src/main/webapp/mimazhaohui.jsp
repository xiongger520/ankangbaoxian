<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/lib/html5shiv.js"></script>
    <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link href="/static/h-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/static/h-ui.admin/css/H-ui.login.css" rel="stylesheet" type="text/css" />
    <link href="/static/h-ui.admin/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/lib/Hui-iconfont/1.0.8/iconfont.css" rel="stylesheet" type="text/css" />
    <link href="/yanzhengma/drag.css" rel="stylesheet" type="text/css" />
    <!--[if IE 6]>
    <script type="text/javascript" src="/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>后台登录 - H-ui.admin v3.1</title>
    <meta name="keywords" content="H-ui.admin v3.1,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
    <meta name="description" content="H-ui.admin v3.1，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>
<body>
<input type="hidden" id="TenantId" name="TenantId" value="" />
<div class="header" style="background-size: 600px 70px;"></div>
<div class="loginWraper">
    <div id="loginform" class="loginBox">
        <form class="form form-horizontal" action="/zhaohui/yanzheng.do" method="post" onsubmit="return yanzheng()" >
            <div class="row cl">
                <label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60d;</i></label>
                <div class="formControls col-xs-8">
                    <input id="username"  name="username" type="text" placeholder="账户" class="input-text size-L">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe6c7;</i></label>
                <div class="formControls col-xs-8">
                    <input  id="un_pwdsalt" name="un_pwdsalt" type="text" placeholder="预留手机号" class="input-text size-L">
                </div>
            </div>
            <div class="row cl">
                <div class="formControls col-xs-8 col-xs-offset-3">
                    <div id="drag"></div>
            </div>
            </div>

            <div class="row cl">

                <div class="formControls col-xs-8 col-xs-offset-3">
                    <a href="/login.jsp" onclick="">返回登录</a>
                </div>
            </div>

            <div class="row cl">
                <div class="formControls col-xs-8 col-xs-offset-3">
                    <input name="" type="submit"  class="btn btn-success radius size-L" value="&nbsp;下&nbsp;&nbsp;一&nbsp;&nbsp;步&nbsp;">

                </div>
            </div>
        </form>
    </div>
</div>
<div class="footer">Copyright 你的公司名称 by H-ui.admin v3.1</div>
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/yanzhengma/drag.js"></script>
<script>
    $('#drag').drag();

    if('${zh_flag}'!=""){
        layer.msg('${zh_flag}',{icon:5,time:1000});
    }

   function yanzheng(){
       //alert(1)
        var username = $('#username').val();
        var password = $('#un_pwdsalt').val();


        if(isNull(username) && isNull(password)){
            layer.msg('请输入账号和预留手机号!!!',{icon:5,time:1000});
            return false;
        }
        if(isNull(username)){
            layer.msg('请输入账号!!!',{icon:5,time:1000});

            return false;
        }

        if(isNull(password)){
            layer.msg('请输入预留手机号!!!',{icon:5,time:1000});
            return false;
        }

        if( $('#drag').text()!='验证通过'){
            layer.msg('请滑动验证码!!!',{icon:5,time:1000});
            return false;
        }

    }

    function isNull(input){
        if(input == null || input == '' || input == undefined){
            return true;
        }
        else{
            return false;
        }
    }
</script>
</body>
</html>
