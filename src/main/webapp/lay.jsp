<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--
项目二组  孙明珠
-->
<!--_meta 作为公共模版分离出去-->

<!--_meta 作为公共模版分离出去-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="Bookmark" href="/favicon.ico" >
    <link rel="Shortcut Icon" href="/favicon.ico" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/lib/html5shiv.js"></script>
    <script type="text/javascript" src="/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
    <script type="text/javascript" src="lib/DD_belatedPNG_0.0.8a-min.js" ></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <!--/meta 作为公共模版分离出去-->

    <title>新建网站角色 - 管理员管理 - H-ui.admin v3.1</title>
    <meta name="keywords" content="H-ui.admin v3.1,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
    <meta name="description" content="H-ui.admin v3.1，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span>购买保险<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>

<article class="page-container">
    <div class="text-c">

    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20"><button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button>  </div>
    <form action=""  method="post" class="form form-horizontal" id="form-admin-role-add">

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>车主身份证号：</label>
            <div class="formControls col-xs-8 col-sm-9" id="Id">
                <input type="text" class="input-text" value="" placeholder="" id="card" name="card">
            </div>
        </div>

        <div class="row cl" id="carId">
            <%--用户车牌号的div的--%>
        </div>

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>车牌号：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="" placeholder="" id="roleName" name="roleName" readonly="readonly">
            </div>
        </div>

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2">保险选择：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <table class="table table-border table-bordered table-hover table-bg table-sort">
                    <thead>
                            <tr class="text-c">
                                <th width="35">自选险</th>
                                <th width="100">保险名称(点击保险可查看详情)</th>
                            </tr>
                    </thead>
                    <tbody>
                         <c:forEach items="${list}" var="s">
                            <tr class="text-c" >
                                <th style="" >
                                    <c:choose>
                                        <c:when test="${s.ci_name=='机动车交通事故责任强制保险'}">
                                            <input type="checkbox" onclick="jianyan()" class="zixuan qiangzhi" checked="checked" value="${s.ci_id}" name="user-Character-0-0" id="user-Character-0-0" disabled="true">
                                </th>
                                        </c:when>
                                        <c:otherwise>

                                            <input type="checkbox"  onclick="jianyan()"  class="zixuan" value="${s.ci_id}" name="user-Character-0-0" id="user-Character-0-0">

                                        </c:otherwise>
                                    </c:choose>
                                <th>
                                    <span  class="abq" style="color: #9cb945" onMouseUp="baoXianXinXi('${s.ci_id}')">${s.ci_name}</span>
                                </th>
                            </tr>
                            </c:forEach>
                    </tbody>
                </table>

                <table class="table table-border table-bordered table-hover table-bg table-sort">
                    <thead>
                            <tr class="text-c">
                                <th width="20">保险套餐</th>
                                <th width="100">保险名称(点击保险可查看详情)</th>

                            </tr>
                    </thead>
                    <tbody>
                            <c:forEach items="${list1}" var="s">
                            <tr>
                                <th>
                                    <input type="radio" onclick="jianyan()" class="taocan" value="${s.ip_id}" name="taocan" id="user-Character-0-1">
                                        ${s.ip_name}
                                </th>
                                <th>
                                    <c:if test="${s.ip_name=='套餐1'}">
                                        <c:forEach var="key" items="${套餐1}">
                                            <label class="">
                                                    <%--<input type="checkbox" class="taocanqingdan" name="套餐1"  value="${key.ci_id}">--%>
                                                <span  class="abq" style="color: #9cb945;align-content: center;" onMouseUp="baoXianXinXi('${key.ci_id}')"> ${key.ci_name}</span> </br></label>

                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${s.ip_name=='套餐2'}">
                                        <c:forEach var="key" items="${套餐2}">
                                            <label class="">
                                                    <%--<input type="checkbox" class="taocanqingdan" name="套餐2" value="${key.ci_id}">--%>
                                                <span  class="abq" style="color: #9cb945" onMouseUp="baoXianXinXi('${key.ci_id}')"> ${key.ci_name}</span></br></label>

                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${s.ip_name=='套餐3'}">
                                        <c:forEach var="key" items="${套餐3}">
                                            <label class="">
                                                    <%--<input type="checkbox" class="taocanqingdan" name="套餐3" value="${key.ci_id}">--%>
                                                <span  class="abq" style="color: #9cb945" onMouseUp="baoXianXinXi('${key.ci_id}')"> ${key.ci_name}</span>  </br></label>

                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${s.ip_name=='套餐4'}">
                                        <c:forEach var="key" items="${套餐4}">
                                            <label class="">
                                                    <%--<input type="checkbox" class="taocanqingdan" name="套餐4" value="${key.ci_id}">--%>
                                                <span  class="abq" style="color: #9cb945" onMouseUp="baoXianXinXi('${key.ci_id}')"> ${key.ci_name}</span></br></label>

                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${s.ip_name=='套餐5'}">
                                        <c:forEach var="key" items="${套餐5}">
                                            <label class="">
                                                    <%--<input type="checkbox" class="taocanqingdan" name="套餐5" value="${key.ci_id}">--%>
                                                <span  class="abq" style="color: #9cb945" onMouseUp="baoXianXinXi('${key.ci_id}')"> ${key.ci_name}</span> </br></label>

                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${s.ip_name=='套餐6'}">
                                        <c:forEach var="key" items="${套餐6}">
                                            <label class="">
                                                    <%--<input type="checkbox" class="taocanqingdan" name="套餐6" value="${key.ci_id}">--%>
                                                <span  class="abq" style="color: #9cb945" onMouseUp="baoXianXinXi('${key.ci_id}')"> ${key.ci_name}</span> </br></label>

                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${s.ip_name=='套餐7'}">
                                        <c:forEach var="key" items="${套餐7}">
                                            <label class="">
                                                    <%--<input type="checkbox" class="taocanqingdan" name="套餐7" value="${key.ci_id}">--%>
                                                <span  class="abq" style="color: #9cb945" onMouseUp="baoXianXinXi('${key.ci_id}')"> ${key.ci_name}</span> </br></label>

                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${s.ip_name=='套餐8'}">
                                        <c:forEach var="key" items="${套餐8}">
                                            <label class="">
                                                    <%--<input type="checkbox" class="taocanqingdan" name="套餐8" value="${key.ci_id}">--%>
                                                <span  class="abq" style="color: #9cb945" onMouseUp="baoXianXinXi('${key.ci_id}')"> ${key.ci_name}</span></br></label>

                                        </c:forEach>
                                    </c:if>
                                </th>
                            </tr>
                            </c:forEach>
                    </tbody>
                </table>

            </div>
        </div>

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2">应交金额：</label>
            <div class="formControls col-xs-8 col-sm-9" id="qian">
                <input type="text" class="input-text" readonly="readonly" placeholder="应交金额" value=""  id="qian1" >
            </div>
        </div>

        <div class="row cl">
            <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
                <button type="button" onclick="tijiao()" class="btn btn-success radius" id="admin-role-save" name="admin-role-save"><i class="icon-ok"></i>保险下单</button>
                <input class="btn btn-primary radius" type="reset" value="&nbsp;&nbsp;重置&nbsp;&nbsp;">
            </div>
        </div>
    </form>
</article>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="/lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript">

    /*通过输入的用户名字  动态查询用户名下的车辆*/
    $(function () {
        $("#card").blur(function(){
            $.ajax({
                type:"GET",
                dataType:"json",
                url:"/jiaoyi/queryCaridByIdcard.do",
                data:{
                    "idcard":$("#card").val(), /*分号后面是页面往后台传的文本框中输入的值*/
                },
                success:function (msg) {
                    var $sugg="";
                    $sugg+="<label class=\"form-label col-xs-4 col-sm-2\">所有车辆：</label>";
                    $sugg+="<div class=\"formControls col-xs-8 col-sm-9\">";
                    $sugg+="<dl class=\"permission-list\">";
                    $sugg+="<dt>";
                    $sugg+="<label>用户车辆</label>";
                    $sugg+="</dt>";
                    $sugg+="<dd>";
                    $sugg+="<dl class=\"cl permission-list2\">";
                    if(msg.carList2==""){
                        layer.msg('该用户不存在或信息正在审核',{icon: 5,time:1000});
                    }else{
                        $.each(msg.carList2,function (i,val) {
                            $sugg+="<dd>";
                            $sugg+="<label class=\"\">";
                            $sugg+="<input type=\"radio\" value="+val.c_id+" name=\"user-Character-1-0-0\" id=\"user-Character-1-0-0\" onclick=\"setValue(this.value)\">";
                            $sugg+= val.c_id+"</label></dd>";
                        })
                        $sugg+="</dl></dd></dl></div>";
                        $("#carId").html($sugg);
                    }

                },

                /*error:function(msg){
                    var $sugg="";
                      $sugg+="你还不能购买保险，请先注册。";
                      $("#carId").html($sugg);
                }*/
            });
        });
    })

    /*把radio的值传给文本框*/
    function setValue(val) {
        document.getElementById("roleName").value = val;
    };


    /*页面自带的*/
    $(function(){
        $(".permission-list dt input:radio").click(function(){
            $(this).closest("dl").find("dd input:checkbox").prop("checked",$(this).prop("checked"));
        });
        $(".permission-list2 dd input:checkbox").click(function(){
            var l =$(this).parent().parent().find("input:checked").length;
            var l2=$(this).parents(".permission-list").find(".permission-list2 dd").find("input:checked").length;
            if($(this).prop("checked")){
                $(this).closest("dl").find("dt input:checkbox").prop("checked",true);
                $(this).parents(".permission-list").find("dt").first().find("input:checkbox").prop("checked",true);
            }
            else{
                if(l==0){
                    $(this).closest("dl").find("dt input:checkbox").prop("checked",false);
                }
                if(l2==0){
                    $(this).parents(".permission-list").find("dt").first().find("input:checkbox").prop("checked",false);
                }
            }
        });

        $("#form-admin-role-add").validate({
            rules:{
                roleName:{
                    required:true,
                },
            },
            onkeyup:false,
            focusCleanup:true,
            success:"valid",
            submitHandler:function(form){
               /* $(form).ajaxSubmit();
                var index = parent.layer.getFrameIndex(window.name);
                parent.layer.close(index);*/
            }
        });
    });


    //遍历选择的保险
    var $baoxian1 = [];
    var $baoxian2 = [];
    var $chepai = []
    var flag = 0;


    var jianyan= function (){
        $baoxian1 = [];
        $baoxian2 = [];
        $chepai = [];
        if($("#roleName").val()==""){
            layer.msg('请填写信息，再选择保险!',{icon: 5,time:1000});
            return;
        }
        var obj1 = $(".zixuan")
        var obj2 = $(".taocan")
        $chepai.push($("#roleName").val())
        for(k in obj1){
            if(obj1[k].checked)
                $baoxian1.push(obj1[k].value);
        }
        for(k in obj2){
            if(obj2[k].checked){
                $(".qiangzhi").prop("checked",false);
                $baoxian2.push(obj2[k].value);
            }

        }
       // alert($baoxian1)
        //alert($baoxian2)

        var baoxian=[];
        var baoxian3=[];
        baoxian3.push(0)


        for(var i=0;i<$baoxian1.length;i++){
            baoxian.push($baoxian1[i])
        }
        for(var i=0;i<baoxian3.length;i++){
            baoxian.push(baoxian3[i])
        }
        for(var i=0;i<$baoxian2.length;i++){
            baoxian.push($baoxian2[i])
        }
        for(var i=0;i<baoxian3.length;i++){
            baoxian.push(baoxian3[i])
        }
        for(var i=0;i<$chepai.length;i++){
            baoxian.push($chepai[i])
        }

        baoxian.push(0)
        baoxian.push(flag)
        //alert(JSON.stringify(baoxian))
            $.ajax({
                type: "post",
                url: "/jiaoyi/addbaoxiandingdan.do",
                data: JSON.stringify(baoxian),
                contentType: 'application/json',
                success: function (msg) {
                    $("#qian1").val(msg.zongqian+"  元整");
                    //alert($("#qian1").val())
                    if(flag==1){
                        flag =0;
                        layer.msg('已下订单，请等候通知...', {icon: 1, time: 1000});
                        $("#qian1").val("")
                    }
                },
                error: function () {
                    alert(2)
                }
            })
    }


    /*提交*/
    var tijiao= function(){
        if($baoxian1.length==0&$baoxian2.length==0){
            layer.msg('请选则要买的保险!',{icon: 5,time:1000});
            return;
        }
       layer.msg('所选保险中，套餐已包含的保险，自选险选择无效!',{icon: 5,time:1000});
       layer.confirm('确认要买这些保险吗？',function() {
            layer.closeAll('dialog');
            flag=1;
            jianyan();

           $("#qian1").val("")
           $("#carId").html("")
           $('#form-admin-role-add')[0].reset();

        })

    }


    /*点击保险可以查看保险详情*/
    function baoXianXinXi(name){
        layer.open({
            type: 2,
            title: '保险详细信息',
            fix: false, //不固定
            maxmin: true,
            shadeClose : true,
            closeBtn: 2,
            shade:0.4,
            area: ['500px','500px'],
            content: '/jiaoyi/queryCarInsurById.do?id='+name,

        });
    }




</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>