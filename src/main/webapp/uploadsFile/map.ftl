<#--
<!--
项目二组  鲍康
&ndash;&gt;
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
        body, html{width: 100%;height: 100%;margin:0;font-family:"微软雅黑";font-size:14px;}
        #l-map{height:300px;width:100%;}
        #r-result{width:100%;}
    </style>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=A4749739227af1618f7b0d1b588c0e85"></script>
    <title>关键字输入提示词条</title>
</head>
<body>
<div id="l-map"></div>
<div id="r-result">请输入:<input type="text" id="suggestId" size="20" value="百度" style="width:150px;" /></div>
<div id="searchResultPanel" style="border:1px solid #C0C0C0;width:150px;height:auto; display:none;"></div>
</body>
</html>
<script type="text/javascript">
    // 百度地图API功能
    function G(id) {
        return document.getElementById(id);
    }

    var map = new BMap.Map("l-map");
    map.centerAndZoom("北京",12);                   // 初始化地图,设置城市和地图级别。

    var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
            {"input" : "suggestId"
                ,"location" : map
            });

    ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件
        var str = "";
        var _value = e.fromitem.value;
        var value = "";
        if (e.fromitem.index > -1) {
            value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
        }
        str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;

        value = "";
        if (e.toitem.index > -1) {
            _value = e.toitem.value;
            value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
        }
        str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
        G("searchResultPanel").innerHTML = str;
    });

    var myValue;
    ac.addEventListener("onconfirm", function(e) {    //鼠标点击下拉列表后的事件
        var _value = e.item.value;
        myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
        G("searchResultPanel").innerHTML ="onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;

        setPlace();
    });

    function setPlace(){
        map.clearOverlays();    //清除地图上所有覆盖物
        function myFun(){
            var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
            map.centerAndZoom(pp, 18);
            map.addOverlay(new BMap.Marker(pp));    //添加标注
        }
        var local = new BMap.LocalSearch(map, { //智能搜索
            onSearchComplete: myFun
        });
        local.search(myValue);
    }
</script>
-->
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>关键字查询</title>
    <link rel="stylesheet" type="text/css" href="/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/static/h-ui.admin/css/style.css" />
    <style type="text/css">
        body
        {
            margin: 0;
            height: 100%;
            width: 100%;
            position: absolute;
            font-size: 12px;
        }
        .main-container
        {
            margin: 30px;
        }
        .search-form
        {
            margin: 0px;
            width: 100%;
            height: 60px;
        }
        #mapContainer
        {
            position: absolute;
            top: 68px;
            margin: 15px;
            left: 200px;
            border: 1px solid #080808;
            right: 8px;
            bottom: 0;
        }
        .Hui-aside{
            margin-top: 39px;
            background-color: #88B5E2;
            color: #0C0C0C;
            font-weight:600;
            font-size: 16px;
        }
        .menu_dropdown ul{
            background-color: #E8F7F9;
        }
    </style>
</head>
<body>
<aside class="Hui-aside">

    <div class="menu_dropdown bk_2">
        <dl id="menu-article">
            <dt><#--<i class="Hui-iconfont">&#xe616;</i>--> 支点名称：金水支点<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a   onmousemove="chakan('河南省郑州市金水区')">地址：河南省郑州市金水区</a></li>
                    <li><a   >联系人：佳佳</a></li>
                    <li><a   >联系人电话：666666</a></li>
                    <li><a   href="javascript:void(0)">类型：金融保险服务;金融保险服务机构;金融保险机构</a></li>
                </ul>
            </dd>
        </dl>
        <dl id="menu-article">
            <dt><#--<i class="Hui-iconfont">&#xe616;</i>--> 支点名称：金水支点<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a onmousemove="chakan('河南省郑州市二七区')">地址：河南省郑州市二七区</a></li>
                    <li><a >联系人：佳佳</a></li>
                    <li><a >联系人电话：666666</a></li>
                    <li><a href="javascript:void(0)">类型：金融保险服务;金融保险服务机构;金融保险机构</a></li>
                </ul>
            </dd>
        </dl>
    </div>
</aside>
<div class="main-container">
    <div class="search-form">
        <form id="form1">
            <div class="text-c">
                <input type="text" class="input-text" style="width:650px" placeholder="输入地点" value="郑州科技学院" id="SearchKey" name="">
                <button type="button" class="btn btn-success radius" id="" name="" onclick="placeSearch();"><i class="Hui-iconfont">&#xe665;</i> &nbsp;&nbsp;查询&nbsp;&nbsp;</button>
            </div>
            <#--<div class="row cl" >

                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="郑州科技学院" placeholder="" style="display: inline-block" id="SearchKey" name="u_name">
                    <input class="btn btn-primary radius" type="button" id="btn_Search" value="&nbsp;&nbsp;查询&nbsp;&nbsp;" onclick="placeSearch();" >
                </div>
            </div>-->
            <#--<input type="text" id="SearchKey" style="width: 400px;" value="郑州科技学院"/>-->
            <#--<input type="button" id="btn_Search" value="查询" onclick="placeSearch();" />-->
            <#--<div class="row cl">
                <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
                    <input class="btn btn-primary radius" type="button" id="btn_Search" value="&nbsp;&nbsp;查询&nbsp;&nbsp;" onclick="placeSearch();" >

                </div>
            </div>-->
        </form>

    </div>
    <div id="mapContainer">
    </div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/h-ui.admin/js/H-ui.admin.js"></script>
<!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=1994ba35a603f806df16000f3799b1da"></script>
<script type="text/javascript">

    var chakan=function(op){
        $("#SearchKey").val(op);
        placeSearch();
    }

    var marker;
    var windowsArr;
    var toolBar = null;
    var overView;
    var editorTool;
    var map = new AMap.Map("mapContainer", {
        resizeEnable: true,zoom:12
    });
    map.plugin(["AMap.ToolBar"], function () {
        toolBar = new AMap.ToolBar();
        map.addControl(toolBar);
    });
    //在地图中添加鹰眼插件
    map.plugin(["AMap.OverView"], function () {
        //加载鹰眼
        //初始化隐藏鹰眼
        overView = new AMap.OverView({
            visible: true
        });
        map.addControl(overView);
    });
    //添加地图类型切换插件
    map.plugin(["AMap.MapType"], function () {
        //地图类型切换
        var mapType = new AMap.MapType({
            defaultType: 0, //默认显示卫星图
            showRoad: true //叠加路网图层
        });
        map.addControl(mapType);
    });
    //加载比例尺插件
    map.plugin(["AMap.Scale"], function () {
        scale = new AMap.Scale();
        map.addControl(scale);
    });




    //基本地图加载
    function placeSearch() {
        map = new AMap.Map("mapContainer", {
            resizeEnable: true
        });
        map.plugin(["AMap.ToolBar"], function () {
            toolBar = new AMap.ToolBar();
            map.addControl(toolBar);
        });
        //在地图中添加鹰眼插件
        map.plugin(["AMap.OverView"], function () {
            //加载鹰眼
            //初始化隐藏鹰眼
            overView = new AMap.OverView({
                visible: true
            });
            map.addControl(overView);
        });
        //添加地图类型切换插件
        map.plugin(["AMap.MapType"], function () {
            //地图类型切换
            var mapType = new AMap.MapType({
                defaultType: 0, //默认显示卫星图
                showRoad: true //叠加路网图层
            });
            map.addControl(mapType);
        });
        //加载比例尺插件
        map.plugin(["AMap.Scale"], function () {
            scale = new AMap.Scale();
            map.addControl(scale);
        });
        //设置多边形的属性
        var polygonOption = {
            strokeColor: "#0000ff",
            strokeOpacity: 1,
            strokeWeight: 3
        };
        var polygonArr;
        //在地图中添加MouseTool插件
        map.plugin(["AMap.MouseTool"], function () {
            var mouseTool = new AMap.MouseTool(map);
            //使用鼠标工具绘制多边形
            mouseTool.polygon(polygonOption);
            AMap.event.addListener(mouseTool, "draw", function (e) {
                //obj属性就是绘制完成的覆盖物对象
                var drawObj = e.obj;
                //获取节点个数
                var pointsCount = e.obj.getPath().length;

                polygonArr = new Array(); //构建多边形经纬度坐标数组
                for (var i = 0; i < pointsCount; i++) {
                    polygonArr.push(new AMap.LngLat(e.obj.getPath()[i].lng, e.obj.getPath()[i].lat));
                }
                var polygon = new AMap.Polygon({
                    map: map,
                    path: polygonArr,
                    fillColor: "#f5deb3",
                    fillOpacity: 0.35
                });
                //添加编辑控件
                map.plugin(["AMap.PolyEditor"], function () {
                    editorTool = new AMap.PolyEditor(map, polygon);
                });
            });
        });

        marker = new Array();
        windowsArr = new Array();
        var searchKey = document.getElementById("SearchKey").value;
        var MSearch;
        AMap.service(["AMap.PlaceSearch"], function () {
            MSearch = new AMap.PlaceSearch({ //构造地点查询类
                pageSize: 10,
                pageIndex: 1,
                city: "021" //城市
            });
            //关键字查询
            MSearch.search(searchKey, function (status, result) {
                if (status === 'complete' && result.info === 'OK') {
                    keywordSearch_CallBack(result);
                }
            });
        });
    }
    //进入页面加载地图
    placeSearch();



    function edditpolygon() {
        editorTool.open();
    }
    function endEddit() {
        editorTool.close();
    }
    //添加marker&infowindow
    function addmarker(i, d) {
        var lngX = d.location.getLng();
        var latY = d.location.getLat();
        var markerOption = {
            map: map,
            icon: "http://webapi.amap.com/images/" + (i + 1) + ".png",
            position: new AMap.LngLat(lngX, latY),
            topWhenMouseOver: true

        };
        var mar = new AMap.Marker(markerOption);
        marker.push(new AMap.LngLat(lngX, latY));

        var infoWindow = new AMap.InfoWindow({
            content: "<h3><font color=\"#00a6ac\">  " + (i + 1) + ". " + d.name + "</font></h3>" + TipContents(d.type, d.address, d.tel),
            size: new AMap.Size(300, 0),
            autoMove: true,
            offset: new AMap.Pixel(0, -20)
        });
        windowsArr.push(infoWindow);
        var aa = function (e) { infoWindow.open(map, mar.getPosition()); };
        AMap.event.addListener(mar, "mouseover", aa);
    }
    //回调函数
    function keywordSearch_CallBack(data) {
        var resultStr = "";
        var poiArr = data.poiList.pois;
        var resultCount = poiArr.length;
        for (var i = 0; i < resultCount; i++) {
            resultStr += "<div id='divid" + (i + 1) + "' onmouseover='openMarkerTipById1(" + i + ",this)' onmouseout='onmouseout_MarkerStyle(" + (i + 1) + ",this)' style=\"font-size: 12px;cursor:pointer;padding:0px 0 4px 2px; border-bottom:1px solid #C1FFC1;\"><table><tr><td><img src=\"http://webapi.amap.com/images/" + (i + 1) + ".png\"></td>" + "<td><h3><font color=\"#00a6ac\">名称: " + poiArr[i].name + "</font></h3>";
            resultStr += TipContents(poiArr[i].type, poiArr[i].address, poiArr[i].tel) + "</td></tr></table></div>";
            addmarker(i, poiArr[i]);
        }
        map.setFitView();
    }
    function TipContents(type, address, tel) {  //窗体内容
        if (type == "" || type == "undefined" || type == null || type == " undefined" || typeof type == "undefined") {
            type = "暂无";
        }
        if (address == "" || address == "undefined" || address == null || address == " undefined" || typeof address == "undefined") {
            address = "暂无";
        }
        if (tel == "" || tel == "undefined" || tel == null || tel == " undefined" || typeof address == "tel") {
            tel = "暂无";
        }
        var str = "  地址：" + address + "<br />  电话：" + tel + " <br />  类型：" + type;
        return str;
    }
    function openMarkerTipById1(pointid, thiss) {  //根据id 打开搜索结果点tip
        thiss.style.background = '#CAE1FF';
        windowsArr[pointid].open(map, marker[pointid]);
    }
    function onmouseout_MarkerStyle(pointid, thiss) { //鼠标移开后点样式恢复
        thiss.style.background = "";
    }
</script>
</body>
</html>
