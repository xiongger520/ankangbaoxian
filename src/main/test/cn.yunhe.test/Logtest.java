package cn.yunhe.test;

import cn.yunhe.dao.LoginMapper;
import cn.yunhe.model.Busilog;
import cn.yunhe.model.InserInclude;
import cn.yunhe.model.UserNumber;
import cn.yunhe.model.Users;
import cn.yunhe.service.ILoginService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Resource;
import java.util.*;

public class Logtest {

    ApplicationContext apc=null;

    @Before
    public void getapp(){
        apc= new ClassPathXmlApplicationContext("spring/spring-mybatis.xml");
    }
    @Resource
    private ILoginService LoginService;
    @Test
    public void test(){
        //phoneBizImplProxy.salePhone(156);
        ILoginService phoneBiz= (ILoginService)apc.getBean("loginServiceImpl");
        Map<String,Object> ma = new HashMap<String, Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行登录");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("登录");//(添加，删除，修改)
        blog.setUn_id(1);
        UserNumber us = new UserNumber();//业务数据
        us.setUsername("admin");
        us.setPassword("admin");
        ma.put("blog",blog);
        ma.put("user",us);
        phoneBiz.Loginuser(ma);



    }

}
