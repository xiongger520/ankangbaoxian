package cn.yunhe.websocket;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.logging.SimpleFormatter;

/**
 * websocket服务
 * @author  :  Amayadream
 * @time   :  2016.01.08 09:50
 */
@ServerEndpoint(value = "/chatServer", configurator = HttpSessionConfigurator.class)
public class ChatServer {
    private static int onlineCount = 0; //静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
    private static CopyOnWriteArraySet<ChatServer> webSocketSet = new CopyOnWriteArraySet<ChatServer>();
    private Session session;    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private String userid;      //用户名
    private HttpSession httpSession;    //request的session

    private static List<String> list = new ArrayList<String>();   //在线列表,记录用户名称
    private static Map routetab = new HashMap<>();  //用户名和websocket的session绑定的路由表
    private static Map routetab1 = new HashMap<>();  //用户名和websocket的httpsession绑定的路由表

    /**
     * 连接建立成功调用的方法
     * @param session  可选的参数。session为与某个客户端的连接会话，需要通过它来给客户端发送数据
     */
    @OnOpen
    public void onOpen(Session session, EndpointConfig config){
        this.session = session;
        webSocketSet.add(this);     //加入set中
        addOnlineCount();           //在线数加1;
        this.httpSession = (HttpSession) config.getUserProperties().get(HttpSession.class.getName());

        this.userid=(String) httpSession.getAttribute("userid");    //获取当前用户
       if(list.contains(userid)){
           System.out.println("routetab1.get(userid).equals(httpSession)"+routetab1.get(userid).equals(httpSession));
           if(routetab1.get(userid).equals(httpSession)){
               System.out.println("是一个电脑");
               subOnlineCount();           //在线数减1
               for (String u:list) {  //先移除重复userid
                   if(u.equals(userid)){
                       list.remove(u);
                       break;
                   }
               }
               list.add(userid); //添加userid
               routetab.put(userid, session);   //将用户名和session绑定到路由表
               routetab1.put(userid,httpSession);
               String message = getMessage("[" + userid + "]加入聊天室,当前在线人数为" + getOnlineCount() + "位", "notice", list);
               broadcast(message);     //广播
           }else{
               System.out.println("不同电脑同用户");
               SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
               JSONObject jsonObject = new JSONObject();
               Map<String, String> ingredients = new HashMap<String, String>();
               ingredients.put("content", "该账号在异地登录，您已被迫下线");
               ingredients.put("from", userid);
               ingredients.put("to", userid);
               jsonObject.put("time", sf.format(new Date()));
               jsonObject.put("type", "xiaxian");
               jsonObject.put("message", ingredients);

               //String message = "{'message':{'content':'已经登录',‘from’:"+userid+",'to':'+userid+','time':"+new Date()+"},'type':'message'}";
               //broadcast(message);     //广播
               System.out.println(jsonObject);
               onMessage(jsonObject.toString());
               list.add(userid);
               routetab.put(userid, session);   //将用户名和session绑定到路由表
               routetab1.put(userid,httpSession);
           }


        }else {
           System.out.println("新用户");
            list.add(userid);           //将用户名加入在线列表
            routetab.put(userid, session);   //将用户名和session绑定到路由表
            routetab1.put(userid,httpSession);
            String message = getMessage("[" + userid + "]加入聊天室,当前在线人数为" + getOnlineCount() + "位", "notice", list);
            broadcast(message);     //广播
        }
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(){
        webSocketSet.remove(this);  //从set中删除

        for (String u:list) {
            if(u.equals(userid)){
                list.remove(u);
                subOnlineCount();           //在线数减1
                break;
            }
        }

            if(!list.contains(userid)){
                System.out.println("未完全关闭，没移除了");
            }else {
                //从在线列表移除这个用户
               /* routetab.remove(userid);
                if(httpSession.equals(routetab1.get(userid))){
                    routetab1.remove(userid);
                }*/
                System.out.println("完全关闭，移除了");
            }

        System.out.println("移除了");


        String message = getMessage("[" + userid +"]离开了聊天室,当前在线人数为"+getOnlineCount()+"位", "notice", list);
        broadcast(message);         //广播
    }

    /**
     * 接收客户端的message,判断是否有接收人而选择进行广播还是指定发送
     * @param _message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String _message) {
        System.out.println("调用了发送的方法");
        System.out.println("内容是："+_message);
        JSONObject chat = JSON.parseObject(_message);
        JSONObject message = JSON.parseObject(chat.get("message").toString());
        if(message.get("to") == null || message.get("to").equals("")){      //如果to为空,则广播;如果不为空,则对指定的用户发送消息
            broadcast(_message);
        }else{
            String [] userlist = message.get("to").toString().split(",");
            if((Session) routetab.get(message.get("from"))!=null)
            singleSend(_message, (Session) routetab.get(message.get("from")));      //发送给自己,这个别忘了
            for(String user : userlist){
                if(!user.equals(message.get("from"))){
                    singleSend(_message, (Session) routetab.get(user));     //分别发送给每个指定用户
                }
            }
        }
    }



    /**
     * 发生错误时调用
     * @param error
     */
    @OnError
    public void onError(Throwable error){
        error.printStackTrace();
    }

    /**
     * 广播消息
     * @param message
     */
    public void broadcast(String message){
        for(ChatServer chat: webSocketSet){
            try {
                chat.session.getBasicRemote().sendText(message);
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }
        }
    }

    /**
     * 对特定用户发送消息
     * @param message
     * @param session
     */
    public void singleSend(String message, Session session){
        try {
            session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 组装返回给前台的消息
     * @param message   交互信息
     * @param type      信息类型
     * @param list      在线列表
     * @return
     */
    public String getMessage(String message, String type, List list){
        JSONObject member = new JSONObject();
        member.put("message", message);
        member.put("type", type);
        member.put("list", list);
        return member.toString();
    }

    public  int getOnlineCount() {
        return onlineCount;
    }

    public  void addOnlineCount() {
        ChatServer.onlineCount++;
    }

    public  void subOnlineCount() {
        ChatServer.onlineCount--;
    }
}
