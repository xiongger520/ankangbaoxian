package cn.yunhe.shiro;

import cn.yunhe.dao.IPowerMapper;
import cn.yunhe.model.Power;
import cn.yunhe.service.ILoginService;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

// AuthorizationFilter抽象类实现了javax.servlet.Filter接口，它是个过滤器。
public class CustomRolesAuthorizationFilter extends AuthorizationFilter {

    @Resource(name="loginServiceImpl")
    private ILoginService loginService;
    @Resource
    private IPowerMapper powerMapper;


    @Override
    protected boolean isAccessAllowed(ServletRequest req, ServletResponse resp, Object mappedValue) throws Exception {
        //存放的是当前登录用户的 角色
        Subject subject = getSubject(req, resp);
        String principal = String.valueOf(subject.getPrincipals());
        //rolesArray存放的是配置文件中的  角色的集合
//        String[] rolesArray = (String[]) mappedValue;

        HttpServletRequest request = (HttpServletRequest) req;
        String url = request.getRequestURI();
        System.out.println(url);
        //从数据库获取权限的角色集合
        Power power = powerMapper.queryPowerByUrl(url);
        String[] rolesArray;
        if(null==power){
            rolesArray = null;
        }else{
            rolesArray = new String[]{power.getP_name()};
            System.out.println(rolesArray);
        }
        if (rolesArray == null || rolesArray.length == 0) { //没有角色限制，有权限访问  
            return true;
        }
        for (int i = 0; i < rolesArray.length; i++) {
            if (subject.hasRole(rolesArray[i])) { //若当前用户是rolesArray中的任何一个，则有权限访问  
                return true;
            }
        }
        HttpSession session = request.getSession();
        session.setAttribute("quanxian",false);
        return false;
    }

    public ILoginService getLoginService() {
        return loginService;
    }

    public void setLoginService(ILoginService loginService) {
        this.loginService = loginService;
    }

    public IPowerMapper getPowerMapper() {
        return powerMapper;
    }

    public void setPowerMapper(IPowerMapper powerMapper) {
        this.powerMapper = powerMapper;
    }
}