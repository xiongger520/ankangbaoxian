package cn.yunhe.controller;

import cn.yunhe.model.*;
import cn.yunhe.service.CarsService;
import cn.yunhe.service.HeyanService;
import cn.yunhe.service.UserGuanliService;
import cn.yunhe.utils.EmailUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/24
 * @描述
 */
@Controller
@RequestMapping("/heyan")
public class HeyanguanliAction {

   @Resource
    private HeyanService heyanService;
    @Resource
    private UserGuanliService userGuanli;

    @RequestMapping("/tocar-heyan")
    public ModelAndView tocarheyan(){
        ModelAndView mv = new ModelAndView();
        List<Cars> carslist = userGuanli.queryCars();
        mv.addObject("carslist",carslist);
        mv.addObject("count", carslist.size());
        mv.setViewName("car-heyan");
        return mv;
    }

   @RequestMapping("/editCarstates")
    public ModelAndView editCarstates(Cars ca,HttpSession session){
        

        ModelAndView mv =new ModelAndView();
        Map<String,Object> ma = new HashMap<String, Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行修改车辆状态操作");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
       UserNumber userNumber = (UserNumber)session.getAttribute("UsernumberRoleUsers");
       blog.setUn_id(userNumber.getUn_id());


       ma.put("blog",blog);
        ma.put("ca",ca);
        int c=heyanService.editCarstate(ma);
        System.out.println("改改改改改改改改改改改");
        System.out.println(c);
        mv.addObject("c",c);
        mv.setView(new MappingJackson2JsonView());
        return mv;

    }

    @RequestMapping("/touser-heyan")
    public ModelAndView touserheyan(){
        ModelAndView mv = new ModelAndView();
        List<Users> userdlist = userGuanli.queryUsers();
        //System.out.println(userdlist.get(1).getU_bir());
        mv.addObject("userlist",userdlist);
        mv.addObject("count", userdlist.size());
        mv.setViewName("user-heyan");
        return mv;
    }

    @RequestMapping("/editUser")
    public ModelAndView editUser(Users user,HttpSession session){
        ModelAndView mv =new ModelAndView();
        Map<String,Object> ma = new HashMap<String, Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行修改用户状态操作");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        UserNumber userNumber = (UserNumber)session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());


        ma.put("blog",blog);
        ma.put("user",user);
        int u=heyanService.edituserState(ma);
       // System.out.println("改改改改改改改改改改改");
        //System.out.println(u);
        mv.addObject("u",u);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }



    @RequestMapping("/tolipei-heyan")
    public ModelAndView tolipeiheyan(){
        ModelAndView mv = new ModelAndView();
        List<Claimant> clist=heyanService.queryClaimantAll();
        mv.addObject("clist",clist);
        mv.addObject("count", clist.size());
        mv.setViewName("lipei-heyan");
        return mv;
    }

    @RequestMapping("/editClaimantstate")
    public ModelAndView editClaimantstate(Claimant cla,ClaiRecord crd,HttpSession session){
        ModelAndView mv =new ModelAndView();
        Map<String,Object> ma = new HashMap<String, Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行修改理赔状态和添加赔付记录操作");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改，添加");//(添加，删除，修改)
        UserNumber userNumber = (UserNumber)session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());


        ma.put("blog",blog);
        ma.put("cla",cla);

        ma.put("crd",crd);
        int u=heyanService.editClaimant(ma);
       // System.out.println("改改改改改改改改改改改");
        //System.out.println(u);
        mv.addObject("u",u);
        mv.setView(new MappingJackson2JsonView());
        //此处从session中获取
        int un_id=userNumber.getUn_id();
        String un_number=userNumber.getUsers().getU_phone();
       /* int un_id=1;*/

        /*ma.put("un_id",un_id);
        ma.put("un_number",un_number);
        int r=heyanService.addRecord(ma);*/
        /*System.out.println(r);*/
        return mv;

    }

    @RequestMapping("/tobaoxian-heyan")
    public ModelAndView tobaoxianheyan(){
        ModelAndView mv = new ModelAndView();
        List<CarInsur> calist=heyanService.queryCarIn();
        mv.addObject("calist",calist);
        mv.addObject("count", calist.size());
        mv.setViewName("baoxian-heyan");
        return mv;
    }

    @RequestMapping("/editCarIns")
    public ModelAndView editCarIns(CarInsur insur,HttpSession session){
        ModelAndView mv =new ModelAndView();
        Map<String,Object> ma = new HashMap<String, Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行修改保险状态操作");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        UserNumber userNumber = (UserNumber)session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());


        ma.put("blog",blog);
        ma.put("insur",insur);
        int i=heyanService.editCarIn(ma);
        // System.out.println("改改改改改改改改改改改");
        //System.out.println(u);
        mv.addObject("i",i);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }


    @RequestMapping("/tobaoxiandan-heyan")
    public ModelAndView tobaoxiandanheyan(){
        ModelAndView mv = new ModelAndView();
        List<InsurContract> insclist=heyanService.queryIcAll();
        mv.addObject("insclist",insclist);
        mv.addObject("count", insclist.size());
       /* UserNumber user=(UserNumber) session.getAttribute("UsernumberRoleUsers");
        String name=user.getUsername();
        mv.addObject("name",name);
        System.out.println("名字"+name);*/
        mv.setViewName("baoxiandan-heyan");
        return mv;
    }

    @RequestMapping("/heyanBaoxiandan")
    public ModelAndView heyanBaoxiandan(InsurContract sct,HttpSession session){
        ModelAndView mv =new ModelAndView();
        Map<String,Object> ma = new HashMap<String, Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行修改保险单状态操作");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        UserNumber userNumber = (UserNumber)session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());

        ma.put("blog",blog);
        sct.setUn_shenid(userNumber.getUn_id());
        ma.put("sct",sct);
        int d=heyanService.editBaoxiandan(ma);
        // System.out.println("改改改改改改改改改改改");
        //System.out.println(u);
        mv.addObject("d",d);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }

    @RequestMapping("/totuibao-heyan")
    public ModelAndView totuibaoheyan(){
        ModelAndView mv = new ModelAndView();
        List<Surrender> slist=heyanService.querySurrender();
        mv.addObject("slist",slist);
        mv.addObject("count", slist.size());
        mv.setViewName("tuibao-heyan");
        return mv;
    }

    @RequestMapping("/editSurr")
    public ModelAndView editSurr(Surrender sur,HttpSession session,String u_emal){
        ModelAndView mv =new ModelAndView();
        Map<String,Object> ma = new HashMap<String, Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行修改保险状态操作");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        UserNumber userNumber = (UserNumber)session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());


        ma.put("blog",blog);
        sur.setUn_shenid(userNumber.getUn_id());
        ma.put("sur",sur);
        System.out.println(sur.getIc_id());
        //调用后台方法进行退保核验计算
        InsurContract list = heyanService.queryIcByid(sur.getIc_id());

        Date addtime = list.getIc_totime();
        int days = (int) ((addtime.getTime()-new Date().getTime()) / (1000*3600*24));
        int num = list.getCars().getChuxian_num();
        if (sur.getR_state()==1){

        if(num>0){
            sur.setR_state(2);
            ma.put("sur",sur);
            heyanService.editSurrender(ma);
            System.out.println("该保已经出过险，不予退保");
            mv.addObject("s","该保已经出过险，不予退保");
        }else if(list.getIc_state()==3){
            sur.setR_state(2);
            ma.put("sur",sur);
            heyanService.editSurrender(ma);
            System.out.println("该保已经退保，不予退保");
            mv.addObject("s","该保已经退保，不予退保");
        } else if(days<270){
            sur.setR_state(2);
            ma.put("sur",sur);
            heyanService.editSurrender(ma);
            System.out.println(days);
            mv.addObject("s","该保已经超过3个月的退保期限，不予退保");
        }else{
            int s=heyanService.editSurrender(ma);
            BigDecimal qian = new BigDecimal(0.8).multiply(list.getIc_money()).setScale(2,BigDecimal.ROUND_HALF_UP);

            Busilog blog1=new Busilog();//日志信息的描述
            blog.setLog_busimasg("进行退保保险订单状态修改操作");//简单的业务描述
            blog.setLog_addtime(new Date());
            blog.setLog_busitype("修改");//(添加，删除，修改)

            blog.setUn_id(userNumber.getUn_id());
            ma.put("blog",blog);
            ma.put("ic_id",sur.getIc_id());
            int flag = heyanService.editInsur(ma);
            if(flag==1){
                EmailUtil emailUtil = new EmailUtil();
                emailUtil.send_qqmail(u_emal, "【安康保险】", "<body><p>尊敬的用户，您的退保业务已经受理，已经将退保金额打入您的账户，共有"+qian+"元，请注意查收</p></body>");
                mv.addObject("s",s);
                mv.addObject("s1","已经将退保金额打入用户的账户，共有"+qian+"元，请注意查收" );
            }else{
                mv.addObject("s","退保失败，请联系管理员");
            }

        }

        }else{
            int s = heyanService.editSurrender(ma);
            mv.addObject("s",s);
            mv.addObject("s1","审核不通过" );
        }


        // System.out.println("改改改改改改改改改改改");
        //System.out.println(u);

        mv.setView(new MappingJackson2JsonView());
        return mv;
    }

    @RequestMapping("/toxubao-heyan")
    public ModelAndView toxubaoheyan(){
        ModelAndView mv = new ModelAndView();
        List<Renewal>  slist=heyanService.queryRenewal();
        mv.addObject("slist",slist);
        mv.addObject("count", slist.size());
        mv.setViewName("xubao-heyan");
        return mv;
    }

    @RequestMapping("/editRenewal")
    public ModelAndView editRenewal(int ic_id,int r_state,String r_id,HttpSession session) {
        ModelAndView mv = new ModelAndView();
        Map<String, Object> ma = new HashMap<String, Object>();
        Busilog blog = new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行修改保险状态操作");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        UserNumber userNumber = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());


        ma.put("blog", blog);
        //调用后台方法进行退保核验计算
        InsurContract list = heyanService.queryIcByid(ic_id);
        Date addtime = list.getIc_totime();
        int days = (int) ((new Date().getTime() - addtime.getTime()) / (1000*3600*24));
        if(r_state==1){
        //距离保险失效不足30天，或不超过7天可续保
        if(days>7){
            ma.put("r_id",r_id);
            ma.put("r_state",2);
            ma.put("un_shenid",userNumber.getUn_id());
            heyanService.editxubao(ma);
            mv.addObject("s","该保已经超过续保期限，不予续保");
        }else if(days<-30){
            ma.put("r_id",r_id);
            ma.put("r_state",2);
            ma.put("un_shenid",userNumber.getUn_id());
            heyanService.editxubao(ma);
            mv.addObject("s","该保还未到续保时间，不予续保");
        }else if(list.getIc_state()==4){
            ma.put("r_id",r_id);
            ma.put("r_state",2);
            ma.put("un_shenid",userNumber.getUn_id());
            heyanService.editxubao(ma);
            mv.addObject("s","续保申请已经通过，请交费");
        } else if(list.getCars().getChuxian_num()==0){
            BigDecimal qian = new BigDecimal(0.8).multiply(list.getIc_money()).setScale(2,BigDecimal.ROUND_HALF_UP);

            ma.put("ic_money",qian);
            ma.put("ic_id",ic_id);
            int flag = heyanService.editInsurCont(ma);
            if(flag==1){
                ma.put("ic_id",ic_id);
                ma.put("r_id",r_id);
                ma.put("r_state",r_state);
                ma.put("un_shenid",userNumber.getUn_id());
                heyanService.editxubao(ma);
                mv.addObject("s1","1");
                mv.addObject("s","续保审核通过，该用户需要缴纳续保资金"+qian +"元");
            }else{
                mv.addObject("s","续保异常，请重新操作");
            }

        }else{

            ma.put("ic_money",list.getIc_money());
            ma.put("ic_id",ic_id);
            int flag = heyanService.editInsurCont(ma);
            if(flag==1){
                ma.put("r_id",r_id);
                ma.put("r_state",r_state);
                ma.put("un_shenid",userNumber.getUn_id());
                heyanService.editxubao(ma);
                mv.addObject("s1","1");
                mv.addObject("s","续保审核通过，该用户需要缴纳续保资金"+list.getIc_money() +"元");
            }else{
                mv.addObject("s","续保异常，请重新操作");
            }
        }

        }else{
            ma.put("r_id",r_id);
            ma.put("r_state",r_state);
            ma.put("un_shenid",userNumber.getUn_id());
            heyanService.editxubao(ma);
            mv.addObject("s1","1");
        }

        mv.setView(new MappingJackson2JsonView());

        return mv;
    }

    public HeyanService getHeyanService() {
        return heyanService;
    }

    public void setHeyanService(HeyanService heyanService) {
        this.heyanService = heyanService;
    }

    public UserGuanliService getUserGuanli() {
        return userGuanli;
    }

    public void setUserGuanli(UserGuanliService userGuanli) {
        this.userGuanli = userGuanli;
    }
}
