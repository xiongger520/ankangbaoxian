package cn.yunhe.controller;

import cn.yunhe.model.*;
import cn.yunhe.service.IBillService;
import cn.yunhe.service.IBuyInsurService;
import cn.yunhe.service.IInsurInformationBillService;
import cn.yunhe.service.IInsurJiaoFeiService;
import cn.yunhe.utils.EmailUtil;
import org.apache.commons.io.FileUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/24
 * @描述
 */
@Controller
@RequestMapping("/jiaoyi")
public class JiaoyiguanliAction {

    /*购买保险*/
     @Resource
    private IBuyInsurService buyInsurService;
     /*保险信息单*/
     @Resource
    private IInsurInformationBillService insurInformationBillService;
     /*保险缴费*/
     @Resource
    private IInsurJiaoFeiService insurJiaoFeiService;
    /*公司账单图*/
    @Resource
    private IBillService billService;


     /*保险单的导入导出*/
    @RequestMapping("/tobaoxiandan-info")
    public ModelAndView tobaoxiandaninfo(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("baoxiandan-info");
        return mv;
    }
    /*保险单的的上传   两个用一个上传*/
    @RequestMapping("/file")
    public ModelAndView upFiles(MultipartFile file){

        /*定义json视图*/
        ModelAndView mv=new ModelAndView();
        mv.setViewName("baoxiandan-info");

        /*获取原文件名*/
        String fileName=file.getOriginalFilename();
        file.getContentType();
        System.out.println("文件名"+fileName);
        /*目标文件*/
        File dest=new File(fileName);
        try{
            file.transferTo(dest);
            mv.addObject("succ",true);
            mv.addObject("msg","上传成功");

        }catch (IOException e){
            mv.addObject("succ",true);
            mv.addObject("msg","上传失败");
            System.out.println("上传失败");
            e.printStackTrace();
        }catch (Exception e){
            mv.addObject("msg","上传文件过大");
            System.out.println("上传文件不符合标准");
            e.printStackTrace();
        }
        return  mv;
    }
    /*保险单的下载  交强险保险单的下载*/
    @RequestMapping("/download")
    public ResponseEntity<byte[]> downloadFiles(@RequestHeader("User-Agent") String agent)throws Exception{

        File file=new File("C:\\Users\\Administrator\\IdeaProjects\\AnKangBaoXian\\src\\main\\webapp\\uploadsFile\\机动车交通事故责任强制保险单.docx");
        //设置响应的说明和额外的参数
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);

        //由注解@RequestHeader("User-Agent")获取到浏览器的类型来解决中文乱码的问题（因为浏览器的兼容性不同）
        String filename = "";
        if (agent.toUpperCase().contains("CHROME")) {
            filename = new String(file.getName().getBytes("utf-8"),"iso-8859-1");
        }else if (agent.toUpperCase().contains("MSIE")) {
            filename = new String(file.getName().getBytes("iso-8859-1"),"utf-8");
        }

        headers.setContentDispositionFormData("attachment", filename);

        //将文件转成byte数组
        byte[] by = FileUtils.readFileToByteArray(file);
        //三个参数分别是一个文件的byte数组，头部信息以及响应码（201）
        ResponseEntity<byte[]> rn = new ResponseEntity<byte[]>(by,headers, HttpStatus.CREATED);
        return rn;
    }
    /*保险单的下载  商业险的保险单的下载*/
    @RequestMapping("/download2")
    public ResponseEntity<byte[]> downloadFiles2(@RequestHeader("User-Agent") String agent)throws Exception{

        File file=new File("C:\\Users\\Administrator\\IdeaProjects\\AnKangBaoXian\\src\\main\\webapp\\uploadsFile\\商业险保险单.docx");
        //设置响应的说明和额外的参数
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);

        //由注解@RequestHeader("User-Agent")获取到浏览器的类型来解决中文乱码的问题（因为浏览器的兼容性不同）
        String filename = "";
        if (agent.toUpperCase().contains("CHROME")) {
            filename = new String(file.getName().getBytes("utf-8"),"iso-8859-1");
        }else if (agent.toUpperCase().contains("MSIE")) {
            filename = new String(file.getName().getBytes("iso-8859-1"),"utf-8");
        }

        headers.setContentDispositionFormData("attachment", filename);

        //将文件转成byte数组
        byte[] by = FileUtils.readFileToByteArray(file);
        //三个参数分别是一个文件的byte数组，头部信息以及响应码（201）
        ResponseEntity<byte[]> rn = new ResponseEntity<byte[]>(by,headers, HttpStatus.CREATED);
        return rn;
    }


    /*购买保险  查询出所有自选保险*/
    @RequestMapping("/tobaoxiandan-add")
    public ModelAndView queryInsurName(){
        int i=1;
        ModelAndView mv = new ModelAndView();
        List<CarInsur> list=buyInsurService.queryInsurName();
        mv.addObject("list",list);

        List<InsurPack> list1=buyInsurService.queryInsurPackName();
        mv.addObject("list1",list1);
        List<CarInsur> list0 = new ArrayList<CarInsur>();
       /* Map<Integer,String> map = new HashMap<Integer,String>();*/
        for (InsurPack InsurPack:list1) {

            List<Integer> list2 = new ArrayList<Integer>();
            list2.add(InsurPack.getInserInclude().getIi_id());
            list2.add(InsurPack.getInserInclude().getCi1());
            list2.add(InsurPack.getInserInclude().getCi2());
            list2.add(InsurPack.getInserInclude().getCi3());
            list2.add(InsurPack.getInserInclude().getCi4());
            list2.add(InsurPack.getInserInclude().getCi5());
            list2.add(InsurPack.getInserInclude().getCi6());
            list2.add(InsurPack.getInserInclude().getCi7());
            list2.add(InsurPack.getInserInclude().getCi8());
            list2.add(InsurPack.getInserInclude().getCi9());
            list2.add(InsurPack.getInserInclude().getCi10());
            list2.add(InsurPack.getInserInclude().getCi11());
            list2.add(InsurPack.getInserInclude().getCi12());
            list2.add(InsurPack.getInserInclude().getCi13());
            list2.add(InsurPack.getInserInclude().getCi14());
            list2.add(InsurPack.getInserInclude().getCi15());
            list2.add(InsurPack.getInserInclude().getCi16());
            list2.add(InsurPack.getInserInclude().getCi17());
            list2.add(InsurPack.getInserInclude().getCi18());
            list2.add(InsurPack.getInserInclude().getCi19());
            list2.add(InsurPack.getInserInclude().getCi20());
            for (int j=1;j<=20;j++){
               /* System.out.println(list2.get(j));*/
                if(list2.get(j)!=0){
                    CarInsur cari = buyInsurService.queryInsurPackInsur(list2.get(j));
                   /* System.out.println(cari.getCi_name());*/
                    list0.add(cari);
                }

            }
            String key=InsurPack.getIp_name();
            mv.addObject(key,list0);
            list0=new ArrayList<CarInsur>();
           /* System.out.println(InsurPack.getInserInclude());*/
        }

        mv.setViewName("forward:/lay.jsp");
        return mv;
    }
    /*购买保险 */
    @RequestMapping("/addbaoxiandingdan")
    public ModelAndView addbaoxian(@RequestBody List<String> list,HttpSession session){
        ModelAndView mv = new ModelAndView();
        List<Integer> list1 = new ArrayList<Integer>(); /* 自选保险*/
        int list2 =0;   /*保险套餐*/
        String c_id="";  /*车牌号*/
        int flag=0;/* 0：只计算 1：购买 */
        int index=0;
        for (String a:list) {
            if("0".equals(a)){
                ++index;
                continue;
            }
            if(index==0){
                list1.add(Integer.parseInt(a));
            }
            if(index==1){
                list2=Integer.parseInt(a);
            }
            if(index==2){
               c_id=a;
            }
            if(index==3){
                flag=Integer.parseInt(a);
            }
        }
        System.out.println("+++++"+flag);
        Map<String,Object> ma = new HashMap<String, Object>();
        ma.put("list1",list1);
        //获得所有自选险信息
        List<CarInsur> carInsur=new ArrayList<CarInsur>();
        if(list1.size()!=0)
            carInsur=buyInsurService.queryCarInsur(ma);
        /*System.out.println(carInsur.size());*/
        //获得套餐信息
        InsurPack ip = buyInsurService.queryInsurPack(list2);
        List<CarInsur> carInsur1=new ArrayList<CarInsur>();
        List<Integer> carInsur2=new ArrayList<Integer>();//用作比较是否存在重复险
        //获得套餐所包含的保险编号
        if (ip!=null){
            List<Integer> ipid = new ArrayList<Integer>();
            ipid.add(ip.getInserInclude().getCi1());
            ipid.add(ip.getInserInclude().getCi2());
            ipid.add(ip.getInserInclude().getCi3());
            ipid.add(ip.getInserInclude().getCi4());
            Map<String,Object> ma1 = new HashMap<String, Object>();
            ma1.put("list1",ipid);
            //获得套餐中所包含的保险信息
            carInsur1=buyInsurService.queryCarInsur(ma1);
            for (CarInsur a:carInsur1) {
                carInsur2.add(a.getCi_id());
            }


        }
        //获得车辆信息
        Cars car = buyInsurService.queryCarUser(c_id);

        System.out.println(car.getUsers().getU_jifen());
        System.out.println(list1.size()+"======="+list2+"===="+c_id);
        //获得会员信息
        Member member = buyInsurService.queryUsermember(car.getUsers().getU_jifen());
        System.out.println(member.getM_name());

        double zongqian = 0.0;
        double maxbaoe=0.0;


        Map<String,Object> map = new HashMap <String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        //当前登录的账号信息
        UserNumber userNumber= (UserNumber) session.getAttribute("UsernumberRoleUsers");

        blog.setLog_busimasg("添加保险订单");//简单的业务描述

        blog.setLog_addtime(new Date());
        blog.setLog_busitype("添加");//(添加，删除，修改)
        blog.setUn_id(userNumber.getUn_id());//添加当前用户的id
        map.put("blog",blog);
        //计算自选险价钱 （车价*保险费率+保险基础价）*会员优惠率
        for (CarInsur ci: carInsur) {
            maxbaoe+=ci.getMaxmoney().doubleValue();
            zongqian+=(new BigDecimal(ci.getIns_rale()).multiply(car.getC_price())).add(ci.getCi_money()).multiply(new BigDecimal(member.getM_discount())).doubleValue();
            if(!carInsur2.contains(ci.getCi_id()))
            if(flag==1){
                System.out.println("开始添加订单1");
                InsurContract ic = new InsurContract();
                Cars ca = new Cars();
                ca.setC_id(c_id);
                CarInsur cin = new CarInsur();
                cin.setCi_id(ci.getCi_id());
                ic.setCars(ca);
                ic.setCarInsur(cin);
                ic.setIp_id(0);//表示自选险
                ic.setUn_id(userNumber.getUn_id());//获取当前用户id
                ic.setUsername(userNumber.getUsername());
                System.out.println(ic.getUn_id());
                ic.setIc_money((new BigDecimal(ci.getIns_rale()).multiply(car.getC_price())).add(ci.getCi_money()).multiply(new BigDecimal(member.getM_discount())));
                ic.setMax_money(ci.getMaxmoney());
                map.put("blog",blog);
                map.put("baoxian",ic);
                System.out.println("自选："+ic.getCarInsur().getCi_id());
                buyInsurService.addbaoxian(map);
            }
        }
        System.out.println(zongqian);
        //计算套餐险价钱 （车价*保险费率+保险基础价）*套餐优惠率*会员优惠率
        for (CarInsur ci:carInsur1) {
            maxbaoe+=ci.getMaxmoney().doubleValue();
            zongqian+=(new BigDecimal(ci.getIns_rale()).multiply(car.getC_price())).add(ci.getCi_money()).multiply(new BigDecimal(ip.getIp_rate())).multiply(new BigDecimal(member.getM_discount())).doubleValue();
            if(flag==1){
                System.out.println("开始添加订单2");
                InsurContract ic = new InsurContract();
                Cars ca = new Cars();
                ca.setC_id(c_id);
                CarInsur cin = new CarInsur();
                cin.setCi_id(ci.getCi_id());
                ic.setCars(ca);
                ic.setCarInsur(cin);
                ic.setIp_id(ci.getCi_id());//表示套餐险
                //获取当前用户id
                ic.setUsername(userNumber.getUsername());
                ic.setUn_id(userNumber.getUn_id());
                ic.setIc_money((new BigDecimal(ci.getIns_rale()).multiply(car.getC_price())).add(ci.getCi_money()).multiply(new BigDecimal(ip.getIp_rate())).multiply(new BigDecimal(member.getM_discount())));
                ic.setMax_money(ci.getMaxmoney());
                map.put("blog",blog);
                map.put("baoxian",ic);
                System.out.println("套餐："+ic.getCarInsur().getCi_id());
                System.out.println("111111"+ic.getUn_id());
                buyInsurService.addbaoxian(map);
            }
        }
        System.out.println(maxbaoe);
        System.out.println(zongqian);
        mv.addObject("zongqian",zongqian);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }
    /*通过用户查用户的车牌号*/
    @RequestMapping("/queryCarByUserName")
    public ModelAndView queryCarByUserName(String username){
        ModelAndView mv = new ModelAndView();
        List<Cars> list = buyInsurService.queryCaridByUser(username);
        mv.addObject("carList",list);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }
    /*通过用户身份证查用户的车牌号*/
    @RequestMapping("/queryCaridByIdcard")
    public ModelAndView queryCaridByIdcard(String idcard){
        ModelAndView mv = new ModelAndView();
        List<Cars> list2 = buyInsurService.queryCaridByIdcard(idcard);
        mv.addObject("carList2",list2);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }
    /*通过id查出保险的详细内容*/
    @RequestMapping("/queryCarInsurById")
    public ModelAndView queryCarInsurById(int id) {
       ModelAndView mv=new ModelAndView();
       CarInsur list3=buyInsurService.queryCarInsurById(id);
        System.out.println(list3);
       mv.addObject("baoxianlist",list3);
       mv.setViewName("baoxian-xinix");
       return mv;
    }
    /*保险单信息管理*/
    @RequestMapping("/tobaoxiandan-list")
    public ModelAndView tobaoxiandanlist(){
        ModelAndView mv = new ModelAndView();

        /*查询保险订单所有内容  将数据遍历到页面上*/
        List<InsurContract> list=insurInformationBillService.queryInsurContract();
        mv.addObject("list",list);

        mv.setViewName("baoxiandan-list");
        return mv;
    }
    /*保险单的下载  商业险的保险单的下载*/
    @RequestMapping("/download3")
    public ResponseEntity<byte[]> downloadFiles3(@RequestHeader("User-Agent") String agent,String uploadsFile)throws Exception{
        URLEncoder.encode(uploadsFile,"utf-8");
        System.out.println(uploadsFile);
         String path="C:\\Users\\Administrator\\IdeaProjects\\AnKangBaoXian\\src\\main\\webapp\\uploadsFile\\"+uploadsFile;
        File file=new File(path);
        //设置响应的说明和额外的参数
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);

        //由注解@RequestHeader("User-Agent")获取到浏览器的类型来解决中文乱码的问题（因为浏览器的兼容性不同）
        String filename = "";
        if (agent.toUpperCase().contains("CHROME")) {
            filename = new String(file.getName().getBytes("utf-8"),"iso-8859-1");
        }else if (agent.toUpperCase().contains("MSIE")) {
            filename = new String(file.getName().getBytes("iso-8859-1"),"utf-8");
        }
        ResponseEntity<byte[]> rn=null;
       try{
           headers.setContentDispositionFormData("attachment", filename);
           //将文件转成byte数组
           byte[] by = FileUtils.readFileToByteArray(file);
           //三个参数分别是一个文件的byte数组，头部信息以及响应码（201）
            rn = new ResponseEntity<byte[]>(by,headers, HttpStatus.CREATED);
       }catch (FileNotFoundException e){
              e.printStackTrace();
            path="C:\\Users\\Administrator\\IdeaProjects\\AnKangBaoXian\\src\\main\\webapp\\uploadsFile\\暂无有效文件.txt";
            file=new File(path);
            headers = new HttpHeaders();
           headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);

           //由注解@RequestHeader("User-Agent")获取到浏览器的类型来解决中文乱码的问题（因为浏览器的兼容性不同）

           if (agent.toUpperCase().contains("CHROME")) {
               filename = new String(file.getName().getBytes("utf-8"),"iso-8859-1");
           }else if (agent.toUpperCase().contains("MSIE")) {
               filename = new String(file.getName().getBytes("iso-8859-1"),"utf-8");
           }

           headers.setContentDispositionFormData("attachment", filename);
               //将文件转成byte数组
           byte[] by = FileUtils.readFileToByteArray(file);
           rn = new ResponseEntity<byte[]>(by,headers, HttpStatus.CREATED);
           System.out.println(e.toString());
       }finally {
           return rn;
       }
    }


    /*保险缴费*/
    @RequestMapping("/tobaoxian-jiaofei")
    public ModelAndView tobaoxianjiaofei(){
        ModelAndView mv = new ModelAndView();

        /*查询已审核保险订单所有内容  将数据遍历到页面上*/
        List<InsurContract> list=insurJiaoFeiService.queryInsurContracts();
        mv.addObject("list",list);

        mv.setViewName("baoxian-jiaofei");
        return mv;
    }
    @RequestMapping("/editJiaoFeiNumber")
    @ResponseBody
    public String editJiaoFeiNumber(InsurContract insurContract,@RequestParam("u_id") int u_id,@RequestParam("u_phone") String u_phone,@RequestParam("u_emal") String u_emal,HttpSession session ){
        ModelAndView mv = new ModelAndView();
        Map<String,Object> map = new HashMap <String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        if(insurContract.getIc_ispayment()==1 ){
            blog.setLog_busimasg("已缴费"+insurContract.getIc_id());//简单的业务描述
        }else {
            blog.setLog_busimasg("未缴费"+insurContract.getIc_id());//简单的业务描述
        }

        //当前登录的账号信息
        UserNumber userNumber= (UserNumber) session.getAttribute("UsernumberRoleUsers");

        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        blog.setUn_id(userNumber.getUn_id());//添加当前用户的id

        map.put("insurContract",insurContract);
        map.put("blog",blog);
        boolean flag=insurJiaoFeiService.editJiaoFeiNumber(map);
        if(flag){
             System.out.println("phone"+u_phone);//手机号
             /*--------------------------进行邮箱提醒------------------------*/
             System.out.println("qqEmail"+u_emal);
              EmailUtil. send_qqmail(u_emal, "中国安康保险公司发送", "<body><p>【安康保险】尊敬的用户，恭喜你已成功缴费！祝你生活愉快！！</p></body>");


            CarInsurRecord carInsurRecord = new CarInsurRecord();
            carInsurRecord.setInsurContract(insurContract);
            carInsurRecord.setCri_money(insurContract.getIc_money());
            carInsurRecord.setUn_id(userNumber.getUn_id());   //以后从session中取当前登录账号的信息
            carInsurRecord.setNu_mumber(userNumber.getUn_pwdsalt());//以后从session中取当前登录账号的信息
            map.put("carI",carInsurRecord);
            insurJiaoFeiService.addCarInsurRecord(map);
            String jifen = insurContract.getIc_money().divide(new BigDecimal("100")).toString();
            System.out.println("+++++++++"+jifen);
           double jife =  Double.parseDouble(jifen);
           int jif =(int)Math.floor(jife);
           System.out.println("+++++++++"+jif+"---"+u_id);
           Users user = new Users();
           user.setU_id(u_id);
           user.setU_jifen(jif);
           map.put("user",user);
           insurJiaoFeiService.editUserJiFen(map);//进行1:100的比率进行积分增加

        }
        return "0";
    }

     /*公司账单图*/
     @RequestMapping("/tobaoxiandan-bill")
     public ModelAndView tobaoxiandanbill(){
         ModelAndView mv = new ModelAndView();
         List<JiaoBill> list=billService.queryCar_insur_record();
         /*for (JiaoBill jiaoBill:list) {
             System.out.println(jiaoBill.getYear_cir_addtime()+","+jiaoBill.getMonth_cir_addtime()+","+jiaoBill.getCir_money());
         }*/
         List<PeiBill> list1=billService.queryClai_record();
         for (PeiBill peiBill:list1) {
             System.out.println(peiBill.getPei_year_cir_addtime()+","+peiBill.getPei_month_cir_addtime()+","+peiBill.getPei_cir_money());
         }
         mv.addObject("list",list);
         mv.addObject("list1",list1);
         mv.setViewName("baoxiandan-bill");
         return mv;
     }

     /*登录后修改密码*/
     @RequestMapping("/toEditpwd")
     public ModelAndView toEditpwd(){
         ModelAndView mv = new ModelAndView();
         mv.setViewName("personal-edit");
         return mv;
     }
    /*登录后修改密码*/
    @RequestMapping("/toEditpwd2")
    public ModelAndView toEditpwd2(String newname,String oldpassword,String newpassword, HttpSession session){
        ModelAndView mv = new ModelAndView();
         /*mv.setView(new MappingJackson2JsonView());*/
         Map<String,Object> map = new HashMap<String,Object>();
         Busilog blog=new Busilog();//日志信息的描述
        //当前登录账号信息
         UserNumber userNumber= (UserNumber) session.getAttribute("UsernumberRoleUsers");

        //ByteSource
        ByteSource oldsalt = ByteSource.Util.bytes(userNumber.getUsername());
        //将旧密码加密
        SimpleHash oldsh = new SimpleHash("MD5",oldpassword,oldsalt,1024);
        oldpassword = oldsh.toString();

         blog.setLog_busimasg("修改密码"+userNumber.getUsername());//简单的业务描述
         blog.setLog_addtime(new Date());
         blog.setLog_busitype("修改");//(添加，删除，修改)
         blog.setUn_id(userNumber.getUn_id());

         //更改后的账号信息
         UserNumber usernumber = new UserNumber();

        //ByteSource
        ByteSource salt = ByteSource.Util.bytes(userNumber.getUsername());
        //将密码加密
        SimpleHash sh = new SimpleHash("MD5",newpassword,salt,1024);
        System.out.println(sh);
        String password = sh.toString();

        usernumber.setUsername(userNumber.getUsername());
         usernumber.setPassword(password);
         usernumber.setUn_nickname(newname);
         int flag = 0;
         if(oldpassword.equals(userNumber.getPassword())){
             map.put("usernumber",usernumber);
             map.put("blog",blog);
             flag=billService.editPwd_name(map);
             System.out.println("2222222222222");
             System.out.println(flag);

             if (flag==1){
                 session.invalidate();
                 mv.addObject("flag",1);
                 mv.setViewName("personal-edit");
             }

         }
         if(!oldpassword.equals(userNumber.getPassword())){
           session.setAttribute("error","旧密码输入错误！");
           mv.setViewName("personal-edit");
         }

        return mv;
    }

     /*get,set方法*/
     /*购买保险*/
    public IBuyInsurService getBuyInsurService() {
        return buyInsurService;
    }
    public void setBuyInsurService(IBuyInsurService buyInsurService) {
        this.buyInsurService = buyInsurService;
    }
    /*保险信息单*/
    public IInsurInformationBillService getInsurInformationBillService() {
        return insurInformationBillService;
    }
    public void setInsurInformationBillService(IInsurInformationBillService insurInformationBillService) {
        this.insurInformationBillService = insurInformationBillService;
    }
    /*保险缴费*/
    public IInsurJiaoFeiService getInsurJiaoFeiService() {
        return insurJiaoFeiService;
    }
    public void setInsurJiaoFeiService(IInsurJiaoFeiService insurJiaoFeiService) {
        this.insurJiaoFeiService = insurJiaoFeiService;
    }
    /*公司账单图*/
    public IBillService getBillService() {
        return billService;
    }
    public void setBillService(IBillService billService) {
        this.billService = billService;
    }
}
