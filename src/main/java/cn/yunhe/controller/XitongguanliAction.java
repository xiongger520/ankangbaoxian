package cn.yunhe.controller;

import cn.yunhe.model.Busilog;
import cn.yunhe.model.Dept;
import cn.yunhe.model.UserNumber;
import cn.yunhe.service.IBusilogService;
import cn.yunhe.service.IDeptService;
import cn.yunhe.service.IUserNumberService;
import com.github.pagehelper.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/24
 * @描述
 */
@RestController
@RequestMapping("/xitong")
public class XitongguanliAction {

    @Resource
    private IBusilogService busilogService;
    @Resource
    private IUserNumberService userNumberService;
    @Resource
    private IDeptService deptService;

    @RequestMapping("/tosystem-log")
    public ModelAndView tosystemlog(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("system-log");
        return mv;
    }

    /**
     * 获取所有日志信息
     * @return
     */
    @RequestMapping("/queryAllBusilog")
    public ModelAndView queryAllBusilog(){
        ModelAndView mv = new ModelAndView();
        List<Busilog> list = busilogService.queryAllBusilog();
        mv.addObject("list",list);
        mv.setViewName("system-log");
        return mv;
    }

    @RequestMapping("/queryLikeBusilog")
    public ModelAndView queryLikeBusilog(int pageNum, int pageSize,String begintime,String endtime,Busilog busilog){
        ModelAndView mv = new ModelAndView();
        Map map = new HashMap();
        map.put("begintime",begintime);
        map.put("endtime",endtime);
        map.put("busilog",busilog);
        map.put("pageSize", pageSize);
        map.put("pageNum", pageNum);
        Page page = busilogService.queryLikeBusilog(map);
        map.put("page", page);
        map.put("totalPage", page.getPages());
        List<Busilog> list = page.getResult();
        map.put("result",list);
        mv.addObject("totalPage",page.getPages());
        mv.addObject("result",list);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }
        @RequestMapping("/queryBusilogByTiaojian")
    public Map queryBusilogByTiaojian(String tiaojian){
        ModelAndView mv = new ModelAndView();
        List<Busilog> list = busilogService.queryBusilogByTiaojian(tiaojian);
        Map map = new HashMap();
        map.put("result",list);
        return map;
    }

    @RequestMapping("/toadmin-zaixian")
    public ModelAndView toadminzaixian(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/xitong/queryAllUserNumber.do");
        return mv;
    }

    @RequestMapping("/queryAllUserNumber")
    public ModelAndView queryAllUserNumber(){
        ModelAndView mv = new ModelAndView();
        List<UserNumber> list = userNumberService.queryAllUserNumber();
        mv.addObject("list",list);
        mv.addObject("count",list.size());
        mv.setViewName("admin-zaixian");
        return mv;
    }

    @RequestMapping("/editUserNumber")
    public String editUserNumber(UserNumber userNumber, HttpSession session){
        ModelAndView mv = new ModelAndView();
        Map<String,Object> map = new HashMap <String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        if(userNumber.getUn_state()==1){
            blog.setLog_busimasg("冻结账号"+userNumber.getUn_id());//简单的业务描述
        }else {
            blog.setLog_busimasg("解冻账号"+userNumber.getUn_id());//简单的业务描述
        }
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber un = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(un.getUn_id());

        map.put("userNumber",userNumber);
        map.put("blog",blog);
        userNumberService.editUserNumber(map);
        return "1";
    }

    @RequestMapping("/tozhidian")
    public ModelAndView tozhidian(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/xitong/queryLikeDept.do");
        return mv;
    }

    @RequestMapping("/queryLikeDept")
    public ModelAndView queryLikeDept(){
        ModelAndView mv = new ModelAndView();
        List<Dept> list = deptService.queryLikeDept();
        mv.addObject("list",list);
        mv.addObject("count",list.size());
        mv.setViewName("zhidian");
        return mv;
    }

    @RequestMapping("/addZhiDian")
    public ModelAndView addZhiDian(Dept dept,HttpSession session){
        dept.setAddtime(new Date());
        ModelAndView mv = new ModelAndView();
        mv.setView(new MappingJackson2JsonView());
        System.out.println(dept.getAddtime());
        Map<String,Object> map = new HashMap <String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("添加支点"+dept.getD_name());//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("添加");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber userNumber = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());
        map.put("dept",dept);
        map.put("blog",blog);
        int flag = deptService.addDept(map);
        return mv;
    }

    @RequestMapping("/delDeptById")
    public ModelAndView delDeptById(Dept dept,HttpSession session){
        ModelAndView mv = new ModelAndView();
        Map<String,Object> map = new HashMap <String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("删除支点"+dept.getD_id());//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("删除");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber userNumber = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());
        map.put("dept",dept);
        map.put("blog",blog);
        int flag = deptService.delDept(map);
        mv.addObject("flag",flag);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }

    @RequestMapping("/delDepts")
    public ModelAndView delDepts(@RequestBody List<String> dept,HttpSession session){
        ModelAndView mv = new ModelAndView();
        Map<String,Object> ma = new HashMap<String, Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("删除"+dept.size()+"个支点"+dept);//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("删除");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber userNumber = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());
        ma.put("blog",blog);
        ma.put("dept",dept);
        int flag = deptService.delDepts(ma);
        mv.addObject("flag",flag);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }

    @RequestMapping("/toEditDept")
    public ModelAndView toEditDept(Dept dept){
        ModelAndView mv = new ModelAndView();
        Dept d = deptService.queryDeptById(dept);
        mv.addObject("dept",d);
        mv.setViewName("zhidian-edit");
        return mv;
    }

    @RequestMapping("/editDept")
    public ModelAndView editDept(Dept dept,HttpSession session){
        ModelAndView mv = new ModelAndView();
        mv.setView(new MappingJackson2JsonView());
        dept.setAddtime(new Date());
        Map<String,Object> map = new HashMap <String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("修改支点"+dept.getD_name());//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber userNumber = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());
        map.put("dept",dept);
        map.put("blog",blog);
        int flag = deptService.editDept(map);
        return mv;
    }

    @RequestMapping("tozhiDianXiangQinag")
    public ModelAndView tozhiDianXiangQinag(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("zhiodian-xiangqing");
        return mv;
    }
    @RequestMapping("tozhidianAdd")
    public ModelAndView tozhidianAdd(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("zhidian-add");
        return mv;
    }

    public IUserNumberService getUserNumberService() {
        return userNumberService;
    }

    public void setUserNumberService(IUserNumberService userNumberService) {
        this.userNumberService = userNumberService;
    }

    public IDeptService getDeptService() {
        return deptService;
    }

    public void setDeptService(IDeptService deptService) {
        this.deptService = deptService;
    }

    public IBusilogService getBusilogService() {
        return busilogService;
    }

    public void setBusilogService(IBusilogService busilogService) {
        this.busilogService = busilogService;
    }
}
