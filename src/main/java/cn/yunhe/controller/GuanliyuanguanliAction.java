package cn.yunhe.controller;

import cn.yunhe.dao.IPowerMapper;
import cn.yunhe.model.*;
import cn.yunhe.service.IRoleService;
import cn.yunhe.service.IUserNumberService;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/24
 * @描述
 */
@Controller
@RequestMapping("/guanliyuan")
public class GuanliyuanguanliAction {

    @Resource
    private IRoleService roleService;
    @Resource
    private IUserNumberService userNumberService;
   @Resource
    private IPowerMapper powerMapper;

    @RequestMapping("/toadmin-role")
    public ModelAndView toadminrole(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/guanliyuan/queryAllRole.do");
        return mv;
    }
    /**
     * 测试获取全部角色
     * @return
     */
    @RequestMapping("/queryAllRole")
    public ModelAndView queryAllRole(){
        ModelAndView mv = new ModelAndView();
        List<Role> list = roleService.getAllRole();
        mv.setView(new MappingJackson2JsonView());
        mv.addObject("roleList",list);
        mv.addObject("count",list.size());
        mv.setViewName("admin-role");
        return mv;
    }

    @RequestMapping("/toadmin-role-add")
    public ModelAndView toadminroleadd(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("admin-role-add");
        return mv;
    }
    @RequestMapping("/delRole")
    public ModelAndView delRole(Role role,HttpSession session){
        ModelAndView mv = new ModelAndView();
        mv.setView(new MappingJackson2JsonView());
        Map<String,Object> map = new HashMap<String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("删除角色"+role.getR_id());//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("删除");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber userNumber = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());
        map.put("role",role);
        map.put("blog",blog);
        int flag = roleService.delRole(map);
        mv.addObject("flag",flag);
        return mv;
    }

    @RequestMapping("/delRoles")
    public ModelAndView delRoles(@RequestBody List<String> r_id,HttpSession session){
        ModelAndView mv = new ModelAndView();
        Map<String,Object> ma = new HashMap<String, Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("删除"+r_id.size()+"个角色"+r_id);//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("删除");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber userNumber = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());
        ma.put("blog",blog);
        ma.put("role",r_id);
        int flag = roleService.delRoles(ma);
        mv.addObject("f",flag);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }

    @RequestMapping("/addRole")
    public ModelAndView addRole(Role role,HttpSession session){
        ModelAndView mv = new ModelAndView();
        mv.setView(new MappingJackson2JsonView());
        role.setR_addtime(new Date());
        Map<String,Object> map = new HashMap<String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("新增角色"+role.getR_name());//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("添加");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber userNumber = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());
        map.put("role",role);
        map.put("blog",blog);
        int flag = roleService.addRole(map);
        System.out.println(flag);
        return mv;
    }
    @RequestMapping("/queryRole")
    public ModelAndView queryRole(Role role){
        ModelAndView mv = new ModelAndView();
        Role r = roleService.queryRoleById(role);
        mv.addObject("role1",r);
        mv.setViewName("admin-role-edit");
        return mv;
    }
    @RequestMapping("/editRole")
    public ModelAndView editRole(Role role,HttpSession session){
        role.setR_addtime(new Date());
        ModelAndView mv = new ModelAndView();
        mv.setView(new MappingJackson2JsonView());
        Map<String,Object> map = new HashMap<String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("修改角色"+role.getR_id());//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber userNumber = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());
        role.setUn_id(userNumber.getUn_id());
        map.put("role",role);
        map.put("blog",blog);
        int flag = roleService.editRoleById(map);
        System.out.println(flag);
        return mv;
    }

    @RequestMapping("/toadmin-list")
    public ModelAndView toadminlist(){
        ModelAndView mv = new ModelAndView();
        List<UserNumber> list = userNumberService.queryAllUserNumber();
        mv.addObject("list",list);
        mv.addObject("count",list.size());
        mv.setViewName("admin-list");
        return mv;
    }

    @RequestMapping("/toadmin-add")
    public ModelAndView toadminadd(){
        ModelAndView mv = new ModelAndView();
        List<Role> list = roleService.getAllRole();
        mv.addObject("list",list);
        mv.setViewName("admin-add");
        return mv;
    }
    @RequestMapping("/toadmin-edit")
    public ModelAndView toadminedit(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("admin-edit");
        return mv;
    }
    @RequestMapping("/addUserNumber")
    public ModelAndView addUserNumber(UserNumber userNumber,Role role,Users users,HttpSession session){
        ModelAndView mv = new ModelAndView();
        userNumber.setUsers(users);
        userNumber.setRole(role);
        Map<String,Object> map = new HashMap<String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("新增账号"+userNumber.getUn_id());//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("添加");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber un = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(un.getUn_id());

        //ByteSource
        ByteSource salt = ByteSource.Util.bytes(userNumber.getUsername());
        String password = userNumber.getPassword();
        //将密码加密
        SimpleHash sh = new SimpleHash("MD5",password,salt,1024);
        System.out.println(sh);
        password = sh.toString();
        userNumber.setPassword(password);
        map.put("userNumber",userNumber);
        map.put("blog",blog);
        int flag = userNumberService.addUserNumber(map);
        if(flag>0){
            mv.setViewName("redirect:/guanliyuan/toadmin-list.do");
        }else {
            mv.setViewName("admin-add");
        }
        return mv;
    }

    @RequestMapping("/delUserNumber")
    public ModelAndView delUserNumber(UserNumber userNumber,HttpSession session){
        ModelAndView mv = new ModelAndView();
        mv.setView(new MappingJackson2JsonView());
        Map<String,Object> map = new HashMap<String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("删除账号"+userNumber.getUn_id());//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("删除");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber un = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(un.getUn_id());
        map.put("userNumber",userNumber);
        map.put("blog",blog);
        int flag = userNumberService.delUserNumber(map);
        mv.addObject("flag",flag);
        return mv;
    }

    @RequestMapping("/delUserNumbers")
    public ModelAndView delUserNumbers(@RequestBody List<String> userNumber,HttpSession session){
        ModelAndView mv = new ModelAndView();
        Map<String,Object> ma = new HashMap<String, Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("删除"+userNumber.size()+"个管理员"+userNumber);//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("删除");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber un = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(un.getUn_id());
        ma.put("blog",blog);
        ma.put("userNumber",userNumber);
        int flag = userNumberService.delUserNumbers(ma);
        mv.addObject("flag",flag);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }

    @RequestMapping("/queryUserNumberById")
    public ModelAndView queryUserNumberById(UserNumber userNumber){
        ModelAndView mv = new ModelAndView();
        List<Role> list = roleService.getAllRole();
        mv.addObject("list",list);
        UserNumber u = userNumberService.queryUserNumberById(userNumber);
        mv.setView(new MappingJackson2JsonView());
        mv.addObject("userNumber",u);
        mv.setViewName("admin-edit");
        return mv;
    }

    @RequestMapping("/queryUserNumber")
    public ModelAndView queryUserNumber(UserNumber userNumber){
        ModelAndView mv = new ModelAndView();
        List<Role> list = roleService.getAllRole();
        mv.addObject("list",list);
        UserNumber u = userNumberService.queryUserNumberById(userNumber);
        mv.setView(new MappingJackson2JsonView());
        mv.addObject("userNumber",u);
        mv.setViewName("admin-xiangqing");
        return mv;
    }

    @RequestMapping("/queryUser")
    public ModelAndView queryUser(UserNumber userNumber){
        ModelAndView mv = new ModelAndView();
        List<Role> list = roleService.getAllRole();
        mv.addObject("list",list);
        UserNumber u = userNumberService.queryUserNumberById(userNumber);
        mv.setView(new MappingJackson2JsonView());
        mv.addObject("userNumber",u);
        mv.setViewName("user-xiangqing");
        return mv;
    }

    @RequestMapping("/editUserNumberById")
    public ModelAndView editUserNumberById(UserNumber userNumber,Role role,HttpSession session){
        ModelAndView mv = new ModelAndView();
        userNumber.setRole(role);
        mv.setView(new MappingJackson2JsonView());
        Map<String,Object> map = new HashMap<String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("修改账号"+userNumber.getUn_id());//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber un = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(un.getUn_id());
        map.put("userNumber",userNumber);
        map.put("blog",blog);
        int flag = userNumberService.editUserNumberById(map);
        mv.addObject("flag",flag);
        return mv;
    }


    @RequestMapping("/toPowerList")
    public ModelAndView toPowerList(){
        ModelAndView mv = new ModelAndView();
        List<Power> list = powerMapper.queryAllPower();
        mv.addObject("list",list);
        mv.addObject("count",list.size());
        mv.setViewName("admin-power");
        return mv;
    }

    @RequestMapping("/power-add")
    public ModelAndView toAddPower(){
        ModelAndView mv = new ModelAndView();
        mv.setView(new MappingJackson2JsonView());
        mv.setViewName("admin-power-add");
        return mv;
    }

    @RequestMapping("/addPower")
    public ModelAndView addPower(Power power,HttpSession session){
        ModelAndView mv = new ModelAndView();
        Map<String,Object> map = new HashMap<String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("添加权限"+power.getP_name());//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("添加");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber un = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(un.getUn_id());
        power.setUserNumber(un);
        power.setAddtime(new Date());
        map.put("blog",blog);
        map.put("power",power);
        int flag = powerMapper.addPower(map);
        mv.addObject("flag",flag);
        return mv;
    }

    @RequestMapping("/toPowerEdit")
    public ModelAndView toPowerEdit(Power power){
        ModelAndView mv = new ModelAndView();
        Power p = powerMapper.queryPowerById(power);
        mv.addObject("power",p);
        mv.setViewName("admin-power-edit");
        return mv;
    }

    @RequestMapping("/editPower")
    public ModelAndView editPower(Power power,HttpSession session){
        ModelAndView mv = new ModelAndView();
        mv.setView(new MappingJackson2JsonView());
        Map<String,Object> map = new HashMap<String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("修改权限"+power.getP_id());//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber un = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(un.getUn_id());
        power.setUserNumber(un);
        power.setAddtime(new Date());
        map.put("blog",blog);
        map.put("power",power);
        int flag = powerMapper.editPower(map);
        mv.addObject("flag",flag);
        return mv;
    }

    @RequestMapping("/delPower")
    public ModelAndView delPower(Power power,HttpSession session){
        ModelAndView mv = new ModelAndView();
        mv.setView(new MappingJackson2JsonView());
        Map<String,Object> map = new HashMap<String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("删除权限"+power.getP_id());//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("删除");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber un = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(un.getUn_id());
        power.setAddtime(new Date());
        map.put("blog",blog);
        map.put("power",power);
        int flag = powerMapper.delPower(map);
        mv.addObject("flag",flag);
        return mv;
    }

    @RequestMapping("/delPowers")
    public ModelAndView delPowers(@RequestBody List<String> p_id,HttpSession session){
        ModelAndView mv = new ModelAndView();
        Map<String,Object> ma = new HashMap<String, Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("删除"+p_id.size()+"个角色"+p_id);//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("删除");//(添加，删除，修改)
        //获取session中的当前登录用户id
        UserNumber userNumber = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());
        ma.put("blog",blog);
        ma.put("power",p_id);
        int flag = powerMapper.delPowers(ma);
        mv.addObject("flag",flag);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }

    public IPowerMapper getPowerMapper() {
        return powerMapper;
    }

    public void setPowerMapper(IPowerMapper powerMapper) {
        this.powerMapper = powerMapper;
    }

    public IUserNumberService getUserNumberService() {
        return userNumberService;
    }

    public void setUserNumberService(IUserNumberService userNumberService) {
        this.userNumberService = userNumberService;
    }

    public IRoleService getRoleService() {
        return roleService;
    }

    public void setRoleService(IRoleService roleService) {
        this.roleService = roleService;
    }
}
