package cn.yunhe.controller;

import cn.yunhe.model.Users;
import cn.yunhe.service.impl.UsersServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/24
 * @描述
 */
@Controller
@RequestMapping("/yonghu")
public class YonghuguanliAction {
    @Resource(name = "userService")
    private UsersServiceImpl usersServiceImpl;

    @RequestMapping("/tomember-add")
    public ModelAndView tomemberadd(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("member-add");
        return mv;
    }

    @RequestMapping("/tocar-add")
    public ModelAndView tocaradd(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("car-add");
        return mv;
    }

    @RequestMapping("/tomember-list")
    public ModelAndView tomemberlist(){
        ModelAndView mv = new ModelAndView();

        mv.setViewName("member-list");
        return mv;
    }

    @RequestMapping("/tomember-del")
    public ModelAndView tomemberdel(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("member-del");
        return mv;
    }

    @RequestMapping("/tocar-list")
    public ModelAndView tocarlist(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("car-list");
        return mv;
    }

    @RequestMapping("/tocar-edit")
    public ModelAndView tocaredit(String c_id){
        ModelAndView mv = new ModelAndView();
        System.out.println(222);
        mv.addObject("c_id",c_id);
        mv.setViewName("car-edit");
        return mv;
    }

    @RequestMapping("/touser-edit")
    public ModelAndView touseredit(Users user){
        ModelAndView mv = new ModelAndView();
        System.out.println(user.getU_id());
        Users use=usersServiceImpl.queryUser(user.getU_id());
        mv.addObject("user",use);
        mv.setViewName("user-edit");
        return mv;
    }
}
