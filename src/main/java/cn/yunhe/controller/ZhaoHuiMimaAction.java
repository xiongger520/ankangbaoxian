package cn.yunhe.controller;

import cn.yunhe.model.Busilog;
import cn.yunhe.model.UserNumber;
import cn.yunhe.service.IZhaoHuiMimaService;
import cn.yunhe.service.impl.ZhaoHuiMimaServiceImpl;
import cn.yunhe.utils.IndustrySMS;
import cn.yunhe.utils.RandomStringUtil;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/zhaohui")
public class ZhaoHuiMimaAction {
    @Resource
    private IZhaoHuiMimaService iZhaoHuiMimaService;
    private static String phone="";
    private static String username="";
    private static String yanzheng="";
    private static int userid=0;

    @RequestMapping(value = "/yanzheng")
    public ModelAndView yanzheng(UserNumber userNumber,HttpSession session){
        ModelAndView mv = new ModelAndView();
        System.out.println("userNumber"+userNumber.getUn_pwdsalt());
        UserNumber userNumber1=iZhaoHuiMimaService.queryUserNumber(userNumber);
        if (userNumber1==null){
            session.setAttribute("zh_flag","该账户不存在！");
            mv.setViewName("redirect:/mimazhaohui.jsp");
        }else if (!userNumber1.getUn_pwdsalt().equals(userNumber.getUn_pwdsalt())){
            session.setAttribute("zh_flag","手机号不正确！");
            mv.setViewName("redirect:/mimazhaohui.jsp");
        }else if(userNumber1.getUn_state()==0){
            session.setAttribute("zh_flag","该账号被冻结，无法修改！");
            mv.setViewName("redirect:/mimazhaohui.jsp");
        }else{
            phone=userNumber.getUn_pwdsalt();
            username=userNumber.getUsername();
            userid=userNumber1.getUn_id();
            session.removeAttribute("zh_flag");
            mv.setViewName("redirect:/querenzhaohui.jsp");
        }
        return mv;
    }

    @RequestMapping(value = "/zhaohui")
    public ModelAndView zhaohui(UserNumber userNumber,String duanxin,HttpSession session){
        ModelAndView mv = new ModelAndView();
        System.out.println("userNumber"+userNumber.getUn_pwdsalt());
        if(!duanxin.equals(yanzheng)){
            session.setAttribute("zhh_flag","验证码不正确");
            mv.setViewName("redirect:/mimazhaohui.jsp");
        }else{
            Map<String,Object> ma = new HashMap<String, Object>();
            Busilog blog=new Busilog();//日志信息的描述
            blog.setLog_busimasg("进行用户登录操作");//简单的业务描述
            blog.setLog_addtime(new Date());
            blog.setLog_busitype("登录");//(添加，删除，修改)
            blog.setUn_id(userid);//session中的当前用户id
            ma.put("blog",blog);
            userNumber.setUn_pwdsalt(phone);
            userNumber.setUsername(username);

            //密码加密
            String password = userNumber.getPassword();
            //ByteSource
            ByteSource salt = ByteSource.Util.bytes(username);
            //将密码加密
            SimpleHash sh = new SimpleHash("MD5",password,salt,1024);
            System.out.println(sh);
            password = sh.toString();
            userNumber.setPassword(password);

            ma.put("user",userNumber);
            int flag =  iZhaoHuiMimaService.editUserNumberpassword(ma);
            if(flag==1){
                session.removeAttribute("zhh_flag");
                phone="";
                username="";
                yanzheng="";
                userid=0;
                mv.setViewName("redirect:/login.jsp");
            }

        }


        return mv;
    }

    @RequestMapping("/captcha")
    public ModelAndView Captcha(@RequestParam(value="user" ,required =false )UserNumber user, HttpSession session) {
        ModelAndView mv = new ModelAndView();
        //已经查询回来手机号码了。
        String msg = RandomStringUtil.getRandomCode(6,0);
        String smsContent = "【安康保险】尊敬的用户，您的验证码为"+msg;
        System.out.println("".equals(phone));
        if ("".equals(phone)){
            mv.addObject("flag",2);
            mv.setView(new MappingJackson2JsonView());
            return mv;
        }
        IndustrySMS.execute(phone,smsContent);
        yanzheng=msg;
        mv.addObject("flag",1);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }

    public IZhaoHuiMimaService getiZhaoHuiMimaService() {
        return iZhaoHuiMimaService;
    }

    public void setiZhaoHuiMimaService(IZhaoHuiMimaService iZhaoHuiMimaService) {
        this.iZhaoHuiMimaService = iZhaoHuiMimaService;
    }
}
