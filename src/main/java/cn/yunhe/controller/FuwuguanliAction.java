package cn.yunhe.controller;

import cn.yunhe.model.*;
import cn.yunhe.service.Fw_ClaimantService;
import cn.yunhe.service.Fw_DingDanService;
import cn.yunhe.service.impl.CarsServiceImpl;
import cn.yunhe.service.impl.Fw_ClaimantServiceImpl;
import cn.yunhe.service.impl.Fw_DingDanServiceImpl;
import cn.yunhe.service.impl.UsersServiceImpl;
import org.apache.commons.codec.language.bm.Lang;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/24
 * @描述
 */
@Controller
@RequestMapping("/fuwu")
public class FuwuguanliAction {

    @Resource
    private Fw_DingDanService dingDanService;

    @Resource
    private UsersServiceImpl usersService;
    @Resource
    private CarsServiceImpl carsService;

    @Resource
    private Fw_ClaimantService claimantService;

    @RequestMapping("/touser-msg")
    public ModelAndView tousermsg(HttpSession session){
        ModelAndView mv = new ModelAndView();


        /*用户*/
        UserNumber userNumber= (UserNumber) session.getAttribute("UsernumberRoleUsers");
        Users users = usersService.queryUser(userNumber.getUsers().getU_id());
        mv.addObject("users",users);


        /*车辆*/
        List<Cars> list = carsService.queryCars(userNumber.getUsers().getU_id());
        mv.addObject("list",list);

        /*订单信息*/
        List<InsurContract> list1 = dingDanService.queryInsur(userNumber.getUsers().getU_id());
        mv.addObject("list1",list1);


        mv.setViewName("user-msg");
        return mv;
    }


    /*报案信息查询的控制层*/
    @RequestMapping("/tobaoanxinxi-list")
    public ModelAndView tobaoanxinxilist(HttpSession session){
        ModelAndView mv = new ModelAndView();
        /*查询理赔信息表*/
        UserNumber u_id = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        System.out.println(u_id.getUsers().getU_id());
        List<Claimant> list =
                claimantService.queryClaimant(u_id.getUsers().getU_id());
        mv.addObject("list",list);
        for (Claimant c:list) {
            System.out.println(c.getCl_addtime());
        }

        /*查询出险调查表*/
        /*System.out.println(list.get(1).getC_id());
        System.out.println("11111111");*/

        List<Survey> l_survey = claimantService.querySurvey(1);
        mv.addObject("l_survey",l_survey);

        mv.setViewName("baoanxinxi-list");
        return mv;
    }


    /*退保申请*/
    /*第一次访问*/
    @RequestMapping("/totuibao-list")
    public ModelAndView totuibaolist(HttpSession session){
        ModelAndView mv = new ModelAndView();

        /*用户*/
        UserNumber userNumber= (UserNumber) session.getAttribute("UsernumberRoleUsers");
        Users users = usersService.queryUser(userNumber.getUsers().getU_id());
        mv.addObject("fw_users",users);

        /*车辆*/
        List<Cars> list = carsService.queryCars(userNumber.getUsers().getU_id());
        mv.addObject("list",list);

        /*订单信息*/
        List<InsurContract> list1 = dingDanService.queryInsur(userNumber.getUsers().getU_id());
        mv.addObject("list1",list1);

        mv.setViewName("tuibao-list");
        return mv;
    }


    @RequestMapping("/tosystem-log")
    public ModelAndView tosystemlog(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("system-log");
        return mv;
    }

    @RequestMapping("/tomap")
    public ModelAndView tomap(){
        ModelAndView mv = new ModelAndView();

        List<Dept> list = dingDanService.queryDept();
        mv.addObject("list",list);

        mv.setViewName("map");
        return mv;
    }


    /*退保申请*/
    /*提交表单*/
    @RequestMapping("/tuibaodingdan")
    public String zhezhaoceng(Surrender surrender,HttpSession session){

        System.out.println(surrender.getIc_id()+","+surrender.getS_msg());
        Map<String ,Object> map = new HashMap<String, Object>();

        /*登录层处理完毕时，可直接获去session*/
        UserNumber userNumber = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        int un_id = userNumber.getUn_id();

        Users users = usersService.queryUser(userNumber.getUsers().getU_id());
        int id = users.getU_id();
        Date shijian=new Date();


        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行用户退保操作");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        blog.setUn_id(userNumber.getUn_id());//session中的当前用户id
        map.put("blog",blog);

        map.put("surrender",surrender);
        map.put("id",id);
        map.put("shijian",shijian);
        map.put("un_id",un_id);
        dingDanService.addSurr(map);
        return "redirect:/fuwu/totuibao-list.do";
    }

    @RequestMapping("/baoxian")
    public ModelAndView tobaodanjilian(String ci_id,HttpSession session){
        ModelAndView mv = new ModelAndView();
    /*查询理赔信息表*/
        List<InsurContract> list =
                dingDanService.queryInsurCarid(ci_id);
        mv.addObject("list",list);



        mv.setView(new MappingJackson2JsonView());
        return mv;
    }


    //车险计算器
    @RequestMapping("/cheXian")
    public ModelAndView cheXianJS(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("cheXianJs");
        return mv;
    }

    //车险计算器
    @RequestMapping("/cheXianjs")
    public ModelAndView chexianjisuan(BigDecimal jiage,int fangan){
        ModelAndView mv = new ModelAndView();
        if(fangan==0){
            fangan=1;
        }
        List<Integer> list = new ArrayList<Integer>();
        if (fangan==1){
            list.clear();
            list.add(1);
            list.add(5);
            list.add(8);
           List<CarInsur> lis = dingDanService.querycarinsur(list);
           BigDecimal bg = new BigDecimal("0");
            for (CarInsur ca:lis) {
                if(!ca.getCi_name().equals("机动车交通事故责任强制保险")){
                    BigDecimal big = new BigDecimal(ca.getIns_rale()).multiply(jiage).add(ca.getCi_money());
                    bg=bg.add(big);
                }
            }
            System.out.println(bg);

           mv.addObject("list",lis);
           mv.addObject("shangye",bg);
        }else if(fangan==2){
            list.clear();
            list.add(1);
            list.add(2);
            list.add(5);
            list.add(8);
            list.add(9);
            List<CarInsur> lis = dingDanService.querycarinsur(list);
            BigDecimal bg = new BigDecimal("0");
            for (CarInsur ca:lis) {
                if(!ca.getCi_name().equals("机动车交通事故责任强制保险")){
                    BigDecimal big = new BigDecimal(ca.getIns_rale()).multiply(jiage).add(ca.getCi_money());
                    bg=bg.add(big);
                }
            }
            System.out.println(bg);

            mv.addObject("list",lis);
            mv.addObject("shangye",bg);

        }else{
            list.clear();
            list.add(1);
            list.add(2);
            list.add(5);
            list.add(8);
            list.add(9);
            list.add(21);
            List<CarInsur> lis = dingDanService.querycarinsur(list);
            BigDecimal bg = new BigDecimal("0");
            for (CarInsur ca:lis) {
                if(!ca.getCi_name().equals("机动车交通事故责任强制保险")){
                    BigDecimal big = new BigDecimal(ca.getIns_rale()).multiply(jiage).add(ca.getCi_money());
                    bg=bg.add(big);
                }
            }
            mv.addObject("list",lis);
            mv.addObject("shangye",bg);
        }
        System.out.println(jiage);
        mv.addObject("jiage",jiage);
        /*mv.setView(new MappingJackson2JsonView());*/
        mv.setViewName("chexianjisuan");
        return mv;
    }

    @RequestMapping("/xuBao")
    public ModelAndView xuBao(int id,HttpSession session){
        ModelAndView mv=new ModelAndView();


        InsurContract insurContract1=dingDanService.queryIn(id);


        System.out.println(insurContract1.getCars().getC_id());
        mv.addObject("insurContract",insurContract1);

        BigDecimal money = insurContract1.getIc_money();


         /*用户*/
        UserNumber userNumber = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        Users users = usersService.queryUser(userNumber.getUsers().getU_id());

        mv.addObject("users",users);

        int jifen = users.getU_jifen();

        if (jifen<200){
           mv.addObject("money",money);
        }else if (jifen<400){
            money = money.multiply(new BigDecimal(0.9));
            mv.addObject("money",money);
        }else if (jifen<600){
            money = money.multiply(new BigDecimal(0.8));
            mv.addObject("money",money);
        }else if (jifen<800){
            money = money.multiply(new BigDecimal(0.7));
            mv.addObject("money",money);
        }else if (jifen < 1000 || jifen == 100){
            money = money.multiply(new BigDecimal(0.6));
            mv.addObject("money",money);
        }
        mv.addObject("money",money);

        mv.setViewName("xuBao");
        return mv;
    }

    @RequestMapping("/xubao_2")
    public ModelAndView xubao_2(InsurContract insurContract,HttpSession session){
        ModelAndView mv=new ModelAndView();
        UserNumber userNumber = (UserNumber) session.getAttribute("UsernumberRoleUsers");
        mv.setView(new MappingJackson2JsonView());
        Map<String ,Object> map = new HashMap<String, Object>();

        int ic_id = insurContract.getIc_id();
        map.put("ic_id",ic_id);

        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行用户续保操作");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        blog.setUn_id(userNumber.getUn_id());//session中的当前用户id
        map.put("blog",blog);



        int u_id = userNumber.getUsers().getU_id();
        map.put("u_id",u_id);

        int un_id = userNumber.getUn_id();
        map.put("un_addid",un_id);
        Date date = new Date();

        map.put("r_addtime",date);
        dingDanService.addRenewal(map);
//        mv.setViewName("xunbao_2");
        return mv;
    }

    public UsersServiceImpl getUsersService() {
        return usersService;
    }

    public void setUsersService(UsersServiceImpl usersService) {
        this.usersService = usersService;
    }

    public CarsServiceImpl getCarsService() {
        return carsService;
    }

    public void setCarsService(CarsServiceImpl carsService) {
        this.carsService = carsService;
    }

    public Fw_ClaimantService getClaimantService() {
        return claimantService;
    }

    public void setClaimantService(Fw_ClaimantService claimantService) {
        this.claimantService = claimantService;
    }

    public Fw_DingDanService getDingDanService() {
        return dingDanService;
    }

    public void setDingDanService(Fw_DingDanService dingDanService) {
        this.dingDanService = dingDanService;
    }
}
