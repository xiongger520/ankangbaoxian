package cn.yunhe.controller;

import cn.yunhe.model.Busilog;
import cn.yunhe.model.CarInsur;
import cn.yunhe.model.UserNumber;
import cn.yunhe.service.IBaoxianService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @创建人 项目二组 卢春晓
 * @创建时间 2017/12/24
 * @描述
 */
@Controller
@RequestMapping("/baoxian")
public class BaoxianguanliAction {
    @Resource
    private IBaoxianService baoxianService;

    @RequestMapping("/tobaoxian-add")
    public ModelAndView tobaoxian(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("product-add");
        return mv;
    }

    @RequestMapping("/toarticle-list")
    public ModelAndView toarticle(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("article-list");
        return mv;
    }

    @RequestMapping("/addBaoxian")
    public ModelAndView addBaoxian(CarInsur cai,HttpSession session){

        ModelAndView mv=new ModelAndView();
        Map<String,Object> ma = new HashMap<String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行添加");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("添加");//(添加，删除，修改)
        UserNumber userNumber = (UserNumber)session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());

        ma.put("blog",blog);
        ma.put("cai",cai);
        ma.put("un_id",userNumber.getUn_id());
       // System.out.print("4564654654654");
        boolean ba=baoxianService.addBaoxian(ma);
        mv.addObject("flag",ba);
        mv.addObject("fag","添加成功");
       // System.out.print("状态是"+ba);
       if (ba){
            mv.setViewName("product-add");

        }


        return mv;

    }

    @RequestMapping("/chazhao")
    public ModelAndView queren(String ci_name){
        ModelAndView mv=new ModelAndView();
        int cn=baoxianService.getCarInsur(ci_name);
       // System.out.println(1111111);
            mv.addObject("flag",cn);
            mv.setView(new MappingJackson2JsonView());
        return mv;
    }


    @RequestMapping("/carinsurAll")
    public ModelAndView carinsurAll(){
        ModelAndView mv=new ModelAndView();
        List<CarInsur> list=baoxianService.getAllCarInsur();
        mv.addObject("list",list);
        mv.addObject("count", list.size());
      // System.out.println(list.get(1)+"数据数据数据数据数据数据数据数据");
        mv.setViewName("article-list");
        return mv;
    }


    @RequestMapping("/editStates")
    public ModelAndView editStates(CarInsur cis, HttpSession session){
        ModelAndView mv =new ModelAndView();
        Map<String,Object> ma = new HashMap<String, Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行修改保险状态操作");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        UserNumber userNumber = (UserNumber)session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());

        ma.put("blog",blog);
        ma.put("cis",cis);
        int ed=baoxianService.editState(ma);
      //  System.out.println("改改改改改改改改改改改");
        mv.addObject("ed",ed);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }



    public IBaoxianService getBaoxianService() {
        return baoxianService;
    }

    public void setBaoxianService(IBaoxianService baoxianService) {
        this.baoxianService = baoxianService;
    }
}
