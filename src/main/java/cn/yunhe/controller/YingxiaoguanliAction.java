package cn.yunhe.controller;

import cn.yunhe.model.*;
import cn.yunhe.service.IBaoxianService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @创建人 项目二组 卢春晓
 * @创建时间 2017/12/29
 * @描述
 */
@Controller
@RequestMapping("/yingxiao")
public class YingxiaoguanliAction {

    @Resource
    private IBaoxianService baoxianService;

    @RequestMapping("/toyingxiao-add")
    public ModelAndView toyingxiaoadd(){
        ModelAndView mv = new ModelAndView();
        List<CarInsur> alist=baoxianService.queryCarInsurAll();
        mv.addObject("alist",alist);
        //System.out.println("成功成功成功成功成功成功");
        mv.setViewName("yingxiao-add");
        return mv;
    }

    @RequestMapping("/topicture-list")
    public ModelAndView topicturelist(){
        ModelAndView mv = new ModelAndView();
        List<InsurPack> list=baoxianService.queryInsurPack();
        mv.addObject("list",list);
        mv.addObject("count", list.size());
        mv.setViewName("picture-list");
        return mv;
    }


    @RequestMapping("/querytaocan")
    public ModelAndView querytaocan(String ip_name){
        ModelAndView mv=new ModelAndView();
        int n=baoxianService.queryTao(ip_name);
        //System.out.println(1111111);
        mv.addObject("flag",n);
        mv.setView(new MappingJackson2JsonView());
        return mv;

    }

    @RequestMapping("/addtaocan")
    public ModelAndView addtaocan(InserInclude inser, InsurPack pack, HttpSession session){
        ModelAndView mv=new ModelAndView();
        Map<String,Object> ma = new HashMap<String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行添加");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("添加");//(添加，删除，修改)
        UserNumber userNumber = (UserNumber)session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());

        ma.put("blog",blog);
        ma.put("inser",inser);
        ma.put("un_id",userNumber.getUn_id());
        // System.out.print("4564654654654");

        int ii_id=baoxianService.addInserInclude(ma);
        InserInclude ii = new InserInclude();
        ii.setIi_id(ii_id);
        pack.setInserInclude(ii);
        ma.put("pack",pack);
        int ad=baoxianService.addTaocan(ma);
        mv.addObject("ad",ad);
        mv.setView(new MappingJackson2JsonView());
       /* mv.addObject("flag",ii_id);*/
        // System.out.print("状态是"+ad);
        return mv;
    }


    @RequestMapping("/edittaocan")
    public ModelAndView edittaocan(InsurPack ipk,HttpSession session){
        ModelAndView mv =new ModelAndView();
        Map<String,Object> ma = new HashMap<String, Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行修改保险状态操作");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("修改");//(添加，删除，修改)
        UserNumber userNumber = (UserNumber)session.getAttribute("UsernumberRoleUsers");
        blog.setUn_id(userNumber.getUn_id());

        ma.put("blog",blog);
        ma.put("ipk",ipk);
        int m=baoxianService.editTaocan(ma);
       // System.out.println("改改改改改改改改改改改");
        mv.addObject("m",m);
        mv.setView(new MappingJackson2JsonView());
        return mv;
    }


}
