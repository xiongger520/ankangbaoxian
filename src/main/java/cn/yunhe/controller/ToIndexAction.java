package cn.yunhe.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/24
 * @描述
 */
@Controller
@RequestMapping("/index")
public class ToIndexAction {

    @RequestMapping("/toindex")
    public ModelAndView toindex(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("index");
        return mv;
    }
    @RequestMapping("/towelcome")
    public ModelAndView towelcome(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("welcome");
        return mv;
    }

}
