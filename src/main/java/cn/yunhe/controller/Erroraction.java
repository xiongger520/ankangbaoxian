package cn.yunhe.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
@Controller
@RequestMapping("/error")
public class Erroraction {

    @RequestMapping("/toerror")
    public ModelAndView toadminrole(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/error/404");
        return mv;
    }

    @RequestMapping("/towuquan")
    public ModelAndView towuquan(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/error/401");
        return mv;
    }
}
