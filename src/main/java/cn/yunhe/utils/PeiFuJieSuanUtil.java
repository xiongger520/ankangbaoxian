package cn.yunhe.utils;

import cn.yunhe.model.Cars;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2018/1/5
 * @描述
 */
public class PeiFuJieSuanUtil {

    /**
     * 机动车全车盗抢保险
     1.
     （一） 被保险机动车被盗窃、抢劫、抢夺，经出险当地县级以上公安刑侦部门立案证明，满60天未查明下落的全车损失；
     （二） 被保险机动车全车被盗窃、抢劫、抢夺后，受到损坏或车上零部件、附属设备丢失需要修复的合理费用；
     （三） 被保险机动车在被抢劫、抢夺过程中，受到损坏需要修复的合理费用。
     2.保险人按下列方式赔偿：
     （一）被保险机动车全车被盗抢的，按以下方法计算赔款：

     赔款＝保险金额×（1－绝对免赔率之和）

     （二）被保险机动车发生本条款第五十一条第（二）款、第（三）款列明的损失，保险人按实际修复费用在保险金额内计算赔偿。

     *
     */

    /**
     * 机动车全车盗抢保险
     *
     * @param many 保险金额
     * @param manyMax 最大保额
     * @param rate 绝对免赔率之和
     * @param flag 全车or损失
     * @return
     */
    public BigDecimal daoQiangJieSuan(BigDecimal many, BigDecimal manyMax, Double rate, int flag,Cars car){
        BigDecimal peiFu = null;
        BigDecimal r = new BigDecimal(Double.toString(1-rate));
        BigDecimal carMany = car.getC_price();
        if(0==flag){
            //赔款＝保险金额×（1－绝对免赔率之和）
            peiFu = carMany.multiply(r);
        }else if (many.compareTo(carMany)<=0){
            peiFu = many.multiply(r);
        }else{
            peiFu = carMany.multiply(r);
        }
        if(peiFu.compareTo(manyMax)>=0){
            peiFu = manyMax;
        }
        return peiFu;
    }

    /**
     * 1.1机动车损失保险
     （ ，实行30%的绝对免赔率；
     （三）违反安全装载规定、但不是事故发生的直接原因的，增加10%的绝对免赔率；
     （四）对于投保人与保险人在投保时协商确定绝对免赔额的，本保险在实行免赔率的基础上增加每次事故绝对免赔额。


     1.2机动车损失赔款按以下方法计算
     （一）全部损失
     赔款＝（保险金额－被保险人已从第三方获得的赔偿金额 ）×（1－事故责任免赔率）×（1－绝对免赔率之和）－绝对免赔额
     （二）部分损失
     被保险机动车发生部分损失，保险人按实际修复费用在保险金额内计算赔偿：
     赔款＝（实际修复费用－被保险人已从第三方获得的赔偿金额）×（1－事故责任免赔率）×（1－绝对免赔率之和）－绝对免赔额
     （三）施救费
     施救的财产中，含有本保险合同未保险的财产，应按本保险合同保险财产的实际价值占总施救财产的实际价值比例分摊施救费用。
     */

    /**
     * 机动车损失保险
     * @param many  申请赔付金额
     * @param manyMax 保险最大保额
     * @param diSanFangMany 第三方获得
     * @param mianPeiMany 绝对免赔额
     * @param mianPeiRate 事故责任免赔率
     * @param jueDuiMianPei 绝对免赔率
     * @param flag 全部损失 or 部分损失
     * @return
     */
    public BigDecimal cheSunJieSuan(BigDecimal many,BigDecimal manyMax,BigDecimal diSanFangMany,BigDecimal mianPeiMany,Double mianPeiRate,Double jueDuiMianPei,int flag,Cars cars){
        BigDecimal peiFu = null;
        BigDecimal r = new BigDecimal(Double.toString(1-mianPeiRate));
        BigDecimal carMany = cars.getC_price();
        if(0==flag){
            //赔款＝（保险最大保额－被保险人已从第三方获得的赔偿金额 ）×（1－事故责任免赔率）×（1－绝对免赔率之和）－绝对免赔额
            BigDecimal peiFu1 = (carMany.subtract(diSanFangMany));
            BigDecimal peiFu2 = peiFu1.multiply(new BigDecimal(1-mianPeiRate));
            BigDecimal peiFu3 = peiFu2.multiply(new BigDecimal(1-jueDuiMianPei));
            peiFu = peiFu3.subtract(mianPeiMany);
            System.out.println("//赔款＝（车价－被保险人已从第三方获得的赔偿金额 ）×（1－事故责任免赔率）×（1－绝对免赔率之和）－绝对免赔额");
            System.out.println(peiFu+"=（"+carMany+"－"+diSanFangMany+"）×（1－"+mianPeiRate+"）×（1－"+jueDuiMianPei+"）－"+mianPeiMany);
        }else if (many.compareTo(carMany)<=0){
            //赔款＝（申请赔付金额－被保险人已从第三方获得的赔偿金额 ）×（1－事故责任免赔率）×（1－绝对免赔率之和）－绝对免赔额
            BigDecimal peiFu1 = (many.subtract(diSanFangMany));
            BigDecimal peiFu2 = peiFu1.multiply(new BigDecimal(1-mianPeiRate));
            BigDecimal peiFu3 = peiFu2.multiply(new BigDecimal(1-jueDuiMianPei));
            peiFu = peiFu3.subtract(mianPeiMany);
            System.out.println("//赔款＝（申请赔付金额－被保险人已从第三方获得的赔偿金额 ）×（1－事故责任免赔率）×（1－绝对免赔率之和）－绝对免赔额");
            System.out.println(peiFu+"=（"+many+"－"+diSanFangMany+"）×（1－"+mianPeiRate+"）×（1－"+jueDuiMianPei+"）－"+mianPeiMany);
        }else{
            peiFu = carMany;
        }
        if(peiFu.compareTo(manyMax)>=0){
            peiFu = manyMax;
        }else if(peiFu.compareTo(new BigDecimal(0))<=0){
            peiFu = new BigDecimal(0);
        }
        return peiFu;
    }

    /**
     * 机动车车上人员责任保险

     1.责任比例：
     被保险机动车一方负主要事故责任的，事故责任比例为70%；
     被保险机动车一方负同等事故责任的，事故责任比例为50%；
     被保险机动车一方负次要事故责任的，事故责任比例为30%。
     涉及司法或仲裁程序的，以法院或仲裁机构最终生效的法律文书为准。
     2.免赔率
     被保险机动车一方负次要事故责任的，实行5%的事故责任免赔率；
     负同等事故责任的，实行10%的事故责任免赔率；
     负主要事故责任的，实行15%的事故责任免赔率；
     负全部事故责任或单方肇事事故的，实行20%的事故责任免赔率。
     3. 赔款计算
     （一）对每座的受害人，当（依合同约定核定的每座车上人员人身伤亡损失金额－应由机动车交通事故责任强制保险赔偿的金额）×事故责任比例    高于或等于   每次事故每座赔偿限额时：

     赔款=每次事故每座赔偿限额×（1－事故责任免赔率）

     （二）对每座的受害人，当（依合同约定核定的每座车上人员人身伤亡损失金额－应由机动车交通事故责任强制保险赔偿的金额）×事故责任比例  低于   每次事故每座赔偿限额时：

     赔款=（依合同约定核定的每座车上人员人身伤亡损失金额－应由机动车交通事故责任强制保险赔偿的金额）×事故责任比例×（1－事故责任免赔率）
     *
     */

    /**
     * 机动车车上人员责任保险
     * 该方法只计算每座的赔偿
     * @param xianEMany 每座限额
     * @param sunShiMany 伤亡损失
     * @param qiangXianMany 交强险赔偿金额
     * @param zeRenRate 事故责任比率
     * @param rate 事故责任免赔率
     * @return
     */
    public BigDecimal renYuanJieSuan(BigDecimal sunShiMany,BigDecimal xianEMany,BigDecimal qiangXianMany,Double rate,Double zeRenRate){
        BigDecimal peiFu = new BigDecimal(0);
        BigDecimal flag1 = sunShiMany.subtract(qiangXianMany);
        BigDecimal flag = flag1.multiply(new BigDecimal(zeRenRate));

        if(flag.compareTo(xianEMany)<0){
            //赔款=每次事故每座赔偿限额×（1－事故责任免赔率）
            peiFu = flag.multiply(new BigDecimal(1-rate));
        }else{
            //赔款=（依合同约定核定的每座车上人员人身伤亡损失金额－应由机动车交通事故责任强制保险赔偿的金额）×事故责任比例×（1－事故责任免赔率）
            peiFu = xianEMany.multiply(new BigDecimal(1-rate));
        }
        return peiFu;
    }


    /**
     * 机动车第三者责任保险

     1.按照下列规定确定事故责任比例：
     被保险机动车一方负主要事故责任的，事故责任比例为70%；
     被保险机动车一方负同等事故责任的，事故责任比例为50%；
     被保险机动车一方负次要事故责任的，事故责任比例为30%。
     涉及司法或仲裁程序的，以法院或仲裁机构最终生效的法律文书为准。
     2.免赔率
     （一）被保险机动车一方负次要事故责任的，实行5%的事故责任免赔率；
     负同等事故责任的，实行10%的事故责任免赔率；
     负主要事故责任的，实行15%的事故责任免赔率；
     负全部事故责任的，实行20%的事故责任免赔率；
     （二）违反安全装载规定的，实行10%的绝对免赔率。
     3.赔款计算
     1、当（依合同约定核定的第三者损失金额－机动车交通事故责任强制保险的分项赔偿限额）×事故责任比例  等于或高于  每次事故赔偿限额时：
     赔款=每次事故赔偿限额×（1－事故责任免赔率）×（1－绝对免赔率之和）
     2、当（依合同约定核定的第三者损失金额－机动车交通事故责任强制保险的分项赔偿限额）×事故责任比例  低于  每次事故赔偿限额时：
     赔款＝（依合同约定核定的第三者损失金额－机动车交通事故责任强制保险的分项赔偿限额）×事故责任比例×（1－事故责任免赔率）×（1－绝对免赔率之和）
     *
     */

    /**
     * 机动车第三者责任保险
     * @param xianEMany 最大限额
     * @param sunShiMany 损失金额
     * @param shiGuRate 事故责任比例
     * @param zeRenRate 事故责任免赔
     * @param jueDuiRate 绝对免赔率之和
     * @return
     */
    public BigDecimal DiSanZhe(BigDecimal xianEMany,BigDecimal sunShiMany,Double shiGuRate,Double zeRenRate,Double jueDuiRate){
        BigDecimal peiFu = null;
        //（依合同约定核定的第三者损失金额）×事故责任比例
        BigDecimal flag1 = sunShiMany;
        BigDecimal flag = flag1.multiply(new BigDecimal(shiGuRate));

        if(flag.compareTo(xianEMany)<0){
            //赔款＝（依合同约定核定的第三者损失金额）×事故责任比例×（1－事故责任免赔率）×（1－绝对免赔率之和）
            BigDecimal peiFu1 = flag.multiply(new BigDecimal(1-zeRenRate));
            peiFu = peiFu1.multiply(new BigDecimal(1-jueDuiRate));
            System.out.println("//赔款＝（依合同约定核定的第三者损失金额）×事故责任比例×（1－事故责任免赔率）×（1－绝对免赔率之和）");
            System.out.println(peiFu+"＝（"+sunShiMany+"）×"+shiGuRate+"×（1－"+zeRenRate+"）×（1－"+jueDuiRate+"）");
        }else{
            //赔款=每次事故赔偿限额×（1－事故责任免赔率）×（1－绝对免赔率之和）
            peiFu = xianEMany.multiply(new BigDecimal(1-zeRenRate));
            peiFu = peiFu.multiply(new BigDecimal(1-jueDuiRate));
            System.out.println("//赔款=每次事故赔偿限额×（1－事故责任免赔率）×（1－绝对免赔率之和）");
            System.out.println(peiFu+"="+xianEMany+"×（1－"+shiGuRate+"）×（1－"+jueDuiRate+"）");
        }
        return peiFu;
    }

    /**
     *
     * 1.保险责任
     （一）死亡伤残赔偿限额为110000元；
     （二）医疗费用赔偿限额为10000元；
     （三）财产损失赔偿限额为2000元；
     （四）被保险人无责任时，无责任死亡伤残赔偿限额为11000元；
     无责任医疗费用赔偿限额为1000元；
     无责任财产损失赔偿限额为100元。
     死亡伤残赔偿限额 和 无责任死亡伤残赔偿限额项下负责赔偿丧葬费、死亡补偿费、受害人亲属办理丧葬事宜支出的交通费用、残疾赔偿金、残疾辅助器具费、护理费、康复费、交通费、被扶养人生活费、住宿费、误工费，被保险人依照法院判决或者调解承担的精神损害抚慰金。
     医疗费用赔偿限额 和 无责任医疗费用赔偿限额项下负责赔偿医药费、诊疗费、住院费、住院伙食补助费，必要的、合理的后续治疗费、整容费、营养费。
     *
     */

    public Map<String,BigDecimal> qiangZhiJieSuan(BigDecimal siShangMany, BigDecimal yiLiaoMany, BigDecimal caiChanMany, Double flag){
        BigDecimal peiFu = null;
        BigDecimal peiFu1 = null;
        BigDecimal peiFu2 = null;
        BigDecimal peiFu3 = null;
        Map<String,BigDecimal> map = new HashMap<String, BigDecimal>();
        if(0==flag){
            peiFu1 = siShangMany.compareTo(new BigDecimal(11000))<0?siShangMany:new BigDecimal(11000);
            peiFu2 = yiLiaoMany.compareTo(new BigDecimal(1000))<0?yiLiaoMany:new BigDecimal(1000);
            peiFu3 = caiChanMany.compareTo(new BigDecimal(100))<0?caiChanMany:new BigDecimal(100);

        }else {
            peiFu1 = siShangMany.compareTo(new BigDecimal(110000))<0?siShangMany:new BigDecimal(110000);
            peiFu2 = yiLiaoMany.compareTo(new BigDecimal(10000))<0?yiLiaoMany:new BigDecimal(10000);
            peiFu3 = caiChanMany.compareTo(new BigDecimal(2000))<0?caiChanMany:new BigDecimal(2000);
        }
        map.put("sishang",peiFu1);
        peiFu = peiFu1;
        map.put("yiliao",peiFu2);
        peiFu = peiFu.add(peiFu2);
        map.put("caichan",peiFu3);
        peiFu = peiFu.add(peiFu3);
        map.put("he",peiFu);
        return map;
    }

    /**
     * 其他险
     *     涉水险的费用各大保险公司略有差异，一般涉水损失险的保费为车损险保费的5%，
     常见的一辆价值十万元的家用轿车，车辆涉水险保费一般在一百元左右。目前，
     多数保险公司的车损险有20%的不计免赔率，即使购买了附加“涉水险”，也仅能
     获得80%左右的赔偿。也就是说，车主购买车损险和涉水险的同时，还得再投保一
     个不计免赔险，车辆在涉水过程中出现发动机故障，才能获得全额赔偿。

     */

    /**
     * 其他险
     * @param many 损失金额
     * @param maxMany 最大赔付金额
     * @param rate 不计免赔率
     * @return
     */
    public BigDecimal qiTaJieSuan(BigDecimal many,BigDecimal maxMany,Double rate){
        BigDecimal peiFu = null;
        if(many.compareTo(maxMany)<=0){
            peiFu = many.multiply(new BigDecimal(1-rate));
        }else if(many.compareTo(maxMany)>0){
            peiFu = maxMany.multiply(new BigDecimal(1-rate));
        }
        return peiFu;
    }

    public static void main(String[] args) {
        PeiFuJieSuanUtil peiFuJieSuanUtil = new PeiFuJieSuanUtil();
        Cars cars = new Cars();
        cars.setC_price(new BigDecimal(2000000));
        BigDecimal b = peiFuJieSuanUtil.daoQiangJieSuan(new BigDecimal(10000),new BigDecimal(20000),0.02,0,cars);
//        BigDecimal b = peiFuJieSuanUtil.cheSunJieSuan(new BigDecimal(0),new BigDecimal(200000),new BigDecimal(50000),new BigDecimal(100000),0.3,0.03,0,cars);
        //BigDecimal b = peiFuJieSuanUtil.renYuanJieSuan(new BigDecimal(1000000),new BigDecimal(20000),new BigDecimal(2000),0.002,0.3);
//        BigDecimal b = peiFuJieSuanUtil.DiSanZhe(new BigDecimal(100000),new BigDecimal(1000000),new BigDecimal(1000),0.3,0.02,0.03);
        System.out.printf(String.valueOf(b));
//        Map b = peiFuJieSuanUtil.qiangZhiJieSuan(new BigDecimal(100000),new BigDecimal(1000),new BigDecimal(100),0.0);
//        System.out.println(b.get("he"));
//        System.out.println(b.get("sishang"));
//        System.out.println(b.get("yiliao"));
//        System.out.println(b.get("caichan"));

    }


}