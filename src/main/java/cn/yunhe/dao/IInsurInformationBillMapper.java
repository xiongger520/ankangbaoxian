package cn.yunhe.dao;

import cn.yunhe.model.InsurContract;

import java.util.List;

public interface IInsurInformationBillMapper {  /*保险单信息管理*/

    /*查询保险订单所有内容  将数据遍历到页面上*/
    public List<InsurContract> queryInsurContract ();

}
