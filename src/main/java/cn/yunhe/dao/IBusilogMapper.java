


package cn.yunhe.dao;

import cn.yunhe.model.Busilog;
import java.util.List;
import java.util.Map;



public interface IBusilogMapper {

	public void insertLog(Busilog blo);


    List<Busilog> queryAllBusilog();

    /**
     * 模糊查询匹配日志列表
     * @param cond
     * @return
     */
    List<Busilog> queryLikeBusilog(Map<String ,Object> cond);
    /**
     * 模糊查询匹配日志列表数量
     * @param cond
     * @return
     */
    Integer queryLikeBusilogCount(Map<String ,Object> cond);

    /**
     * 模糊搜索全匹配
     * @param tiaojian
     * @return
     */
    List<Busilog> queryBusilogByTiaojian(String tiaojian);

}