package cn.yunhe.dao;

import cn.yunhe.model.*;


import java.util.List;
import java.util.Map;

public interface HeyanMapper {

    //修改车辆状态
    int editCarstate(Map<String,Object> ma);

    //修改用户状态
    int edituserState(Map<String,Object>ma);

    //查询理赔信息
    List<Claimant> queryClaimantAll();
    //修改理赔状态
    int editClaimant(Map<String,Object> ma);
    //添加赔付记录
    int addRecord(Map<String,Object> ma);
    //查询保险信息
    List<CarInsur> queryCarIn();
    //修改保险状态
    int editCarIn(Map<String,Object> ma);
    //查询退保申请
    List<Surrender> querySurrender();
    //审核退保申请
    int editSurrender(Map<String,Object> ma);
    //查询保险单
    List<InsurContract> queryIcAll();
    //保险单核验
    int editBaoxiandan(Map<String,Object> ma);

    //查询要退的保险订单
    InsurContract queryIcByid(int ic_id);

    //将要退的保险单删除
    int editInsur(Map<String,Object> map);

    //保险单核验
    int editxubao(Map<String,Object> ma);

    //查询续保申请
    List<Renewal> queryRenewal();

    //修改订单的状态，修改为续保，并更新应交金额
     int editInsurCont(Map<String,Object> map);






}
