package cn.yunhe.dao;

import cn.yunhe.model.CarInsur;
import cn.yunhe.model.Dept;
import cn.yunhe.model.InsurContract;
import cn.yunhe.model.Surrender;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface Fw_DingDanMapper {

    /*根据用户id返回订单*/
    List<InsurContract> queryInsur(int id);

    /*根据车id返回订单*/
    List<InsurContract> queryInsurCarid(String c_id);

    /*新增退保信息*/
    void addSurr(Map<String ,Object> map);

    /*根据订单id返回订单*/
    InsurContract queryIn(int id);

    /*续保*/
    void addRenewal(Map<String,Object> map);

     /*查询多个保险 */
    public List<CarInsur> querycarinsur(List<Integer> list);

    /*支点遍历*/
    List<Dept> queryDept();

}
