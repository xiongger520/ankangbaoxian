package cn.yunhe.dao;

import cn.yunhe.model.*;

import java.util.List;
import java.util.Map;

public interface IBuyInsurMapper {    /*购买保险*/

     /*通过用户查用户的车牌号*/
     public List<Cars> queryCaridByUser(String name);
     /*通过用户身份证查用户的车牌号*/
     public List<Cars> queryCaridByIdcard(String idcard);

     /*遍历出所有的自选保险种类   供用户选择*/
     public List<CarInsur> queryInsurName();

     /*遍历出所有保险套餐的名称   供用户选择*/
     public  List<InsurPack> queryInsurPackName();

     /*查询保险套餐里的具体  保险的种类*/
     public  CarInsur queryInsurPackInsur(int id);

     /*查询出 用户购买自选保险的 详细内容*/
     public List<CarInsur> queryCarInsur(Map<String,Object> list1);


     /*查询出 用户购买保险套餐包含的保险以及套餐信息*/
     public InsurPack queryInsurPack( int ip_id);

     /*查询出 用户购买保险套餐包含的保险以及套餐信息*/
     public Cars queryCarUser( String c_id);

     /*查询出 用户用户的等级以及优惠率*/
     public Member queryUsermember(int score);

     /*添加保险订单*/
     public int addbaoxian(Map<String,Object> baoxian);


     /*通过id查出保险的详细内容*/
     public  CarInsur  queryCarInsurById(int id);




}
