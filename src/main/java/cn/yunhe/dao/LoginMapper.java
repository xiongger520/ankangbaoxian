package cn.yunhe.dao;

import cn.yunhe.model.Busilog;
import cn.yunhe.model.Power;
import cn.yunhe.model.UserNumber;
import cn.yunhe.model.Users;

import java.util.Map;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/24
 * @描述
 */
public interface LoginMapper {
    public UserNumber LoginUserByUserid(Map<String,Object> map);
    public UserNumber queryUserByUserid(String username);
    public Power queryquanxian(int p_id);

}
