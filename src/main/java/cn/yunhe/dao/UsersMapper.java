package cn.yunhe.dao;

import cn.yunhe.model.Users;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersMapper {

    /*根据id查询用户信息*/
    Users queryUser(int id);
}
