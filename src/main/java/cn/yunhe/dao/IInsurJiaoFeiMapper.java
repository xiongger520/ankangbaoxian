package cn.yunhe.dao;

import cn.yunhe.model.CarInsurRecord;
import cn.yunhe.model.InsurContract;
import cn.yunhe.model.Users;

import java.util.List;
import java.util.Map;

public interface IInsurJiaoFeiMapper {  //保险缴费

    /*查询订单表*/
    public List<InsurContract> queryInsurContracts ();

    /*修改缴费信息*/
   public boolean editJiaoFeiNumber(Map<String,Object> map);

   /*添加保险缴费记录*/
   public Integer addCarInsurRecord(Map<String,Object> map);


   /*添加保险缴费记录*/
   public Integer editUserJiFen(Map<String,Object> map);



}
