package cn.yunhe.dao;

import cn.yunhe.model.Cars;

import java.util.List;
import java.util.Map;

public interface CarsMapper {

    /*查询已登录用户的现入保车辆*/
    List<Cars> queryCars(int id);
}
