package cn.yunhe.dao;

import cn.yunhe.model.CarInsur;
import cn.yunhe.model.InsurPack;

import java.util.List;
import java.util.Map;

public interface IBaoxianMapper {

    boolean addBaoxian(Map<String,Object> ma);

    List<CarInsur> getAllCarInsur();

    List<CarInsur> queryCarInsurAll();

    int getCarInsur(String ci_name);

    int editState(Map<String,Object> ci_id);

    int addInserInclude(Map<String,Object> ma);

    int addTaocan(Map<String,Object>ma);

    List<InsurPack> queryInsurPack();

    int editTaocan(Map<String,Object>ma);

    int queryTao(String ip_name);

}
