package cn.yunhe.dao;


import cn.yunhe.model.Admin;

import java.util.List;

public interface IAdminMapper {

    List<Admin> queryAllAdmin();

}
