package cn.yunhe.dao;

import cn.yunhe.model.Dept;
import java.util.List;
import java.util.Map;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/29
 * @描述
 */
public interface IDeptMapper {
    /**
     * 获得所有支点信息
     * @return
     */
    List<Dept> queryLikeDept();

    /**
     * 新增支点
     * @param map
     * @return
     */
    int addDept(Map<String,Object> map);

    /**
     * 删除支点
     * @return
     */
    int delDept(Map<String,Object> map);

    /**
     * 删除多个支点
     * @param dept
     * @return
     */
    int delDepts(Map<String,Object> dept);

    /**
     * 修改支点
     * @param map
     * @return
     */
    int editDept(Map<String,Object> map);

    /**
     * 根据id获得支点
     * @param dept
     * @return
     */
    Dept queryDeptById(Dept dept);
}
