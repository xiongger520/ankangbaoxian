package cn.yunhe.dao;

import cn.yunhe.model.UserNumber;

import java.util.Map;

public interface IZhaoHuiMima {

    public UserNumber queryUserNumber(UserNumber userNumber);
    public int editUserNumberpassword(Map<String,Object> map);

}
