package cn.yunhe.model;

import java.math.BigDecimal;
import java.util.Date;

public class LiPeiJieSuan {
    private int cl_id;
    private BigDecimal zh;
    private String c_id;
    private String u_name;
    private String u_emal;
    private int u_id;
    private int cl_jiesuan;
    private int cl_state;
    private Date cl_addtime;
    public LiPeiJieSuan() {
    }

    public int getCl_id() {
        return cl_id;
    }

    public void setCl_id(int cl_id) {
        this.cl_id = cl_id;
    }

    public BigDecimal getZh() {
        return zh;
    }

    public void setZh(BigDecimal zh) {
        this.zh = zh;
    }

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public int getU_id() {
        return u_id;
    }

    public void setU_id(int u_id) {
        this.u_id = u_id;
    }

    public String getU_name() {
        return u_name;
    }

    public void setU_name(String u_name) {
        this.u_name = u_name;
    }

    public String getU_emal() {
        return u_emal;
    }

    public void setU_emal(String u_emal) {
        this.u_emal = u_emal;
    }

    public int getCl_jiesuan() {
        return cl_jiesuan;
    }

    public void setCl_jiesuan(int cl_jiesuan) {
        this.cl_jiesuan = cl_jiesuan;
    }

    public int getCl_state() {
        return cl_state;
    }

    public void setCl_state(int cl_state) {
        this.cl_state = cl_state;
    }

    public Date getCl_addtime() {
        return cl_addtime;
    }

    public void setCl_addtime(Date cl_addtime) {
        this.cl_addtime = cl_addtime;
    }
}
