package cn.yunhe.model;

import java.util.Date;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/26
 * @描述   角色信息表
 */
public class Role {

    /**
     * 角色编号
     */
    private int r_id;
    /**
     * 角色名称
     */
    private String r_name;
    /**
     * 外键  权限编号
     */
    private String powers;
    /**
     * 权限备注
     */
    private String r_msg;
    /**
     * 添加人编号
     */
    private int un_id;
    /**
     * 添加日期
     */
    private Date r_addtime;
    /**
     * 审核人编号
     */
    private int un_shenid;

    private String username;

    public Role() {
    }

    public Role(int r_id, String r_name,String powers, String r_msg, int un_id, Date r_addtime, int un_shenid) {
        this.r_id = r_id;
        this.r_name = r_name;
        this.powers = powers;
        this.r_msg = r_msg;
        this.un_id = un_id;
        this.r_addtime = r_addtime;
        this.un_shenid = un_shenid;
    }

    public int getR_id() {
        return r_id;
    }

    public void setR_id(int r_id) {
        this.r_id = r_id;
    }

    public String getR_name() {
        return r_name;
    }

    public void setR_name(String r_name) {
        this.r_name = r_name;
    }

    public String getPowers() {
        return powers;
    }

    public void setPowers(String powers) {
        this.powers = powers;
    }

    public String getR_msg() {
        return r_msg;
    }

    public void setR_msg(String r_msg) {
        this.r_msg = r_msg;
    }

    public int getUn_id() {
        return un_id;
    }

    public void setUn_id(int un_id) {
        this.un_id = un_id;
    }

    public Date getR_addtime() {
        return r_addtime;
    }

    public void setR_addtime(Date r_addtime) {
        this.r_addtime = r_addtime;
    }

    public int getUn_shenid() {
        return un_shenid;
    }

    public void setUn_shenid(int un_shenid) {
        this.un_shenid = un_shenid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
