package cn.yunhe.model;

import java.math.BigDecimal;
import java.util.Date;
/*
* 理赔信息表
* */
public class Claimant {

    private int cl_id;
    private String c_id;
    private Users users;
    private CarInsur carInsur;//保险信息表
    private double sgzr;//事故责任免赔率
    private double jdmp;//绝对免赔率之和
    private double cl_type;//责任比率
    private int cl_state;//审核状态
    private int un_id;//添加人id
    private Date cl_addtime;
    private int un_shenid;//审核人id
    private int cl_jiesuan;//结算标志（0未结算，1结算）
    private BigDecimal cl_money;//死亡金额
    private BigDecimal cl_money1;//医疗金额
    private BigDecimal cl_money2;//财产金额
    private BigDecimal zonghe;//金额总和
    private BigDecimal dsf_money;//第三方获得的金额
    private String clds_type ;//车辆丢失的状态
    private String msg_table;//这个不用管

    private BigDecimal jdmpe;//绝对免赔额
    private double jdmp_cs;//绝对免赔率（车辆损失险）
    private BigDecimal zwss;//座位平均损失金额
    private double jdmp_ds;//第三方责任险绝对免赔率
    private double jdmp_dq;//盗抢险绝对免赔率
    private int category;//盗抢险种类
    private BigDecimal pfje;//其他附加险金额
    private Boolean aqgd;//是否违反安全装载规定


    public Claimant() {
    }

    public Claimant(int cl_id) {
        this.cl_id = cl_id;
    }

    public Claimant(String c_id, Users users, CarInsur carInsur, double sgzr, double jdmp, double cl_type, int un_id, Date cl_addtime, BigDecimal cl_money, BigDecimal cl_money1, BigDecimal cl_money2, BigDecimal zonghe, BigDecimal dsf_money, String clds_type, String msg_table) {
        this.c_id = c_id;
        this.users = users;
        this.carInsur = carInsur;
        this.sgzr = sgzr;
        this.jdmp = jdmp;
        this.cl_type = cl_type;
        this.un_id = un_id;
        this.cl_addtime = cl_addtime;
        this.cl_money = cl_money;
        this.cl_money1 = cl_money1;
        this.cl_money2 = cl_money2;
        this.zonghe = zonghe;
        this.dsf_money = dsf_money;
        this.clds_type = clds_type;
        this.msg_table = msg_table;
    }

    public int getCl_id() {
        return cl_id;
    }

    public void setCl_id(int cl_id) {
        this.cl_id = cl_id;
    }

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public double getCl_type() {
        return cl_type;
    }

    public void setCl_type(double cl_type) {
        this.cl_type = cl_type;
    }

    public int getCl_state() {
        return cl_state;
    }

    public void setCl_state(int cl_state) {
        this.cl_state = cl_state;
    }

    public int getUn_id() {
        return un_id;
    }

    public void setUn_id(int un_id) {
        this.un_id = un_id;
    }

    public Date getCl_addtime() {
        return cl_addtime;
    }

    public void setCl_addtime(Date cl_addtime) {
        this.cl_addtime = cl_addtime;
    }

    public int getUn_shenid() {
        return un_shenid;
    }

    public void setUn_shenid(int un_shenid) {
        this.un_shenid = un_shenid;
    }

    public int getCl_jiesuan() {
        return cl_jiesuan;
    }

    public void setCl_jiesuan(int cl_jiesuan) {
        this.cl_jiesuan = cl_jiesuan;
    }

    public CarInsur getCarInsur() {
        return carInsur;
    }

    public void setCarInsur(CarInsur carInsur) {
        this.carInsur = carInsur;
    }

    public double getSgzr() {
        return sgzr;
    }
    public void setSgzr(double sgzr) {
        this.sgzr = sgzr;
    }

    public double getJdmp() {
        return jdmp;
    }

    public void setJdmp(double jdmp) {
        this.jdmp = jdmp;
    }

    public BigDecimal getCl_money1() {
        return cl_money1;
    }

    public void setCl_money1(BigDecimal cl_money1) {
        this.cl_money1 = cl_money1;
    }

    public BigDecimal getCl_money2() {
        return cl_money2;
    }

    public void setCl_money2(BigDecimal cl_money2) {
        this.cl_money2 = cl_money2;
    }

    public BigDecimal getCl_money() {
        return cl_money;
    }

    public void setCl_money(BigDecimal cl_money) {
        this.cl_money = cl_money;
    }

    public BigDecimal getZonghe() {
        return zonghe;
    }

    public void setZonghe(BigDecimal zonghe) {
        this.zonghe = zonghe;
    }

    public BigDecimal getDsf_money() {
        return dsf_money;
    }

    public void setDsf_money(BigDecimal dsf_money) {
        this.dsf_money = dsf_money;
    }

    public String getClds_type() {
        return clds_type;
    }

    public void setClds_type(String clds_type) {
        this.clds_type = clds_type;
    }

    public String getMsg_table() {
        return msg_table;
    }

    public void setMsg_table(String msg_table) {
        this.msg_table = msg_table;
    }

   public BigDecimal getJdmpe() {
        return jdmpe;
    }

    public void setJdmpe(BigDecimal jdmpe) {
        this.jdmpe = jdmpe;
    }

    public double getJdmp_cs() {
        return jdmp_cs;
    }

    public void setJdmp_cs(double jdmp_cs) {
        this.jdmp_cs = jdmp_cs;
    }

     public BigDecimal getZwss() {
        return zwss;
    }

    public void setZwss(BigDecimal zwss) {
        this.zwss = zwss;
    }

    public double getJdmp_ds() {
        return jdmp_ds;
    }

    public void setJdmp_ds(double jdmp_ds) {
        this.jdmp_ds = jdmp_ds;
    }

  /*  public double getJdmp_cs1() {
        return jdmp_cs1;
    }

    public void setJdmp_cs1(double jdmp_cs1) {
        this.jdmp_cs1 = jdmp_cs1;
    }*/

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public BigDecimal getPfje() {
        return pfje;
    }

    public void setPfje(BigDecimal pfje) {
        this.pfje = pfje;
    }
     public Boolean getAqgd() {
            return aqgd;
        }

    public void setAqgd(Boolean aqgd) {
        this.aqgd = aqgd;
    }
}
