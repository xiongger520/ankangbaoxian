package cn.yunhe.model;

import java.util.Date;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/26
 * @描述 保险套餐
 */
public class InsurPack {

    /**
     * 套餐编号
     */
    private int ip_id;
    /**
     *  外键  保险清单编号
     */
    private InserInclude inserInclude;
    /**
     * 套餐名称
     */
    private String ip_name;
    /**
     * 套餐状态（0待审核，1上架，2下架）
     */
    private int ip_state;
    /**
     * 审核人编号
     */
    private int un_id;
    /**
     * 添加时间
     */
    private Date ip_addtime;
    /**
     * 套餐优惠率
     */
    private Double ip_rate;

    private int includenum;

    public InsurPack() {
    }

    public InserInclude getInserInclude() {
        return inserInclude;
    }

    public void setInserInclude(InserInclude inserInclude) {
        this.inserInclude = inserInclude;
    }

    public Double getIp_rate() {
        return ip_rate;
    }

    public void setIp_rate(Double ip_rate) {
        this.ip_rate = ip_rate;
    }

    public int getIp_id() {
        return ip_id;
    }

    public void setIp_id(int ip_id) {
        this.ip_id = ip_id;
    }


    public String getIp_name() {
        return ip_name;
    }

    public void setIp_name(String ip_name) {
        this.ip_name = ip_name;
    }

    public int getIp_state() {
        return ip_state;
    }

    public void setIp_state(int ip_state) {
        this.ip_state = ip_state;
    }

    public int getUn_id() {
        return un_id;
    }

    public void setUn_id(int un_id) {
        this.un_id = un_id;
    }

    public Date getIp_addtime() {
        return ip_addtime;
    }

    public void setIp_addtime(Date ip_addtime) {
        this.ip_addtime = ip_addtime;
    }

    public int getIncludenum() {
        return includenum;
    }

    public void setIncludenum(int includenum) {
        this.includenum = includenum;
    }
}
