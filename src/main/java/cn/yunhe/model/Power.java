package cn.yunhe.model;

import java.util.Date;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/26
 * @描述  权限信息表
 */
public class Power {

    /**
     * 权限编号
     */
    private int p_id;
    /**
     * 预留
     */
    private String p_name;
    /**
     * 添加人编号
     */
    private UserNumber userNumber;
    /**
     * 请求路径
     */
    private String url;
    /**
     * 权限描述
     */
    private String p_msg;
    /**
     * 添加日期
     */
    private Date addtime;

    public Power() {
    }

    public int getP_id() {
        return p_id;
    }

    public void setP_id(int p_id) {
        this.p_id = p_id;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public UserNumber getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(UserNumber userNumber) {
        this.userNumber = userNumber;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getP_msg() {
        return p_msg;
    }

    public void setP_msg(String p_msg) {
        this.p_msg = p_msg;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }
}
