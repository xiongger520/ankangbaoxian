package cn.yunhe.model;

public class JiaoBill {

    private String  year_cir_addtime;
    private String  month_cir_addtime;
    private String  cir_money;


    public String getYear_cir_addtime() {
        return year_cir_addtime;
    }
    public void setYear_cir_addtime(String year_cir_addtime) {
        this.year_cir_addtime = year_cir_addtime;
    }
    public String getMonth_cir_addtime() {
        return month_cir_addtime;
    }
    public void setMonth_cir_addtime(String month_cir_addtime) {
        this.month_cir_addtime = month_cir_addtime;
    }
    public String getCir_money() {
        return cir_money;
    }
    public void setCir_money(String cir_money) {
        this.cir_money = cir_money;
    }
}
