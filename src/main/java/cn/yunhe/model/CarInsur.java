package cn.yunhe.model;

import java.math.BigDecimal;
import java.util.Date;
/*
* 保险记录表
*
* */
public class CarInsur {
    private int ci_id;
    private String ci_name;
    private String ci_type;
    private double comp_rate;
    private BigDecimal maxmoney;
    private double ins_rale;
    private int ci_state;
    private int un_id;
    private Date ci_addtime;
    private BigDecimal ci_money;

    public CarInsur() {
    }

    public CarInsur(int ci_id) {
        this.ci_id = ci_id;
    }

    public CarInsur(String ci_name, String ci_type, double comp_rate, BigDecimal maxmoney, double ins_rale, Date ci_addtime, BigDecimal ci_money) {
        this.ci_name = ci_name;
        this.ci_type = ci_type;
        this.comp_rate = comp_rate;
        this.maxmoney = maxmoney;
        this.ins_rale = ins_rale;
        this.ci_addtime = ci_addtime;
        this.ci_money = ci_money;
    }

    public CarInsur(int ci_id, String ci_name, String ci_type, double comp_rate, BigDecimal maxmoney, double ins_rale, int ci_state, int un_id, Date ci_addtime, BigDecimal ci_money) {
        this.ci_id = ci_id;
        this.ci_name = ci_name;
        this.ci_type = ci_type;
        this.comp_rate = comp_rate;
        this.maxmoney = maxmoney;
        this.ins_rale = ins_rale;
        this.ci_state = ci_state;
        this.un_id = un_id;
        this.ci_addtime = ci_addtime;
        this.ci_money = ci_money;
    }

    public int getCi_id() {
        return ci_id;
    }

    public void setCi_id(int ci_id) {
        this.ci_id = ci_id;
    }

    public String getCi_name() {
        return ci_name;
    }

    public void setCi_name(String ci_name) {
        this.ci_name = ci_name;
    }

    public String getCi_type() {
        return ci_type;
    }

    public void setCi_type(String ci_type) {
        this.ci_type = ci_type;
    }

    public double getComp_rate() {
        return comp_rate;
    }

    public void setComp_rate(double comp_rate) {
        this.comp_rate = comp_rate;
    }

    public BigDecimal getMaxmoney() {
        return maxmoney;
    }

    public void setMaxmoney(BigDecimal maxmoney) {
        this.maxmoney = maxmoney;
    }

    public double getIns_rale() {
        return ins_rale;
    }

    public void setIns_rale(double ins_rale) {
        this.ins_rale = ins_rale;
    }

    public int getCi_state() {
        return ci_state;
    }

    public void setCi_state(int ci_state) {
        this.ci_state = ci_state;
    }

    public int getUn_id() {
        return un_id;
    }

    public void setUn_id(int un_id) {
        this.un_id = un_id;
    }

    public Date getCi_addtime() {
        return ci_addtime;
    }

    public void setCi_addtime(Date ci_addtime) {
        this.ci_addtime = ci_addtime;
    }

    public BigDecimal getCi_money() {
        return ci_money;
    }

    public void setCi_money(BigDecimal ci_money) {
        this.ci_money = ci_money;
    }
}
