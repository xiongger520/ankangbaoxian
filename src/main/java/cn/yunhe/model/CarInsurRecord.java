package cn.yunhe.model;

import java.math.BigDecimal;
import java.util.Date;

/*
* 保险缴费记录表
* */
public class CarInsurRecord {

    private int cri_id;
    private InsurContract insurContract;//外键  保险单编号
    private BigDecimal cri_money;
    private Date cri_addtime;
    private int un_id;
    private String nu_mumber;
    private String cri_msg;

    public CarInsurRecord() {
    }

    public int getCri_id() {
        return cri_id;
    }

    public void setCri_id(int cri_id) {
        this.cri_id = cri_id;
    }

    public InsurContract getInsurContract() {
        return insurContract;
    }

    public void setInsurContract(InsurContract insurContract) {
        this.insurContract = insurContract;
    }

    public BigDecimal getCri_money() {
        return cri_money;
    }

    public void setCri_money(BigDecimal cri_money) {
        this.cri_money = cri_money;
    }

    public Date getCri_addtime() {
        return cri_addtime;
    }

    public void setCri_addtime(Date cri_addtime) {
        this.cri_addtime = cri_addtime;
    }

    public int getUn_id() {
        return un_id;
    }

    public void setUn_id(int un_id) {
        this.un_id = un_id;
    }

    public String getNu_mumber() {
        return nu_mumber;
    }

    public void setNu_mumber(String nu_mumber) {
        this.nu_mumber = nu_mumber;
    }

    public String getCri_msg() {
        return cri_msg;
    }

    public void setCri_msg(String cri_msg) {
        this.cri_msg = cri_msg;
    }
}
