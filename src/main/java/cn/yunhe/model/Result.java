package cn.yunhe.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * @描述
 * @创建人 李辉
 * @创建时间 2017/12/22
 * @修改人和其它信息
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Result {
    /*业务代码*/
    private int code;
    /*提示信息*/
    private String message;
    /*业务数据*/
    private List<Object> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }
}

