package cn.yunhe.model;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/26
 * @描述  账号信息表
 */
public class UserNumber {

    private int un_id;
    private String username;
    private String password;
    private String un_pwdsalt;
    private String un_nickname;
    private Role role;
    private int un_state;
    private Users users;

    public UserNumber() {
    }

    public UserNumber(int un_id, String username, String password, String un_pwdsalt, String un_nickname, Role role, int un_state, Users users) {
        this.un_id = un_id;
        this.username = username;
        this.password = password;
        this.un_pwdsalt = un_pwdsalt;
        this.un_nickname = un_nickname;
        this.role = role;
        this.un_state = un_state;
        this.users = users;
    }

    public int getUn_id() {
        return un_id;
    }

    public void setUn_id(int un_id) {
        this.un_id = un_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUn_pwdsalt() {
        return un_pwdsalt;
    }

    public void setUn_pwdsalt(String un_pwdsalt) {
        this.un_pwdsalt = un_pwdsalt;
    }

    public String getUn_nickname() {
        return un_nickname;
    }

    public void setUn_nickname(String un_nickname) {
        this.un_nickname = un_nickname;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getUn_state() {
        return un_state;
    }

    public void setUn_state(int un_state) {
        this.un_state = un_state;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }
}
