package cn.yunhe.model;

import java.util.Date;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/26
 * @描述  出险调查表
 */
public class Survey {

    /**
     * 调查编号
     */
    private int sur_id;
    private String c_id;
    private Date s_time;
    private String s_site;
    private String client_a;
    private String client_b;
    private String client_c;
    private String pic;
    private String s_msg;

    public Survey() {
    }

    public Survey(int sur_id, String c_id, Date s_time, String s_site, String client_a, String client_b, String client_c, String pic, String s_msg) {
        this.sur_id = sur_id;
        this.c_id = c_id;
        this.s_time = s_time;
        this.s_site = s_site;
        this.client_a = client_a;
        this.client_b = client_b;
        this.client_c = client_c;
        this.pic = pic;
        this.s_msg = s_msg;
    }

    public int getSur_id() {
        return sur_id;
    }

    public void setSur_id(int sur_id) {
        this.sur_id = sur_id;
    }

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public Date getS_time() {
        return s_time;
    }

    public void setS_time(Date s_time) {
        this.s_time = s_time;
    }

    public String getS_site() {
        return s_site;
    }

    public void setS_site(String s_site) {
        this.s_site = s_site;
    }

    public String getClient_a() {
        return client_a;
    }

    public void setClient_a(String client_a) {
        this.client_a = client_a;
    }

    public String getClient_b() {
        return client_b;
    }

    public void setClient_b(String client_b) {
        this.client_b = client_b;
    }

    public String getClient_c() {
        return client_c;
    }

    public void setClient_c(String client_c) {
        this.client_c = client_c;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getS_msg() {
        return s_msg;
    }

    public void setS_msg(String s_msg) {
        this.s_msg = s_msg;
    }
}
