package cn.yunhe.model;

import java.util.Date;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/26
 * @描述  续保申请表
 */
public class Renewal {

    /**
     * 续保编号
     */
    private int r_id;
    /**
     * 外键  订单编号
     */
    private InsurContract insurContract;
    /**
     * 外键 用户编号
     */
    private Users users;
    /**
     * 续保状态（0待审核，1审核通过，2审核不通过）
     */
    private int r_state;
    /**
     * 添加人员编号
     */
    private int un_addid;
    /**
     * 审核人员编号
     */
    private int un_shenid;
    /**
     * 添加时间
     */
    private Date r_addtime;

    public Renewal() {
    }

    public InsurContract getInsurContract() {
        return insurContract;
    }

    public void setInsurContract(InsurContract insurContract) {
        this.insurContract = insurContract;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public int getR_id() {
        return r_id;
    }

    public void setR_id(int r_id) {
        this.r_id = r_id;
    }

    public int getR_state() {
        return r_state;
    }

    public void setR_state(int r_state) {
        this.r_state = r_state;
    }

    public int getUn_addid() {
        return un_addid;
    }

    public void setUn_addid(int un_addid) {
        this.un_addid = un_addid;
    }

    public int getUn_shenid() {
        return un_shenid;
    }

    public void setUn_shenid(int un_shenid) {
        this.un_shenid = un_shenid;
    }

    public Date getR_addtime() {
        return r_addtime;
    }

    public void setR_addtime(Date r_addtime) {
        this.r_addtime = r_addtime;
    }
}
