package cn.yunhe.model;

import java.math.BigDecimal;
import java.util.List;

public class LiPei {

    private List<Integer> ci_id;//保险id
    private BigDecimal cl_money;//伤亡金额
    private BigDecimal cl_money1;//医疗金额
    private BigDecimal cl_money2;//财产金额
    private BigDecimal peifu_dsf;//第三方赔付金额
    private BigDecimal jdmpe;//绝对免赔额
    private double jdmp_cs;//绝对免赔率（车辆损失险）
    private  Double sgzr;//事故责任等级（【事故责任免赔率】    0.05：次要事故责任     0.10：同等事故责任      0.15：主要事故责任    0.20：全部事故责任）
    private  BigDecimal dsf_money;//第三方获得金额
    private BigDecimal zwss;//座位平均损失金额
    private double jdmp_ds;//第三方责任险绝对免赔率
    private double jdmp_dq;//盗抢险绝对免赔率
    private int category;//盗抢险种类(0,1 全车丢失，维修损失)
    private List<BigDecimal> pfje;//其他附加险金额
//    private double aqgd;//是否违反安全装载规定


    public List<Integer> getCi_id() {
        return ci_id;
    }

    public void setCi_id(List<Integer> ci_id) {
        this.ci_id = ci_id;
    }

    public BigDecimal getCl_money() {
        return cl_money;
    }

    public void setCl_money(BigDecimal cl_money) {
        this.cl_money = cl_money;
    }

    public BigDecimal getCl_money1() {
        return cl_money1;
    }

    public void setCl_money1(BigDecimal cl_money1) {
        this.cl_money1 = cl_money1;
    }

    public BigDecimal getCl_money2() {
        return cl_money2;
    }

    public void setCl_money2(BigDecimal cl_money2) {
        this.cl_money2 = cl_money2;
    }

    public BigDecimal getJdmpe() {
        return jdmpe;
    }

    public void setJdmpe(BigDecimal jdmpe) {
        this.jdmpe = jdmpe;
    }

    public double getJdmp_cs() {
        return jdmp_cs;
    }

    public void setJdmp_cs(double jdmp_cs) {
        this.jdmp_cs = jdmp_cs;
    }

    public Double getSgzr() {
        return sgzr;
    }

    public void setSgzr(Double sgzr) {
        this.sgzr = sgzr;
    }

    public BigDecimal getDsf_money() {
        return dsf_money;
    }

    public void setDsf_money(BigDecimal dsf_money) {
        this.dsf_money = dsf_money;
    }

    public BigDecimal getZwss() {
        return zwss;
    }

    public void setZwss(BigDecimal zwss) {
        this.zwss = zwss;
    }

    public double getJdmp_ds() {
        return jdmp_ds;
    }

    public void setJdmp_ds(double jdmp_ds) {
        this.jdmp_ds = jdmp_ds;
    }

    public double getJdmp_dq() {
        return jdmp_dq;
    }

    public void setJdmp_dq(double jdmp_dq) {
        this.jdmp_dq = jdmp_dq;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public List<BigDecimal> getPfje() {
        return pfje;
    }

    public void setPfje(List<BigDecimal> pfje) {
        this.pfje = pfje;
    }

    public BigDecimal getPeifu_dsf() {
        return peifu_dsf;
    }

    public void setPeifu_dsf(BigDecimal peifu_dsf) {
        this.peifu_dsf = peifu_dsf;
    }

    //    public double getAqgd() {
//        return aqgd;
//    }
//
//    public void setAqgd(double aqgd) {
//        this.aqgd = aqgd;
//    }
}
