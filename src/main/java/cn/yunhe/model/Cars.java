package cn.yunhe.model;

import java.math.BigDecimal;
import java.util.Date;

/*
* 汽车信息表
*
* 更改时间：12.27
* 更改内容：chexian_num改为chuxian_num;
* 更改理由：与数据库名称不符
* 更改人：智
* */
public class Cars {

    private  String c_id;
    private Users users;//外键  用户信息表
    private String c_color;
    private Date c_buydate;
    private String c_enginenum;
    private String c_type;
    private String c_brand;
    private String c_brandtype;
    private String c_pic;
    private int c_isauto;
    private String c_enginesize;
    private int seatnum;
    private BigDecimal c_price;
    private String c_evaluater;
    private int c_state;
    private int un_id;
    private Date c_addtime;
    private int un_shenid;
    private int chuxian_num;

    public Cars() {
    }

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public String getC_color() {
        return c_color;
    }

    public void setC_color(String c_color) {
        this.c_color = c_color;
    }

    public Date getC_buydate() {
        return c_buydate;
    }

    public void setC_buydate(Date c_buydate) {
        this.c_buydate = c_buydate;
    }

    public String getC_enginenum() {
        return c_enginenum;
    }

    public void setC_enginenum(String c_enginenum) {
        this.c_enginenum = c_enginenum;
    }

    public String getC_type() {
        return c_type;
    }

    public void setC_type(String c_type) {
        this.c_type = c_type;
    }

    public String getC_brand() {
        return c_brand;
    }

    public void setC_brand(String c_brand) {
        this.c_brand = c_brand;
    }

    public String getC_brandtype() {
        return c_brandtype;
    }

    public void setC_brandtype(String c_brandtype) {
        this.c_brandtype = c_brandtype;
    }

    public String getC_pic() {
        return c_pic;
    }

    public void setC_pic(String c_pic) {
        this.c_pic = c_pic;
    }

    public int getC_isauto() {
        return c_isauto;
    }

    public void setC_isauto(int c_isauto) {
        this.c_isauto = c_isauto;
    }

    public String getC_enginesize() {
        return c_enginesize;
    }

    public void setC_enginesize(String c_enginesize) {
        this.c_enginesize = c_enginesize;
    }

    public int getSeatnum() {
        return seatnum;
    }

    public void setSeatnum(int seatnum) {
        this.seatnum = seatnum;
    }

    public BigDecimal getC_price() {
        return c_price;
    }

    public void setC_price(BigDecimal c_price) {
        this.c_price = c_price;
    }

    public String getC_evaluater() {
        return c_evaluater;
    }

    public void setC_evaluater(String c_evaluater) {
        this.c_evaluater = c_evaluater;
    }

    public int getC_state() {
        return c_state;
    }

    public void setC_state(int c_state) {
        this.c_state = c_state;
    }

    public int getUn_id() {
        return un_id;
    }

    public void setUn_id(int un_id) {
        this.un_id = un_id;
    }

    public Date getC_addtime() {
        return c_addtime;
    }

    public void setC_addtime(Date c_addtime) {
        this.c_addtime = c_addtime;
    }

    public int getUn_shenid() {
        return un_shenid;
    }

    public void setUn_shenid(int un_shenid) {
        this.un_shenid = un_shenid;
    }

    public int getChuxian_num() {
        return chuxian_num;
    }

    public void setChuxian_num(int chuxian_num) {
        this.chuxian_num = chuxian_num;
    }
}
