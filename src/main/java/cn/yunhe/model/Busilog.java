package cn.yunhe.model;

import java.util.Date;

/**
 * 日志记录表
 */
public class Busilog {

    private int log_id;
    private int un_id;
    private String log_busitype;
    private String log_busimasg;
    private Date log_addtime;
    private String username;

    public Busilog() {

    }

    public int getLog_id() {
        return log_id;
    }

    public void setLog_id(int log_id) {
        this.log_id = log_id;
    }

    public int getUn_id() {
        return un_id;
    }

    public void setUn_id(int un_id) {
        this.un_id = un_id;
    }

    public String getLog_busitype() {
        return log_busitype;
    }

    public void setLog_busitype(String log_busitype) {
        this.log_busitype = log_busitype;
    }

    public String getLog_busimasg() {
        return log_busimasg;
    }

    public void setLog_busimasg(String log_busimasg) {
        this.log_busimasg = log_busimasg;
    }

    public Date getLog_addtime() {
        return log_addtime;
    }

    public void setLog_addtime(Date log_addtime) {
        this.log_addtime = log_addtime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
