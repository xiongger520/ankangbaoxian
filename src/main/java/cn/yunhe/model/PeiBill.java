package cn.yunhe.model;

public class PeiBill {

    private String  pei_year_cir_addtime;
    private String  pei_month_cir_addtime;
    private String  pei_cir_money;

    public String getPei_year_cir_addtime() {
        return pei_year_cir_addtime;
    }
    public void setPei_year_cir_addtime(String pei_year_cir_addtime) {
        this.pei_year_cir_addtime = pei_year_cir_addtime;
    }
    public String getPei_month_cir_addtime() {
        return pei_month_cir_addtime;
    }
    public void setPei_month_cir_addtime(String pei_month_cir_addtime) {
        this.pei_month_cir_addtime = pei_month_cir_addtime;
    }
    public String getPei_cir_money() {
        return pei_cir_money;
    }
    public void setPei_cir_money(String pei_cir_money) {
        this.pei_cir_money = pei_cir_money;
    }
}
