package cn.yunhe.model;

import java.util.Date;
/*
* 公司支点表
* */
public class Dept {

    private int d_id;
    private String d_name;
    private String d_phone;
    private String d_ownername;
    private String ownermobile;
    private String incity;
    private String address;
    private double gpslon ;
    private double gpslat;
    private String remark ;
    private Date addtime ;

    public Dept() {
    }

    public int getD_id() {
        return d_id;
    }

    public void setD_id(int d_id) {
        this.d_id = d_id;
    }

    public String getD_name() {
        return d_name;
    }

    public void setD_name(String d_name) {
        this.d_name = d_name;
    }

    public String getD_phone() {
        return d_phone;
    }

    public void setD_phone(String d_phone) {
        this.d_phone = d_phone;
    }

    public String getD_ownername() {
        return d_ownername;
    }

    public void setD_ownername(String d_ownername) {
        this.d_ownername = d_ownername;
    }

    public String getOwnermobile() {
        return ownermobile;
    }

    public void setOwnermobile(String ownermobile) {
        this.ownermobile = ownermobile;
    }

    public String getIncity() {
        return incity;
    }

    public void setIncity(String incity) {
        this.incity = incity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getGpslon() {
        return gpslon;
    }

    public void setGpslon(double gpslon) {
        this.gpslon = gpslon;
    }

    public double getGpslat() {
        return gpslat;
    }

    public void setGpslat(double gpslat) {
        this.gpslat = gpslat;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }
}
