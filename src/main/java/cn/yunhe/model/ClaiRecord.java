package cn.yunhe.model;

import java.math.BigDecimal;
import java.util.Date;

public class ClaiRecord {

    private int cla_id ;
    private InsurContract  insurContract  ;//外键  保险单
    private Claimant claimant ;//外键  报案单
    private BigDecimal cla_money ;
    private Date cla_addtime;
    private UserNumber userNumber;//外键  账号表
    private String un_number;
    private String cla_msg;

    public ClaiRecord() {
    }

    public int getCla_id() {
        return cla_id;
    }

    public void setCla_id(int cla_id) {
        this.cla_id = cla_id;
    }

    public InsurContract getInsurContract() {
        return insurContract;
    }

    public void setInsurContract(InsurContract insurContract) {
        this.insurContract = insurContract;
    }

    public Claimant getClaimant() {
        return claimant;
    }

    public void setClaimant(Claimant claimant) {
        this.claimant = claimant;
    }

    public BigDecimal getCla_money() {
        return cla_money;
    }

    public void setCla_money(BigDecimal cla_money) {
        this.cla_money = cla_money;
    }

    public Date getCla_addtime() {
        return cla_addtime;
    }

    public void setCla_addtime(Date cla_addtime) {
        this.cla_addtime = cla_addtime;
    }

    public UserNumber getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(UserNumber userNumber) {
        this.userNumber = userNumber;
    }

    public String getUn_number() {
        return un_number;
    }

    public void setUn_number(String un_number) {
        this.un_number = un_number;
    }

    public String getCla_msg() {
        return cla_msg;
    }

    public void setCla_msg(String cla_msg) {
        this.cla_msg = cla_msg;
    }
}
