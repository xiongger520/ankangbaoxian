package cn.yunhe.model;

import java.util.Date;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/26
 * @描述   退保申请单
 */
public class Surrender {

    /**
     * 退保编号
     */
    private int s_id;
    /**
     * 订单编号
     */
    private int ic_id;
    /**
     * 外键 用户编号
     */
    private Users users;
    /**
     * 退保缘由
     */
    private String s_msg;
    /**
     * 退保状态（0待审核，1审核通过，2审核不通过）
     */
    private int r_state;
    /**
     * 添加人员编号
     */
    private int un_addid;
    /**
     * 添加日期
     */
    private Date s_addtime;
    /**
     * 审核人编号
     */
    private int un_shenid;

    public Surrender() {
    }

    public Surrender(int s_id, int ic_id, Users users, String s_msg, int r_state, int un_addid, Date s_addtime, int un_shenid) {
        this.s_id = s_id;
        this.ic_id = ic_id;
        this.users = users;
        this.s_msg = s_msg;
        this.r_state = r_state;
        this.un_addid = un_addid;
        this.s_addtime = s_addtime;
        this.un_shenid = un_shenid;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public int getIc_id() {
        return ic_id;
    }

    public void setIc_id(int ic_id) {
        this.ic_id = ic_id;
    }

    public String getS_msg() {
        return s_msg;
    }

    public void setS_msg(String s_msg) {
        this.s_msg = s_msg;
    }

    public int getR_state() {
        return r_state;
    }

    public void setR_state(int r_state) {
        this.r_state = r_state;
    }

    public int getUn_addid() {
        return un_addid;
    }

    public void setUn_addid(int un_addid) {
        this.un_addid = un_addid;
    }

    public Date getS_addtime() {
        return s_addtime;
    }

    public void setS_addtime(Date s_addtime) {
        this.s_addtime = s_addtime;
    }

    public int getUn_shenid() {
        return un_shenid;
    }

    public void setUn_shenid(int un_shenid) {
        this.un_shenid = un_shenid;
    }
}
