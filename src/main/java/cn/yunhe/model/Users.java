package cn.yunhe.model;

import java.util.Date;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/26
 * @描述  用户信息表
 */

/*修改时间：12.27
* 修改人：裴新智
* */
public class Users {

    private int u_id;
    private String u_name;
    private String u_sex;
    private Date u_bir;
    private String u_addr;
    private String u_phone;
    private String u_idcard;
    private String u_emal;
    private String u_qq;
    private String u_other_num;
    private String u_code;
    private String u_remark;
    private int u_jifen;
    private int un_id;
    private Date u_addtime;
    private int u_state;

    public Users() {
    }

    public Users(int u_id, String u_name, String u_sex, Date u_bir, String u_addr, String u_phone, String u_idcard, String u_emal, String u_qq, String u_other_num, String u_code, String u_remark, int u_jifen, int un_id, Date u_addtime, int u_state) {
        this.u_id = u_id;
        this.u_name = u_name;
        this.u_sex = u_sex;
        this.u_bir = u_bir;
        this.u_addr = u_addr;
        this.u_phone = u_phone;
        this.u_idcard = u_idcard;
        this.u_emal = u_emal;
        this.u_qq = u_qq;
        this.u_other_num = u_other_num;
        this.u_code = u_code;
        this.u_remark = u_remark;
        this.u_jifen = u_jifen;
        this.un_id = un_id;
        this.u_addtime = u_addtime;
        this.u_state = u_state;
    }

    public int getU_id() {
        return u_id;
    }

    public void setU_id(int u_id) {
        this.u_id = u_id;
    }

    public String getU_name() {
        return u_name;
    }

    public void setU_name(String u_name) {
        this.u_name = u_name;
    }

    public String getU_sex() {
        return u_sex;
    }

    public void setU_sex(String u_sex) {
        this.u_sex = u_sex;
    }

    public Date getU_bir() {
        return u_bir;
    }

    public void setU_bir(Date u_bir) {
        this.u_bir = u_bir;
    }

    public String getU_addr() {
        return u_addr;
    }

    public void setU_addr(String u_addr) {
        this.u_addr = u_addr;
    }

    public String getU_phone() {
        return u_phone;
    }

    public void setU_phone(String u_phone) {
        this.u_phone = u_phone;
    }

    public String getU_idcard() {
        return u_idcard;
    }

    public void setU_idcard(String u_idcard) {
        this.u_idcard = u_idcard;
    }

    public String getU_emal() {
        return u_emal;
    }

    public void setU_emal(String u_emal) {
        this.u_emal = u_emal;
    }

    public String getU_qq() {
        return u_qq;
    }

    public void setU_qq(String u_qq) {
        this.u_qq = u_qq;
    }

    public String getU_other_num() {
        return u_other_num;
    }

    public void setU_other_num(String u_other_num) {
        this.u_other_num = u_other_num;
    }

    public String getU_code() {
        return u_code;
    }

    public void setU_code(String u_code) {
        this.u_code = u_code;
    }

    public String getU_remark() {
        return u_remark;
    }

    public void setU_remark(String u_remark) {
        this.u_remark = u_remark;
    }

    public int getU_jifen() {
        return u_jifen;
    }

    public void setU_jifen(int u_jifen) {
        this.u_jifen = u_jifen;
    }

    public int getUn_id() {
        return un_id;
    }

    public void setUn_id(int un_id) {
        this.un_id = un_id;
    }

    public Date getU_addtime() {
        return u_addtime;
    }

    public void setU_addtime(Date u_addtime) {
        this.u_addtime = u_addtime;
    }

    public int getU_state() {
        return u_state;
    }

    public void setU_state(int u_state) {
        this.u_state = u_state;
    }
}
