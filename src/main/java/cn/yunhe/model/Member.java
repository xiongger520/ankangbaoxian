package cn.yunhe.model;

import java.util.Date;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/26
 * @描述  会员等级表
 */
public class Member {
    /**
     * 等级编号
     */
    private int m_id;
    /**
     * 等级名称
     */
    private String m_name;
    /**
     * 最小信用（最小信用和最大信用构成一个等级区间）
     */
    private int m_minscore;
    /**
     * 最大信用
     */
    private int m_maxscore;
    /**
     * 保险折扣
     */
    private Double m_discount;
    /**
     * 添加时间
     */
    private Date m_addtime;

    public Member() {
    }

    public Member(int m_id, String m_name, int m_minscore, int m_maxscore, Double m_discount, Date m_addtime) {
        this.m_id = m_id;
        this.m_name = m_name;
        this.m_minscore = m_minscore;
        this.m_maxscore = m_maxscore;
        this.m_discount = m_discount;
        this.m_addtime = m_addtime;
    }

    public int getM_id() {
        return m_id;
    }

    public void setM_id(int m_id) {
        this.m_id = m_id;
    }

    public String getM_name() {
        return m_name;
    }

    public void setM_name(String m_name) {
        this.m_name = m_name;
    }

    public int getM_minscore() {
        return m_minscore;
    }

    public void setM_minscore(int m_minscore) {
        this.m_minscore = m_minscore;
    }

    public int getM_maxscore() {
        return m_maxscore;
    }

    public void setM_maxscore(int m_maxscore) {
        this.m_maxscore = m_maxscore;
    }

    public Double getM_discount() {
        return m_discount;
    }

    public void setM_discount(Double m_discount) {
        this.m_discount = m_discount;
    }

    public Date getM_addtime() {
        return m_addtime;
    }

    public void setM_addtime(Date m_addtime) {
        this.m_addtime = m_addtime;
    }
}
