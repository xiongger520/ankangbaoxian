package cn.yunhe.model;
/*
*
* 保险清单表
*/

public class InserInclude {

    private int ii_id;
    private int ci1;
    private int ci2;
    private int ci3;
    private int ci4;
    private int ci5;
    private int ci6;
    private int ci7;
    private int ci8;
    private int ci9;
    private int ci10;
    private int ci11;
    private int ci12;
    private int ci13;
    private int ci14;
    private int ci15;
    private int ci16;
    private int ci17;
    private int ci18;
    private int ci19;
    private int ci20;

    public InserInclude() {
    }

    public InserInclude(int ii_id, int ci1, int ci2, int ci3, int ci4, int ci5, int ci6, int ci7, int ci8, int ci9, int ci10, int ci11, int ci12, int ci13, int ci14, int ci15, int ci16, int ci17, int ci18, int ci19, int ci20) {
        this.ii_id = ii_id;
        this.ci1 = ci1;
        this.ci2 = ci2;
        this.ci3 = ci3;
        this.ci4 = ci4;
        this.ci5 = ci5;
        this.ci6 = ci6;
        this.ci7 = ci7;
        this.ci8 = ci8;
        this.ci9 = ci9;
        this.ci10 = ci10;
        this.ci11 = ci11;
        this.ci12 = ci12;
        this.ci13 = ci13;
        this.ci14 = ci14;
        this.ci15 = ci15;
        this.ci16 = ci16;
        this.ci17 = ci17;
        this.ci18 = ci18;
        this.ci19 = ci19;
        this.ci20 = ci20;
    }

    public InserInclude(int ii_id, int ci1, int ci2, int ci3, int ci4) {
        this.ii_id = ii_id;
        this.ci1 = ci1;
        this.ci2 = ci2;
        this.ci3 = ci3;
        this.ci4 = ci4;
    }

    public int getIi_id() {
        return ii_id;
    }

    public void setIi_id(int ii_id) {
        this.ii_id = ii_id;
    }

    public int getCi1() {
        return ci1;
    }

    public void setCi1(int ci1) {
        this.ci1 = ci1;
    }

    public int getCi2() {
        return ci2;
    }

    public void setCi2(int ci2) {
        this.ci2 = ci2;
    }

    public int getCi3() {
        return ci3;
    }

    public void setCi3(int ci3) {
        this.ci3 = ci3;
    }

    public int getCi4() {
        return ci4;
    }

    public void setCi4(int ci4) {
        this.ci4 = ci4;
    }

    public int getCi5() {
        return ci5;
    }

    public void setCi5(int ci5) {
        this.ci5 = ci5;
    }

    public int getCi6() {
        return ci6;
    }

    public void setCi6(int ci6) {
        this.ci6 = ci6;
    }

    public int getCi7() {
        return ci7;
    }

    public void setCi7(int ci7) {
        this.ci7 = ci7;
    }

    public int getCi8() {
        return ci8;
    }

    public void setCi8(int ci8) {
        this.ci8 = ci8;
    }

    public int getCi9() {
        return ci9;
    }

    public void setCi9(int ci9) {
        this.ci9 = ci9;
    }

    public int getCi10() {
        return ci10;
    }

    public void setCi10(int ci10) {
        this.ci10 = ci10;
    }

    public int getCi11() {
        return ci11;
    }

    public void setCi11(int ci11) {
        this.ci11 = ci11;
    }

    public int getCi12() {
        return ci12;
    }

    public void setCi12(int ci12) {
        this.ci12 = ci12;
    }

    public int getCi13() {
        return ci13;
    }

    public void setCi13(int ci13) {
        this.ci13 = ci13;
    }

    public int getCi14() {
        return ci14;
    }

    public void setCi14(int ci14) {
        this.ci14 = ci14;
    }

    public int getCi15() {
        return ci15;
    }

    public void setCi15(int ci15) {
        this.ci15 = ci15;
    }

    public int getCi16() {
        return ci16;
    }

    public void setCi16(int ci16) {
        this.ci16 = ci16;
    }

    public int getCi17() {
        return ci17;
    }

    public void setCi17(int ci17) {
        this.ci17 = ci17;
    }

    public int getCi18() {
        return ci18;
    }

    public void setCi18(int ci18) {
        this.ci18 = ci18;
    }

    public int getCi19() {
        return ci19;
    }

    public void setCi19(int ci19) {
        this.ci19 = ci19;
    }

    public int getCi20() {
        return ci20;
    }

    public void setCi20(int ci20) {
        this.ci20 = ci20;
    }
}
