package cn.yunhe.model;
import java.math.BigDecimal;
import java.util.Date;

/*
* 保险订单信息表
* */
public class InsurContract {

    private int ic_id;
    private Cars cars;//外键 车辆信息表
    private int ip_id;
    private CarInsur carInsur;//外键 保险表
    private int un_id;
    private String username;
    private Date ic_addtime;
    private Date ic_totime;
    private int ic_state;
    private int un_shenid;
    private int ic_ispayment;
    private BigDecimal ic_money;
    private BigDecimal max_money;
    private String msg_table;

    public InsurContract() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getIc_id() {
        return ic_id;
    }

    public void setIc_id(int ic_id) {
        this.ic_id = ic_id;
    }

    public Cars getCars() {
        return cars;
    }

    public void setCars(Cars cars) {
        this.cars = cars;
    }

    public int getIp_id() {
        return ip_id;
    }

    public void setIp_id(int ip_id) {
        this.ip_id = ip_id;
    }

    public CarInsur getCarInsur() {
        return carInsur;
    }

    public void setCarInsur(CarInsur carInsur) {
        this.carInsur = carInsur;
    }

    public int getUn_id() {
        return un_id;
    }

    public void setUn_id(int un_id) {
        this.un_id = un_id;
    }

    public Date getIc_addtime() {
        return ic_addtime;
    }

    public void setIc_addtime(Date ic_addtime) {
        this.ic_addtime = ic_addtime;
    }

    public Date getIc_totime() {
        return ic_totime;
    }

    public void setIc_totime(Date ic_totime) {
        this.ic_totime = ic_totime;
    }

    public int getIc_state() {
        return ic_state;
    }

    public void setIc_state(int ic_state) {
        this.ic_state = ic_state;
    }

    public int getUn_shenid() {
        return un_shenid;
    }

    public void setUn_shenid(int un_shenid) {
        this.un_shenid = un_shenid;
    }

    public int getIc_ispayment() {
        return ic_ispayment;
    }

    public void setIc_ispayment(int ic_ispayment) {
        this.ic_ispayment = ic_ispayment;
    }

    public BigDecimal getIc_money() {
        return ic_money;
    }

    public void setIc_money(BigDecimal ic_money) {
        this.ic_money = ic_money;
    }

    public BigDecimal getMax_money() {
        return max_money;
    }

    public void setMax_money(BigDecimal max_money) {
        this.max_money = max_money;
    }

    public String getMsg_table() {
        return msg_table;
    }

    public void setMsg_table(String msg_table) {
        this.msg_table = msg_table;
    }
}
