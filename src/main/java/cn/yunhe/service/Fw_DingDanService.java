package cn.yunhe.service;

import cn.yunhe.model.CarInsur;
import cn.yunhe.model.Dept;
import cn.yunhe.model.InsurContract;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface Fw_DingDanService {

    /*根据用户id返回订单*/
    List<InsurContract> queryInsur(int id);

    List<InsurContract> queryInsurCarid(String c_id);

    /*新增退保信息*/
    void addSurr(Map<String ,Object> map);

    /*根据订单id返回订单*/
    InsurContract queryIn(int id);

    /*续保*/
    void addRenewal(Map<String,Object> map);

    /*查询多个保险 */
    public List<CarInsur> querycarinsur(List<Integer> list);

    /*支点遍历*/
    List<Dept> queryDept();
}
