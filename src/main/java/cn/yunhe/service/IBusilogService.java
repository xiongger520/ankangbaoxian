package cn.yunhe.service;

import cn.yunhe.model.Busilog;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/27
 * @描述
 */
public interface IBusilogService {

    /**
     * 获得所有日志
     * @return
     */
    List<Busilog> queryAllBusilog();

    /**
     * 模糊查询（根据时间区间和名称）
     */
    Page queryLikeBusilog(Map<String ,Object> cond);
    /**
     * 模糊搜索全匹配
     * @param tiaojian
     * @return
     */
     List<Busilog> queryBusilogByTiaojian(String tiaojian);

}
