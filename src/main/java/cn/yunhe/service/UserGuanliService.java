package cn.yunhe.service;

import cn.yunhe.model.Cars;
import cn.yunhe.model.UserNumber;
import cn.yunhe.model.Users;

import java.util.List;
import java.util.Map;

public interface UserGuanliService {

    public int adduser(Map<String,Object> user);
    public int usernum(UserNumber usernum);

    public List<Users> queryUsers();
    public int queryUser(String idcard);
    public Integer queryUserid(Users idcard);

    public List<Users> querydongjieUsers();
    public List<Cars> queryCars();
    public int edittingyonguser(Map<String,Object> uid);
    public int edithuanyuanuser(Map<String,Object> uid);
    public int  editUsers(Map<String,Object> uid);

    public int queryCarsid(String c_id);
    public int addcar(Map<String,Object> car);
    public int editCarsid(Map<String,Object> car);
    public int delCarsid(Map<String,Object> car);

}
