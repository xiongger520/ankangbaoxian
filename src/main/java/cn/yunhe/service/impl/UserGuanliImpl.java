package cn.yunhe.service.impl;

import cn.yunhe.dao.UserGuanliMapper;
import cn.yunhe.model.Cars;
import cn.yunhe.model.UserNumber;
import cn.yunhe.model.Users;
import cn.yunhe.service.UserGuanliService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class UserGuanliImpl implements UserGuanliService {
    @Resource
    private UserGuanliMapper userGuanli;

    @Override
    public int adduser(Map<String, Object> user) {


        userGuanli.adduser(user);      //首先添加用户，进行主键回填
        Long u_id = (Long) user.get("u_id");     //从map中获得回填的主键
        int ii= new Long(u_id).intValue();
        System.out.println("gssjf===="+ii);

         //addusernum(Map<String,Object> usernum);
        return ii;
    }

    @Override
    public int usernum(UserNumber usernum) {
        int flag = userGuanli.usernum(usernum);
        return flag;
    }


    @Override
    public List<Users> queryUsers() {

        List<Users> userdlist = userGuanli.queryUsers();

        return userdlist;
    }

    @Override
    public int queryUser(String idcard) {
        int num = userGuanli.queryUser(idcard);
        return num;
    }

    @Override
    public Integer queryUserid(Users idcard) {
        Integer id = userGuanli.queryUserid(idcard);
        return id;
    }

    public List<Users> querydongjieUsers(){
        List<Users> userdlist = userGuanli.querydongjieUsers();
        return userdlist;
    }

    public List<Cars> queryCars(){
        List<Cars> carslist = userGuanli.queryCars();
        return carslist;
    }

    @Override
    public int edittingyonguser(Map<String, Object> uid) {
        int flag = userGuanli.edittingyonguser(uid);
        System.out.println("45566644");
        return flag;
    }
    @Override
    public int  edithuanyuanuser(Map<String,Object> uid){
        int flag = userGuanli.edithuanyuanuser(uid);
        System.out.println("还原4");
        return flag;
    }

    @Override
    public int editUsers(Map<String, Object> uid) {
        int flag = userGuanli.editUsers(uid);
        System.out.println(4545);
        return flag;
    }

    public int queryCarsid(String c_id){
        int num = userGuanli.queryCarsid(c_id);
        return num;
    }

    @Override
    public int addcar(Map<String, Object> car) {
        int num = userGuanli.addcar(car);
        return num;
    }

    ;
    public int editCarsid(Map<String,Object> car){
        int flag = userGuanli.editCarsid(car);
        System.out.println(1001);
        return flag;
    };

    public int delCarsid(Map<String,Object> car){
        int flag = userGuanli.delCarsid(car);
        System.out.println(1002);
        return flag;
    }

    public UserGuanliMapper getUserGuanli() {
        return userGuanli;
    }

    public void setUserGuanli(UserGuanliMapper userGuanli) {
        this.userGuanli = userGuanli;
    }
}
