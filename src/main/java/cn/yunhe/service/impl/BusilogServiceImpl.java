package cn.yunhe.service.impl;

import cn.yunhe.dao.IBusilogMapper;
import cn.yunhe.model.Busilog;
import cn.yunhe.service.IBusilogService;
import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/27
 * @描述
 */
@Service("busilogService")
public class BusilogServiceImpl implements IBusilogService{

    @Resource
    private IBusilogMapper busilogMapper;

    /**
     * 获得所有日志
     * @return
     */
    @Override
    public List<Busilog> queryAllBusilog() {
        return busilogMapper.queryAllBusilog();
    }

    /**
     * 模糊查询（根据时间区间和名称）
     */
    @Override
    public Page queryLikeBusilog(Map<String, Object> cond) {
        Page page = new Page();
        // 根据条件查询符合的用户列表记录总数，赋值给page的totalNum变量
        page.setTotal(busilogMapper.queryLikeBusilogCount(cond));
        // 从请求参数中获取每页大小
        int pageSize = Integer.parseInt(String.valueOf(cond.get("pageSize")));
        page.setPageSize(pageSize);
        // 从请求参数中获取当前页码
        int curPageNum = Integer.parseInt(String.valueOf(cond.get("pageNum")));
        page.setPageNum(curPageNum);
        //动态计算总页数(总记录数 除以 每页大小，加上  总页数 求余 每页大小，如果余数不为0，则 加 1，否则 加 0 )
        page.setPageNum((int) (page.getTotal() / pageSize + (page.getTotal() % pageSize == 0 ? 0 : 1)));
        //根据条件查询符合的用户列表记录，赋值给page的result变量
        page = PageHelper.startPage(curPageNum, pageSize);
        List list = busilogMapper.queryLikeBusilog(cond);
        return page;
    }

    @Override
    public List<Busilog> queryBusilogByTiaojian(String tiaojian) {
        return busilogMapper.queryBusilogByTiaojian(tiaojian);
    }

    public IBusilogMapper getBusilogMapper() {
        return busilogMapper;
    }

    public void setBusilogMapper(IBusilogMapper busilogMapper) {
        this.busilogMapper = busilogMapper;
    }
}
