package cn.yunhe.service.impl;

import cn.yunhe.dao.IAdminMapper;
import cn.yunhe.model.Admin;
import cn.yunhe.service.IAdminService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("adminService")
public class AdminServiceImpl implements IAdminService {

    @Resource
    private IAdminMapper adminMapper;

    @Override
    public List<Admin> queryAllAdmin() {
        return adminMapper.queryAllAdmin();
    }

    public IAdminMapper AdminMapper() {
        return adminMapper;
    }

    public void setAdminMapper(IAdminMapper adminMapper) {
        this.adminMapper = adminMapper;
    }

}