package cn.yunhe.service.impl;

import cn.yunhe.dao.LoginMapper;
import cn.yunhe.model.Busilog;
import cn.yunhe.model.Power;
import cn.yunhe.model.UserNumber;
import cn.yunhe.model.Users;
import cn.yunhe.service.ILoginService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class LoginServiceImpl implements ILoginService {
    @Resource
    private LoginMapper loginMapper;


    @Override
    public UserNumber LoginUserByUserid(Map<String,Object> map){
        UserNumber users= loginMapper.LoginUserByUserid(map);
        return users;
    }

    @Override
    public UserNumber queryUserByUserid(String username) {
        return loginMapper.queryUserByUserid(username);
    }

    @Override
    public Power queryquanxian(int p_id) {
        Power power = loginMapper.queryquanxian(p_id);
        return power;
    }

    @Override
    public void Zhuxiaouser(Map<String, Object> map) {
        Zhuxiaousers(map);
    }
    public void Zhuxiaousers(Map<String, Object> map) {
        //用于记录登录日志，没有具体含义
    }

    @Override
    public void Loginuser(Map<String, Object> map) {
        Loginusers(map);
    }

    public void Loginusers(Map<String, Object> map) {
        //用于登录注销日志，没有具体含义
    }

}
