package cn.yunhe.service.impl;

import cn.yunhe.dao.Fw_ClaimantMapper;
import cn.yunhe.model.Claimant;
import cn.yunhe.model.Survey;
import cn.yunhe.service.Fw_ClaimantService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("claimantService")
public class Fw_ClaimantServiceImpl implements Fw_ClaimantService {

    @Resource
    private Fw_ClaimantMapper claimantMapper;

    @Override
    public List<Claimant> queryClaimant(int id) {
        return claimantMapper.queryClaimant(id);
    }

    @Override
    public List<Survey> querySurvey(int id) {
        return claimantMapper.querySurvey(id);
    }

    public Fw_ClaimantMapper getClaimantMapper() {
        return claimantMapper;
    }

    public void setClaimantMapper(Fw_ClaimantMapper claimantMapper) {
        this.claimantMapper = claimantMapper;
    }
}
