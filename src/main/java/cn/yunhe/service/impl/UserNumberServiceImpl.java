package cn.yunhe.service.impl;
import cn.yunhe.dao.IUserNumberMapper;
import cn.yunhe.model.UserNumber;
import cn.yunhe.service.IUserNumberService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/28
 * @描述
 */
@Service("userNumberService")
public class UserNumberServiceImpl implements IUserNumberService {
    @Resource
    private IUserNumberMapper userNumberMapper;
    @Override
    public List<UserNumber> queryAllUserNumber() {
        return userNumberMapper.queryAllUserNumber();
    }

    @Override
    public int editUserNumber(Map<String, Object> map) {
        return userNumberMapper.editUserNumber(map);
    }

    @Override
    public int addUserNumber(Map<String, Object> map) {
        return userNumberMapper.addUserNumber(map);
    }

    @Override
    public UserNumber queryUserNumberById(UserNumber userNumber) {
        return userNumberMapper.queryUserNumberById(userNumber);
    }

    @Override
    public int editUserNumberById(Map<String, Object> map) {
        return userNumberMapper.editUserNumberById(map);
    }

    @Override
    public int delUserNumber(Map<String, Object> map) {
        int flag = 0;
        try{
            flag = userNumberMapper.delUserNumber(map);
        }catch (Exception e){
            System.out.println("外键约束");
        }
        return flag;
    }

    @Override
    public int delUserNumbers(Map<String, Object> userNumber) {
        return userNumberMapper.delUserNumbers(userNumber);
    }

    public IUserNumberMapper getUserNumberMapper() {
        return userNumberMapper;
    }

    public void setUserNumberMapper(IUserNumberMapper userNumberMapper) {
        this.userNumberMapper = userNumberMapper;
    }
}
