package cn.yunhe.service.impl;

import cn.yunhe.dao.HeyanMapper;
import cn.yunhe.model.*;
import cn.yunhe.service.HeyanService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
@Service
public class IHeyenServiceImpl implements HeyanService {

    @Resource
    private HeyanMapper heyanMapper;
    @Override
    public int editCarstate(Map<String, Object> c_id) {
        int c=heyanMapper.editCarstate(c_id);
        return c;
    }

    @Override
    public int edituserState(Map<String, Object> u_id) {
        int u=heyanMapper.edituserState(u_id);
        return u;
    }

    @Override
    public List<Claimant> queryClaimantAll() {
        return heyanMapper.queryClaimantAll();
    }

    @Override
    public int editClaimant(Map<String, Object> cl_id) {
        int cal=heyanMapper.editClaimant(cl_id);
        return cal;
    }

    @Override
    public int addRecord(Map<String, Object> crd) {
        int r=heyanMapper.addRecord(crd);
        return r;
    }

    @Override
    public List<CarInsur> queryCarIn() {
        return heyanMapper.queryCarIn();
    }

    @Override
    public int editCarIn(Map<String, Object> ci_id) {
        int i=heyanMapper.editCarIn(ci_id);
        return i;
    }

    @Override
    public List<Surrender> querySurrender() {
        return heyanMapper.querySurrender();
    }

    @Override
    public int editSurrender(Map<String, Object> s_id) {
        int s=heyanMapper.editSurrender(s_id);
        return s;
    }

    @Override
    public List<InsurContract> queryIcAll() {
        return heyanMapper.queryIcAll();
    }

    @Override
    public int editBaoxiandan(Map<String, Object> ic_id) {
        int d=heyanMapper.editBaoxiandan(ic_id);
        return d;
    }

    @Override
    public InsurContract queryIcByid(int ic_id) {
        return heyanMapper.queryIcByid(ic_id);
    }

    @Override
    public int editInsur(Map<String, Object> map) {

        return heyanMapper.editInsur(map);
    }

    @Override
    public int editxubao(Map<String, Object> ma) {
        return heyanMapper.editxubao(ma);
    }

    @Override
    public List<Renewal> queryRenewal() {
        return heyanMapper.queryRenewal();
    }

    @Override
    public int editInsurCont(Map<String, Object> map) {
        return heyanMapper.editInsurCont(map);
    }

    public HeyanMapper getHeyanMapper() {
        return heyanMapper;
    }

    public void setHeyanMapper(HeyanMapper heyanMapper) {
        this.heyanMapper = heyanMapper;
    }
}
