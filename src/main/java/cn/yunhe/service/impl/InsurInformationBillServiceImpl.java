package cn.yunhe.service.impl;

import cn.yunhe.dao.IInsurInformationBillMapper;
import cn.yunhe.model.InsurContract;
import cn.yunhe.service.IInsurInformationBillService;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.List;

@Service("/insurInformationBillService")
public class InsurInformationBillServiceImpl implements IInsurInformationBillService{

     @Resource
    private IInsurInformationBillMapper insurInformationBillMapper;

    @Override
    public List<InsurContract> queryInsurContract() {
        return insurInformationBillMapper.queryInsurContract();
    }

    /* get set */
    public IInsurInformationBillMapper getInsurInformationBillMapper() {
        return insurInformationBillMapper;
    }

    public void setInsurInformationBillMapper(IInsurInformationBillMapper insurInformationBillMapper) {
        this.insurInformationBillMapper = insurInformationBillMapper;
    }
}
