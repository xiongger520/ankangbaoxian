package cn.yunhe.service.impl;

import cn.yunhe.dao.IRoleMapper;
import cn.yunhe.model.Role;
import cn.yunhe.service.IRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/26
 * @描述
 */
@Service("roleService")
public class RoleServiceImpl implements IRoleService{

    @Resource
    private IRoleMapper roleMapper;

    @Override
    public List<Role> getAllRole() {
        return roleMapper.getAllRole();
    }

    @Override
    public int addRole(Map<String, Object> map) {
        return roleMapper.addRole(map);
    }

    @Override
    public Role queryRoleById(Role role) {
        return roleMapper.queryRoleById(role);
    }

    @Override
    public int editRoleById(Map<String, Object> map) {
        return roleMapper.editRoleById(map);
    }

    @Override
    public int delRoles(Map<String, Object> role) {
        return roleMapper.delRoles(role);
    }

    @Override
    public int delRole(Map<String, Object> map) {
        int flag = 0;
        try{
            flag = roleMapper.delRole(map);
        }catch (Exception e){
            System.out.println("外键约束");
        }
        return flag;
    }

    public IRoleMapper getRoleMapper() {
        return roleMapper;
    }

    public void setRoleMapper(IRoleMapper roleMapper) {
        this.roleMapper = roleMapper;
    }
}
