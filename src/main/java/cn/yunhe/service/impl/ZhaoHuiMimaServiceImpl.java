package cn.yunhe.service.impl;

import cn.yunhe.dao.IZhaoHuiMima;
import cn.yunhe.model.UserNumber;
import cn.yunhe.service.IZhaoHuiMimaService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class ZhaoHuiMimaServiceImpl implements IZhaoHuiMimaService {
    @Resource
    public IZhaoHuiMima iZhaoHuiMima;
    @Override
    public UserNumber queryUserNumber(UserNumber userNumber) {
        UserNumber userNumber1 = iZhaoHuiMima.queryUserNumber(userNumber);
        return userNumber1;
    }

    @Override
    public int editUserNumberpassword(Map<String,Object> map) {
        int flag=iZhaoHuiMima.editUserNumberpassword(map);
        return flag;
    }

    public IZhaoHuiMima getiZhaoHuiMima() {
        return iZhaoHuiMima;
    }

    public void setiZhaoHuiMima(IZhaoHuiMima iZhaoHuiMima) {
        this.iZhaoHuiMima = iZhaoHuiMima;
    }
}
