package cn.yunhe.service.impl;

import cn.yunhe.dao.IPowerMapper;
import cn.yunhe.model.Power;
import cn.yunhe.service.IPowerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2018/1/11
 * @描述
 */
@Service
public class PowerSerciceImpl implements IPowerService{

    @Resource
    private IPowerMapper powerMapper;

    @Override
    public int addPower(Map<String, Object> map) {
        int flag = powerMapper.addPower(map);
        return flag;
    }

    @Override
    public int delPower(Map<String, Object> map) {
        return powerMapper.delPower(map);
    }

    @Override
    public Power queryPowerById(Power power) {
        return powerMapper.queryPowerById(power);
    }

    @Override
    public List<Power> queryAllPower() {
        return powerMapper.queryAllPower();
    }

    @Override
    public int editPower(Map<String, Object> map) {
        return powerMapper.editPower(map);
    }

    @Override
    public int delPowers(Map<String, Object> map) {
        return powerMapper.delPowers(map);
    }

    @Override
    public Power queryPowerByUrl(String url) {
        return powerMapper.queryPowerByUrl(url);
    }

    public IPowerMapper getPowerMapper() {
        return powerMapper;
    }

    public void setPowerMapper(IPowerMapper powerMapper) {
        this.powerMapper = powerMapper;
    }
}
