package cn.yunhe.service.impl;

import cn.yunhe.dao.IBillMapper;
import cn.yunhe.model.CarInsurRecord;
import cn.yunhe.model.JiaoBill;
import cn.yunhe.model.PeiBill;
import cn.yunhe.service.IBillService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service("billService")
public class BillServiceImpl implements IBillService{

    @Resource
    private IBillMapper billMapper;

    /*查询 缴费记录表 里的每年各个月的 收入*/
    public List<JiaoBill> queryCar_insur_record() {
         return  billMapper.queryCar_insur_record();
    }
    /*查询 赔付记录表 里的每年各个月的收入*/
    public  List<PeiBill> queryClai_record() {
        return billMapper.queryClai_record();
    }



    /*修改密码和昵称*/
    public int editPwd_name(Map<String, Object> map) {
        return billMapper.editPwd_name(map);
    }

    /*get set*/
    public IBillMapper getBillMapper() {
        return billMapper;
    }
    public void setBillMapper(IBillMapper billMapper) {
        this.billMapper = billMapper;
    }
}
