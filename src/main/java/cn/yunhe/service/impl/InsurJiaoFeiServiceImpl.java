package cn.yunhe.service.impl;

import cn.yunhe.dao.IInsurJiaoFeiMapper;
import cn.yunhe.model.CarInsurRecord;
import cn.yunhe.model.InsurContract;
import cn.yunhe.service.IInsurJiaoFeiService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service("/insurJiaoFeiService")
public class InsurJiaoFeiServiceImpl  implements IInsurJiaoFeiService{


    @Resource
    private IInsurJiaoFeiMapper iInsurJiaoFeiMapper;

    @Override
    public List<InsurContract> queryInsurContracts() {
        return iInsurJiaoFeiMapper.queryInsurContracts();
    }
   /*更改缴费信息*/
    public boolean editJiaoFeiNumber(Map<String, Object> map) {
      boolean flag=iInsurJiaoFeiMapper.editJiaoFeiNumber(map);
      return flag;
    }
    /*添加保险缴费记录*/

    @Override
    public Integer addCarInsurRecord(Map<String,Object> map) {
        return iInsurJiaoFeiMapper.addCarInsurRecord(map);
    }

    @Override
    public Integer editUserJiFen(Map<String, Object> map) {
        return iInsurJiaoFeiMapper.editUserJiFen(map);
    }

    public IInsurJiaoFeiMapper getiInsurJiaoFeiMapper() {
        return iInsurJiaoFeiMapper;
    }

    public void setiInsurJiaoFeiMapper(IInsurJiaoFeiMapper iInsurJiaoFeiMapper) {
        this.iInsurJiaoFeiMapper = iInsurJiaoFeiMapper;
    }


}
