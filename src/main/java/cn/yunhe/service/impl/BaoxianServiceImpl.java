package cn.yunhe.service.impl;

import cn.yunhe.dao.IBaoxianMapper;
import cn.yunhe.model.CarInsur;
import cn.yunhe.model.InsurPack;
import cn.yunhe.service.IBaoxianService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
@Service("baoxianServiceImpl")
public class BaoxianServiceImpl implements IBaoxianService {

    @Resource
    private IBaoxianMapper baoxianMapper;


    @Override
    public boolean addBaoxian(Map<String, Object> cai) {
        boolean ba= baoxianMapper.addBaoxian(cai);
        return ba;
    }

    @Override
    public List<CarInsur> getAllCarInsur() {
        return baoxianMapper.getAllCarInsur();
    }

    @Override
    public int getCarInsur(String ci_name) {
        int cn=baoxianMapper.getCarInsur(ci_name);
        return cn;
    }

    @Override
    public int editState(Map<String, Object> ci_id) {
        int ed=baoxianMapper.editState(ci_id);
        return ed;
    }

    @Override
    public List<CarInsur> queryCarInsurAll() {
        return baoxianMapper.queryCarInsurAll();
    }

    @Override
    public int addInserInclude(Map<String, Object> inser) {
        baoxianMapper.addInserInclude(inser);
      /*  System.out.println("45645"+inser.get("ii_id"));*/

        Long ii_id = (Long) inser.get("ii_id");
        int id = new Long(ii_id).intValue();
        return id;
    }

    @Override
    public int addTaocan(Map<String, Object> pack) {
        int ta=baoxianMapper.addTaocan(pack);
        return ta;
    }

    @Override
    public List<InsurPack> queryInsurPack() {
        return baoxianMapper.queryInsurPack();
    }

    @Override
    public int editTaocan(Map<String, Object> ip_id) {
        int xiu=baoxianMapper.editTaocan(ip_id);
        return xiu;
    }

    @Override
    public int queryTao(String ip_name) {
        int n=baoxianMapper.queryTao(ip_name);
        return n;
    }

    public IBaoxianMapper getBaoxianMapper() {
        return baoxianMapper;
    }

    public void setBaoxianMapper(IBaoxianMapper baoxianMapper) {
        this.baoxianMapper = baoxianMapper;
    }
}
