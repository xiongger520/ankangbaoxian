package cn.yunhe.service.impl;

import cn.yunhe.dao.IBuyInsurMapper;
import cn.yunhe.model.CarInsur;
import cn.yunhe.model.Cars;
import cn.yunhe.model.InsurPack;
import cn.yunhe.model.Member;
import cn.yunhe.service.IBuyInsurService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service("buyInsurService")
public class BuyInsurServiceImpl implements IBuyInsurService{

    @Resource
    private IBuyInsurMapper buyInsurMapper;

    /*通过用户查用户的车牌号*/
    public List<Cars> queryCaridByUser(String name) {
        return buyInsurMapper.queryCaridByUser(name);
    }
    /*通过用户身份证查用户的车牌号*/
    public List<Cars> queryCaridByIdcard(String idcard) {
        return buyInsurMapper.queryCaridByIdcard(idcard);
    }

    /*查询出所有保险的种类*/
    public List<CarInsur> queryInsurName() {
        return buyInsurMapper.queryInsurName();
    }

    /*保险套餐*/
    /*遍历出所有保险套餐的名称  */
    public List<InsurPack> queryInsurPackName() {
        return buyInsurMapper.queryInsurPackName();
    }
    /*查询保险套餐里的具体  保险的种类*/
    public CarInsur queryInsurPackInsur(int id) {
        return buyInsurMapper.queryInsurPackInsur(id);
    }

    /*查询出 用户购买自选保险的 详细内容*/
    public List<CarInsur> queryCarInsur(Map<String,Object> list1) {
        return buyInsurMapper.queryCarInsur(list1);
    }

    @Override
    public InsurPack queryInsurPack(int ip_id) {
        return buyInsurMapper.queryInsurPack(ip_id);
    }

    @Override
    public Cars queryCarUser(String c_id) {

        return buyInsurMapper.queryCarUser( c_id);
    }

    @Override
    public Member queryUsermember(int score) {
        return buyInsurMapper.queryUsermember(score);
    }

    @Override
    public int addbaoxian(Map<String, Object> baoxian) {
        return buyInsurMapper.addbaoxian(baoxian);
    }

    /*通过id查出保险的详细内容*/
    public CarInsur queryCarInsurById(int id) {
        return buyInsurMapper.queryCarInsurById(id);
    }

    public IBuyInsurMapper getBuyInsurMapper() {
        return buyInsurMapper;
    }

    public void setBuyInsurMapper(IBuyInsurMapper buyInsurMapper) {
        this.buyInsurMapper = buyInsurMapper;
    }
}
