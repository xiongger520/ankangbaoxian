package cn.yunhe.service.impl;

import cn.yunhe.dao.LipeiMapper;
import cn.yunhe.model.*;
import cn.yunhe.service.ILiPeiService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("liPeiService")
public class LiPeiServiceImpl implements ILiPeiService{
    @Resource
    private LipeiMapper lipeiMapper;
    /**
     * 增加报案单
     */

    public void addClaimant(Map<String,Object> map){

        lipeiMapper.addClaimant(map);
    }

    /**
     * 增加调查表
     * @return
     */
    public void addSurvey(Map<String,Object> map)
    {
        lipeiMapper.addSurvey(map);
    }

    /**
     * 查找全部报案单
     * @return
     */
    public List<Claimant> queryClaimant(){
        return lipeiMapper.queryClaimant();
    }

    /**
     * 通过车牌号得到车辆信息
     */
    public Cars queryCarById(String c_id){

        return lipeiMapper.queryCarById(c_id);
    };
    /**
     * 查找全部报案单
     * @return
     */
    public List<LiPeiJieSuan>querystate(){
        return lipeiMapper.querystate();
    }
    public int  edit_jiesuan(Map<String,Object> cl_id){
        int flag = lipeiMapper.edit_jiesuan(cl_id);
        return flag;
    }
    /**
     * 增加理赔记录
     */
   public void addClairecord(Map<String,Object> map){
       lipeiMapper.addClairecord(map);
   };

    /**
     * 查询全部理赔记录
     * @return
     */
    public List<ClaiRecord> queryClaiRecord(){
        return lipeiMapper.queryClaiRecord();
    }

    @Override
    public List<InsurContract> queryInsurContractById(String c_id) {
        return lipeiMapper.queryInsurContractById(c_id);
    }
    /**
     * 根据车辆查询报案单信息
     */
    public List<Claimant> queryClaimantById(String c_id,int cl_jiesuan){
        return lipeiMapper.queryClaimantById(c_id,cl_jiesuan);
    }


    /**
     * 根据用户ID查询用户
     */
    public Users queryUserById(int u_id){
        return lipeiMapper.queryUserById(u_id);
    }

    @Override
    public List<CarInsur> queryCarInsur(List<Integer> list) {
        return lipeiMapper.queryCarInsur(list);
    }

    @Override
    public int editCarChuxianNum(Map<String,Object> map) {
        return lipeiMapper.editCarChuxianNum(map);
    }


    public LipeiMapper getLipeiMapper() {
        return lipeiMapper;
    }

    public void setLipeiMapper(LipeiMapper lipeiMapper) {
        this.lipeiMapper = lipeiMapper;
    }
}
