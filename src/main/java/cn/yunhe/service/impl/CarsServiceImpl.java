package cn.yunhe.service.impl;

import cn.yunhe.dao.CarsMapper;
import cn.yunhe.model.Cars;
import cn.yunhe.service.CarsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("carsService")
public class CarsServiceImpl implements CarsService {

    @Resource
    private CarsMapper carsMapper;
    @Override
    public List<Cars> queryCars(int id) {
        return carsMapper.queryCars(id);
    }

    public CarsMapper getCarsMapper() {
        return carsMapper;
    }

    public void setCarsMapper(CarsMapper carsMapper) {
        this.carsMapper = carsMapper;
    }
}
