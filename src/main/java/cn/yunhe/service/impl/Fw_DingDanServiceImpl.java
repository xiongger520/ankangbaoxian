package cn.yunhe.service.impl;

import cn.yunhe.dao.Fw_DingDanMapper;
import cn.yunhe.model.CarInsur;
import cn.yunhe.model.Dept;
import cn.yunhe.model.InsurContract;
import cn.yunhe.service.Fw_DingDanService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("Fw_DingDanService")
public class Fw_DingDanServiceImpl implements Fw_DingDanService {

    @Resource
    private Fw_DingDanMapper dingDanMapper;

    @Override
    public List<InsurContract> queryInsur(int id) {
        return dingDanMapper.queryInsur(id);
    }

    @Override
    public List<InsurContract> queryInsurCarid(String c_id) {

        return dingDanMapper.queryInsurCarid(c_id);
    }

    @Override
    public void addSurr(Map<String, Object> map) {
        dingDanMapper.addSurr(map);
    }

    @Override
    public InsurContract queryIn(int id) {
        return dingDanMapper.queryIn(id);
    }

    @Override
    public void addRenewal(Map<String, Object> map) {
        dingDanMapper.addRenewal(map);
    }

    @Override
    public List<CarInsur> querycarinsur(List<Integer> list) {
        return dingDanMapper.querycarinsur(list);
    }

    @Override
    public List<Dept> queryDept() {
        return dingDanMapper.queryDept();
    }


    public Fw_DingDanMapper getDingDanMapper() {
        return dingDanMapper;
    }

    public void setDingDanMapper(Fw_DingDanMapper dingDanMapper) {
        this.dingDanMapper = dingDanMapper;
    }
}
