package cn.yunhe.service.impl;

import cn.yunhe.dao.UsersMapper;
import cn.yunhe.model.Users;
import cn.yunhe.service.UsersService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("userService")
public class UsersServiceImpl implements UsersService {

    @Resource
    private UsersMapper usersMapper;

    @Override
    public Users queryUser(int id) {
        return usersMapper.queryUser(id);

    }

    public UsersMapper getUsersMapper() {
        return usersMapper;
    }

    public void setUsersMapper(UsersMapper usersMapper) {
        this.usersMapper = usersMapper;
    }
}
