package cn.yunhe.service.impl;

import cn.yunhe.dao.IDeptMapper;
import cn.yunhe.model.Dept;
import cn.yunhe.service.IDeptService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/29
 * @描述
 */
@Service("deptService")
public class DeptServiceImpl implements IDeptService{
    @Resource
    private IDeptMapper deptMapper;

    @Override
    public List<Dept> queryLikeDept() {
        return deptMapper.queryLikeDept();
    }

    @Override
    public int addDept(Map<String, Object> map) {
        int flag = deptMapper.addDept(map);
        return flag;
    }

    @Override
    public int delDepts(Map<String, Object> dept) {
        return deptMapper.delDepts(dept);
    }

    @Override
    public int delDept(Map<String, Object> map) {
        return deptMapper.delDept(map);
    }

    @Override
    public int editDept(Map<String, Object> map) {
        return deptMapper.editDept(map);
    }

    @Override
    public Dept queryDeptById(Dept dept) {
        return deptMapper.queryDeptById(dept);
    }

    public IDeptMapper getDeptMapper() {
        return deptMapper;
    }

    public void setDeptMapper(IDeptMapper deptMapper) {
        this.deptMapper = deptMapper;
    }
}
