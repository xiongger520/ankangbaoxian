package cn.yunhe.service;

import cn.yunhe.model.JiaoBill;
import cn.yunhe.model.PeiBill;

import java.util.List;
import java.util.Map;


public interface IBillService {

    /*查询 缴费记录表 里的每年各个月的 收入*/
    public List<JiaoBill> queryCar_insur_record();

    /*查询 赔付记录表 里的每年各个月的收入*/
    public  List<PeiBill> queryClai_record();



    /*修改密码和昵称*/
    public int editPwd_name(Map<String,Object> map);
}
