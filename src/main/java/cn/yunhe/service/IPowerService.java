package cn.yunhe.service;

import cn.yunhe.model.Power;

import java.util.List;
import java.util.Map;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2018/1/11
 * @描述
 */
public interface IPowerService {
    /**
     * 新增权限
     * @param map
     * @return
     */
    int addPower(Map<String,Object> map);

    /**
     * 删除权限
     * @param map
     * @return
     */
    int delPower(Map<String,Object> map);

    /**
     * 根据id查询权限
     * @param power
     * @return
     */
    Power queryPowerById(Power power);

    /**
     * 根据url查询权限
     * @param url
     * @return
     */
    Power queryPowerByUrl(String url);

    /**
     * 修改权限信息
     * @param map
     * @return
     */
    int editPower(Map<String,Object> map);

    /**
     * 获取所有的权限信息
     * @param
     * @return
     */
    List<Power> queryAllPower();

    /**
     * 删除多个权限
     * @param map
     * @return
     */
    int delPowers(Map<String,Object> map);
}
