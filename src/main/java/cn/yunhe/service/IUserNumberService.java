package cn.yunhe.service;

import cn.yunhe.model.UserNumber;
import java.util.List;
import java.util.Map;


/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/28
 * @描述
 */
public interface IUserNumberService {

    /**
     * 获取所有管理员账号
     * @return
     */
    List<UserNumber> queryAllUserNumber();
    /**
     * 修改管理员信息
     * @param map
     */
    int editUserNumber(Map<String,Object> map);

    /**
     * 新增管理员
     * @param map
     */
    int addUserNumber(Map<String,Object> map);

    /**
     * 根据id获取账号信息
     * @param userNumber
     * @return
     */
    UserNumber queryUserNumberById(UserNumber userNumber);

    /**
     * 修改管理○信息
     * @param map
     * @return
     */
    int editUserNumberById(Map<String,Object> map);
    /**
     * 删除账号
     * @param map
     * @return
     */
    int delUserNumber(Map<String,Object> map);
    /**
     * 删除多个账号
     * @param userNumber
     * @return
     */
    int delUserNumbers(Map<String,Object> userNumber);
}
