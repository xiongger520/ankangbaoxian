package cn.yunhe.service;

import cn.yunhe.model.*;

import java.util.List;
import java.util.Map;

public interface HeyanService {
    int editCarstate(Map<String,Object> c_id);

    int edituserState(Map<String,Object> u_id);

    List<Claimant> queryClaimantAll();

    int editClaimant(Map<String,Object> cl_id);

    int addRecord(Map<String,Object> ma);

    List<CarInsur> queryCarIn();

    int editCarIn(Map<String,Object> ci_id);

    List<Surrender> querySurrender();

    int editSurrender(Map<String,Object> s_id);

    List<InsurContract> queryIcAll();

    int editBaoxiandan(Map<String,Object> ic_id);

    //查询要退的保险订单
    InsurContract queryIcByid(int ic_id);

    //将要退的保险单删除
    int editInsur(Map<String,Object> map);

    //保险单核验
    int editxubao(Map<String,Object> ma);

    //查询续保申请
    List<Renewal> queryRenewal();

    //修改订单的状态，修改为续保，并更新应交金额
    int editInsurCont(Map<String,Object> map);

}
