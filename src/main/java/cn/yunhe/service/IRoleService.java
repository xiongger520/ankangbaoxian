package cn.yunhe.service;

import cn.yunhe.model.Role;

import java.util.List;
import java.util.Map;

/**
 * @创建人 项目二组 鲍康
 * @创建时间 2017/12/26
 * @描述
 */
public interface IRoleService {

    List<Role> getAllRole();

    /**
     * 新增角色
     * @param map
     * @return
     */
    int addRole(Map<String,Object> map);

    /**
     * 根据id查询角色信息
     * @param role
     * @return
     */
    Role queryRoleById(Role role);

    /**
     * 修改角色信息
     * @param map
     * @return
     */
    int editRoleById(Map<String,Object> map);

    /**
     * 删除角色
     * @param map
     * @return
     */
    int delRole(Map<String,Object> map);

    /**
     * 删除多个角色
     * @param role
     * @return
     */
    int delRoles(Map<String,Object> role);
}
