package cn.yunhe.service.aspect;

import cn.yunhe.dao.IBusilogMapper;
import cn.yunhe.model.Busilog;
import cn.yunhe.model.UserNumber;
import cn.yunhe.model.Users;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/*
* 主要用于记录日志，属于一个切面
* */
@Aspect
@Component("logutil")
public class LogAspect {

   /* public void log(String type,int num){
        System.out.println("日志："+currdate()+type+num+"部手机");
    }

    public String currdate(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/hh HH:mm:ss");
        return sdf.format(new Date());
    }*/
   @Resource
   private IBusilogMapper iBusilogMapper;

    @AfterReturning("execution( * add*(..)) && within(cn.yunhe.service.impl.*)")
    public void afterReturnnig(JoinPoint jp) throws Throwable{
        Object[] args = jp.getArgs();
        // Users US = (Users) args[1];
        Map<String,Object> ma = (Map<String,Object>) args[0];
        String methpodname = jp.getSignature().getName();
        Busilog bu =(Busilog) ma.get("blog");
        System.out.println("日志：");
        iBusilogMapper.insertLog(bu);

    }

    @AfterReturning("execution(* edit*(..)) && within(cn.yunhe.service.impl.*)")
    public void afterReturnnig1(JoinPoint jp) throws Throwable{
        Object[] args = jp.getArgs();
        // Users US = (Users) args[1];
        Map<String,Object> ma = (Map<String,Object>) args[0];
        String methpodname = jp.getSignature().getName();
        Busilog bu =(Busilog) ma.get("blog");
        System.out.println("日志：");
        iBusilogMapper.insertLog(bu);


    }

    @AfterReturning("execution( * del*(..)) && within(cn.yunhe.service.impl.*)")
    public void afterReturnnig2(JoinPoint jp) throws Throwable{
        Object[] args = jp.getArgs();
        // Users US = (Users) args[1];
        Map<String,Object> ma = (Map<String,Object>) args[0];
        String methpodname = jp.getSignature().getName();
        Busilog bu =(Busilog) ma.get("blog");
        System.out.println("日志：");
        iBusilogMapper.insertLog(bu);
    }

    @AfterReturning("execution(* Login*(..)) && within(cn.yunhe.service.impl.*)")
    public void afterReturnnig3(JoinPoint jp) throws Throwable{
        Object[] args = jp.getArgs();
        // Users US = (Users) args[1];
        Map<String,Object> ma = (Map<String,Object>) args[0];
        String methpodname = jp.getSignature().getName();
        Busilog bu =(Busilog) ma.get("blog");
        System.out.println("日志：");
        iBusilogMapper.insertLog(bu);
    }

    @AfterReturning("execution( * Zhuxiao*(..)) && within(cn.yunhe.service.impl.*)")
    public void afterReturnnig4(JoinPoint jp) throws Throwable{
        Object[] args = jp.getArgs();
        // Users US = (Users) args[1];
        Map<String,Object> ma = (Map<String,Object>) args[0];
        String methpodname = jp.getSignature().getName();
        Busilog bu =(Busilog) ma.get("blog");
        System.out.println("日志：");
        iBusilogMapper.insertLog(bu);
    }

    public IBusilogMapper getiBusilogMapper() {
        return iBusilogMapper;
    }

    public void setiBusilogMapper(IBusilogMapper iBusilogMapper) {
        this.iBusilogMapper = iBusilogMapper;
    }
}
