package cn.yunhe.service;

import cn.yunhe.model.Claimant;
import cn.yunhe.model.Survey;

import java.util.List;

public interface Fw_ClaimantService {

    /*根据用户id，查询理赔信息表*/
    List<Claimant> queryClaimant(int id);

    /*根据用户u_id，查询出险信息表*/
    List<Survey> querySurvey(int id);
}
