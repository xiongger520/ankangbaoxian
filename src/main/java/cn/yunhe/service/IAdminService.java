package cn.yunhe.service;

import cn.yunhe.model.Admin;


import java.util.List;

public interface IAdminService {
    List<Admin> queryAllAdmin();
}
