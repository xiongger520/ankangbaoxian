package cn.yunhe.service;

import cn.yunhe.model.Busilog;
import cn.yunhe.model.Power;
import cn.yunhe.model.UserNumber;
import cn.yunhe.model.Users;

import java.util.Map;

public interface ILoginService {
    public UserNumber LoginUserByUserid(Map<String,Object> map);
    public UserNumber queryUserByUserid(String username);
    public Power queryquanxian(int p_id);
    public void  Zhuxiaouser(Map<String,Object> map);
    public void  Loginuser(Map<String,Object> map);
}
