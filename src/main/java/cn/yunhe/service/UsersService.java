package cn.yunhe.service;

import cn.yunhe.model.Users;

public interface UsersService {

    /*根据id查询用户信息*/
    Users queryUser(int id);
}
