package cn.yunhe.service;

import cn.yunhe.model.CarInsur;
import cn.yunhe.model.InsurPack;

import java.util.List;
import java.util.Map;

public interface IBaoxianService {
    //添加保险
    boolean addBaoxian(Map<String,Object> ma);
    //查询所有
    List<CarInsur> getAllCarInsur();
    //根据名称查询
    int getCarInsur(String ci_name);
    //根据id修改状态
    int editState(Map<String,Object> ci_id);
    //只查上架保险
    List<CarInsur> queryCarInsurAll();
    //添加清单
    int addInserInclude(Map<String,Object> ma);
    //添加套餐
    int addTaocan(Map<String,Object>ma);
    //查询套餐
    List<InsurPack> queryInsurPack();
    //根据id修改套餐状态
    int editTaocan(Map<String,Object> ip_id);
    //根据名称查询套餐
    int queryTao(String ip_name);


}
