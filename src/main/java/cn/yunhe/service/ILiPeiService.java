package cn.yunhe.service;

import cn.yunhe.model.*;

import java.util.List;
import java.util.Map;

public interface ILiPeiService {
    /**
     * 增加报案单
     */
    void addClaimant(Map<String, Object> map);

    /**
     * 增加调查表
     */
    void addSurvey(Map<String, Object> map);

    /**
     * 查找全部报案单
     */
    List<Claimant> queryClaimant();

    /**
     * 通过车牌号得到车辆信息
     */
    Cars queryCarById(String c_id);

    /**
     * 查找待结算
     */
    List<LiPeiJieSuan> querystate();

    public int edit_jiesuan(Map<String, Object> cl_id);

    /**
     * 增加理赔记录
     */
    void addClairecord(Map<String, Object> map);

    /**
     * 查找全部理赔记录
     */
    List<ClaiRecord> queryClaiRecord();

    /**
     * 根据车辆查询保险订单
     */
    List<InsurContract> queryInsurContractById(String c_id);

    /**
     * 根据车辆查询报案单信息
     */

    List<Claimant> queryClaimantById(String c_id,int cl_jiesuan);
    /**
     * 根据用户ID查询用户
     */
    Users queryUserById(int u_id);

      /**
     * 查询多个保险
     * @param list
     * @return
     */
    List<CarInsur> queryCarInsur(List<Integer> list);
    /**
     * 修改车辆出险次数
     * @param map
     * @return
     */
    int editCarChuxianNum(Map<String,Object> map);

}
