package cn.yunhe.service;

import cn.yunhe.model.InsurContract;

import java.util.List;

public interface IInsurInformationBillService {

    /*查询保险订单所有内容  将数据遍历到页面上*/
    public List<InsurContract> queryInsurContract ();
}
