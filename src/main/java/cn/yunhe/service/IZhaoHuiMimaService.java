package cn.yunhe.service;

import cn.yunhe.model.UserNumber;

import java.util.Map;

public interface IZhaoHuiMimaService {

    public UserNumber queryUserNumber(UserNumber userNumber);
    public int editUserNumberpassword(Map<String,Object> map);
}
