package cn.yunhe.test;

import cn.yunhe.model.*;
import cn.yunhe.service.ILiPeiService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/spring-mybatis.xml"})
public class Demo1 {
    @Resource//注入
    private ILiPeiService liPeiService;
    @Test
    public void test1(){
        Map<String,Object> ma = new HashMap<String,Object>();
       InsurContract insurContract=new InsurContract();
              insurContract.setIc_id(1);
        ClaiRecord claiRecord=new ClaiRecord();
           claiRecord.setInsurContract(insurContract);
             ma.put("claiRecord",claiRecord);
          liPeiService.addClairecord(ma);
    }


    @Test
    public void test2(){
        Map<String,Object> ma = new HashMap<String,Object>();
        Busilog blog=new Busilog();//日志信息的描述
        blog.setLog_busimasg("进行添加报案单");//简单的业务描述
        blog.setLog_addtime(new Date());
        blog.setLog_busitype("添加");//(添加，删除，修改)
        blog.setUn_id(1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Claimant cla = new Claimant();

            cla.setCl_addtime(new Date());

        ma.put("claimant",cla);
        ma.put("blog",blog);
        liPeiService.addClaimant(ma);
    }



    @Test
    public void test3(){
        System.out.println("1111");
    }
    @Test
    public void test4(){
        Map<String,Object> ma = new HashMap<String,Object>();
        ma.put("cl_id",5);
        int flag=liPeiService.edit_jiesuan(ma);
        System.out.println(flag);
    }

}

